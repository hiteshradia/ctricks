#include <sys/ioctl.h>
#include <net/if.h> 
#include <unistd.h>
#include <netinet/in.h>
#include <string.h>
#include <stdio.h>


void printBits(size_t const size, void const * const ptr)
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i=size-1;i>=0;i--)
    {
        for (j=7;j>=0;j--)
        {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}


uint32_t gray2bin(uint32_t gray)
{
uint32_t bin;
int i;
	bin =gray;
	for(i=0;i<31;i++)
 	{
		gray >>=1;
		bin = bin ^ (gray);
		
	}
return bin;
}


uint32_t bin2gray(uint32_t bin)
{
uint32_t gray = bin ^ (bin>>1);
return gray;
}

int main()
{
int i;
int gray,bin;
int prevgray,nextgray;
	for(i=0;i<20;i++)
	{
		gray=bin2gray(i);
		bin=gray2bin(gray);
		prevgray= gray ^1;
		nextgray= gray ^ ((gray & -gray ) <<1);
		printf("%04x g=%04x b=%04x pg=%04x ng=%04x\n",i,gray,bin,prevgray,nextgray);
		printf("i=");
		printBits(1,&i);
		printf("g=");
		printBits(1,&gray);
		printf("b=");
		printBits(1,&bin);

	}
}
