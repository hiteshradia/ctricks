#include <errno.h>        /* errno declaration & error codes.            */
#include <netdb.h>        /* getaddrinfo(3) et al.                       */
#include <netinet/in.h>   /* sockaddr_in & sockaddr_in6 definition.      */
#include <stdio.h>        /* printf(3) et al.                            */
#include <stdlib.h>       /* exit(2).                                    */
#include <string.h>       /* String manipulation & memory functions.     */
#include <sys/poll.h>     /* poll(2) and related definitions.            */
#include <sys/socket.h>   /* Socket functions (socket(2), bind(2), etc). */
#include <unistd.h>       /* getopt(3), read(2), etc.                    */
#include <net/if.h>
#include <getopt.h>
#include <sys/types.h>
#include <time.h>
#include <fcntl.h>

#define BUFLEN 2999
#define SRV_IP "fe80::200:ff:fe00:1"

static char iface[20] = {0};
static char remote_ip[30] = {0};
static uint16_t remote_port = 0;
static char filename[50] = {0};
static int interval=1000;

void mysleep(int sleep_secs, long int sleep_nsecs) {

    struct timespec myntsleep, myntleft_sleep;
    int sts;

        myntsleep.tv_nsec = sleep_nsecs;
        myntsleep.tv_sec = sleep_secs;
        myntleft_sleep.tv_nsec = 0;
        myntleft_sleep.tv_sec = 0;
                do
                {
                        sts = nanosleep(&myntsleep, &myntleft_sleep);
                if((myntleft_sleep.tv_nsec == 0) && (myntleft_sleep.tv_sec == 0)) {
                break;
                }
                        memcpy(&myntsleep,&myntleft_sleep, sizeof(myntsleep));
                memset(&myntleft_sleep, 0, sizeof(myntleft_sleep));
                }
                while( 1 );
}


void diep(char *s)
{
  perror(s);
  exit(1);
}
void usage() {
    printf("\nusage: udpipv6-ft\n");
    printf(" \n******** Options ******\n");
    printf("\n\t -h:\tThis help \n");
    printf("\t -I:\tlocal Interface\n");
    printf("\t -r:\tremote host ipv6 address\n");
    printf("\t -p:\tremote port\n");
    printf("\t -f:\tfilename or path to file\n");
    printf("\t -i:\tinterval msecs\n");
}
void Options(int argc, char *argv[])
{
    int index = 0;
    int t;
    static struct option opts[] =
    {
        {"help", no_argument, 0, 'h'},
        {"interface", required_argument, 0, 'I'},
        {"remote ip", required_argument, 0, 'r'},
        {"remote port", required_argument, 0, 'p'},
        {"filename", required_argument, 0, 'f'},
        {"interval", required_argument, 0, 'i'},
        {0, 0, 0, 0}
    };
    while(1) {
        t = getopt_long(argc, argv, "hI:p:r:f:i:", opts, &index);
        if(t < 0) {
            break;
        }
        switch(t) {
            case 'h':
                usage();
		exit(1);
            break;
            case 'I':
                strcpy(iface, optarg);
                break;
            case 'r':
                strcpy(remote_ip, optarg);
            break;

            case 'p':
                remote_port = atoi(optarg);
            break;

            case 'f':
                strcpy(filename, optarg);
            break;

            case 'i':
                interval = atoi(optarg);
            break;

            default:
               usage();
            break;
        }
    }
}
int main(int argc, char*argv[])
{
int ret;
struct sockaddr_in6 server_addr ;
struct sockaddr si_other;
int Udp_Socket ,slen=sizeof(server_addr);
char buf[BUFLEN];
int fd, pktcnt=0;
int tmp_secs=0;
long int tmp_nsecs=0;
    unsigned long tmp_timediff_usec;



    Options(argc ,argv);
    if((Udp_Socket=socket(AF_INET6, SOCK_DGRAM,IPPROTO_UDP)) < 0){
        printf("socket creation failed..\n");
        return -1;
    }
    bzero(buf,BUFLEN);

    tmp_timediff_usec = interval * 1000;
    tmp_secs = tmp_timediff_usec / 1000000;
    tmp_nsecs = (tmp_timediff_usec % 1000000)*1000;



    fd= open(filename ,O_RDONLY);
       if (fd <=0) {
        printf("Error opening %s file\n", filename);
        return -1;
    }
    bzero(&server_addr,sizeof(server_addr));
    server_addr.sin6_family = AF_INET6;
    server_addr.sin6_port = htons(remote_port);
    server_addr.sin6_scope_id = if_nametoindex(iface);
    if (inet_pton(AF_INET6,remote_ip, &server_addr.sin6_addr)==0) {
       fprintf(stderr, "inet_aton() failed\n");
       exit(1);
    }

    if (connect(Udp_Socket, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
                        perror("rlogin: connect");
                        exit(5);
                        }

    if((read(fd, buf, (BUFLEN - 1))) > 0 )
    {
    while(1) {
     printf("Sending %d pkt\n", ++pktcnt);
     if (sendto(Udp_Socket, buf, strlen(buf),0, NULL, 0)==-1)
     {
	printf("ERR:\n");
         diep("sendto()");
     }
     mysleep( tmp_secs , tmp_nsecs);
    }
     //printf("send %s\n",buf);
     close(Udp_Socket);
    }
    else
    {
        diep("file read error");
    }
  return 0;

}


