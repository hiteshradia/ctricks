/*
binary tree ADT
*/

#include <stdlib.h>
#include <stdio.h>

/******************************/


struct btree_node {
    int val;
    int height;
    struct btree_node* left;
    struct btree_node* right;
}; 



/********************btree functions *******************/
/*iterate and free the tree*/

void
chop_off_the_tree(struct btree_node* tree)
{

    if( tree != NULL )
    {
        chop_off_the_tree(tree->left);
        chop_off_the_tree(tree->right);
        free(tree);
    }
    return;

}


/*  allocate a new node and insert a value */
struct btree_node* btree_newnode(int val) 
{
    struct btree_node* node = malloc(sizeof(struct btree_node));
    node->val = val;
    node->height = 1;
    node->left = NULL;
    node->right = NULL;

    return(node);
}
 

#if 0

implementation
Following is the implementation for AVL Tree Insertion. The following implementation uses the recursive BST insert to insert a new node. In the recursive BST insert, after insertion, we get pointers to all ancestors one by one in bottom up manner. So we don’t need parent pointer to travel up. The recursive code itself travels up and visits all the ancestors of the newly inserted node.
1) Perform the normal BST insertion.
2) The current node must be one of the ancestors of the newly inserted node. Update the height of the current node.
3) Get the balance factor (left subtree height – right subtree height) of the current node.
4) If balance factor is greater than 1, then the current node is unbalanced and we are either in Left Left case or left Right case. To check whether it is left left case or not, compare the newly inserted key with the key in left subtree root.
5) If balance factor is less than -1, then the current node is unbalanced and we are either in Right Right case or Right Left case. To check whether it is Right Right case or not, compare the newly inserted key with the key in right subtree root.

 
 
 
 
struct node* insert(struct node* node, int key)
{
    /* 1.  Perform the normal BST rotation */
    if (node == NULL)
        return(newNode(key));
 
    if (key < node->key)
        node->left  = insert(node->left, key);
    else
        node->right = insert(node->right, key);
 
    /* 2. Update height of this ancestor node */
    node->height = max(height(node->left), height(node->right)) + 1;
 
    /* 3. Get the balance factor of this ancestor node to check whether
       this node became unbalanced */
    int balance = getBalance(node);
 
    // If this node becomes unbalanced, then there are 4 cases
 
    // Left Left Case
    if (balance > 1 && key < node->left->key)
        return rightRotate(node);
 
    // Right Right Case
    if (balance < -1 && key > node->right->key)
        return leftRotate(node);
 
    // Left Right Case
    if (balance > 1 && key > node->left->key)
    {
        node->left =  leftRotate(node->left);
        return rightRotate(node);
    }
 
    // Right Left Case
    if (balance < -1 && key < node->right->key)
    {
        node->right = rightRotate(node->right);
        return leftRotate(node);
    }
 
    /* return the (unchanged) node pointer */
    return node;
}
 
// A utility function to print preorder traversal of the tree.
// The function also prints height of every node
void preOrder(struct node *root)
{
    if(root != NULL)
    {
        printf("%d ", root->key);
        preOrder(root->left);
        preOrder(root->right);
    }
}
 
/* Drier program to test above function*/
int main()
{
  struct node *root = NULL;
 
  /* Constructing tree given in the above figure */
  root = insert(root, 10);
  root = insert(root, 20);
  root = insert(root, 30);
  root = insert(root, 40);
  root = insert(root, 50);
  root = insert(root, 25);
 
  /* The constructed AVL Tree would be
            30
           /  \
         20   40
        /  \     \
       10  25    50
  */
 
  printf("Pre order traversal of the constructed AVL tree is \n");
  preOrder(root);
 
  return 0;
}

#endif
// A utility function to get height of the tree
int height(struct btree_node *N)
{
    if (N == NULL)
        return 0;
    return N->height;
}
 
// A utility function to get maximum of two integers
int max(int a, int b)
{
    return (a > b)? a : b;
}
// Get Balance factor of node N
int getBalance(struct btree_node *N)
{
    if (N == NULL)
        return 0;
    return height(N->left) - height(N->right);
}

// A utility function to right rotate subtree rooted with y
#if 0
a) Left Left Case
T1, T2, T3 and T4 are subtrees.
         z                                      y 
        / \                                   /   \
       y   T4      Right Rotate (z)          x      z
      / \          - - - - - - - - ->      /  \    /  \ 
     x   T3                               T1  T2  T3  T4
    / \
  T1   T2
#endif
// See the diagram given above.
struct btree_node *rightRotate(struct btree_node *z)
{
    struct btree_node *y = z->left;
    struct btree_node *T3 = y->right;
 
    // Perform rotation
    y->right = z;
    z->left = T3;
 
    // Update heights
    z->height = max(height(z->left), height(z->right))+1;
    y->height = max(height(y->left), height(y->right))+1;
 
    // Return new root
    return y;
}

#if 0
c) Right Right Case

  z                                y
 /  \                            /   \ 
T1   y     Left Rotate(z)       z      x
    /  \   - - - - - - - ->    / \    / \
   T2   x                     T1  T2 T3  T4
       / \
     T3  T4
#endif
// A utility function to left rotate subtree rooted with x
// See the diagram given above.
struct btree_node *leftRotate(struct btree_node *z)
{
    struct btree_node *y = z->right;
    struct btree_node *T2 = y->left;
 
    // Perform rotation
    y->left = z;
    z->right = T2;
 
    //  Update heights
    z->height = max(height(z->left), height(z->right))+1;
    y->height = max(height(y->left), height(y->right))+1;
 
    // Return new root
    return y;
}

/* insert into our binary tree*/
struct btree_node* btree_insert(struct btree_node* node, int val) 
{
    /* tree is empty, return a new  node*/
    if (node == NULL) {
        return(btree_newnode(val));
    }
        /*insert as per binary tree rules*/
        if (val <  node->val) 
            node->left = btree_insert(node->left,val);
        else if (val > node->val)
            node->right = btree_insert(node->right, val);

 /* 2. Update height of this ancestor node */
    node->height = max(height(node->left), height(node->right)) + 1;
 
    /* 3. Get the balance factor of this ancestor node to check whether
       this node became unbalanced */
    int balance = getBalance(node);
 
    // If this node becomes unbalanced, then there are 4 cases
 
    // Left Left Case node=z
    if (balance > 1 && val < node->left->val)
        return rightRotate(node);
 
    // Right Right Case noe=z
    if (balance < -1 && val > node->right->val)
        return leftRotate(node);
#if 0 
b) Left Right Case

     z                               z                           x
    / \                            /   \                        /  \ 
   y   T4  Left Rotate (y)        x    T4  Right Rotate(z)    y      z
  / \      - - - - - - - - ->    /  \      - - - - - - - ->  / \    / \
T1   x                          y    T3                    T1  T2 T3  T4
    / \                        / \
  T2   T3                    T1   T2

#endif

    // Left Right Case node=z node->left =y
    if (balance > 1 && val > node->left->val)
    {
        node->left =  leftRotate(node->left);
        return rightRotate(node);
    }

#if 0 
d) Right Left Case

   z                            z                            x
  / \                          / \                          /  \ 
T1   y   Right Rotate (y)    T1   x      Left Rotate(z)   z      y
    / \  - - - - - - - - ->     /  \   - - - - - - - ->  / \    / \
   x   T4                      T2   y                  T1  T2  T3  T4
  / \                              /  \
T2   T3                           T3   T4
#endif
    // Right Left Case node=z node->right=y
    if (balance < -1 && val < node->right->val)
    {
        node->right = rightRotate(node->right);
        return leftRotate(node);
    }
 
    /* return the (unchanged) node pointer */
    return node;

} 

struct btree_node* btree_find(struct btree_node* node, int val)
{
    if(node == NULL)
        return NULL;
    if(val < node->val)
        return btree_find(node->left ,val);
    else
    if(val > node->val)
        return btree_find( node->right ,val);
    else
        return node;
}



struct btree_node* btree_findmin(struct btree_node* node)
{
    if( node == NULL )
        return NULL;
    else
    if( node->left == NULL )
        return node;
    else
        return btree_findmin(node->left);
}


struct btree_node* btree_delete(struct btree_node* node, int val)
{
    struct btree_node* tmpnode;
    if(node == NULL)
        return NULL;
    else
    if(val < node->val)  /* Go left */
        node->left = btree_delete(node->left, val);
    else
    if(val > node->val)  /* Go right */
        node->right = btree_delete(node->right, val);
    else  /* Found value to be deleted */
    if( node->left && node->right)  /* Two children */
    {
        /* Replace with smallest in right subtree */
        tmpnode = btree_findmin(node->right);
        node->val = tmpnode->val;
        node->right = btree_delete(node->right, node->val);
    }
    else  /* One or zero children */
    {
        tmpnode = node;
        if(node->left == NULL ) /* Also handles 0 children */
            node = node->right;
        else if( node->right == NULL )
            node = node->left;
        free(tmpnode);
    }
     return node;
}


void print_btree(struct btree_node* node)
{
    if(node == NULL)
        return;
    else{
        print_btree(node->left);
        printf("%d -> ",node->val);
        print_btree(node->right);
    }
    return;
}
#if 0
/* Compute the "height" of a tree -- the number of
    nodes along the longest path from the root node
    down to the farthest leaf node.*/
int height(struct btree_node* node)
{
    if (node==NULL)
        return 0;
    else
    {
        /* compute the height of each subtree */
        int lheight = height(node->left);

        int rheight = height(node->right);
	printf("lheight:%d  rheight: %d\n",lheight, rheight);
        /* use the larger one */
        if (lheight > rheight)
            return(lheight+1);
        else return(rheight+1);
    }
}
#endif
 
struct btree_node * root1=NULL;
/* Print nodes at a given level */
void print_level(struct btree_node* root, int level,int height)
{
    int i;
    if (root == NULL)
        return;
    if (level == 1)
        printf(" %2d", root->val);
    else if (level > 1)
    {
	//	for(i=1;i<level;i++)
	//		printf(" ");
        if(root!=root1)
        print_level(root->left, level-1,level);
		for(i=1;i<=height/2;i++)
			printf("*");
		print_level(root->right, level-1,level);
    }
}
 
/* Function to print level order traversal a tree*/
void print_levelorder(struct btree_node* root)
{
    int h = height(root);
    int i,j;
    for (i=1; i<=h; i++)
	{
        for(j=i;j<h*2;j++)
            printf("  ");

        print_level(root, i,i);
		printf("\n");
	}	
}



/*********************************************************/
struct btree_node * root=NULL;
int main()
{
root =btree_insert(root,6);
root =btree_insert(root,2);
root =btree_insert(root,3);
root =btree_insert(root,1);
root =btree_insert(root,10);
root =btree_insert(root,5);
root =btree_insert(root,11);
root =btree_insert(root,7);
root =btree_insert(root,12);
root =btree_insert(root,13);
root =btree_insert(root,14);
//root =btree_insert(root,15);
print_btree(root);
printf("\n");
root1=root;
print_levelorder(root);
printf("heght=%d\n",height(root));
}

