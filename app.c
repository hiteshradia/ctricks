#include "appinfo.h"

struct applicationInfo *
wme_find_application(struct wme_application_table *at,
	uint32_t psid)
{
	struct applicationInfo *ainfo;
	uint8_t hash;
        hash = 	wme_table_hash(psid);
    WME_TABLE_LOCK_BH(at);
	LIST_FOREACH(ainfo, &at->at_hash[hash], ai_hash) {
        if (ainfo->psid == psid ) {
            WME_TABLE_UNLOCK_BH(at);
            return ainfo;
		}
	}
	WME_TABLE_UNLOCK_BH(at);
	return NULL;	
}

struct applicationInfo *
wme_alloc_application(struct wme_application_table *at, struct astEntry *astentry,struct radio *radio)
{
	uint8_t hash;
	struct applicationInfo *ainfo;
	ainfo = (struct applicationInfo *) kmalloc (sizeof(struct applicationInfo), GFP_ATOMIC);

	if (ainfo == NULL)
		return NULL;

	memset(ainfo, 0 , sizeof(struct applicationInfo));
	ainfo->at = at;
	ainfo->psid = astentry->psid;
//	memcpy (&ainfo->acm, &astentry->acm, sizeof(wme_acm));
	ainfo->type = astentry->type;
	ainfo->priority = astentry->priority;
	ainfo->status = astentry->status;
	//ainfo->notif_ipv6addr = astentry->notif_ipv6addr;
	ainfo->notif_ipaddr = astentry->notif_ipaddr;
	ainfo->notif_port = astentry->notif_port;
    ainfo->radio = radio;
	
	hash = 	wme_table_hash(ainfo->psid);
	WME_TABLE_LOCK(at);
	TAILQ_INSERT_TAIL(&at->at_application, ainfo, ai_list);
	LIST_INSERT_HEAD(&at->at_hash[hash], ainfo, ai_hash);
	WME_TABLE_UNLOCK(at);
	return ainfo;
	
}

void
wme_free_application(struct applicationInfo *ainfo)
{
	struct wme_application_table *at = ainfo->at;
	if (at != NULL) {
		WME_TABLE_LOCK(at);
		TAILQ_REMOVE(&at->at_application, ainfo, ai_list);
		LIST_REMOVE(ainfo, ai_hash);
		WME_TABLE_UNLOCK(at);
	}
	kfree(ainfo);
}

void 
wme_free_application_list(struct wme_application_table *at)
{
    struct applicationInfo *ainfo, *temp;
    uint8_t hash = 0;
 
    if (at != NULL) {
        WME_TABLE_LOCK(at);
        for (hash = 0; hash < 32; hash++) {
            LIST_FOREACH_SAFE(ainfo, &at->at_hash[hash], ai_hash, temp) {
                TAILQ_REMOVE(&at->at_application, ainfo, ai_list);
                LIST_REMOVE(ainfo, ai_hash);
                kfree(ainfo);
            }
        }
        WME_TABLE_UNLOCK(at); 
    }
}

int
activeApps (struct wme_application_table *at, struct radio *radio)
{
	uint16_t active = 0;
	uint8_t hash;
	struct applicationInfo *ainfo;
	WME_TABLE_LOCK(at);
	for (hash = 0; hash <32; hash ++) {
		LIST_FOREACH(ainfo, &at->at_hash[hash], ai_hash) {
		if (ainfo->status == APP_ACTIVE && /*ainfo->type == type &&*/ ainfo->radio == radio)
			active++;
		}
	} 
	WME_TABLE_UNLOCK(at);
	return active;
}
/* added a function to find any registered app */
int
anyApp (struct wme_application_table *at)
{
        uint16_t active = 0;
        uint8_t hash;
        struct applicationInfo *ainfo;
        WME_TABLE_LOCK(at);
        for (hash = 0; hash <32; hash ++) {
                LIST_FOREACH(ainfo, &at->at_hash[hash], ai_hash) {
                if (ainfo->status == APP_ACTIVE || ainfo->status == APP_INACTIVE )
                        active++;
                }
        }
        WME_TABLE_UNLOCK(at);
        return active;
}
