/*
 * Copyright (c) 2010, Atheros Communications Inc.
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 * 
 */

#include <linux/skbuff.h>
#include <linux/netdevice.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/rtnetlink.h>
#include <linux/wireless.h>
#include <linux/netlink.h>
#include <net/iw_handler.h>
#include <net/sock.h>
#include <linux/ip.h>		// struct iphdr
#include <linux/ipv6.h>		// struct iphdr
#include <linux/if_ether.h> // struct ethhdr
#include <linux/udp.h>		// struct udphdr

#include <a_base_types.h>
#include <hif_api.h>
#include <htc_api.h>
#include <wmi_api.h>
#include <atd_wave.h>
#include <atd_wsupp_msg.h>
#include <atd_hdr.h>
#include <atd_cmd.h>
#include <atd_internal.h>

#define WSUPP_HELPER_DEBUG 0

/* netlink library prototypes/structures */
typedef void * atd_tgt_netlink_handle_t;
typedef void (*atd_tgt_netlink_cb_t)(void *ctx, struct sk_buff *skb, int addr);
typedef struct atd_tgt_netlink_softc {
    struct sock *sock;
    atd_tgt_netlink_cb_t input;
    void *ctx;
} atd_tgt_netlink_softc_t;

/* netlink functions used by wsupp helper */
//******* to_resolve
//static atd_tgt_netlink_handle_t atd_tgt_netlink_create(atd_tgt_netlink_cb_t input,
//        void *ctx, uint32_t unit, uint32_t groups);
static void atd_tgt_netlink_delete(atd_tgt_netlink_handle_t nlhdl);
//static struct sk_buff *atd_tgt_netlink_alloc(uint32_t len);
//static a_status_t atd_tgt_netlink_broadcast(atd_tgt_netlink_handle_t nlhdl,
//        struct sk_buff *skb, int groups);


/* global values */
static int wh_netlink_num = NETLINK_GENERIC + 4;
static atd_tgt_netlink_handle_t wh_netlink_handle = NULL;

/* HTC callbacks  */
static void wh_htc_txcomp(void *ctx, struct sk_buff *skb, htc_endpointid_t ep);
//********* wave_service
//static void wh_htc_rx(void *ctx, struct sk_buff *skb, htc_endpointid_t ep);
static void wh_htc_wave_rx(void *ctx, struct sk_buff *skb, htc_endpointid_t ep);

unsigned short csum(unsigned short *, int);
#if 0
struct l4addr{
        struct in6_addr ip;
        uint16_t port;
} __attribute__((__packed__));
                
typedef struct l4addr layer4addr_t;
#endif

int wsmpdev_udp_dispatch(char *data, layer4addr *udpaddr);

unsigned short csum(unsigned short *buf, int nwords)
{
    unsigned long sum;
    for (sum = 0; nwords > 0; nwords--)
    	sum += *buf++;
    sum = (sum >> 16) + (sum & 0xffff);
    sum += (sum >> 16);
    return ~sum;
}
uint16_t ip_checksum(const void *buf, size_t hdr_len)
{
         unsigned long sum = 0;
         const uint16_t *ip1;

         ip1 = buf;
         while (hdr_len > 1)
         {
                //printf("%02x\n",htons(*ip1));
                 sum += htons(*ip1++);
                 if (sum & 0x80000000)
                         sum = (sum & 0xFFFF) + (sum >> 16);
                 hdr_len -= 2;
         }

         while (sum >> 16)
                 sum = (sum & 0xFFFF) + (sum >> 16);
                //printf("ip_checksum %d",sum);
         return(~sum);
 }

//typedef unsigned short u16;
//typedef unsigned long u32;
/*
u16 ip_sum_calc(u16 len_ip_header, u16 buff[])
{
	u16 word16;
	u32 sum=0;
	u16 i;
    
	// make 16 bit words out of every two adjacent 8 bit words in the packet
	// and add them up
	for (i=0;i<len_ip_header;i=i+2){
		word16 =((buff[i]<<8)&0xFF00)+(buff[i+1]&0xFF);
		sum = sum + (u32) word16;	
	}
								
	// take only 16 bits out of the 32 bit sum and add up the carries
	while (sum>>16)
	sum = (sum & 0xFFFF)+(sum >> 16);

	// one's complement the result
	sum = ~sum;
						
   	return ((u16) sum);
}
*/

/* private structures for wpa_supplicant message helper */
typedef struct wave_helper {
    /* tgt device links */
    atd_tgt_dev_t           *wh_tdev;
    /* htc stuff */
    htc_handle_t            wh_htc;
    htc_endpointid_t        wh_epid;
    /* locking */
    struct mutex            wh_mutex;
    spinlock_t              wh_spinlock;
    a_uint32_t              wh_stop;
    /* wsupp message queue handling */
    // to_reslove
//    wsupp_helper_work_t     wh_work;
    wave_helper_work_t     wh_work;
    struct sk_buff_head     wh_msgq;
} wave_helper_t;

#if 0
static void
wh_dump_wsupp_msg(char *banner, wsupp_msg_t *wmsg)
{
#if WSUPP_HELPER_DEBUG
    printk("%s: wmsg: type: %d %d if: %s len: %d: data: %s\n",
        banner,
        wmsg->wm_type, wmsg->wm_wmi,
        wmsg->wm_ifname, wmsg->wm_len,
        wmsg->wm_type == WSUPP_MSG_TYPE_WMI ? "WMI" : wmsg->wm_data);
#endif
}
#endif

#if 0
static void
wh_netlink_input(void *ctx, struct sk_buff *skb, int addr)
{
    struct sk_buff *msgbuf;
    wsupp_helper_t *wh = (wsupp_helper_t *) ctx;
    atd_tgt_dev_t *tdev;
    atd_wsupp_message_t *msg;
    a_uint32_t reserve, msgbuf_len;
    a_status_t status;

    wh_assert(wh);

    /* check if this is WMI related */
    if (atd_tgt_wsupp_response(wh->wh_tdev, skb->data, skb->len))
        return;

    /* the normal path */
    tdev = wh->wh_tdev;
    reserve = htc_get_reserveheadroom(tdev->htc_handle);
    msgbuf_len = skb->len + sizeof(atd_wsupp_message_t);

    msgbuf = alloc_skb(msgbuf_len + reserve + 1, GFP_ATOMIC);
    if (!msgbuf) {
        printk("%s: can't alloc skb", __func__);
        goto fail;
    }
    skb_reserve(msgbuf, reserve);

    msg = (atd_wsupp_message_t *) skb_put(msgbuf, msgbuf_len);
    msg->len = skb->len;
    memcpy(msg->data, skb->data, skb->len);

    msg->data[msg->len] = '\0';
    wh_dump_wsupp_msg((char *) __func__, (wsupp_msg_t *) msg->data);

    status = htc_send(wh->wh_htc, msgbuf, wh->wh_epid);
    if (status != A_STATUS_OK) {
        dev_kfree_skb_any(msgbuf);
    }

    return;
fail:
    /* skb would be freed by netlink framework */
    return;
}
#endif
typedef struct {
    u_int16_t   val;
	} __attribute__((packed)) unaligned_u_int16_t;

//****************************************************** ///for_receive
#if 1
void register_wsmp_recv(wave_helper_t *wh);
wave_helper_t *wave_wh;
void send_to_host(struct in6_addr *ip, uint16_t port,uint8_t *buf ,int len);


atd_tx_hdr_t atd_hdr_rx;
struct ethhdr eth_hdr_rx;
struct iphdr ip_hdr_rx;
//struct ipv6hdr ip_hdr_rx;
struct udphdr udp_hdr_rx;

struct wsmp_recv_funcs {
        void (*recv_status)(struct in6_addr *ip, uint16_t port,uint8_t *buf, int len);
};

extern struct wsmp_recv_funcs *g_recv_funcs;

struct wsmp_recv_funcs htc_pkt_ops = {
    .recv_status = send_to_host,    
};
void register_wsmp_recv(wave_helper_t *wh)
{
    g_recv_funcs = &htc_pkt_ops;
    wave_wh = wh;	
}
#endif
//****************************************************** ///for_receive
#if 1
void send_to_host(struct in6_addr *ip, uint16_t port,uint8_t *buf ,int wsmind_len)
{
    struct sk_buff *ret_skb;
    a_status_t status = A_STATUS_OK;
    atd_tgt_dev_t *tdev;
    a_uint32_t reserve;
    int offset = 0;

    wh_assert(wave_wh);
    tdev = wave_wh->wh_tdev;
    reserve = htc_get_reserveheadroom(tdev->htc_handle);

    ret_skb = alloc_skb(sizeof(atd_tx_hdr_t) + sizeof(struct ethhdr) + sizeof(struct iphdr) + sizeof(struct udphdr) + (wsmind_len) + reserve + 1, GFP_ATOMIC);
    if(ret_skb == NULL) {
        printk("%s Can't alloc skb \n", __func__);
        goto out;
    }
    skb_reserve(ret_skb, reserve);
  	
    ip_hdr_rx.check = 0;
    //memcpy(&ip_hdr_rx.saddr,ip,sizeof(struct in6_addr));  	
    ip_hdr_rx.tot_len = htons(sizeof(struct iphdr) + sizeof(struct udphdr) + wsmind_len);
    //ip_hdr_rx.check = csum((unsigned short *)&ip_hdr_rx, 10);
    ip_hdr_rx.check = htons(ip_checksum(&ip_hdr_rx ,sizeof(struct iphdr)));	  	

    udp_hdr_rx.check = 0;
    //udp_hdr_rx.source = htons(9999);
    //udp_hdr_rx.dest = htons(8888);
    udp_hdr_rx.dest = htons(port);
    udp_hdr_rx.len = sizeof(struct udphdr) + wsmind_len;

    /****** Copy atd_headr ********/		
    memcpy(ret_skb->data, &atd_hdr_rx, sizeof(atd_tx_hdr_t));
    offset = offset + sizeof(atd_tx_hdr_t);
	
    /****** Copy ethternet_header ********/		
    memcpy(ret_skb->data+offset, &eth_hdr_rx, sizeof(struct ethhdr));
    offset = offset + sizeof(struct ethhdr);	

    /****** Copy ip_header ********/		
    memcpy(ret_skb->data+offset, &ip_hdr_rx, sizeof(ip_hdr_rx));
    offset = offset + sizeof(ip_hdr_rx);	

    /****** Copy udp_header ********/		
    memcpy(ret_skb->data+offset, &udp_hdr_rx, sizeof(struct udphdr));
    offset = offset + sizeof(struct udphdr);
		
    memcpy(ret_skb->data+offset, buf, wsmind_len);
    offset = offset + wsmind_len;
	
    skb_put(ret_skb, offset);
    //skb_put(ret_skb, sizeof(struct construct_htc) + sizeof(struct iphdr) + sizeof(struct udphdr) + wsmind_len);

    //printk("****Rakesh********* %s %d \n",__func__,__LINE__); 	

    status = htc_send(wave_wh->wh_htc, ret_skb, 2);
    if (status != A_STATUS_OK) {
        dev_kfree_skb_any(ret_skb);
    }
		

out:
	return;

}
#endif

#define IPV4 1
static void
wh_wave_output(void *ctx, struct sk_buff *buf)
{

    struct sk_buff *ret_skb;
    int offset = 0, ret = -1;	
    uint16_t tmp_port = 0;
    uint32_t tmp_addr = 0;
    //uint8_t tmp_addr[16] ;
    uint8_t tmp_mac[ETH_ALEN];
    char ret_buf[100] = "";
    a_status_t status;
    wave_helper_t *wh = (wave_helper_t *) ctx;
    atd_tgt_dev_t *tdev;
    a_uint32_t reserve;

    /********* atd Header **********/
    char atd_hdr[sizeof(atd_tx_hdr_t)];
    /********* Ether Header **********/
    struct ethhdr *ethh = NULL;	
    char eth_hdr[sizeof(struct ethhdr)];	
    /********* IPv4 or IPv6 Header **********/
#if IPV4
    struct iphdr *iph = NULL;	
    char ip_hdr[sizeof(struct iphdr)];	
#else
    struct ipv6hdr *ipv6_h = NULL;	
    char ipv6_hdr[sizeof(struct ipv6hdr)];	
#endif
    /********* Udp Header **********/
    struct udphdr *udph = NULL;	
    uint16_t udp_hdr[sizeof(struct udphdr)];

    //layer4addr udpaddr;  //Addr given to Wsmpdev
    wh_assert(wh);

    /******* Copy atd_header*********/
    memcpy(atd_hdr, buf->data, sizeof(atd_tx_hdr_t));
    skb_pull(buf, sizeof(atd_tx_hdr_t));

    /*********** get the ethernet header *******************/
    ethh = (struct ethhdr *)buf->data;
    offset = sizeof(struct ethhdr); 	
    memcpy(eth_hdr, ethh, offset);		
    ethh = (struct ethhdr *)eth_hdr;
    /************ get the ip header *************************/
#if IPV4
    //memcpy(&udpaddr.ip, &iph->saddr, 4);  ///Source address
    iph = (struct iphdr *)(buf->data + offset);	
    offset += sizeof(struct iphdr);	//20
    memcpy(ip_hdr, iph, sizeof(struct iphdr));	
    iph = (struct iphdr *)ip_hdr;
#else
    //memcpy(&udpaddr.ip, &ipv6_h->saddr, sizeof(struct in6_addr));  ///Source address
    ipv6_h = (struct ipv6hdr *)(buf->data + offset); 	
    offset += sizeof(struct ipv6hdr);
    memcpy(ipv6_hdr, ipv6_h, sizeof(struct ipv6hdr));	
    ipv6_h = (struct ipv6hdr *)ipv6_hdr;
#endif
	/*********** get the udp header ************************/
    udph = (struct udphdr *)(buf->data + offset);	
    offset += sizeof(struct udphdr); //8
    memcpy(udp_hdr, udph, sizeof(struct udphdr));
    //printk("FUNC =%s LINE = %d port = %d \n",__func__,__LINE__,udph->source);	

    udph = (struct udphdr *)udp_hdr;
    //udpaddr.port = udph->source;             ///Source Port
	
    skb_pull(buf, offset);


   /******************* DEBUG **********/
#if 0
    {
        int i;
	for(i=0;i<=130;i++)
	    printk("i = %d %02x \n",i,buf->data[i]);  	
	    printk("\n");  	
	for(i=30;i<115;i++)
	   printk("%02x",buf->data[i]);  	
	   printk("\n");  	
    }
#endif
    ret = wsmpdev_udp_dispatch((char *)buf->data,NULL);	

    /********* Swap mac address*********/
    memcpy(tmp_mac, ethh->h_source, ETH_ALEN);
    memcpy(ethh->h_source, ethh->h_dest, ETH_ALEN);
    memcpy(ethh->h_dest, tmp_mac, ETH_ALEN);

#if IPV4
    tmp_addr = iph->saddr;
    iph->saddr = tmp_addr + 1;
    iph->daddr = iph->daddr;
    iph->tot_len = htons(sizeof(struct iphdr) + sizeof(struct udphdr) + sizeof(ret));
    iph->check = 0;
    //iph->check = (csum((unsigned short *)ip_hdr ,10));
    iph->check = htons(ip_checksum(ip_hdr ,sizeof(struct iphdr)));
#else
    memcpy(tmp_addr, &udpaddr.ip, sizeof(struct in6_addr));
    memcpy(&ipv6_h->daddr,&udpaddr.ip,sizeof(struct in6_addr));		
    memset(&ipv6_h->saddr,0,sizeof(struct in6_addr)); 
    ipv6_h->payload_len =  sizeof(struct ipv6hdr) + sizeof(struct udphdr) + sizeof(ret);
#endif

    /****** Swap Udp Port*********/	
    tmp_port = udph->source;
    udph->source = udph->dest;
    udph->dest = tmp_port;
    udph->len = sizeof(struct udphdr) + sizeof(ret); 
    udph->check = 0;	

    tdev = wh->wh_tdev;
    reserve = htc_get_reserveheadroom(tdev->htc_handle); 
    ret_skb = alloc_skb(offset + sizeof(atd_tx_hdr_t) + sizeof(ret) + reserve + 1, GFP_ATOMIC);
    if(ret_skb == NULL) {
        printk("%s Can't alloc skb \n", __func__);
        goto out;
    }
 
    skb_reserve(ret_skb, reserve);

/*Copy atd_header*/	
	memcpy(ret_buf, atd_hdr, sizeof(atd_tx_hdr_t));
	memcpy(&atd_hdr_rx, ret_buf, sizeof(atd_tx_hdr_t));  ///for_receive
	offset = sizeof(atd_tx_hdr_t);
	
/*Copy ether_header*/
	memcpy(ret_buf + offset, eth_hdr, sizeof(struct ethhdr));
        memcpy(&eth_hdr_rx, eth_hdr, sizeof(struct ethhdr));	//for_receive	
	offset += sizeof(struct ethhdr);
	
/* Copy ip_header */

#if IPV4
	memcpy(ret_buf + offset, ip_hdr, sizeof(struct iphdr));
    	memcpy(&ip_hdr_rx, ip_hdr, sizeof(struct iphdr));	//for_receive
	offset += sizeof(struct iphdr);
#else

//	memcpy(ret_buf + offset, ipv6_hdr, sizeof(struct ipv6hdr));
//	offset = offset + sizeof(struct ipv6hdr);
#endif
		
/* Copy Udp_header */
	memcpy(ret_buf + offset, udp_hdr, sizeof(struct udphdr));
    	memcpy(&udp_hdr_rx, udp_hdr, sizeof(struct udphdr)); //for_receive
	offset += sizeof(struct udphdr);
          
	ret = htonl(ret);
	
	memcpy(ret_buf + offset, &ret, sizeof(ret));
	offset += sizeof(ret);


	memcpy(ret_skb->data, ret_buf, offset);
	skb_put(ret_skb, offset);
	status = htc_send(wh->wh_htc, ret_skb, 2);

    if (status != A_STATUS_OK) {
        dev_kfree_skb_any(ret_skb);
    }
out:
    dev_kfree_skb_any(buf);

}

static void
wh_work_wave_run(void *arg)
{
    unsigned long flags;
    wave_helper_t *wh = arg;
    struct sk_buff *skb;

    wh_assert(wh);

    /* protect the work thread from detach */
    mutex_lock(&wh->wh_mutex);

    do {
        /* protect the message queue  */
        spin_lock_irqsave(&wh->wh_spinlock, flags);
        skb = skb_dequeue(&wh->wh_msgq);
        spin_unlock_irqrestore(&wh->wh_spinlock, flags);

        if(!skb) break;

        wh_wave_output(wh, skb);

    } while(1);

    mutex_unlock(&wh->wh_mutex);
}

wave_handle_t
atd_tgt_wave_helper_init(void *tdev, htc_handle_t htc_handle)
{
    wave_helper_t *wh;
    htc_epcallback_t cb;

    wh = (wave_helper_t *) kmalloc(sizeof(*wh), GFP_ATOMIC);
    if (wh == NULL)
        return NULL;
#if 0
    if (wh_netlink_handle == NULL) {
        wh_netlink_handle = atd_tgt_netlink_create(wh_netlink_input, wh,
                wh_netlink_num, 0);
        if (wh_netlink_handle == NULL) {
            printk("no netlink interface created\n");
            kfree(wh);
            return NULL;
        }
        printk("Netlink interface number created: %d\n", wh_netlink_num);
    }
#endif
    /* helper context setup */
    wh->wh_tdev = tdev;
    wh->wh_htc = htc_handle;
    mutex_init(&wh->wh_mutex);
    spin_lock_init(&wh->wh_spinlock);
    wh->wh_stop = 0;

    skb_queue_head_init(&wh->wh_msgq);
    wave_helper_init_work(&wh->wh_work, wh_work_wave_run, wh);

    /* register service */
    cb.ep_rx       = wh_htc_wave_rx;
    cb.ep_txcomp   = wh_htc_txcomp;
    cb.ep_ctx      = wh;
    wh->wh_epid = htc_register_service(htc_handle, &cb, WMI_WAVE_SVC);
    register_wsmp_recv(wh);	

    return (wave_handle_t) wh;
}

void
atd_tgt_wave_helper_cleanup(wave_handle_t wave_handle)
{
    wave_helper_t *wh = wave_handle;
    struct sk_buff *skb;

    /* should run on sleepable context and HTC/HIF disabled */
    wh_assert(wh);

    /* prevent msg from queuing */
    wh->wh_stop = 0;

    /* disable work and acquire the mutex to cleanup msgq */
    mutex_lock(&wh->wh_mutex);
    while((skb = skb_dequeue(&wh->wh_msgq)))
        dev_kfree_skb_any(skb);
    mutex_unlock(&wh->wh_mutex);
    mutex_destroy(&wh->wh_mutex);

    kfree(wh);

    /* cleanup netlink when needed */
    if (wh_netlink_handle) {
        printk("Netlink interface number deleted: %d\n", wh_netlink_num);
        atd_tgt_netlink_delete(wh_netlink_handle);
    }

}

static void
wh_htc_txcomp(void *ctx, struct sk_buff *skb, htc_endpointid_t ep)
{
    dev_kfree_skb_any(skb);
}

static void
wh_htc_wave_rx(void *ctx, struct sk_buff *skb, htc_endpointid_t ep)
{
    unsigned long flags;
    wave_helper_t *wh = ctx;


    wh_assert(wh);

    if (wh->wh_stop) {
        dev_kfree_skb_any(skb);
        return;
    }

    spin_lock_irqsave(&wh->wh_spinlock, flags);
    skb_queue_tail(&wh->wh_msgq, skb);
    spin_unlock_irqrestore(&wh->wh_spinlock, flags);

    wave_helper_sched_work(&wh->wh_work);
}

int
atd_tgt_wave_helper_wmi_send(wave_handle_t wave_handle,
        int cmdid, char *data, int datalen)
{
    struct sk_buff *skb;
    atd_wave_message_t *msg;
    wave_helper_t *wh = (wave_helper_t *) wave_handle;

    wh_assert(wh);

    skb = alloc_skb(sizeof(atd_wave_message_t) + datalen + 1, 
            GFP_ATOMIC);
    if (!skb)
        return -1;

    msg = (atd_wave_message_t *) skb_put(skb, 
            sizeof(atd_wave_message_t) + datalen);
    msg->len = datalen;
    memcpy(msg->data, data, datalen);
   
    /* reuse the htc path */
//***** to_resolve	
#if 0
    wh_htc_rx(wh, skb, 0);
#endif
    return 0;
}

/* ATD TGT Netlink Library */

#ifdef NL_DEBUG
#define nldebug printk
#else
#define nldebug(...)
#endif

/**
 * @brief   linux netlink callback function which hides the buffer 
 *          manipulation and provides user context for the registered
 *          user callback
 */

#if 0
static void 
__atd_tgt_netlink_input(struct sk_buff *skb)
{
    struct sock *sock = skb->sk;
    struct nlmsghdr *nlh = nlmsg_hdr(skb);
    atd_tgt_netlink_softc_t *nlsc;

    if (sock == NULL || sock->sk_user_data == NULL) {
        nldebug("%s: failed to get ctx\n", __func__);
        return;
    }
    
    if (skb->len < NLMSG_SPACE(0)) {
        nldebug("%s: invalid packet len\n", __func__);
        return;
    }

    skb_pull(skb, NLMSG_SPACE(0));

    nlsc = (atd_tgt_netlink_softc_t *) sock->sk_user_data;
    nlsc->input(nlsc->ctx, skb, nlh->nlmsg_pid);
}
#endif

/**
 * @brief   linux implementation for netlink interface creation
 */

#if 0
static atd_tgt_netlink_handle_t
atd_tgt_netlink_create(atd_tgt_netlink_cb_t input, void *ctx, a_uint32_t unit, a_uint32_t groups)
{
    atd_tgt_netlink_softc_t *nlsc;

    nlsc = (atd_tgt_netlink_softc_t *) kzalloc(sizeof(atd_tgt_netlink_softc_t), 
                GFP_ATOMIC);
    if (!nlsc)
        return NULL;

    /* FIXME: compatible with earlier kernel version */
    nlsc->sock = netlink_kernel_create(&init_net, unit, groups, 
               __atd_tgt_netlink_input, NULL, THIS_MODULE);
    if (nlsc->sock == NULL) {
        kfree(nlsc);
        return NULL;
    }

    nlsc->input = input;
    nlsc->ctx = ctx;

    nlsc->sock->sk_user_data = nlsc;

    return (atd_tgt_netlink_handle_t) nlsc;
}
#endif

/**
 * @brief   linux implementation for netlink interface removal
 */
#if 1
static void
atd_tgt_netlink_delete(atd_tgt_netlink_handle_t nlhdl)
{
    atd_tgt_netlink_softc_t *nlsc = (atd_tgt_netlink_softc_t *) nlhdl;

    netlink_kernel_release(nlsc->sock);
    
    kfree(nlsc);
}

#endif
/**
 * @brief   allocate necessary space for network buffer which contains
 *          netlink message and user data
 */

#if 0
static struct sk_buff *
atd_tgt_netlink_alloc(a_uint32_t len)
{
    struct sk_buff *skb = NULL;
    skb = alloc_skb(NLMSG_SPACE(len), GFP_ATOMIC);
    if(skb == NULL) {
        return NULL;
    }

    skb_reserve(skb, NLMSG_SPACE(0));

    return skb;
}
#endif

/**
 * @brief   internal function which prepare netlink message in linux
 */
#if 0
static void
atd_tgt_netlink_prepare(struct sk_buff *skb, 
                      int addr,
                      int groups)
{
    int datalen;
    struct nlmsghdr *nlh = NULL;

    datalen = skb->len;
    nlh = (struct nlmsghdr *) skb_push(skb, NLMSG_SPACE(0));
    nlh->nlmsg_pid = addr;
    nlh->nlmsg_flags = 0;
    nlh->nlmsg_len = NLMSG_LENGTH(datalen);

    //NETLINK_CB(skb).pid = 0;
    //NETLINK_CB(skb).dst_pid = addr;
    NETLINK_CB(skb).dst_group = groups;
}

#endif

/**
 * @brief   linux implementation for sending netlink broadcast message
 */
#if 0
static a_status_t
atd_tgt_netlink_broadcast(atd_tgt_netlink_handle_t nlhdl, 
                      struct sk_buff *skb, 
                      int groups)
{
    int ret;
    atd_tgt_netlink_softc_t *nlsc = (atd_tgt_netlink_softc_t *) nlhdl;

    atd_tgt_netlink_prepare(skb, 0, groups);

    ret = netlink_broadcast(nlsc->sock, skb, 0, groups, GFP_ATOMIC);

    if (ret)
        return A_STATUS_FAILED;
    return ret;
}

#endif
