#include<stdio.h>
#include<stdlib.h>

void main()
{
pid_t pid;
pid=getpid();

        printf("in parent process pid=%d\n",pid);
pid=fork();

    if(pid==0){
        pid_t childpid=getpid();
        printf("in child process pid=%d\n",childpid);
    }
    else{
        pid_t parentpid=getpid();
        printf("in parent process pid=%d\n",parentpid);

    }
}
