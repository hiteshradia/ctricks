#include <stdio.h>
int log_2(unsigned int a)
{
	int count = 0;

	while(a >>= 1U)
	{
		count++;
	}

	return count;
}


int lsb_set(int a)
{
	log_2(a & -a);

}

int main()
{
int a = 0x9;
int ans = 0;
printf("%04x %04x\n",a, (a& -a));
ans = lsb_set(a);

printf("%d\n",ans);
}
