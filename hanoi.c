#include<stdio.h>
#include<stdint.h>
#include<stdlib.h>
#include<string.h>

uint8_t step[7][2];//assumption of max 7 steps
uint8_t count=0;
uint8_t *initial,*final,*cur_pos,*is_processed;

inline int remove_n_put(uint8_t from,uint8_t to)
{
        //DIL //  step = (uint8_t**)malloc(sizeof(uint8_t*));
        //DIL   //* (step +count)= (uint8_t *)malloc(2*sizeof(uint8_t));
        step[count][0]=from;
        step[count][1]=to;
        count ++;
        return 0;
}


int main()
{
        //read inputs
        uint8_t n,k;
        int i;
        scanf("%hhu",&n);
        scanf("%hhu",&k);
        printf("rings=%hhu pegs=%hhu\n",n,k);

        //memory
        initial=malloc((n+1)*sizeof(uint8_t));
        memset(initial,0,((n+1)*sizeof(uint8_t)));

        final=malloc((n+1)*sizeof(uint8_t));
        memset(final,0,((n+1)*sizeof(uint8_t)));

        cur_pos=malloc((n+1)*sizeof(uint8_t));
        memset(cur_pos,0,((n+1)*sizeof(uint8_t)));

        is_processed=malloc((n+1)*sizeof(uint8_t));
        memset(is_processed,0,((n+1)*sizeof(uint8_t)));

        for(i=1;i<=n;i++)
        {
                scanf("%hhu",&initial[i]);
                cur_pos[i-1]=initial[i];
        }
        for(i=1;i<=n;i++)
        {
                scanf("%hhu",&final[i]);
        }
        printf("initial\n");
        for(i=1;i<=n;i++)
        {
                printf("%hhu ",initial[i]);
        }
        printf("\nfinal\n");
        for(i=1;i<=n;i++)
        {
                printf("%hhu ",final[i]);
        }
        for(i=n;i>0;i--)
        {

                process(i);
        }
        printf("\n%d\n",count);
        for(i=0;i<7;i++)
        {
                printf("%d %d\n",step[i][0],step[i][1]);
        }

        free(initial);
        free(final);
}



int process(int current)
{
        int j=0;
        uint8_t to ,from;
        if(initial[current]==final[current])//go to the smaller ring if the current is already at its position
                return 0;

        to=final[current];
        from=initial[current];
        //check number 1
        if(current==1)
        {
                remove_n_put(from,to);
                cur_pos[current]=to;
                is_processed[current]=1;
                return 0;

        }
        // is this the topmost ring in the current tower  == we can remove
        for(j=current-1;j>0;j--)
        {
                if(initial[j]==from)
                {
                        //recursion will be best here how???
                        process(j);
                }
        }
        //where to put == directly put if dest tower has no smaller rings
        for(j=current-1;j>0;j--)
        {
                if(cur_pos[j]==to)
                {
                        process(j);
                }
        } 
//is there a tower with no rings
for(j=1;j<n;j++)
{


}
        //finally put
        remove_n_put(from,to);
        cur_pos[current]=to;
        is_processed[current]=1;
        return 0;

}
