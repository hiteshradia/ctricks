typedef uint32_t Time32;

typedef uint16_t  CertificateDuration;

typedef enum{self=0,certificate_digest_with_ecdsap224=1,certificate_digest_with_ecdsap256=2,certificate=3,certificate_chain=4,certificate_digest_with_other_algorithm=5,}SignerIdentifierType;

typedef enum { message_anonymous=0, message_identified_not_localized =1,message_identified_localized =2,message_csr = 3,wsa =4,wsa_csr =5,message_ca=6, wsa_ca =7, crl_signer=8,message_ra =9,root_ca=255} SubjectType;

typedef enum{ use_start_validity =0,lifetime_is_duration=1,encryption_key=2}CertificateContentFlags;

typedef enum{message_anonymous_flags=0,message_identified_not_localized_flags=1,message_identified_localized_flags=2,message_csr_flags=3,wsa_flags =4,wsa_csr_flags=5,message_ca_flags=6,wsa_ca_flags=7,crl_signer_flags =8,message_ra_flags =9}SubjectTypeFlags;

typedef enum {from_issuer=0, specified=1} ArrayType;

typedef enum { ecdsa_nistp224_with_sha224 =0,  ecdsa_nistp256_with_sha_256 =1, ecies_nistp256 =2} PKAlgorithm;

typedef enum { x_coordinate_only =0,compressed_lsb_y_0 =1,compressed_lsb_y_1 =2, uncompressed =3} EccPublicKeyType;

typedef enum {unsecured=0, signed=1, encrypted=2, certificate_request=3, certificate_response=4, anonymous_certificate_response=5, certificate_request_error=6, crl_request=7, crl=8, signed_partial_payload=9, signed_external_payload=10, signed_wsa=11, certificate_response_acknowledgment=12, crl_req =236, crl_req_error =237, misbehavior_report_req =238, misbehavior_report_ack =239, cert_bootstrap_req =240, cert_bootstrap_cfm =241, cert_bootstrap_ack =242, anonymous_cert_request_req =243, anonymous_cert_request_cfm =244, anonymous_cert_request_status_req =245, anonymous_cert_request_status_cfm =246, sig_enc_cert =247, certificate_and_private_key_reconstruction_value =248, anonymous_cert_response_ack =249, anonymous_cert_decryption_key_req =250, anonymous_cert_decryption_key_cfm =251, anonymous_cert_decryption_key_error =252, anonymous_cert_decryption_key_ack =253,symmetric_encrypted=254}ContentType;

typedef enum {from_issuer_region =0, circle =1, rectangle =2, polygon =3, none =4} RegionType;


typedef struct {
    uint8_t version_and_type;
    uint8_t subject_type;
    uint8_t cf;
    uint8_t signer_id[DIGESTLEN];
    uint8_t signature_alg;
    uint32_t scopelen;
    CertSpecificData scope;
    Time32 expiration;
    Time32 start_validity_or_lft;
    uint32_t crl_series;
    PublicKey verification_key;
    PublicKey encryption_key;
    EccPublicKey reconstruction_value;
} Certificate;

typedef struct {
    int32_t latitude;
    int32_t longitude;
} TwoDLocation;

typedef struct {
int32_t latitude;
int32_t longitude;
uint8_t elevation[2];
} ThreeDLocation;

typedef struct {
    uint8_t region_type;
    int len;
    uint8_t * location;
} GeographicRegion;


struct {
 uint8_t type;
 uint32_t permissionslen;
 uint8_t* permissions_list;
 } genericPsidArray;

typedef struct{
uint32_t namelen;
uint8_t * name;
uint16_t permitted_subject_types;
genericPsidArray psid_array;
genericPsidArray psid_prio_array;
}CertSpecificData;

typedef struct {
    uint8_t type;
    uint8_t x[32];
    } EccPublicKey;

typedef struct {
 uint8_t algorithm;
 uint8_t supported_symm_alg;
    EccPublickey public_key;
    //uint8_t type; 
    //uint8_t x[32]; 
 } PublicKey;

typedef struct {
EccPublicKey R;
uint8_t s[32];

} Signature;


/* addr: buffer in which encoded length will be copied
 * retIdx: no.of bytes to be incremented for buffer index
 */

uint32_t decode_length(uint8_t *addr , int *retIdx)

{
   uint32_t recv_size = 0;
   if ((addr[0] & 0x80) == 0x00) {
       recv_size = addr[0];
       *retIdx = 1;
   }
   else if ((addr[0] & 0xc0) == 0x80) {
       recv_size = (addr[0] << 8)|(addr[1]);
       recv_size = recv_size & 0x3fff;
       *retIdx = 2;
   }
   else if ((addr[0] & 0xe0) == 0xc0) {
       recv_size = (addr[0] << 16)|(addr[1] << 8)|(addr[2]);
       recv_size = recv_size & 0x1fffff;
       *retIdx = 3;
   }

   else if ((addr[0] & 0xf0) == 0xe0) {
       recv_size = (addr[0] << 24)|(addr[1] << 16)|(addr[2] << 8)|(addr[3]);
       recv_size = recv_size & 0x0fffffff;
       *retIdx = 4;
   }
   return recv_size;
}



/* addr: buffer in which encoded length will be copied
 * val: 32bit length value
 * retIdx: no.of bytes to be incremented for buffer index
 */

uint32_t encode_length(uint8_t *addr ,uint32_t val , int *retIdx)

{

   uint32_t retval = 0;
   uint8_t *p = NULL;
   if(!BIGENDIAN){
    retval =swap_32(val); //later should be changed to htobe32()
    p=&retval;
   }
   else
    p=&val;

   if (val <= 0x7F) {
       *retIdx = 1;
       addr[0] = p[3];
   }

   else if (val >= 0x80 && val <= 0x3FFF) {
       *retIdx = 2;
       addr[0] = p[2] | 0x80;
       addr[1] = p[3];
   }

   else if (val >= 0x4000 && val <= 0x1FFFFF) {
       *retIdx = 3;
       addr[0] = p[1] | 0xd0;
       addr[1] = p[2];
       addr[2] = p[3];

   }

   else if (val >= 0x200000 && val <= 0xFFFFFFF) {

       *retIdx = 4;
       addr[0] = p[0] | 0xe0;
       addr[1] = p[1];
       addr[2] = p[2];
       addr[3] = p[3];
   }

}



