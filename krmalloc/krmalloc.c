#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "krmalloc.h"
typedef long Align;                      /* for alignment to long boundary */

typedef union  header                   /* block header */ 
{
    struct
    {
        union header *ptr;               /* next block if on free list */
        size_t size;                     /* size of this block */
    } s;

    Align x;                             /* force alignment of blocks */
} Header;


static Header *morecore(
    size_t nu);

static Header base = {0};                /* empty list to get started */
static Header *freeptr = NULL;           /* start of free list */


/* malloc: general-purpose storage allocator */
void *kr_malloc (
        size_t nbytes)
{
    Header *p;
    Header *prevptr;
    size_t nunits;
    void *result;
    bool is_allocating;

    nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
	printf("kr_malloc: request for %d blocks of %d bytes each\n",nunits,sizeof(Header));
    prevptr = freeptr;
    if (prevptr == NULL)                 /* no free list yet */
    {
        base.s.ptr = &base;
        freeptr = &base;
        prevptr = &base;
        base.s.size = 0;
    }

    is_allocating = true;
    for (p = prevptr->s.ptr; is_allocating; p = p->s.ptr)
    {
        if (p->s.size >= nunits)         /* big enough */
        {
            if (p->s.size == nunits)     /* exactly */
            {
                prevptr->s.ptr = p->s.ptr;
            }
            else                         /* allocate tail end */
            {
                p->s.size -= nunits;
                p += p->s.size;
                p->s.size = nunits;
            }

            freeptr = prevptr;
            result = p + 1;
            is_allocating = false;       /* we are done */
        }

        if (p == freeptr)                /* wrapped around free list */
        {
            p = morecore(nunits);
            if (p == NULL)
            {
                result = NULL;           /* none left */
                is_allocating = false;
            }
        }
        prevptr = p;
    } /* for */

    return result;
}


#define NALLOC 1024 /* minimum #units to request */
/* morecore: ask system for more memory */

static Header *morecore(
        size_t nu)
{
    char *cp, *sbrk(int);
    Header *up;

    if (nu < NALLOC)
    {
        nu = NALLOC;
    }
    cp = sbrk(nu * sizeof(Header));

    if (cp == (char *)-1) /* no space at all */
    {
        return NULL;
    }

    up = (Header *)cp;
    up->s.size = nu;
    free((void *)(up + 1));

    return freeptr;
}


/* free: put block ap in free list */
void free(
        void *ap)
{
    Header *bp, *p;
    bp = (Header *)ap - 1; /* point to block header */
    for (p = freeptr; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
        if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
        {
            break;
        }


    /* freed block at start or end of arena */
    if (bp + bp->s.size == p->s.ptr)
    {
        bp->s.size += p->s.ptr->s.size;
        bp->s.ptr = p->s.ptr->s.ptr;
    }
    else
    {
        bp->s.ptr = p->s.ptr;
    }

    if (p + p->s.size == bp)
    {
        p->s.size += bp->s.size;
        p->s.ptr = bp->s.ptr;
    }
    else
    {
        p->s.ptr = bp;
    }
    freeptr = p;
}


void printfreelist()
{
    Header *p;
    int i = 0;
    if (freeptr)
    {
        printf("\n\nprinting free list for %p\n",freeptr);
        for (p = freeptr->s.ptr;; p = p->s.ptr)
        {
            ++i;
            printf(" fb%d=%p s=%d\n",i,p,p->s.size);
            if (p ==freeptr) /*wrap*/
            {
                break;
            }
        }
    }
    else
    {
        printf("freeptr is NULL\n");
    }
}
