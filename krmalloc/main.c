#include <stdio.h>
#include <string.h>
#include "krmalloc.h"

int main(
        int argc,
        char *argv[])
{
    char *mystring = NULL;
    char *mystring2 = NULL;
    char *mystring3 = NULL;
    char *mystring4 = NULL;
	printf("sizeof int =%d\n",sizeof(int));
	printf("sizeof long =%d\n",sizeof(long));
	printf("sizeof size_t =%d\n",sizeof(size_t));
	printf("sizeof char * =%d\n",sizeof(char *));
    printfreelist();
    printf("first alloc \n\n");
    mystring = (char *)kr_malloc(100);
    if (argc > 1)
    {
        strcpy(mystring,argv[1]);
    }
    printf("mystring(%p) =%s\n",mystring,mystring);

    printfreelist();
    mystring2 = (char *)kr_malloc(120);
    printfreelist();
    mystring3 = (char *)kr_malloc(120);
    printfreelist();
    mystring4 = (char *)kr_malloc(120);
    printfreelist();

    free(mystring3);
    printfreelist();
}
