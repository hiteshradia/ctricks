/*This file is used to start the translator for application which is created by user. */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <syslog.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <config_sku.h>
#include <sys/time.h>
#include <errno.h>
#include <sys/wait.h> //for WIFEXITED
#include <db_reader.h>

#define APP_PREFIX_LEN  50
#define NM_PARAM        25
#define GEN_PARAM_LEN  50
#define VAL_LEN        350 
#define DISABLE 0
#define DEBUG 1
#define APP_CERT_MGMT 1
#define START_CERTS_TIME 1325376000
#define RELOAD_PERIOD 3600
#define NUMSECS_PERDAY (3600 * 24)
int year = 0;
static char logfile[255]="/tmp/usb/";
char certprefix[]="ModelDeploymentConfigurationItems/1609Certificates/";
int certseqno = 0;
int isconfig =1;
int usb_mounted = 0;
char certfname[255];
char write_file[]="/tmp/usb/ModelDeploymentConfigurationItems/ModelDeploymentRemovable.conf", conf_file[]="/tmp/ModelDeploymentRemovable.conf", tmp_conf_file[]="/tmp/temp.conf";
char srcfname[255], destfname[255];
char syscmd[100];
char mount_date[50];
uint32_t mod_depl = 0;

/* global variables */
static char app_prefix[APP_PREFIX_LEN] = "system:applicationSettings"; 
static char db_names[NM_PARAM][GEN_PARAM_LEN] =  {"app1Status", "app2Status", "app3Status", "app4Status", "app5Status", "app6Status","app7Status", "app8Status", "app1Name", "app2Name", "app3Name", "app4Name", "app5Name", "app6Name", "app7Name", "app8Name","app1arg", "app2arg", "app3arg", "app4arg", "app5arg", "app6arg", "app7arg", "app8arg"};	
static char db_values[NM_PARAM][VAL_LEN] ={ {0} };

void
tr_app (void *op, int arrcnt, char **arr, char *value)
{
        int indx = 0; 

        if (arrcnt <=0)
                return;
        indx = return_index_from_db_name(arr[arrcnt-1], &db_names[0], NM_PARAM);
	if(indx == -1)	{
		return; 
	}
	strcpy((char *)&db_values[indx], value);
	db_values[indx][strlen(value)] = '\0';
	//syslog(LOG_ERR, "i:%d value:%s\n", indx, value);	
}
int get_date(char *str)
{
        char *token = NULL;
        char *string = NULL;

	string=str;
        token = strtok(string," ");                //week_day

        token = strtok(NULL," ");               //month

	token = strtok(NULL," ");               //date

	token = strtok(NULL,":");               //hour

        token = strtok(NULL,":");               //min

        token = strtok(NULL," ");               //sec

        token = strtok(NULL," ");               //year
        year = atoi(token);
        return 0;
}

int get_date_yyyymmdd(int Tdate)
{
        char *token = NULL;
        char *str = NULL;
        char *temp = NULL;
        int ret_date = 0;
        time_t date;
        int i = 0, month_num = 0, day =0, year = 0;
        char mon[12][4]={ {"Jan\0"},
                          {"Feb\0"},
                          {"Mar\0"},
                          {"Apr\0"},
                          {"May\0"},
                          {"Jun\0"},
                          {"Jul\0"},
                          {"Aug\0"},
                          {"Sep\0"},
                          {"Oct\0"},
                          {"Nov\0"},
                          {"Dec\0"}
                        };
        date = Tdate;
        str = ctime(&date);
         //printf("Input date %ld to %s\n", date, str);
          //temp = (char *)malloc(sizeof(int));
          temp = (char *)malloc(10);
        token = strtok(str," ");                //week_day

        token = strtok(NULL," ");               //month
        sscanf(token,"%s",temp);
        for(i=1; i <= 12; i++)
        {
                if(!strcmp(mon[i-1],temp))
                        month_num = i;
        }

        token = strtok(NULL," ");               //date
        sscanf(token,"%s",temp);
        day = atoi(temp);
        //strcat(str,temp);
        token = strtok(NULL,":");               //hour

        token = strtok(NULL,":");               //min

        token = strtok(NULL," ");               //sec

        token = strtok(NULL," ");               //year
        sscanf(token,"%s",temp);
        year = atoi(temp);

        //strcat(str,temp);
        ret_date = (year * 10000) + (month_num * 100) + (day)  ;
        free(temp);
        return ret_date;
}



int get_date_time(char *str)
{
        char *token = NULL;
        char *string = NULL;
	char time[50] = "0", mnth[5] = "0", tmpcmd[100] = "0";
	int month, day, hour, min, sec, year, i;
        char mon[12][4]={ {"Jan\0"},
                          {"Feb\0"},
                          {"Mar\0"},
                          {"Apr\0"},
                          {"May\0"},
                          {"Jun\0"},
                          {"Jul\0"},
                          {"Aug\0"},
                          {"Sep\0"},
                          {"Oct\0"},
                          {"Nov\0"},
                          {"Dec\0"}
                        };	

	string=str;
        token = strtok(string," ");                //week_day

        token = strtok(NULL," ");               //month
	strcpy(mnth, token);
	for(i=1; i <= 12; i++)
        {
                if(!strcmp(mon[i-1],token))
                        month = i;
        }

	token = strtok(NULL," ");               //date
	day = atoi(token);

	token = strtok(NULL,":");               //hour
	hour = atoi(token);

        token = strtok(NULL,":");               //min
	min = atoi(token);

        token = strtok(NULL," ");               //sec
	sec = atoi(token);

        token = strtok(NULL," ");               //year
        year = atoi(token);

	sprintf(mount_date,"%04d-%02d-%02dT%02d:%02d:%02dZ",year,month,day,hour,min,sec);
	sprintf(time,"%s %2d %02d:%02d:%02d", mnth, day, hour, min, sec);
	sprintf(tmpcmd,"echo %s >> /tmp/first_time.txt",time);
	system(tmpcmd);
        return 0;
}

/*int certcopy(char *src, char *dest)
{
    int fdrd, fdwr;
    char rdval;

    if (src==NULL && dest==NULL){
        syslog(LOG_INFO,"one of the argument was NULL\n");
        return -1;
    }

    fdrd = open(src, O_RDONLY);
    if (fdrd <=0) {
        syslog(LOG_INFO,"Error opening %s file\n", src);
        return -1;
    }

    fdwr = open(dest, O_WRONLY | O_CREAT, S_IRWXU | S_IRWXO | S_IRWXG);
    if (fdwr <=0) {
        syslog(LOG_INFO,"Error opening %s file\n", dest);
        close(fdrd);
        return -1;
    }

    while ((read(fdrd, &rdval, 1)) > 0 ) {
        write(fdwr, &rdval, 1);
    }
    close(fdrd);
    close(fdwr);


    return 0;
}*/

int write_mount_time(void)
{
    FILE *rdfd, *wrfd;
    char read_line[200], write_line[200];
    char *sts = NULL, *token = NULL;
    char cmd[100];
    int rdsts;

    rdfd = fopen(conf_file, "r");
    if (rdfd == NULL) {
        printf("Error opening %s file\n", conf_file);
		isconfig =0;
        return -1;
    }
    wrfd = fopen(tmp_conf_file, "w");
    if (wrfd == NULL) {
	fclose(rdfd);
        printf("Error opening %s file\n", tmp_conf_file);
        return -2;
    }

    memset(read_line, 0, sizeof(read_line));
    memset(write_line, 0, sizeof(write_line));
    while(!feof(rdfd) &&  (sts = fgets(read_line, sizeof(read_line), rdfd)) != NULL ) {
	memcpy(write_line, read_line, sizeof(read_line)); 
	if (read_line[0] != '#' && read_line[0] != ';' && read_line[0] != ' '){
            token = strtok(read_line, "=");
            
            if (strcasecmp(token, "MemoryDeviceMountTimeDate") == 0) {
                sprintf(write_line,"%s=%s\n",token,mount_date);
		//syslog(LOG_INFO,"%s\n",write_line);	
                token = strtok(NULL, " ");
            }
            else if (strcasecmp(token, "ModelDeploymentDeviceID") == 0) {
                token = strtok(NULL, " ");
		sscanf(token,"0x%x",&mod_depl);
		//syslog(LOG_INFO,"Mod_depl = 0x%x\n",mod_depl);	
	    }
	    else
		token = strtok(NULL, " ");
	}
	rdsts = fputs(write_line,wrfd);
        memset(read_line, 0, sizeof(read_line));
        memset(write_line, 0, sizeof(write_line));
    }  
    fclose(rdfd);
    fclose(wrfd);
    return 0;
}

/*int copy_certificate()
{
	int fd = 0 ,i, ret = 0;
	struct timeval tv;
	char *strng;
	sprintf(syscmd,"mkdir -p %s", logfile);
        if(system(syscmd)){
                syslog(LOG_INFO,"Mount directory creation failed \n");
            return -2;
            }

	sprintf(syscmd,"mount -t vfat -o iocharset=ascii /dev/sda1 %s",logfile);
    	if(system(syscmd)){
            	syslog(LOG_INFO,"Mount faild on %s\n",logfile);
        	return -2;
        }
	else
            syslog(LOG_INFO,"RM,\t Mounted,\n",logfile);
	
	gettimeofday(&tv, NULL);
	memset(mount_date, '\0', sizeof(mount_date));
	strng = ctime(&tv.tv_sec);
	get_date_time(strng);
	ret = write_mount_time();
        if(ret < 0)
        {
    	    syslog(LOG_INFO,"Writing mount time to %s failed\n",write_file);
	}
	umount(logfile);
	sprintf(syscmd,"mount -t vfat -o iocharset=ascii /dev/sda1 %s",logfile);
    	if(system(syscmd)){
            	syslog(LOG_INFO,"Mount 2 faild on %s\n",logfile);
        	return -2;
        }
	for(i=1;i<=4;i++){
            sprintf(certfname,"%s%s%d",logfile,certprefix,certseqno);
            sprintf(srcfname,"%s.cert",certfname);
            if((fd = open(srcfname, O_RDONLY)) <= 0){
                certseqno++;
                sprintf(certfname,"%s%s%d",logfile,certprefix,certseqno);
                sprintf(srcfname,"%s.cert",certfname);
            }
            else
                close(fd);

            sprintf(destfname,"/tmp/keys/msg_anon_%d.cert",certseqno);
            if (certcopy(srcfname,destfname)){
                syslog(LOG_INFO,"Error copying from %s to %s\n",srcfname, destfname);
                return -1;
            }
            sprintf(srcfname,"%s.key",certfname);
            sprintf(destfname,"/tmp/keys/msg_anon_%d.key",certseqno);
            if (certcopy(srcfname,destfname)){
                syslog(LOG_INFO,"Error copying from %s to %s\n",srcfname, destfname);
                (void)syslog(LOG_INFO,"CL,\t LoadFailed,\n",logfile);
                return -1;
            }
	    else
                (void)syslog(LOG_INFO,"CL,\t Successful,\n",logfile);
            certseqno++;
        }
	sprintf(syscmd,"cp -f /var/security_vad.conf /tmp/security.conf");
	if(system(syscmd)){
                syslog(LOG_INFO,"Error in copying security_vad.conf \n");
            return -2;
        }
	umount(logfile);
    	return 0;
}*/




#ifdef USE_ARADA_CERT_TAR_FILES

int compute_seq_no(int timeunits, struct timeval *ptv)
{
  struct timeval tv;
  int certseqno;
  long int timesince_certs_start;

  if (ptv == NULL) {
     gettimeofday(&tv, NULL);
     ptv = &tv;
  }
  timesince_certs_start = ptv->tv_sec - 5 - START_CERTS_TIME;

  certseqno = (timesince_certs_start / timeunits ) + 1;
	/*if (timesince_certs_start % timeunits == 0)
   {  // We have just entered the next time unit in usecs
        certseqno +=  1;
          }*/

	syslog(LOG_INFO,"%s\t%d\tcertseqno=%d",__func__,__LINE__,certseqno);
  return certseqno;

}
int get_current_tarseqno_filename(char *filename, char *daystr, int idx, struct timeval *ptv)
{
  unsigned int hrfilenum=0,day=0;

  hrfilenum = compute_seq_no(RELOAD_PERIOD, ptv) + idx;
  day = (hrfilenum)/24;
  if(hrfilenum%24 !=0)
     day+=1;
  sprintf(daystr, "day%d",day);
  sprintf(filename, "certs_tar_msg_anon_%d.tar",hrfilenum);
//syslog(LOG_INFO,"%s:%s\n", __func__, filename);
return hrfilenum;
}

int copy_n_extract_tarseqno_filename( const char *filepath, const char *filename, const char *daystr)
{
int wrfd;
   sprintf(syscmd, "/tmp/keys/%s", filename);
    wrfd = open(syscmd, O_RDONLY);
    if (wrfd > 0) {
    close(wrfd);
	//syslog(LOG_INFO,"%s -file exists\n",syscmd);
	return 0;
    }
    // mounting the USB 
    //sprintf(syscmd,"mount -t vfat -o nosuid,iocharset=ascii /dev/sda1 %s",logfile);
    memset(syscmd, 0 , sizeof(syscmd));
    sprintf(syscmd,"mount -t vfat -o sync /dev/sda1 %s",logfile);
    if(system(syscmd) < 0){
        syslog(LOG_INFO, "Mount faild on %s\n",logfile);
        return -2;
    }
   //syslog(LOG_INFO,"%s1:%s\n", __func__, syscmd);
   // This can be optimized to avoid copy if file is present in local storage /tmp/keys
    #if 1
    memset(syscmd, 0 , sizeof(syscmd));
   sprintf(syscmd, "cp -f %s/%s/%s/%s /tmp/keys\n", logfile,filepath, daystr,filename);
   if(system(syscmd) < 0){
 	memset(syscmd, 0 , sizeof(syscmd));
    sprintf(syscmd, "umount /dev/sda1\n");
   	if(system(syscmd) < 0)
       syslog(LOG_INFO, "%s err %d\n",syscmd, errno);
     return -3;
   }
    //syslog(LOG_INFO,"%s\n", syscmd);
    memset(syscmd, 0 , sizeof(syscmd));
    sprintf(syscmd, "umount /dev/sda1\n");
   if(system(syscmd) < 0){
    syslog(LOG_INFO, "%s err %d\n",syscmd, errno);
    return -4;
   }

   // Extract certs from tar file
    memset(syscmd, 0 , sizeof(syscmd));
   sprintf(syscmd, "cd /tmp/keys; tar xvf %s \n", filename);
   if(system(syscmd)<0){
    syslog(LOG_INFO, "%s err %d\n",syscmd, errno);
    return -5;
   }
#endif

   return 0;

}
#endif

#define USE_SHORTLIVED_ONEDAY_CRT_FILES 1
#ifdef USE_SHORTLIVED_ONEDAY_CRT_FILES
int compute_day_seq_no(int timeunits, struct timeval *ptv)
{
  struct timeval tv;
  int tmp_certseqno;
  long int timesince_certs_start;

  if (ptv == NULL) {
     gettimeofday(&tv, NULL);
     ptv = &tv;
  }
  else {
  }

  tmp_certseqno = get_date_yyyymmdd( (ptv->tv_sec + timeunits)) ;

  return tmp_certseqno;

}

int get_current_tarseqno_filename(char *filename, char *daystr, int idx, struct timeval *ptv)
{
  unsigned int hrfilenum=0,day=0;

  hrfilenum = compute_day_seq_no(idx * NUMSECS_PERDAY, ptv);
	//day = (hrfilenum)/24;
  //if(hrfilenum%24 !=0)
    //   day+=1;
     //sprintf(daystr, "day%d",day);
	sprintf(filename, "ShortLived%d.crt",hrfilenum);
  //syslog(LOG_INFO,"%s:%s\n", __func__, filename);
  return hrfilenum;
}

int copy_n_extract_tarseqno_filename( const char *filepath, const char *filename, const char *daystr)
{
int wrfd;
   sprintf(syscmd, "/tmp/keys/%s", filename);
    wrfd = open(syscmd, O_RDONLY);
    if (wrfd > 0) {
    close(wrfd);
        syslog(LOG_INFO,"%s -file exists\n",syscmd);
    return 0;
    }
    // mounting the USB 
    //sprintf(syscmd,"mount -t vfat -o nosuid,iocharset=ascii /dev/sda1 %s",logfile);
    memset(syscmd, 0 , sizeof(syscmd));
    sprintf(syscmd,"mount -t vfat /dev/sda1 %s",logfile);
    if(system(syscmd) < 0){
        syslog(LOG_INFO, "Mount faild on %s\n",logfile);
        return -2;
    }
    memset(syscmd, 0 , sizeof(syscmd));
    system("rm -f /tmp/keys/ShortLived20120101.crt");
   //syslog(LOG_INFO,"%s1:%s\n", __func__, syscmd);
   // This can be optimized to avoid copy if file is present in local storage /tmp/keys
    #if 1
    memset(syscmd, 0 , sizeof(syscmd));
   sprintf(syscmd, "cp -f %s/%s/%s /tmp/keys\n", logfile,filepath, filename);
   if(system(syscmd) < 0){
 	   memset(syscmd, 0 , sizeof(syscmd));
       sprintf(syscmd, "umount /dev/sda1\n");
   	   if(system(syscmd) < 0)
           syslog(LOG_INFO, "%s err %d\n",syscmd, errno);
        return -3;
   }
//syslog(LOG_INFO,"%s\n", syscmd);
 memset(syscmd, 0 , sizeof(syscmd));
    sprintf(syscmd, "umount /dev/sda1\n");
   if(system(syscmd) < 0){
    syslog(LOG_INFO, "%s err %d\n",syscmd, errno);
    return -4;
   }

#endif

   return 0;

}
#endif




int main(int argc, char **argv){
        void *phandle = NULL;
        char buf[1024], cmd[1024];
        int rcount = -1, i, sidx, run_lcm_module = 0;
        FILE *fp, *fp1,*fp2, *fp3;
       	char pid[8] = "",pid1[64] = "", pid_name1[4][64] = { {0} };
	char *str_end = NULL;
	struct timeval tv;
	char *str;
	char hrfilename[100],nexthrfilename[100];
	char daystr[100],nextdaystr[100];
	int hrfilenum,nexthrfilenum;
        int ret=0;
   	char *strng;
	/* read the lcm status file, if status is pre_bootstap run the lcm_module */
	fp3 = fopen("/var/LCMStatus", "r");
	if(fp3 != NULL)
	{
	    fscanf(fp3, "%d", &run_lcm_module);
	    fclose(fp3);
	    if(run_lcm_module == 0) {
		system("/usr/local/bin/lcm_module");
		exit(0);
	    }
	}

	phandle = InitializeParser(app_prefix, tr_app, 0);
														          
        while ((rcount = read (0, buf, sizeof(buf))) > 0) {
	        ParseDatabase(phandle, buf, rcount);
        }
        ShutdownParser(phandle);
	/* if daemon is disabled/enabled  by the user set flag in file */

	for(i = 0; i< 8; i++){
		if(atoi((char *)&db_values[i]) == DISABLE) {
			if((strcmp(db_values[i+8],"INVALID")) && (strcmp(db_values[i+8],""))){
				sprintf(cmd, "/sbin/start-stop-daemon  -K -x %s",(char *)&db_values[i+8]);
				system(cmd);	  			
			}
		}
		else{
			if((strcmp(db_values[i+8],"INVALID")) && (strcmp(db_values[i+8],""))){
				sprintf(pid1,"%s",(char*)&db_values[i+8]);
				str_end = strrchr(pid1, '/');
				sprintf(pid_name1[i],"pidof %s",str_end + 1);
                		if( (fp = popen(pid_name1[i],"r")) )  {
                        		fscanf(fp, "%s",pid);
                        		pclose(fp);
                        		if(strcmp(pid,""))     {
						sprintf(cmd, "/sbin/start-stop-daemon  -K -x %s", (char *)&db_values[i+8]);
						system(cmd);	  			
					}
				}	
			}
		}
	}
	
	// Wait for 90seconds, considering we can have good GPS signal within 90seconds.
	for(sidx=0;sidx<90;sidx++)
	{
		gettimeofday(&tv, NULL);
        	str = ctime(&tv.tv_sec);
        	get_date(str);

		if(year > 2010)
			break;

		sleep(1);
	}
	
    gettimeofday(&tv, NULL);
    memset(mount_date, '\0', sizeof(mount_date));
    strng = ctime(&tv.tv_sec);
    get_date_time(strng);
    ret = write_mount_time();
    if(ret == -1) {
        syslog(LOG_INFO,"RM,\t MountFailed,\n",ret);
        syslog(LOG_INFO,"CL,\t LoadFailed,\n");
        syslog(LOG_INFO,"Writing mount time to %s failed\n",write_file);
    } else if (ret == 0) {
        syslog(LOG_INFO,"RM,\t Mounted,\n",ret);
        syslog(LOG_INFO,"CL,\t Successful,\n");
    }
    
    if((year > 2010) && (ret != -1)){
		hrfilenum = get_current_tarseqno_filename(hrfilename,daystr, 0,NULL);
        	nexthrfilenum = get_current_tarseqno_filename(nexthrfilename,nextdaystr,1, NULL);
        	if (copy_n_extract_tarseqno_filename(certprefix, hrfilename,daystr) == 0)
		{
    			sprintf(syscmd,"cp -f /var/security_vad.conf /tmp/security.conf");
   	 
    			if(system(syscmd) < 0){
        			syslog(LOG_INFO,"Error in copying security_vad.conf \n");
        			return -2;
    			}
        		copy_n_extract_tarseqno_filename(certprefix, nexthrfilename,nextdaystr);	
		}
		//syslog(LOG_INFO,"%s\t%d\thrfilename=%s\n",__func__,__LINE__,hrfilename);
		//syslog(LOG_INFO,"%s\t%d\tnexthrfilename=%s\n",__func__,__LINE__,nexthrfilename);
	}				
	else{
		char tmp_deviceid[50];
		FILE *fp = popen("grep -i productID /etc/board | awk {'print $3'}","r");
		fscanf(fp, "%s",tmp_deviceid);
		pclose(fp);
		if(!(strcmp(tmp_deviceid,"LOCOMATE-200-OBU"))){
    		sprintf(syscmd,"cp -f /var/security_vad.conf /tmp/security.conf");
   	     	if(system(syscmd) < 0){
       			syslog(LOG_INFO,"Error in copying security_vad.conf \n");
       			return -2;
    		}
		}
	}
	sleep(1);
	
	if( (fp2 = popen("pidof usbd","r")))  {
               printf("\n Usbd Application \n"); 
               fscanf(fp2, "%s",pid);
               pclose(fp2);
               if(!strcmp(pid,"")){
#ifdef APP_CERT_MGMT
				   system("/usr/local/bin/usbd -l /tmp/usb/ -x ModelDeploymentPktCaptures/ -s /tmp/testdir/  &");
#else
					if(isconfig==0){
                    	system("/usr/local/bin/usbd -l /tmp/usb/ -x ModelDeploymentPktCaptures/ -s /tmp/testdir/  &");
               		}	
					else{
						system("/usr/local/bin/usbd -l /tmp/usb/ -x ModelDeploymentPktCaptures/ -s /tmp/testdir/ -P ModelDeploymentConfigurationItems/1609Certificates/ &");
    				}
#endif
				}    
	}
	else 
	 printf("pidof usbd fail %d\n", errno);

	
	if( (fp1 = popen("pidof Asm.bin","r")))  {
        	fscanf(fp1, "%s",pid);
                pclose(fp1);
                if(!strcmp(pid,"")) 
	        {       
		    int ret;
		    system("mkdir -p /tmp/usb/");
		    ret = system("mount -t vfat -o sync /dev/sda1 /tmp/usb");
        	    if((WIFEXITED(ret)) && (WEXITSTATUS(ret)==0)){
    		        syslog(LOG_INFO, "usb mount success \n");
			usb_mounted = 1;
			system("mkdir -p /tmp/usb/ModelDeploymentPktCaptures/");
	                system("mkdir -p /tmp/usb/wlan_capture");
     		        system("mkdir -p /tmp/usb/eth_capture");
			system("mkdir -p /tmp/usb/ssl/");
        		//return 0;
        	    }
        	    else{
        		syslog(LOG_INFO, "usb mount err  \n");
        		//return -1;
			system("rm -rf /tmp/usb/");
			usb_mounted = 0;
        	    }
		    system("/usr/local/bin/Asm.bin  -c /tmp &");
	        }
	}
	
	for(i = 0; i < 8; i++){
		int len = 0;
		len = strlen(pid_name1[i]);
		if(len != 0){
			if((!strcmp(pid_name1[i],"INVALID")) && (!strcmp(pid_name1[i],""))){
				printf("INVALID");
				return 0;
			}
			if((strcmp(db_values[i+16],"INVALID")) && (strcmp(db_values[i+16],""))){
				sprintf(cmd, "%s %s > /dev/null 2>&1 &", (char *)&db_values[i+8], (char *)&db_values[i+16]);
				system(cmd);	 
			} 			
			else{
				sprintf(cmd, "%s > /dev/null 2>&1 &", (char *)&db_values[i+8]);
				system(cmd);	  
			}			
		}
	}
	syslog(LOG_INFO, "ST,\t Run,\n");
	/****** capture_app ************/
    if(usb_mounted > 0) {
	if( (fp1 = popen("pidof capture_app","r")))  {
        	fscanf(fp1, "%s",pid);
                pclose(fp1);
                if(!strcmp(pid,"")) 
	         {   
		    sprintf(cmd,"/usr/local/bin/capture_app %04x &",mod_depl);
                    system(cmd);
		 }
	}
	if( (fp1 = popen("pidof eth_app","r")))  {
        	fscanf(fp1, "%s",pid);
                pclose(fp1);
                if(!strcmp(pid,"")) 
	        {
		    sprintf(cmd,"/usr/local/bin/eth_app %04x &",mod_depl);
                    system(cmd);
		}
	}
	if( (fp1 = popen("pidof auto_off_load","r")))  {
        	fscanf(fp1, "%s",pid);
                pclose(fp1);
                if(!strcmp(pid,"")) 
	        {
		    sprintf(cmd,"/usr/local/bin/auto_off_load -c /var/logoffload.conf &");
                    system(cmd);
		}
	}
    }
    if( (fp1 = popen("pidof hbrtc","r")))  {
        fscanf(fp1, "%s",pid);
        pclose(fp1);
        if(!strcmp(pid,""))
        {
            sprintf(cmd,"/usr/local/bin/heartbeat_tr < /var/config > /dev/null");
            system(cmd);
        }
    }
    if(run_lcm_module == 2)
	system("/usr/local/bin/lcm_module");
return 0;	
}

