#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<unistd.h>
#include<arpa/inet.h>
#include<sys/time.h> // for gettimeofday
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <unistd.h>
#include <ctype.h>
#include <math.h>
#include <errno.h>
#include <assert.h>
#include <signal.h>
#include <stdbool.h>
#include <pthread.h>
#include <getopt.h>
#include <sys/mount.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/syscall.h>

#include "usbd.h"

#define LOG_FILENAME_LEN 255

#define COMMAND_LEN 20
#define DATA_SIZE 1000

struct sockaddr_in server_address;
struct sockaddr_in client_address;
fd_set readfds, testfds;
fd_set rfds;

extern int usb_connect();
extern int usb_close_sock();
static int usbsockfd = -1;
usbsock_cmd usbsock;

//sem_t rse_sem;
static pthread_t server_th, cert_th;
static void *usbd_server(void *data);
static void *cert_thread(void *data);
void sig_int(void);
void sig_segv(void);
void sig_term(void);
int USB_Usage(void);
int mount_usb();
int unmount_usb();

static unsigned int usbchkcnt=0;

usbsock_cmd usbsock;

static char mntpath[255];
static char certprefix[255];
static char log_dest[255];
static char scp_dest[255];
char logfile[255];
int vadmode=0,scp_status=0;
int usb_usage=-1;

FILE *fd=NULL;
char file[255],logsyscmd[250];

struct linux_dirent {
	long           d_ino;
	off_t          d_off;
	unsigned short d_reclen;
	char           d_name[];
};
#define BUF_SIZE 1024
char lsbuf[BUF_SIZE];

void usage() {
    printf("\nusage: usb-deamon\n");
    printf(" \n******** Options ******\n");
    printf("\n\t -h:\tThis help \n");
    printf("\t -l:\tUSB mount path (specify path ending with /)\n");
    printf("\t -P:\tPrefex of certificate files including directory path\n");
    printf("\t -x:\tDestination folder in USB where log files can be copied (specify path ending with /)\n");
    printf("\t -s:\tDestination folder for SCP (specify path ending with /)\n");
	exit(0);
}

void Options(int argc, char *argv[])
{
    int index = 0;
    int t;
    static struct option opts[] =
    {
        {"help", no_argument, 0, 'h'},
        {"USB Mount", required_argument, 0, 'l'},
        {"prefix of certificate files", required_argument, 0, 'P'},
        {"log file USB folder", required_argument, 0, 'x'},
        {"folder for SCP", required_argument, 0, 's'},
        {0, 0, 0, 0}
    };
    while(1) {
        t = getopt_long(argc, argv, "hl:P:x:s:", opts, &index);
        if(t < 0) {
            break;
        }
        switch(t) {
            case 'h':
                usage();
            break;
            case 'l':
                strcpy(mntpath, optarg);
                break;
            case 'P':
                strcpy(certprefix, optarg);
				vadmode=1;
            break;

            case 'x':
                strcpy(log_dest, optarg);
            break;

            case 's':
                strcpy(scp_dest, optarg);
                strcpy(file,"NOFILE");
            break;

            default:
               usage();
            break;
        }
    }
}

int mount_usb(){
    FILE *mountfd=NULL;
    char mntbuff[100];
    char logsyscmd[50];
    int set =0 ;
     
    sprintf(logsyscmd,"mount -t vfat /dev/sda1 /tmp/usb");
    mountfd = popen(logsyscmd,"r");
    pclose(mountfd);
    sprintf(logsyscmd,"mount");
    mountfd = popen(logsyscmd,"r");
    while(fgets(mntbuff,sizeof(mntbuff),mountfd)!=NULL){
         if(strstr(mntbuff,"/dev/sda1"))
	    set =1 ;
	 }
    pclose(mountfd);
    if(set == 0) {
        syslog(LOG_INFO, "usb mount err (%d) \n", usbsock.cmd);
	return -1;
    }
//    else {
//        syslog(LOG_INFO, "usb mount success (%d) \n", usbsock.cmd);
//    }
    return 0;
}

int unmount_usb(){

    FILE *mountfd=NULL;
    char mntbuff[100];// = "/mnt";
    char logsyscmd[50];
    int set =0 ;
     
    sprintf(logsyscmd,"umount /dev/sda1");
    mountfd = popen(logsyscmd,"r");
    pclose(mountfd);
    sprintf(logsyscmd,"mount");
    mountfd = popen(logsyscmd,"r");
    while(fgets(mntbuff,sizeof(mntbuff),mountfd)!=NULL){
       if(strstr(mntbuff,"/dev/sda1"))
           set =1 ;
	}
    pclose(mountfd);
    if(set != 0) {
        syslog(LOG_INFO, "usb un mount err (%d) \n", usbsock.cmd);
	return -1;
     }
//     else {
//         syslog(LOG_INFO, "usb un mount success (%d) \n", usbsock.cmd);
//     }
     return 0;
}

int list(void)
{
        struct linux_dirent *d=NULL;
        static int bpos=0;
        char d_type;
        static int fd;
        static int nread=0;
        if(bpos >= nread){
           nread=bpos=0;
    	   sprintf(logsyscmd,"%s%s",mntpath,log_dest);//copy to some temperory directory
           fd = open(logsyscmd, O_RDONLY);
           if (fd == -1){
    	      syslog(LOG_INFO,"list: open err\n");
              strcpy(file,"NOFILE");
              return -1;
	   }

           nread = syscall(SYS_getdents, fd, lsbuf, BUF_SIZE);
           close(fd);
           if (nread == -1){
    	      syslog(LOG_INFO,"list: getdents err\n");
              strcpy(file,"NOFILE");
              return -1;
           }

           if (nread == 0){
    	      syslog(LOG_INFO,"list: nofiles\n");
              strcpy(file,"NOFILE");
              return -1;
           }
        }

        while(bpos < nread) {
           d = (struct linux_dirent *) (lsbuf + bpos);
           d_type = *(lsbuf + bpos + d->d_reclen - 1);
           if (d_type == DT_REG)  {
              bpos += d->d_reclen;
              strcpy(file,(char *) d->d_name);
              return 0;
           }
           bpos += d->d_reclen;
        }
        strcpy(file,"NOFILE");
        return -1;
}



int scp_filels(void){
	
	if (strcmp(file,"NOFILE")!=0){
    	sprintf(logsyscmd,"rm -f %s%s%s",mntpath,log_dest,file);//remove from usb
	    if(system(logsyscmd)<0) {
    	   syslog(LOG_INFO,"Err: %s \n",logsyscmd);
	       return -1;
    	}
	}
						if(unmount_usb()<0){
                            syslog(LOG_INFO,"unmounting failed. (%d)\n",usbsock.cmd);
                        }
						if(mount_usb()<0){
                        syslog(LOG_INFO,"mounting failed. (%d) \n", usbsock.cmd);
                        strcpy(usbsock.fname,"MNTERROR");
                        return -1;
                        }

	list();
						if(unmount_usb()<0){
                            syslog(LOG_INFO,"unmounting failed. (%d)\n",usbsock.cmd);
                        }
						if(mount_usb()<0){
                        syslog(LOG_INFO,"mounting failed. (%d) \n", usbsock.cmd);
                        strcpy(usbsock.fname,"MNTERROR");
                        return -1;
                        }
    if(strcmp(file,"NOFILE")!=0){
    sprintf(logsyscmd,"cp -p -f %s%s%s %s.",mntpath,log_dest,file,scp_dest);//copy to some temperory directory
    if(system(logsyscmd)<0) {
       printf("Err: %s \n",logsyscmd);
       return -1;
    }
    }
    strcpy(usbsock.fname,file);
    return 0;
}

int main(int argc, char*argv[])
{

    pthread_attr_t attr;
    struct sched_param param;
    int ret;

	Options(argc ,argv);
//    sem_init(&rse_sem,0,1);
    signal(SIGINT,(void *)sig_int);
    signal(SIGSEGV,(void *)sig_segv);
    signal(SIGTERM,(void *)sig_term);

    pthread_attr_init (&attr);
    pthread_attr_setschedpolicy (&attr, SCHED_OTHER);
    pthread_attr_setschedparam (&attr, &param);
    pthread_attr_setinheritsched (&attr, PTHREAD_INHERIT_SCHED);

    ret = pthread_create(&server_th, NULL, usbd_server, NULL );
    sched_yield();

	if(vadmode==1){
    	ret = pthread_create(&cert_th, NULL, cert_thread, NULL );
	    sched_yield();
	}


    pthread_join(server_th, NULL);
	if(vadmode==1)
    	pthread_join(cert_th, NULL);

    return 0;

}
int server_sockfd;
void *usbd_server(void *data)
{
	int client_sockfd;
    int server_len; 
    socklen_t client_len;
    int result;static int file_name =1 ;
 //   char ch;
    int fd;
    int nread;
/*  Create and name a socket for the server.  */

	printf("in usbd_server\n");
    server_sockfd = socket(AF_INET, SOCK_STREAM, 0);

    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_port = htons(6666);
    server_len = sizeof(server_address);

    if(bind(server_sockfd, (struct sockaddr *)&server_address, server_len)<0){
	    syslog(LOG_INFO,"bind failed:usbd\n");
	    exit(-1);
    }

/*  Create a connection queue and initialize readfds to handle input from server_sockfd.  */

    listen(server_sockfd, 5);

    FD_ZERO(&readfds);
    FD_SET(server_sockfd, &readfds);

/*  Now wait for clients and requests.
 *      Since we have passed a null pointer as the timeout parameter, no timeout will occur.
 *          The program will exit and report an error if select returns a value of less than 1.  */
    //if(mount_usb()<0){
    //    syslog(LOG_INFO,"mounting failed. (%d) \n", usbsock.cmd);
    //    strcpy(usbsock.fname,"MNTERROR");                                         
    //}

    while(1) {

        testfds = readfds;

        result = select(FD_SETSIZE, &testfds, (fd_set *)0,
            (fd_set *)0, (struct timeval *) 0);

        if(result < 1) {
            syslog(LOG_INFO,"GPSC:select error");
            exit(1);
        }

/*  Once we know we've got activity,
 *      we find which descriptor it's on by checking each in turn using FD_ISSET.  */

        for(fd = 0; fd < FD_SETSIZE; fd++) {
            if(FD_ISSET(fd,&testfds)) {

/*  If the activity is on server_sockfd, it must be a request for a new connection
 *      and we add the associated client_sockfd to the descriptor set.  */
               if(fd == server_sockfd) {
                    client_len = sizeof(client_address);
                    client_sockfd = accept(server_sockfd,
                        (struct sockaddr *)&client_address, &client_len);
                    FD_SET(client_sockfd, &readfds);
                   // printf("adding client on fd %d\n", client_sockfd);
                }

/*  If it isn't the server, it must be client activity.
 *      If close is received, the client has gone away and we remove it from the descriptor set.
 *          Otherwise, we 'serve' the client as in the previous examples.  */

                else {
                    ioctl(fd, FIONREAD, &nread);

                    if(nread == 0) {
                        close(fd);
                        FD_CLR(fd, &readfds);
                     //   printf("removing client on fd %d\n", fd);
                    }

                    else {
                      	read(fd, &usbsock, sizeof(usbsock));
               			switch(usbsock.cmd)
                                {
                                    case LOGCPY:
					if(file_name == 1){
					system("mkdir -p /tmp/usb/ModelDeploymentPktCaptures/");
					file_name =0 ;
					}
    					if(mount_usb()<0){
					    syslog(LOG_INFO,"mounting failed. (%d) \n", usbsock.cmd);
					    strcpy(usbsock.fname,"MNTERROR");                                         
              				    write(fd,&usbsock,sizeof(usbsock));
      	     				}
					else 
                                        {
				            write(fd,&usbsock,sizeof(usbsock));
					    strcpy(logfile,usbsock.fname);
					    sprintf(logsyscmd, "mv -f %s %s%s.",logfile,mntpath,log_dest);
					    system(logsyscmd);
        				    if(unmount_usb()<0){
	    					syslog(LOG_INFO,"unmounting failed. (%d)\n",usbsock.cmd);
        				    }
					    usb_usage = USB_Usage();
                                        }
                                    break;
										
                                    case SCPCPY:						
    					scp_status=usbsock.ret;
					if(scp_status != 9){
    					if(mount_usb()<0){
					    syslog(LOG_INFO,"mounting failed. (%d) \n", usbsock.cmd);
					    strcpy(usbsock.fname,"MNTERROR");                                         
                        write(fd,&usbsock,sizeof(usbsock));
						break;
      	     			}
                        scp_filels();
        				if(unmount_usb()<0){
	    					syslog(LOG_INFO,"unmounting failed. (%d)\n",usbsock.cmd);
        				}
                        write(fd,&usbsock,sizeof(usbsock));
                        usb_usage = USB_Usage();
					}
                                       break;

				    case USBUSAGE:
				    // return usb usage in %. update usbusage on every other operations.
				    // write() for status
				        if(usb_usage == -1) {
					    usb_usage = USB_Usage();
                                            if(usb_usage == -1) 	
					        strcpy(usbsock.fname,"MNTERROR");
 					}

					usbsock.ret = usb_usage;
					usbsock.cmd = scp_status;
					write(fd,&usbsock,sizeof(usbsock));
					break;

                                    case CERTCPY:
                                       #if 0 
                                        //copy the usbsock.fname to local security folder from USB
                                        //and delete the file which seq.no is less then 1 of copied file from both USB and local security folder
                                        //write() for status
			                 usb_usage = USB_Usage();
                                         if(usb_usage == -1) 	
					        strcpy(usbsock.fname,"MNTERROR");
                                      #endif
                                    break;

                                    default:
                                        syslog(LOG_INFO,"Unknown Command");
                                    break;

                                }
                   
                    }
                }
            }
        }
        sched_yield();
    }
	return 0;
}

void *cert_thread(void *data)
{
	
	while(1){
		usbsockfd = usb_connect();
		usbsock.cmd = 4;
		strcpy(usbsock.fname,"cert_file");
		write(usbsockfd,&usbsock,sizeof(usbsock));
		usb_close_sock();
		sleep(60);
#if 0
		- sleep() ideally for 24hr..but, shld be taken care for (24hr - starting_time)
			- compute the file name to be copied
			- delete the old file from USB & local security folder.
		usbsockfd = usb_connect();
		usbsock.cmd = 4;
		strcpy(usbsock.fname,"cert_file");
		write(usbsockfd,&usbsock,sizeof(usbsock));
		read(usbsockfd,&usbsock,sizeof(usbsock)); //for status

#endif
		sched_yield();
	}

}

void sig_int(void)
{
    //sem_destroy(&rse_sem);
    pthread_cancel(server_th);
    if(vadmode==1){
    	pthread_cancel(cert_th);
	usb_close_sock();
    }
    if(server_sockfd >0 )
       close(server_sockfd); 
        unmount_usb();
	
}

void sig_term(void)
{
    sig_int();
}
void sig_segv(void)
{
    sig_int();
}

int USB_Usage()
{
    FILE *pf;
    char command[COMMAND_LEN];
    char data[DATA_SIZE],*copy;
    char tmpstr1[10],tmpstr2[10],tmpstr3[10],tmpstr4[10],tmpstr5[10],tmpstr6[10],tmpstr7[10],tmpstr8[10],tmpstr9[10],tmpstr10[10],tmpstr11[10],tmpstr12[10];
    int usageval = usb_usage;
    if (usbchkcnt % 40 == 0) {
        if(mount_usb()<0){
	    syslog(LOG_INFO,"usbusage:mounting failed. (%d) \n", usbsock.cmd);
	    return -1;
        }
         sprintf(command, "df %s",mntpath);
         pf = popen(command,"r");
                if(!pf){
                       syslog(LOG_INFO, "Could not open pipe for output.\n");
                       return -2;
                       }
         fread(data,sizeof(char),DATA_SIZE , pf);
         sscanf(data, "%s %s %s %s %s %s %s %s %s %s %s %s", tmpstr1, tmpstr2, tmpstr3 ,tmpstr4,
         tmpstr5,tmpstr6,tmpstr7,tmpstr8,tmpstr9,tmpstr10,tmpstr11,tmpstr12);
         copy=strtok(tmpstr12," ");
         usageval =atoi(copy);
         if (pclose(pf) != 0)
            syslog(LOG_INFO," Error: Failed to close command stream \n");
        if(unmount_usb()<0){
	    syslog(LOG_INFO,"usbusage:unmounting failed.\n");
	    return -1;
        }
        syslog(LOG_INFO,"usbusage:%d%% -%d-\n", usageval,usbchkcnt);
    }
    usbchkcnt++;
    return usageval ;
}

