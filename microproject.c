//				Micro Project

///*********************///include files///*************************************************///
#include<stdio.h>
#include<stdlib.h>
#include<string.h>


#define SIZE 10
//*********************//struct declaration//*********************************************//

struct unode
{
char uname[SIZE];
char passwd[SIZE];
struct unode *child,*sibling;
};

struct fnode
{
char fname[SIZE];
char createdby[SIZE];
int isfile;
struct fnode *child,*sibling;
};


//*********************//create unode//****************************************************//

struct unode * createunode(char *name)
{
struct unode *temp;
char pass[SIZE];
printf("\nType UNIX password::");
//getpass(pass);
gets(pass);
temp=(struct unode*)malloc(sizeof (struct unode));
strcpy(temp->uname,name);
strcpy(temp->passwd,pass);
temp->child=NULL;
temp->sibling=NULL;
return temp;
};

//*********************//create fnode//****************************************************//

struct fnode * createfnode(char *name,char *cb,int ifile)
{
struct fnode *temp;
temp=(struct fnode*)malloc(sizeof (struct fnode));
strcpy(temp->fname,name);
strcpy(temp->createdby,cb);
temp->isfile=ifile;
temp->child=NULL;
temp->sibling=NULL;
return temp;
};

//*********************//initialise//****************************************************//

void initialise(struct unode **user,struct fnode **fs)
{
	*user=createunode("root");
	*fs=createfnode("/","root",0);
}


int extract(char *cmd)
{
	if(!strcmp(cmd,"ls")) return 1;
	else if(!strcmp(cmd,"cd")) return 2;
	else if(!strcmp(cmd,"mkdir")) return 3;
	else if(!strcmp(cmd,"pwd")) return 4;
	else if(!strcmp(cmd,"touch")) return 5;
	else if(!strcmp(cmd,"tree")) return 6;

	else if(!strcmp(cmd,"useradd")) return 7;
	else if(!strcmp(cmd,"su")) return 8;
	else if(!strcmp(cmd,"passwd")) return 9;
	else if(!strcmp(cmd,"exit")) return 0;
	else if(!strcmp(cmd,"clear")) return 10;
	else return -9999;
}


void list(struct fnode *fs)
{
	if(fs->child==NULL) printf("\n--directory empty");
	else
	{
		fs=fs->child;
		while (fs!=NULL)
		{	
			printf("%s -- %s\n",fs->fname,fs->createdby);
			fs=fs->sibling;			
		}
	}
}



struct fnode* cdir(struct fnode *fs,char *cmd1)
{
	struct fnode *fs1=fs,*fs2=fs;
	if(fs->child==NULL) 
	{
		printf("\n--directory empty");		
		return fs;
	}
	else
	{

		while(fs2!=NULL)
		{
			
			while (fs1!=NULL)
			{	
				if(!strcmp(fs1->fname,cmd1)) 
				{
					if(fs1->isfile==1)
					{
					printf("\n--cannot cd to file");	
					return fs;					
					}
					else return fs1;
				}
				fs1=fs1->sibling;			
			}
			fs1=fs2=fs2->child;
		
		}
	printf("\n--no such directory");		
	return fs;

	}
	

}

struct fnode* mkdir(struct fnode *fs,char *cmd1,char *ptr)
{
	struct fnode *fs1=fs;
	if(fs->child==NULL) 
	{
		fs->child=createfnode(cmd1,ptr,0);
	}
	else
	{
		fs=fs->child;
		while (fs->sibling!=NULL)
		fs=fs->sibling;	
		
		fs->sibling=createfnode(cmd1,ptr,0);		
		

	}
	return fs1;

}


void pwd(struct fnode *fs)
{
	printf("%s\n",fs->fname);
}


struct fnode* touch(struct fnode *fs,char *cmd1,char *ptr)
{
	struct fnode *fs1=fs;
	if(fs->child==NULL) 
	{
		fs->child=createfnode(cmd1,ptr,1);
	}
	else
	{
		fs=fs->child;
		while (fs->sibling!=NULL)
		fs=fs->sibling;	
		
		fs->sibling=createfnode(cmd1,"root",1);		
		

	}
	return fs1;

}



void tree(struct fnode *fs)
{
	struct fnode *fs1=fs,*fs2=fs;
	if(fs->child==NULL) printf("\n--directory empty");
	else
	{	while(fs2!=NULL)
		{
			fs1=fs2=fs2->child;
			while (fs1!=NULL)
			{	
			printf("%s -- %s\n",fs1->fname,fs1->createdby);			
			fs1=fs1->sibling;			
			}
		
		printf("\n");
		}
	}
}



struct unode* useradd(struct unode *user,char *cmd1)
{
	struct unode *user1=user;
	if(user->child==NULL) 
	{
		user->child=createunode(cmd1);
	}
	else
	{
		
		while (user->child!=NULL)
		user=user->child;	
		
		user->child=createunode(cmd1);		
		

	}
	return user1;

}

struct unode* switchuser(struct unode *user,char *cmd1)
{
	char pass[SIZE];
	struct unode *user2=user;
	
		while(user2!=NULL)
		{
			
			if(!strcmp(user2->uname,cmd1)) 
			{
	
				printf("\npassword::");
				//getpass(pass);
				gets(pass);	
				if(!strcmp(user2->passwd,pass))
					return user2;
				else 
				{printf("\nwrong password"); return user;}
		
			}
		user2=user2->child;					
		}
	printf("\n--no such user");		
	return user;
	

}

struct unode* passwd(struct unode *user)
{
	struct unode *user2=user;
	char pass1[SIZE];
	char pass2[SIZE];
	printf("\nType UNIX password::");
	//getpass(pass1);
	gets(pass1);	
	printf("\nConfirm UNIX password::");
	//getpass(pass2);
	gets(pass2);
	if(!strcmp(pass1,pass2))
	{
		if(!strcmp(user2->passwd,pass1))
		strcpy(user->passwd,pass1);		
	}	
	else printf("\ntwo passwords do not match");
	return user2;

}
//*********************/////MAIN/////****************************************************//

void main()
{
	struct unode *user,*user1;
	struct fnode *fs,*fs1;
	char cmd[SIZE],cmd1[SIZE],ustr[SIZE];
	int op=-9999,i=0,j=0;
	
	initialise(&user,&fs);
	system("clear");
	fs1=fs;
	user1=user;
	while(1)
	{	cmd1[0]='\0';
		i=0;
		j=0;
		strcpy(ustr,user->uname);
		printf("%s @my-server %s -$",user->uname,fs->fname);
		scanf("%[^\n]",cmd);
		getchar();
		while(cmd[i]!=' ' && cmd[i]!='\0')
		i++;
		cmd[i++]='\0';
		while(cmd[i]!='\0')
		{
		cmd1[j]=cmd[i];
		j++;
		i++;
		}
		cmd1[j]='\0';
		op=extract(cmd);
		switch(op)
		{
			case 0: exit(0);
			break;
			case 1: list(fs); 
			break;
			case 2: if(cmd1=="\0") 
				{printf ("\t--Invalid arguement");break;}
				fs=cdir(fs1,cmd1); 
			break;
			case 3: if(cmd1=="\0") 
				{printf ("\t--Invalid arguement");break;}
				fs=mkdir(fs,cmd1,ustr); 
			break;
			case 4: pwd(fs); 
			break;
			case 5: if(cmd1=="\0") 
				{printf ("\t--Invalid arguement");break;}
				fs=touch(fs,cmd1,ustr); 
			break;
			case 6: 
				tree(fs); 
			break;
			case 7: if(cmd1=="\0") 
				{printf ("\t--Invalid arguement");break;}
				user=useradd(user,cmd1); 
			break;
			case 8: if(cmd1=="\0") 
				{printf ("\t--Invalid arguement");break;}
				user=switchuser(user1,cmd1); 
			break;
			case 9: if(cmd1=="\0") 
				{printf ("\t--Invalid arguement");break;}
				user=passwd(user); 
			break;
			case 10:system("clear");
			break; 
			default: printf("\t--please enter valid command!!");
		}
	}
	
}
