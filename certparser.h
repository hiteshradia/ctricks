
#define DIGESTLEN 8
typedef enum { self =0 ,
 certificate_digest_with_ecdsap224=1, 
certificate_digest_with_ecdsap256 =2, 
certificate=3, 
certificate_chain =4, 
certificate_digest_with_other_algorithm=5
 
}SignerIdentifierType;





typedef enum { 
    unsecured =0,
    signed =1, 
    encrypted =2,
    certificate_request=3, 
    certificate_response=4,
    anonymous_certificate_response =5,
    certificate_request_error=6, 
    crl_request=7,
    crl=8,
    signed_partial_payload=9,
    signed_external_payload=10,
    signed_wsa=11,
    certificate_response_acknowledgment =12
 } ContentType;





typedef enum { 
x_coordinate_only = 0, 
compressed_lsb_y_0 = 2, 
compressed_lsb_y_1 = 3, 
uncompressed = 4 
} EccPublicKeyType;



typedef enum { message_anonymous = 0, 
    message_identified_not_localized =1, 
    message_identified_localized =2, 
    message_csr =3, 
    wsa =4, 
    wsa_csr =5, 
    message_ca =6,
    wsa_ca =7,
    crl_signer=8,
    root_ca =255
} SubjectType;

typedef { use_start_validity =0, 
lifetime_is_duration(1), 2
encryption_key (2) 3
} CertificateContentFlags

typedef uint32_t Time32
typedef struct { 
    uint8_t version_and_type; 
    uint8_t subject_type; 
    uint8_t cf; 
    uint8_t signer_id[DIGESTLEN]; 
    uint8_t signature_alg; 
    uint32_t scopelen;
    uint8_t *scope;
    Time32 expiration; 
    Time32 start_validity_or_lft; 
    uint32_t crl_series; 
    PublicKey verification_key; 
    PublicKey encryption_key; 
    EccPublicKey reconstruction_value; 
} Certificate;

typedef struct{
int namelen;
uint8_t * name;
uint16_t permitted_subject_types; 

}CertSpecificData;

typedef struct { 
    uint8_t type; 
    uint8_t x[32]; 
    } EccPublicKey;

typedef struct {
 uint8_t algorithm;
 uint8_t supported_symm_alg;
    EccPublickey public_key;
    //uint8_t type; 
    //uint8_t x[32]; 
 } PublicKey;

typedef struct {
EccPublicKey R;
uint8_t s[32];

} Signature;
