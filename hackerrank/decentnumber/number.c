#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>

//https://en.wikipedia.org/wiki/Diophantine_equation#Linear_Diophantine_equations

int main()
{
	int t, a, b, x, y, i, k;

	scanf("%d", &t);
	for (int a0 = 0; a0 < t; a0++) {
		int n;
		scanf("%d", &n);
		a = n / 3;
		b = (2 * n) / 5;
		k = a;
		x = 2 * n - 5 * k;
		y = -n + 3 * k;
		while (k < (b)) {

			if (x < 0 || y < 0) {

				k++;
				x = 2 * n - 5 * k;
				y = -n + 3 * k;
			}else
				break;
		}
		if (x < 0 || y < 0)
			printf("-1");
		else{
			for (i = 1; i <= x; i++)
				printf("555");
			for (i = 1; i <= y; i++)
				printf("33333");
		}
		printf("\n");
	}
	return 0;
}
