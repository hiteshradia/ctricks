#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#define BIGINT_MAX1 100000
#define BIGINT_MAX 100000
struct bigint
{
    int digits;
  int num[BIGINT_MAX1];    
};

void zeroout(struct bigint *bignum)
{

        memset(bignum->num,0,BIGINT_MAX1);
    bignum->digits=0;
    
}


void copy_bigint(struct bigint *bignum1,struct bigint *bignum2)
{
   // int i;
  //  for(i=0;i<BIGINT_MAX;i++)
    {
       //  memset(bignum1->num,0,bignum1->digits);
      //  memcpy(bignum1->num,bignum2->num,bignum2->digits);
       
        //bignum1->digits=bignum2->digits;
        *bignum1 = *bignum2;
    }
}

void add_bigint(struct bigint * ans,struct bigint *bignum1,struct bigint *bignum2)
{
    int i,carry;
    carry=0;
    int maxdigits = (bignum1->digits > bignum2->digits)?bignum1->digits:bignum2->digits;
    for(i=0;i<maxdigits;i++)
    {
              
      //   printf("%d %d %d %d\n",bignum1->num[i],bignum2->num[i],carry,ans->num[i]);
       ans->num[i] = (bignum1->num[i] + bignum2->num[i] +carry);
     
       carry= ans->num[i]/10;
        
       ans->num[i]%=10;

            
    }
    if (carry) {
            ans->num[i] = ans->num[i] + carry;
            carry= ans->num[i]/10;
            ans->num[i]%=10;
            i++;
        }
    ans->digits=i;
}

void multiply_bigint(struct bigint * ans,struct bigint *bignum1,struct bigint *bignum2)
{
    register int  i,j;
    int carry=0,plus=0;
    register int digit;
    for(i=0;i<bignum1->digits;i++)
    {
       plus=0;
    for(j=i;j<(bignum2->digits +i);j++)
    {
           digit = (bignum1->num[i] * bignum2->num[j-i]);
           digit += ans->num[j];
           digit +=carry;
           carry= digit/10;
           ans->num[j]=digit%10;
            
     }
        
     if (carry) {
            digit = ans->num[j]+carry;
            carry= digit/10;
            ans->num[j]=digit%10;
            plus =1;
        }
        ans->digits=j+plus;       
    }

}


void store_inbigint(struct bigint *bignum,unsigned long long num)
{
        int i=0;
        while(num && i<BIGINT_MAX1)
        {
            bignum->num[i]=num%10;
            num/=10;
            i++;
        }
        bignum->digits=i;
}

void print_bigint(struct bigint*bignum)
{
    int i;
    char start=0;
    for(i=bignum->digits+1;i>=0;i--)
    {
        if(!start && bignum->num[i] !=0)
           start=1;
        if(start)
            printf("%d",bignum->num[i]);
        else if(i==0)
            printf("0");
    }
    printf("\n");
}

struct bigint first;
    struct bigint second;
    struct bigint term[20];

int main() {

    int n,i;
    int  a,b,c;
    //unsigned long long term;
    //zeroout(&first);
   // zeroout(&second);
    //zeroout(&term);
    
    scanf("%d%d%d",&a,&b,&n);

     for(i=3;i<7;i++)
     {
         c = (b*b) +a;
         a=b;
         b=c;
        if(i==n)
        {
            printf("%llu\n",c);
            return 0;
        }
     }
    store_inbigint(&term[5],a);
    store_inbigint(&term[6],b);
    //BIGINT_MAX =50000;
    for(i=7;i<=n;i++)
    {
        
       //zeroout(&term);
        copy_bigint(&term[i],&term[i-2]);
       // print_bigint(&second);
        multiply_bigint(&term[i],&term[i-1],&term[i-1]);
     // print_bigint(&term);
        // print_bigint(&first);
        //add_bigint(&term,&term,&first);
         //print_bigint(&term);
        //term = a + (b*b);
     
        //zeroout(&first);
       // copy_bigint(&first,&second);
        //a=b;    
      
        // zeroout(&second);
         //copy_bigint(&second,&term);
        //b=term;
        // print_bigint(&term);
       // BIGINT_MAX*=2;
    }
    //printf("%llu\n",term);
    print_bigint(&term[n]);

    return 0;
}


