#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#define BIGINT_MAX1 100000
#define BIGINT_MAX 100000
struct bigint
{
    int digits;
  int num[BIGINT_MAX1];    
};

void zeroout(struct bigint *bignum)
{

        memset(bignum->num,0,BIGINT_MAX1);
    bignum->digits=0;
    
}




void multiply_bigint(struct bigint * ans,struct bigint *bignum1,struct bigint *bignum2)
{
    register int  i,j;
    int carry=0,plus=0;
    register int digit;
    for(i=0;i<bignum1->digits;i++)
    {
       plus=0;
    for(j=i;j<(bignum2->digits +i);j++)
    {
           digit = (bignum1->num[i] * bignum2->num[j-i]);
           digit += ans->num[j];
           digit +=carry;
           carry= digit/10000;
           ans->num[j]=digit%10000;
            
     }
        
     if (carry) {
            digit = ans->num[j]+carry;
            carry= digit/10000;
            ans->num[j]=digit%10000;
            plus =1;
        }
        ans->digits=j+plus;       
    }

}


void store_inbigint(struct bigint *bignum,unsigned long long num)
{
        int i=0;
        while(num && i<BIGINT_MAX1)
        {
            bignum->num[i]=num%10000;
            num/=10000;
            i++;
        }
        bignum->digits=i;
}

void print_bigint(struct bigint*bignum)
{
    int i;
printf("%d",bignum->num[bignum->digits-1]);
    for(i=bignum->digits-2;i>=0;i--)
    {
        
            printf("%04u",bignum->num[i]);

    }
    printf("\n");
}

struct bigint first;
    struct bigint second;
    struct bigint term;
struct bigint *pfirst;
    struct bigint* psecond;
    struct bigint* pterm;
int main() {

    int n,i;
    int  a,b,c;

    pfirst = &first;
    psecond=&second;
    pterm=&term;
    
    scanf("%d%d%d",&a,&b,&n);

     for(i=3;i<7;i++)
     {
         c = (b*b) +a;
         a=b;
         b=c;
        if(i==n)
        {
            printf("%d\n",c);
            return 0;
        }
     }
    store_inbigint(pfirst,a);
    store_inbigint(psecond,b);
    //BIGINT_MAX =50000;
    for(i=7;i<=n;i++)
    {
        
       //zeroout(&term);
        //copy_bigint(pterm,pfirst);
        pterm=pfirst;
       // print_bigint(&second);
        multiply_bigint(pterm,psecond,psecond);
     // print_bigint(&term);
        // print_bigint(&first);
        //add_bigint(&term,&term,&first);
         //print_bigint(&term);
        //term = a + (b*b);
     
        //zeroout(&first);
       // copy_bigint(pfirst,psecond);
        pfirst=psecond;
        //a=b;    
      
        // zeroout(&second);
        //copy_bigint(psecond,pterm);
        psecond=pterm;
        //b=term;
        // print_bigint(&term);
       // BIGINT_MAX*=2;
    }
    //printf("%llu\n",term);
    print_bigint(pterm);

    return 0;
}


