#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
int adj[1005][1005];


void update_count(int n, int level)
{
	int i, j;
	static int foundlink = 0;

	for (i = 1; i <= n; i++) {

		if (adj[i][i] == level) {
			for (j = 1; j <= n; j++) {
				if (i != j) {
					if (adj[i][j] != 0) {
						// printf("here i=%d j=%d %d\n",i,j,adj[j][j]);
						if (adj[j][j] < 0) {

							adj[j][j] = level + 1;
							foundlink = 1;
						}


					}
				}
			}
		}


	}
	if (foundlink) {
		foundlink = 0;
		update_count(n, level + 1);
	}

}


int main()
{
	int t, n, m, i, j, s, n1, n2;

	/* Enter your code here. Read input from STDIN. Print output to STDOUT */
	scanf("%d", &t);
	for (i = 1; i <= t; i++) {
		//for(j=0;j<1005;j++)
		//{
		memset(adj, 0, sizeof(adj[0][0]) * 1005 * 1005);
		//}
		scanf("%d%d", &n, &m);
		for (j = 1; j <= m; j++) {
			scanf("%d%d", &n1, &n2);
			adj[n1][n2] = 1;
			adj[n2][n1] = 1;
		}
		scanf("%d", &s);
		for (j = 1; j <= n; j++) {
			adj[j][j] = -1;
		}
		adj[s][s] = 0;
		update_count(n, 0);
		for (j = 1; j <= n; j++) {
			if (j != s) {
				if (adj[j][j] != -1)
					adj[j][j] *= 6;

				printf("%d ", adj[j][j]);

			}
		}
		printf("\n");
	}
	return 0;
}

