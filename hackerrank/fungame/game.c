#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int a[1005];
int b[1005];
int c[1005];

void mysort(int * a, int * b, int* c, int n)
{
	int i, j, temp;

	for (i = 1; i <= n; i++) {
		for (j = i + 1; j <= n; j++) {
			if (c[j] > c[i]) {
				temp = c[j];
				c[j] = c[i];
				c[i] = temp;

				temp = b[j];
				b[j] = b[i];
				b[i] = temp;

				temp = a[j];
				a[j] = a[i];
				a[i] = temp;
			}
		}

	}
}

int main()
{
	int t, n, i, j, temp;
	unsigned long long a_score, b_score;

	/* Enter your code here. Read input from STDIN. Print output to STDOUT */
	scanf("%d", &t);
	for (i = 1; i <= t; i++) {

		scanf("%d", &n);
		for (j = 1; j <= n; j++)
			scanf("%d", &a[j]);
		for (j = 1; j <= n; j++) {
			scanf("%d", &b[j]);
			c[j] = a[j] + b[j];
			// printf("%d ",c[j]);
		}

		a_score = 0;
		b_score = 0;
		mysort(a, b, c, n);
		for (j = 1; j <= n; j++) {
			if (j % 2 != 0)
				a_score += a[j];
			else
				b_score += b[j];
		}


		if (a_score == b_score) {
			printf("Tie\n");
			continue;
		}
		if (a_score > b_score)
			printf("First\n");
		else
			printf("Second\n");




	}
	return 0;
}


