#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int sizeof_l(unsigned int n,unsigned int* l)
    {
    unsigned int i=0;
    for(i=0;i<n;i++){
        if(l[i] == 0)
            break;
    }
        return i;
        
    
}

void lcopy(unsigned int n,unsigned int *ldest,unsigned int*lsrc)
    {
    memcpy(ldest,lsrc,(unsigned)(n*sizeof(int)));
    
}
void printlis(unsigned int n,unsigned int* lis)
    {
    int i=0;
    for(i=0;i<n;i++)
        printf("%d",lis[i]);
    printf("\n");
    
}
    unsigned int arr[10000];
    unsigned int lis[10000][10000];
int main() {
    unsigned int n,i,j;
    unsigned int size_li, size_lj,longest;
    FILE * input;

	input = fopen("input.txt","r"); 
    fscanf(input,"%u",&n);
    for (i=0;i<n;i++){
        fscanf(input,"%u",&arr[i]);
        printf("%u:%u\n",i,arr[i]);
         memset(&lis[i][0],0,(unsigned)(n* sizeof(int)));
    }
    
    lis[0][0] = arr[0];    
    for(i=1;i<n;i++){
        
        for(j=0;j<i;j++)
            {
             size_li = sizeof_l(n,&lis[i][0]);
            size_lj = sizeof_l(n,&lis[j][0]);
          if((arr[j] < arr[i]) &&(size_li < (size_lj+1)))
              lcopy(n,&lis[i][0],&lis[j][0]);
               
        }
        size_li = sizeof_l(n,&lis[i][0]);
        lis[i][size_li] = arr[i];
    }
    
     longest =0;
    for (i=0;i<n;i++){
    //printlis(n,&lis[i][0]);
        size_li = sizeof_l(n,&lis[i][0]);
        longest = (size_li > longest)?size_li:longest;
    }
   
    printf("%u\n",longest);
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */    
    return 0;
}

