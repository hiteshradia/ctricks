#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
    unsigned int n[105];
unsigned int hi[105][105]={0};
 
// A function to count all prime factors of a given number n
unsigned int primeFactors(unsigned int n)
{
    unsigned int count =0;
    // Print the number of 2s that divide n
    while (n%2 == 0)
    {
       // printf("%d ", 2);
        count++;

        n = n/2;
    }
 
    // n must be odd at this point.  So we can skip one element (Note i = i +2)
    for (int i = 3; i <= sqrt(n); i = i+2)
    {
        // While i divides n, print i and divide n
        while (n%i == 0)
        {
                    count++;

            //printf("%d ", i);
            n = n/i;
        }
    }
 
    // This condition is to handle the case whien n is a prime number
    // greater than 2
    if (n > 2)
        {
        //printf ("%d ", n);
        count++;

    }
    return count;
}



int main() {

    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int t,i,j;
    unsigned int x,pf;
    
    scanf("%d",&t);
    for(i=1;i<=t;i++)
    {
     scanf("%u",&n[i]);
          for(j=1;j<=n[i];j++)
          {
              scanf("%u",&hi[i][j]);
          }
    }
    for(i=1;i<=t;i++)
    {
        x=0; //xor sum of primefactorcount of all towers
        for (j=1;j<=n[i];j++)
            {
              if(hi[i][j] > 1){
                pf=primeFactors(hi[i][j]);
                  x=x^pf;
              }

                
            }
        if(x==0)
            printf("2\n");
        else
            printf("1\n");
    }

    return 0;
}
