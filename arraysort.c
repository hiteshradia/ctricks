#include<stdio.h>
#include<stdint.h>
#define IEEE80211_ADDR_LEN 6
#define MAXWSASTAT 10
typedef struct rwstat
{
uint8_t wsamac[IEEE80211_ADDR_LEN];
uint8_t cps; //countper second
uint8_t rcpi; //rcpi Signal strength indicator
uint8_t rank;
} Recvwsastat;
Recvwsastat recvwsastats[MAXWSASTAT];
#if 0
struct Recvwsastat temp;


uint8_t mactorank(uint8_t *macaddr)
{
uint8_t i;
for(i=0;i<MAXWSASTAT;i++)
{
if(IEEE80211_ADDR_EQ(macaddr,recvwsastats[i].wsamac))
return recvwsastats[i].rank;

}
}
#endif

void swaprws(void*a,void*b,int size1)
{
Recvwsastat temp;
temp=*(Recvwsastat *)a;
*(Recvwsastat*)a= *(Recvwsastat*)b;
*(Recvwsastat*)b = temp;

}

int cmprws( const void* a,const void*b)
{
Recvwsastat *temp1 ,*temp2;
temp1=(Recvwsastat*)b;
temp2 =(Recvwsastat *)a;
return (temp1->cps - temp2->cps);
}

void sort(void *base, size_t num, size_t size,
      int (*cmp)(const void *, const void *),
      void (*swap)(void *, void *, int size))
{
    /* pre-scale counters for performance */
    int i = (num/2 - 1) * size, n = num * size, c, r;

    if (!swap)
        //swap = (size == 4 ? u32_swap : generic_swap);
        return;
    /* heapify */
    for ( ; i >= 0; i -= size) {
        for (r = i; r * 2 + size < n; r  = c) {
            c = r * 2 + size;
            if (c < n - size && cmp(base + c, base + c + size) < 0)
                c += size;
            if (cmp(base + r, base + c) >= 0)
                break;
            swap(base + r, base + c, size);
        }
    }

    /* sort */
    for (i = n - size; i >= 0; i -= size) {
        swap(base, base + i, size);
        for (r = 0; r * 2 + size < i; r = c) {
            c = r * 2 + size;
            if (c < i - size && cmp(base + c, base + c + size) < 0)
                c += size;
            if (cmp(base + r, base + c) >= 0)
                break;
            swap(base + r, base + c, size);
        }
    }
}

#if 0
//sorting based on cps and then based on rcpi
// can give rank
int sortcps(int elems)
{
uint8_t i,highcps;
highcps=recvwsaststs[0].cps
for(i=0;i<elems;i++)
{
    if(recvwsastats[i].cps > highcps){
        highcps=recvwsastats[i].cps;
        rank[count]=i;
        count++;
    }

}

}


#endif


int main()
{
int i;
for (i=4;i>=0;i--)
recvwsastats[i].cps =i;
printf("before\n");
for (i=0;i<=4;i++)
printf("b%d\n",recvwsastats[i].cps);

sort((void*)recvwsastats,5,sizeof(Recvwsastat),cmprws,swaprws);

printf("after\n");
for (i=0;i<=4;i++)
printf("a%d\n",recvwsastats[i].cps);

 return 0;
}
