struct autoconf
{
uint8_t seqno;
uint8_t myunitid[6];
uint8_t type;
uint8_t mystatus;

};
enum {AC_INIT=0};
enum {UNSPECIFIED=0,SLAVE_ALONE,MASTER};
#define MAC_ADDR_LEN 6
#define MAC_ADDR_EQ(a1,a2)    (memcmp(a1,a2,MAC_ADDR_LEN) == 0)
#define MAC_ADDR_CMP(a1,a2)    memcmp(a1,a2,MAC_ADDR_LEN)
