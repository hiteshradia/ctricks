#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <syslog.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */

#define TMPBUFSZ 200
#define PUT_ORIGIN      0
#define putbyte(buf,off,b) do {buf[(off)-(PUT_ORIGIN)] = (unsigned char)(b); } while (0)
#define putleword(buf, off, w) do {putbyte(buf, (off)+1, (uint)(w) >> 8); putbyte(buf, (off), (w)); } while (0)
#define putlelong(buf, off, l) do {putleword(buf, (off)+2, (uint)(l) >> 16); putleword(buf, (off), (l)); } while (0)

struct termios options;
int fd;  // File descriptor



/***************************baudrate to code convert*********/
speed_t get_baud(int baudrate) {
    switch (baudrate) {
    case 0:
        return B0;
    case 50:
        return B50;
    case 75:
        return B75;
    case 110:
        return B110;
    case 134:
        return B134;
    case 150:
        return B150;
    case 200:
        return B200;
    case 300:
        return B300;
    case 600:
        return B600;
    case 1200:
        return B1200;
    case 1800:
        return B1800;
    case 2400:
        return B2400;
    case 4800:
        return B4800;
    case 9600:
        return B9600;
    case 19200:
        return B19200;
    case 38400:
        return B38400;
    case 57600:
        return B57600;
    case 115200:
        return B115200;
    default:
        return B9600;
    }
}


//*************************Opening Port***********************
int open_port(char * device)
{
    int fd;   /* File descriptor for the port */

    fd = open(device, O_RDWR | O_NOCTTY |O_NDELAY);  //check if NDELAY is needed
    if (fd == -1)
    {
        /* Could not open the port. */
        perror("open_port: Unable to open /dev/ttyUSB0 - ");
    }

    printf ( "In Open port fd = %i\n", fd);
    return (fd);

}


//***********************Port Setting**********************

int settings(unsigned int speed)
{
    speed_t spdcode =0;
    tcgetattr( fd, &options );
    spdcode=get_baud(speed);
    if(spdcode != cfgetispeed(&options))
    {
        cfmakeraw(&options);
        cfsetispeed(&options, spdcode);
        cfsetospeed(&options, spdcode);
        // options.c_cflag |= ( CLOCAL | CREAD | CS8);    /* Select 8 data bits */
        // options.c_iflag |= IGNPAR;/*IGNPAR  : ignore bytes with parity errors*/

        /* set input mode (non-canonical, no echo,...) */

        // options.c_cc[VTIME]    = 0;   /* inter-character timer unused */
        options.c_cc[VMIN]     = 1; /* blocking read until 1 chars received */
        if ( tcsetattr( fd, TCSAFLUSH, &options ) == -1 )
            (void)syslog(LOG_INFO, " Error with tcsetattr \n");
        else
            (void)syslog(LOG_INFO, "tcsetattr succeed \n");
        printf("changing spd to %d\n",speed);
        (void)tcflush(fd, TCIOFLUSH);
        (void)usleep(200000);
        (void)tcflush(fd, TCIOFLUSH);
    }
    return (0);

}

/********************generic ublox command write func **********/

int ubx_write(unsigned int msg_class, unsigned int msg_id,
              unsigned char *msg, unsigned short data_len)
{
    unsigned char CK_A, CK_B;
    ssize_t i, count,j;
    int ok,msgbuflen=0;
    unsigned char cmd_send[100] = { 0 }; //init send buffer

    /*@ -type @*/
    cmd_send[0] = 0xb5;
    cmd_send[1] = 0x62;

    CK_A = CK_B = 0;
    cmd_send[2] = msg_class;
    cmd_send[3] = msg_id;
    cmd_send[4] = data_len & 0xff;
    cmd_send[5] = (data_len >> 8) & 0xff;

    (void)memcpy(&cmd_send[6], msg, data_len);

    /* calculate CRC */
    for (i = 2; i < 6; i++) {
        CK_A += cmd_send[i];
        CK_B += CK_A;
    }
    /*@ -nullderef @*/
    for (i = 0; i < data_len; i++) {
        CK_A += msg[i];
        CK_B += CK_A;
    }

    cmd_send[6 + data_len] = CK_A;
    cmd_send[7 + data_len] = CK_B;
    msgbuflen = data_len + 8;
    /*@ +type @*/
    printf ("ubxwrite = ");
    for (j=0; j < msgbuflen; j++)
        printf("%02x ",cmd_send[j]);
    printf ("\n");
    tcflush(fd,TCIOFLUSH);
    count = write(fd,cmd_send,msgbuflen);
    (void)tcdrain(fd);
    ok = msgbuflen;
    /*@ +nullderef @*/
    return (ok);
}

int speed_default_buf(unsigned char * buff)
{
    buff[0]  = 0x01;
    buff[1] = 0x00;
    buff[2] = 0x00;
    buff[3] = 0x00;
    buff[4] = 0xc0;
    buff[5] = 0x08;
    buff[6] = 0x00;
    buff[7] = 0x00;
    buff[8] = 0x80;
    buff[9] = 0x25;
    buff[10] = 0x00;
    buff[11] = 0x00;
    buff[12] = 0x07;
    buff[13] = 0x00;
    buff[14] = 0x06;
    buff[15] = 0x00;
    buff[16] = 0x00;
    buff[17] = 0x00;
    buff[18] = 0x00;
    buff[19] = 0x00;
    return 20;

}

int default_updaterate_buf(unsigned char *buf_rate)
{

    buf_rate[0] = 0xE8;
    buf_rate[1] = 0x03;
    buf_rate[2] = 0x01;
    buf_rate[3] = 0x00;
    buf_rate[4] = 0x01;
    buf_rate[5] = 0x00;

    return 6;
}

int default_waas_status(unsigned char *waas)
{

    waas[0] = 0x01;
    waas[1] = 0x03;
    waas[2] = 0x03;
    waas[3] = 0x00;
    waas[4] = 0x51;
    waas[5] = 0x62;
    waas[6] = 0x06;
    waas[7] = 0x06;
    return 8;

}
int main(int argc,char *argv[])
{
    int res,i,n;
    unsigned int speed,len =0;
    int ubx_res,waas;
    unsigned char tmpbuf[TMPBUFSZ] = { 0 };    //init baudrate buffer
    unsigned char send_bytes[100] = { 0 };    //init baudrate buffer
    unsigned char rate_buf[100] = { 0 };    //init update_rate buffer
    unsigned char waas_buf[50] = { 0 };    //init waas buffer
    int update_rate;
    int rdret,rdretry,current_baud=0;

    if (argc != 5) {     //need 3 args -> dev, baud, conffile
        printf( "Please provide device, baudrate and conffile_location by passing three arguments\n");
        printf("eg. ./ubloxconf /dev/ttyS0 115200 200 1\n");
        return -1;
    }
    fd = open_port(argv[1]);
    speed = atoi(argv[2]);
    update_rate = atoi(argv[3]);
    waas = atoi(argv[4]);
//find current baud
    res = settings(9600);
    memset(tmpbuf,0,TMPBUFSZ);
    rdret=0;
    rdretry=2;
    while ((rdret <(TMPBUFSZ/8)) && rdretry !=0)
    {
        rdretry--;
        usleep(50000);
        rdret = read(fd,tmpbuf,TMPBUFSZ);
    }
    for(i=0; i<TMPBUFSZ; i++)
    {
        if(tmpbuf[i] == '$')
            break;
    }

    if((tmpbuf[i+1] == 'P') || (tmpbuf[i+1]=='G'))
    {
        printf("current baud =9600\n");
        current_baud =9600;
    }
    if(!current_baud)
    {
        res = settings(115200);

        memset(tmpbuf,0,TMPBUFSZ);
        rdret=0;
        rdretry=4;
        while ((rdret <(TMPBUFSZ/8)) && rdretry !=0)
        {
            rdretry--;
            usleep(50000);
            rdret = read(fd,tmpbuf,TMPBUFSZ);
        }
        for(i=0; i<TMPBUFSZ; i++)
        {
            if(tmpbuf[i] == '$')
                break;
        }

        if((tmpbuf[i+1] == 'P') || (tmpbuf[i+1]=='G'))
        {
            printf("current baud =115200\n");
            current_baud =115200;
        }
    }
    if(current_baud == 0)
    {
    (void)syslog(LOG_INFO, "ubloxconf ERROR:: cannot find current baud\n");
    printf("ubloxconf ERROR:: cannot find current baud\n");
    }

    if(update_rate)
    {
        ubx_res = default_updaterate_buf(rate_buf);
        putlelong(rate_buf, 0, update_rate);
        (void)ubx_write(0x06u, 0x08, rate_buf, ubx_res);

    }
    (void)usleep(50000);


    if(speed != current_baud && speed != 0)
    {
        len=speed_default_buf(send_bytes);
        putlelong(send_bytes, 8, speed);
        (void)ubx_write(0x06u, 0x00, send_bytes, len);    /* get this port's settings */
        (void)usleep(50000);
        settings(speed);

    }
    if(waas)
    {
        ubx_res = default_waas_status(waas_buf);
        putlelong(waas_buf, 0, waas);
        (void)ubx_write(0x06u, 0x08, waas_buf, ubx_res);
    }
    close( fd );
    return(0);

}
