/*
 * Copyright (c) 2005-2007 Arada Syatems, Inc. All rights reserved.
 * Proprietary and Confidential Material.
 *
 */

#include <stdio.h>
#include <ctype.h>
#include <termio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <time.h>
#include <signal.h>
#include <semaphore.h>
#include "wave.h"

#define SECURITY 1      //193
#define DIGEST  2       //194
#define MSG_CNT_DIGEST 92       //213
#define MSG_CNT_CERT 21         //284
#define MSG_CNT_PLAIN 7         //201
#define DIGEST_DATA_OFFSET 84
#define CERT_DATA_OFFSET 13

void sig_int(void);
void sig_term(void);
void set_args( void * ,void *, int );
extern int gpsc_connect();
extern int gpsc_close_sock();

int user_confirmBeforeJoin(struct wmeNotif_Indication *);
int ReceiveTA_Indication(struct WmeTAIndication *);

static WMEApplicationRequest entry;
struct wmeNotif_Indication notif_done;
struct WmeTAIndication ta_receive;
struct availserviceInfo get_astinfo;

sem_t indication_sem;
static	uint64_t count = 0, blank = 0;
static int pid ;

static  GPSData wsmgps;
static int gpssockfd = -1;
static int Udp_Socket = -1;
static char remote_ip[30] = {0};
static uint16_t remote_port = 0;

enum { ADDR_MAC = 0, UINT8 };
struct arguments{
    u_int8_t macaddr[ 17 ];
    u_int8_t channel;
};

struct rplot{
    long rec_lat;
    long rec_lon;
    uint32_t rec_heading;
    long my_lat;
    long my_lon;
    uint32_t my_heading;
}__attribute__((__packed__));

struct rplot sendgps1;

void SwapGpsdata()
{
uint64_t *lat,*lng,*alt,*tsf,*spd;
uint64_t *at,*ti,*pt,*ex,*ey,*ev,*crs;
uint32_t *dte;

                at=((uint64_t*)(&wsmgps.actual_time));
                *at=swap64(*at);
                ti=((uint64_t*)(&wsmgps.time));
                *ti=swap64(*ti);
                pt=((uint64_t*)(&wsmgps.local_tod));
                *pt=swap64(*pt);
                tsf=((uint64_t*)(&wsmgps.local_tsf));
                *tsf=swap64(*tsf);
                spd=((uint64_t*)(&wsmgps.speed));
                *spd=swap64(*spd);
                 crs=((uint64_t*)(&wsmgps.course));
                *crs=swap64(*crs);
                lat=((uint64_t*)(&wsmgps.latitude));
                *lat=swap64(*lat);
                lng=((uint64_t*)(&wsmgps.longitude));
                *lng=swap64(*lng);
                alt=((uint64_t*)(&wsmgps.altitude));
                *alt=swap64(*alt);
                dte=(uint32_t*)(&wsmgps.date);
                *dte=swap32_(*dte);
                ex=((uint64_t*)(&wsmgps.epx));
                *ex=swap64(*ex);
                ey=((uint64_t*)(&wsmgps.epy));
                *ey=swap64(*ey);
                ev=(uint64_t*)(&wsmgps.epv);
                *ev=swap64(*ev);

}

void ReadGpsdata()
{
char ch='1';
    if(gpssockfd > 0)
    {
       write(gpssockfd, &ch,1);
       read(gpssockfd,&wsmgps,sizeof(wsmgps));
       if(!BIGENDIAN)
            SwapGpsdata();
    }
}

int
AsmDecodeContentType(WSMData *data1)
{               
    int version, Content_type = 0, offset =0;
    version = data1->contents[offset];
    if(version == 2){         
        offset++;     
        Content_type = data1->contents[offset];
        return Content_type;
    }            
    return Content_type;
} 

int extract_data(WSMData *data,struct rplot *pos)
{
        int msg_cnt = 0;
        int i;
        unsigned char * buffer;
        int lat ,lon;
        int lat1 ,lon1;
	uint16_t heading,heading1;
        int tmpid=0,tmpid1=0;
        double latrx,lonrx;
	char * tmp1;
	int content_type;
/*      for(i=0;i<3;i++)
 *              {
 *                      printf("\ndata[%d] : %x %x %x %x %x %x %x %x \n",i*8,*(unsigned char *)&(data->contents[i*8]),*(unsigned char *)&(data->contents[ i*8 + 1]),*(unsigned char *)&(data->contents[i*8 + 2]),*(unsigned char *)&(data->contents[i*8 + 3]),*(unsigned char *)&(data->contents[i*8 + 4]),*(unsigned char *)&(data->contents[i*8 + 5]),*(unsigned char *)&(data->contents[i*8 + 6]),*(unsigned char *)&(data->contents[i*8 + 7]));
 *                              }
 *                              */
	content_type=AsmDecodeContentType(data);
	printf("data.contents--");
	for(i=0; i<20 ;i++)
	printf("%02x ",data->contents[i] &0xff);
	printf("\n");
        if(content_type == 0)
        {
        msg_cnt = MSG_CNT_PLAIN;
        if(*(unsigned char *)&data->contents[2] > 0x80)
        msg_cnt = msg_cnt+1;
        }
        else
        {
                if(*(unsigned char *)&data->contents[DIGEST] == 0x03)
                {
                msg_cnt = MSG_CNT_DIGEST;
                if(*(unsigned char *)&data->contents[84] > 0x7f)
                msg_cnt = msg_cnt+2;
                }
                else if (*(unsigned char *)&data->contents[DIGEST] == 0x02)
                {
                msg_cnt = MSG_CNT_CERT;
                if(*(unsigned char *)&data->contents[13] > 0x7f)
                msg_cnt = msg_cnt+2;
                }
        }

        buffer = (unsigned char *)(data->contents + msg_cnt) ;

        //copy lat lon from buffer: msg_cnt+7 is lat, msg_cnt+11 is lon
         memcpy(&lat,buffer+7,4);
         memcpy(&lon,buffer+11,4);
         memcpy(&heading,buffer+23,2);
        //convert big endian to host
        lat1 = ntohl(lat);
        lon1 = ntohl(lon);
        heading1 = ntohs(heading);
        latrx = (double)lat1/10000000;
        lonrx = (double)lon1/10000000;
	pos->rec_lat=lat1;
	pos->rec_lon=lon1;
	pos->rec_heading = heading1;
	ReadGpsdata();
	pos->my_lat= (long)(wsmgps.latitude*10000000);
	pos->my_lon= (long)(wsmgps.longitude*10000000);
        pos->my_heading = (uint32_t)(((wsmgps.course)*80));
	//printf("reclat=%lf reclon=%lf mylon=%lf mylon=%lf\n",latrx,lonrx,wsmgps.latitude,wsmgps.longitude);
	printf("reclat=%d reclon=%d mylon=%d mylon=%d rec_head %d\n",pos->rec_lat,pos->rec_lon,pos->my_lat,pos->my_lon,pos->rec_heading);
	tmp1=(char *)pos;
        for(i=0;i<sizeof(struct rplot);i++)
          printf("%02x ",*(tmp1+i) &0xff);
        printf("\n");
        //printf("\nRecieved lat , lon  = %lf , %lf\n", latrx,lonrx);
         memcpy(&tmpid,buffer+1,2);//taken first two bytes for dev_id else tempid is 4 bytes
         tmpid1 = ntohl(tmpid);
         tmpid1 =((0x0000FFFF)&(tmpid1 >> 16)); //first 2 bytes are needed
         //data for kml file: bufferlonlat
         return tmpid1;
}
        
void usage() {
    printf("\nWSMP-Forward Application\n");
    printf("\n \n");
    printf("\nINFO: WSMP forward application receives the specified WSMP packets and and forwards to a remote host using UDP \n");
    printf("\n \n");
    printf(" \n******** Options ******\n");
    printf("\n\t -h:\tThis help \n");
    printf("\t -i:\tIPv6 address of remote host(currently supports only IPv6)\n");
    printf("\t -p:\tPort number of remote host side listener\n");
    printf("\t -y:\tPSID of the service of which packets are to be forwarded\n");
    printf("\t -u:\tUser Request Type [1:auto, 2:unconditional, 3:none]\n");
    printf("\t -x:\tExtended Access <0:alternate /1:continuous>\n");
    printf("\t -s:\tService Channel (Mandatory is user request is unconditional)\n");
    printf("\t DEFAULT Values for some options [ -u 1 ] [ -y 32] [ -x 0] [ -s 172]\n");

    exit(0);
}


void Options(int argc, char *argv[])
{
    int index = 0;
    int t,ret;
    static struct option opts[] =
    {
        {"help", no_argument, 0, 'h'},
        {"IP Address", required_argument, 0, 'i'},
        {"Port", required_argument, 0, 'p'},
        {"PSID", required_argument, 0, 'y'},
        {"user request type", required_argument, 0, 'u'},
        {"extended access", required_argument, 0, 'x'},
        {"service channel", required_argument, 0, 's'},
        {0, 0, 0, 0}
    };
    while(1) {
        t = getopt_long(argc, argv, "hi:p:y:u:x:s:", opts, &index);
        if(t < 0) {
            break;
        }
        switch(t) {
        case 'h':
            usage();
            break;
        case 'i':
            strcpy(remote_ip, optarg);
            break;
        case 'p':
            remote_port=atoi(optarg);
            break;

        case 'y':
            entry.psid = atoi(optarg);
            entry.priority = 31;
	    printf("%d\n",entry.psid);
            break;

        case 'u':
            entry.userreqtype = atoi(optarg);
            break;

        case 'x':
            entry.schextaccess = atoi(optarg);
	    entry.schaccess = 0;	
	    entry.serviceport = 0;	
            break;

        case 's':
            entry.channel = atoi(optarg);
            break;

        default:
            usage();
            break;
        }
    }
}
void diep(char *s)
{
  perror(s);
  exit(1);
}



int main(int arg, char *argv[])
{

	WSMIndication rxpkt;
	int  ret = 0;
        struct arguments arg1;
	struct sockaddr_in server_addr ;
	sem_init(&indication_sem,0,0);
	memset(&entry, 0 , sizeof(WMEApplicationRequest));
	Options(arg,argv);
        if((Udp_Socket=socket(AF_INET, SOCK_DGRAM,IPPROTO_UDP)) < 0){
        printf("socket creation failed..\n");
        return -1;
        }
	server_addr.sin_family = AF_INET;
        server_addr.sin_port = htons(remote_port);
	if (inet_pton(AF_INET,remote_ip, &server_addr.sin_addr)==0) {
            fprintf(stderr, "inet_pton() failed\n");
            exit(1);
        }
//	if (connect(Udp_Socket, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
 //           perror("udp connect");
  //          exit(5);
  //      }
        
#if 0
        if (arg < 5) {
            printf("usage: localrx [user req type<1-auto> <2-unconditional> <3-none>] [imm access] [extended access] [channel <optional>] [psid] [PROVIDER MAC <optional>]\n");
            return 0;
        }
#endif 
	pid = getpid();
#if 0
	entry.psid = atoi(argv[5]);
        if ((atoi(argv[1]) > USER_REQ_SCH_ACCESS_NONE) || (atoi(argv[1]) < USER_REQ_SCH_ACCESS_AUTO)) {
            printf("User request type invalid: setting default to auto\n");
            entry.userreqtype = USER_REQ_SCH_ACCESS_AUTO; 
      
        } else {
	    entry.userreqtype = atoi(argv[1]);
        }
        if (entry.userreqtype == USER_REQ_SCH_ACCESS_AUTO_UNCONDITIONAL) {
            if (arg < 5) {
                printf("channel needed for unconditional access\n");
                return 0;
            } else {
                entry.channel = atoi(argv[4]);
            }
        }	
#endif
       
#if 0
        entry.schaccess  = atoi(argv[2]);
        entry.schextaccess = atoi(argv[3]);
        if (arg > 6) {
            strncpy(arg1.macaddr, argv[6], 17);
            set_args(entry.macaddr, &arg1, ADDR_MAC);
        }
#endif
#if 0
	if(entry.userreqtype == USER_REQ_SCH_ACCESS_NONE)
	    user_registerLinkConfirm(user_confirmBeforeJoin);
#endif

        //registerTAIndication(ReceiveTA_Indication);
        wsmgps.latitude == GPS_INVALID_DATA;
    	wsmgps.longitude = GPS_INVALID_DATA;

	    gpssockfd = gpsc_connect("127.0.0.1");

	printf("Invoking WAVE driver \n");

	if (invokeWAVEDevice(WAVEDEVICE_LOCAL, 0) < 0)
	{
		printf("Open Failed. Quitting\n");
		exit(-1);
	}

	printf("Registering User %d\n", entry.psid);
	if ( registerUser(pid, &entry) < 0)
	{
		printf("Register User Failed \n");
		printf("Removing user if already present  %d\n", !removeUser(pid, &entry));
		printf("USER Registered %d with PSID =%u \n", registerUser(pid, &entry), entry.psid );
	}

        if(entry.userreqtype == USER_REQ_SCH_ACCESS_NONE)
        { 
	   sem_wait(&indication_sem);	

	    ret = Get_Available_Serviceinfo(pid, &get_astinfo ,entry.psid);
	    if(get_astinfo.psid == entry.psid)
	    {	
                entry.userreqtype = USER_REQ_SCH_ACCESS_AUTO;
                ret = registerUser(pid, &entry);
	    }
            
	}


	/* catch control-c and kill signal*/
	signal(SIGINT,(void *)sig_int);
	signal(SIGTERM,(void *)sig_term);

	while(1)  {
		ret = rxWSMPacket(pid, &rxpkt);
		if (ret > 0){
			printf("R l%u, P#%llu#\n",rxpkt.data.length,count++);
			extract_data(&rxpkt.data,&sendgps1);
			if (sendto(Udp_Socket, &sendgps1, sizeof(struct rplot),0, &server_addr, sizeof(server_addr))==-1)
     			{
        			printf("ERR:\n");
         			diep("sendto()");
     		        }

//printf("Received WSMP Packet txpower= %d, rateindex=%d, len=%u, Packet No =#%llu#\n", rxpkt.chaninfo.txpower, rxpkt.chaninfo.rate,rxpkt.data.length,count++);
			
		} else {
		//	printf("Rxwsmp err %d\n",ret);
			blank++;
		}

	}

}

int user_confirmBeforeJoin(struct wmeNotif_Indication *received)
{
        memcpy(&notif_done,received,sizeof(struct wmeNotif_Indication));
	sem_post(&indication_sem);
	return 1; 
}

int ReceiveTA_Indication(struct WmeTAIndication *received)
{
	uint64_t tsftimer;
        memcpy(&ta_receive,received,sizeof(struct WmeTAIndication));
	memcpy(&tsftimer,ta_receive.ta_info.timevalue,sizeof(uint64_t));
	tsftimer = (tsftimer + 2000000)/1000; // Received timevalue is in nano seconds so we need to convert to micro seconds,and 2 milliseconds is added to 
					      // tsftimer so sync with transmission.  					
	printf("Received tsfTimer = %llu \n",tsftimer);
	setTsfTimer(tsftimer); 
        return 1;
}
void sig_int(void)
{
	int ret;
	int sem_val;

	removeUser(pid, &entry);
	ret=sem_getvalue(&indication_sem,&sem_val);
	if(ret<=0)
	    sem_post(&indication_sem);
	sem_destroy(&indication_sem);
        gpsc_close_sock(gpssockfd);
	close(Udp_Socket);
	signal(SIGINT,SIG_DFL);
	printf("\n\nPackets received = %llu\n", count); 
	printf("Blank Poll = %llu\n", blank); 
	printf("remoterx killed by kill signal\n");
	exit(0);

}

void sig_term(void)
{
	removeUser(pid, &entry);
	signal(SIGINT,SIG_DFL);
	printf("\n\nPackets received = %llu\n", count); 
	printf("Blank Poll = %llu\n", blank); 
	printf("remoterx killed by kill signal\n");
	exit(0);
}

int extract_macaddr(u_int8_t *mac, char *str)
{
    int maclen = IEEE80211_ADDR_LEN;
    int len = strlen(str);
    int i = 0, j = 0, octet = 0, digits = 0, ld = 0, rd = 0;
    char num[2];
    u_int8_t tempmac[maclen];
    memset(tempmac, 0, maclen);
    memset(mac, 0, maclen);
    if( (len < (2 * maclen - 1)) || (len > (3 * maclen - 1)) )
        return -1;
     while(i < len)
    {
        j = i;
        while( str[i] != ':' && (i < len) ){
         i++;
        }
        if(i > len) exit(0);
        digits = i - j;
        if( (digits > 2) ||  (digits < 1) || (octet >= maclen)){
            return -1;
        }
         num[1] = tolower(str[i - 1]);
        num[0] = (digits == 2)?tolower(str[i - 2]) : '0';
        if ( isxdigit(num[0]) && isxdigit(num[1]) ) {
            ld  =  (isalpha(num[0]))? 10 + num[0] - 'a' : num[0] - '0';
            rd  =  (isalpha(num[1]))? 10 + num[1] - 'a' : num[1] - '0';
            tempmac[octet++] =  ld * 16 + rd ;
        } else {
            return -1;
        }
        i++;
    }
      if(octet > maclen)
        return -1;
    memcpy(mac, tempmac, maclen);
    return 0;
}



void set_args( void *data ,void *argname, int datatype )
{   
    u_int8_t string[1000];
    struct arguments *argument1;
    argument1 = ( struct arguments *)argname;
    switch(datatype) {
        case ADDR_MAC:
            memcpy(string, argument1->macaddr, 17);
            string[17] = '\0';
            if(extract_macaddr( data, string) < 0 )
            {
                printf("invalid address\n");
            }
            break;
        case UINT8:
            memcpy( data, (char *)argname, sizeof( u_int8_t));
            break;
    }
}


