#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>

#define BUFF_SIZE   5		/* total number of slots */
#define NP          3		/* total number of producers */
#define NC          3		/* total number of consumers */
#define NITERS      4		/* number of items produced/consumed */

typedef struct {
    int buf[BUFF_SIZE];   /* shared var */
    int in;         	  /* buf[in%BUFF_SIZE] is the first empty slot  */ 
    int out;        	  /* buf[out%BUFF_SIZE] is the first full slot */
	int filled;
    sem_t mutex;    	  /* enforce mutual exclusion to shared data */
} sbuf_t;

sbuf_t shared;

void *Producer(void *arg)
{
    int i, item, index;

    index = (int)arg;

    for (i=0; i < NITERS; i++) {

        /* Produce item */
        item = i;	

        /* Prepare to write item to buf */

        /* If there are no empty slots, wait */
//        sem_wait(&shared.empty);
wait_again:
        /* If another thread uses the buffer, wait */
        sem_wait(&shared.mutex);
		if(shared.filled <  BUFF_SIZE )
		{
        shared.buf[shared.in] = item;
        printf("[P%d]:%d Producing %d  in %d...\n" ,index,i, item,shared.in); fflush(stdout);
		shared.in = (shared.in+1)%BUFF_SIZE;
		shared.filled++;
        sem_post(&shared.mutex);
        if (i % 2 == 1) sleep(1);
		}
		else 
		{
			printf("[P%d] not adding buf full %d  in %d...\n", index, item,(shared.in+1 %BUFF_SIZE)); fflush(stdout);
        	sem_post(&shared.mutex);
        if (i % 2 == 1) sleep(1);
		    goto wait_again;	
		}
        /* Release the buffer */
        /* Increment the number of full slots */
       // sem_post(&shared.full);

        /* Interleave  producer and consumer execution */
    }
    return NULL;
}

void *Consumer(void *arg)
{
    int i, item, index;
    index = (int)arg;

    for (i=0; i < NITERS; i++) {

        /* Produce item */
//        item = i;

        /* Prepare to write item to buf */

        /* If there are no filled slots, wait */
      //  sem_wait(&shared.full);
        /* If another thread uses the buffer, wait */
wait_again:
        sem_wait(&shared.mutex);
        if(shared.filled != 0)
        {
        item = shared.buf[shared.out];
		shared.filled--;
        printf("[C%d]:%d Consuming %d from %d...\n" ,index,i, item,shared.out); fflush(stdout);
		shared.out = (shared.out+1)%BUFF_SIZE;
        /* Release the buffer */
        sem_post(&shared.mutex);
        if (i % 2 == 1) sleep(1);
		}
		else
		{
        printf("[C%d] Not removing from  %d ...\n", index, shared.out); fflush(stdout);
        sem_post(&shared.mutex);
        if (i % 2 == 1) sleep(1);
		goto wait_again;
		}
        /* Increment the number of empty slots */
        //sem_post(&shared.empty);

        /* Interleave  producer and consumer execution */
    }

    /* Fill in the code here */
}

int main()
{
    pthread_t idP, idC;
    int index;
	shared.in = 0 ; //in= where produce has put  the last item
	shared.out = 0; //out = where consumer will remove the first item
	shared.filled = 0;

    sem_init(&shared.mutex, 0, 1);

    /* Insert code here to initialize mutex*/

    for (index = 0; index < NP; index++)
    {  
       /* Create a new producer */
        printf("creating producer...%d\n",index); fflush(stdout);
       pthread_create(&idP, NULL, Producer, (void*)index);
    }

    /* Insert code here to create NC consumers */
    for (index = 0; index < NC; index++)
    {  
       /* Create a new producer */
        printf("creating consumer...%d\n", index); fflush(stdout);
       pthread_create(&idC, NULL, Consumer, (void*)index);
    }
	while(1);	
}
