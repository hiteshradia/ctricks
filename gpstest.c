#include <stdio.h>   /* Standard input/output definitions */
#include <stdlib.h>   /* Standard input/output definitions */
#include <stdint.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <syslog.h>  /* String function definitions */
#include <signal.h>  /* String function definitions */
#include <sys/time.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <sys/ioctl.h>
#define TMPBUFSZ 1024
int fd;  // File descriptor
fd_set set;
struct termios options,options_old;
struct timeval tv;
void sig_int(void)
{
    close(fd);
    exit(0);
}
//*************************Opening Port***********************
int open_port(char * device)
{
    int fd;   /* File descriptor for the port */

    fd = open(device, O_RDWR | O_NOCTTY);// | O_NONBLOCK );  //check if NDELAY is needed
    if (fd == -1)
    {
        /* Could not open the port. */
        perror("open_port: Unable to open /dev/ttyUSB0 - ");
    }

    //get current settings and save in other variable
    tcgetattr(fd, &options_old);
    (void)memcpy(&options,&options_old, sizeof(options));
    /*
     * Only block until we get at least one character, whatever the
     * third arg of read(2) says.
     */
    /*make raw*/
    memset(options.c_cc, 0, sizeof(options.c_cc));
    options.c_cc[VMIN] = 1;
    options.c_cflag &= ~(PARENB | PARODD | CRTSCTS);
    options.c_iflag &= ~(PARMRK | INPCK);       //no parity
    options.c_cflag &= ~(CSIZE | CSTOPB | PARENB | PARODD);
    options.c_cflag |= ( CREAD | CLOCAL | CS8 ); /* Select 8 data bits */
    options.c_iflag = options.c_oflag = options.c_lflag = (tcflag_t) 0;
    options.c_lflag |= ICANON;
        if ( tcsetattr( fd, TCSANOW, &options ) == -1 )
        (void)syslog(LOG_INFO, " Error with tcsetattr \n");
    else
        (void)syslog(LOG_INFO, "canonical\n");
    printf ( "In Open port fd = %i\n", fd);
    return (fd);

}

int main(int argc,char *argv[])
{
    unsigned char tmpbuf[TMPBUFSZ] = { 0 };    //init baudrate buffer
    uint8_t rv;
    int rdret,i;
    int offset=0;
    uint64_t tod64;
    signal(SIGINT,(void *)sig_int);

    fd = open_port(argv[1]);//| O_NONBLOCK );  //check if NDELAY is needed
    if (fd == -1)
    {
        /* Could not open the port. */
        perror("open_port: Unable to open device ");
    }
    FD_ZERO(&set);
    FD_SET(fd, &set);

    (void)tcflush(fd, TCIOFLUSH);

    while(1)
    {
        rv = select(fd + 1, &set, NULL, NULL, NULL);
        
        rdret = read(fd,tmpbuf+offset,TMPBUFSZ);
        if(rdret < 0)
        {
            printf("read error\n");
        }
      //  offset+=rdret;
        //for(i=0; i < offset; i++)
          //  {
            //    if(tmpbuf[i] == '\n')
              //  {
        gettimeofday(&tv,NULL);
        tod64= (int)tv.tv_usec/1000;
        tod64 += ((uint64_t)tv.tv_sec * 1000);

        printf("tod,%llu,%s",tod64,tmpbuf);
        memset(tmpbuf,0,TMPBUFSZ);
           // offset =0;        
             //   }
           // }


    }


    close( fd );
    return(0);

}
