/*
set ADT for binary trees
*/

#include "set.h"
#include "util.h"
#include <stdlib.h>
#include <stdio.h>

/******************************/


struct btree_node {
    int val;
    struct btree_node* left;
    struct btree_node* right;
}; 

/* typedef struct set set_t is defined in set.h */
struct set {
struct btree_node* root;
int no_of_nodes;
};


/********************btree functions *******************/
/*iterate and free the tree*/

void
chop_off_the_tree(struct btree_node* tree)
{

    if( tree != NULL )
    {
        chop_off_the_tree(tree->left);
        chop_off_the_tree(tree->right);
        free(tree);
    }
    return;

}


/*  allocate a new node and insert a value */
struct btree_node* btree_newnode(int val) 
{
    struct btree_node* node = safe_malloc(sizeof(struct btree_node));
    node->val = val;
    node->left = NULL;
    node->right = NULL;

    return(node);
}
 

/* insert into our binary tree*/
struct btree_node* btree_insert(struct btree_node* node, int val) 
{
    /* tree is empty, return a new  node*/
    if (node == NULL) {
        return(btree_newnode(val));
    }
    else {
        /*insert as per binary tree rules*/
        if (val <  node->val) 
            node->left = btree_insert(node->left,val);
        else if (val > node->val)
            node->right = btree_insert(node->right, val);
        /*return the root of the tree/subtree only*/
        return(node); 
    }
} 

struct btree_node* btree_find(struct btree_node* node, int val)
{
    if(node == NULL)
        return NULL;
    if(val < node->val)
        return btree_find(node->left ,val);
    else
    if(val > node->val)
        return btree_find( node->right ,val);
    else
        return node;
}



struct btree_node* btree_findmin(struct btree_node* node)
{
    if( node == NULL )
        return NULL;
    else
    if( node->left == NULL )
        return node;
    else
        return btree_findmin(node->left);
}


struct btree_node* btree_delete(struct btree_node* node, int val)
{
    struct btree_node* tmpnode;
    if(node == NULL)
        return NULL;
    else
    if(val < node->val)  /* Go left */
        node->left = btree_delete(node->left, val);
    else
    if(val > node->val)  /* Go right */
        node->right = btree_delete(node->right, val);
    else  /* Found value to be deleted */
    if( node->left && node->right)  /* Two children */
    {
        /* Replace with smallest in right subtree */
        tmpnode = btree_findmin(node->right);
        node->val = tmpnode->val;
        node->right = btree_delete(node->right, node->val);
    }
    else  /* One or zero children */
    {
        tmpnode = node;
        if(node->left == NULL ) /* Also handles 0 children */
            node = node->right;
        else if( node->right == NULL )
            node = node->left;
        free(tmpnode);
    }
     return node;
}


void print_btree(struct btree_node* node)
{
    if(node == NULL)
        return;
    else{
        printf("%d -> ",node->val);
        print_btree(node->left);
        print_btree(node->right);
    }
    return;
}
/*********************************************************/



set_t *set_create(void)
{
    set_t *new_set;
    new_set = safe_malloc(sizeof(set_t));
    new_set->root = NULL;
    new_set->no_of_nodes = 0;
    return new_set;
}

void set_destroy(set_t *set)
{
    chop_off_the_tree(set->root);
    free(set);
	return;	
}

int set_insert(set_t *set, int new_val)
{
	if(btree_find(set->root,new_val) == NULL){
        btree_insert(set->root,new_val);
        return 1; /*added*/
    }
    else        
	    return 0; /*repeat*/
}

int set_delete(set_t *set, int del_val)
{
    if(btree_find(set->root,del_val) == NULL)
        return 0; /*missing */
    else
    {
        btree_delete(set->root,del_val);
        return 1;   
    }
}


int set_search(set_t *set, int search_val)
{
    if(btree_find(set->root,search_val) == NULL)
        return 0; /*absent*/
    else
        return 1; /*present*/
}

void set_print(set_t *set)
{
    print_btree(set->root);
}
