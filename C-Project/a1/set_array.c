/*
set ADT for arrays
*/

#include "set.h"
#include "util.h"
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#define NO_OF_BITS_IN_INT ((unsigned)(sizeof(int) * 8))

/******************************/

/* typedef struct set set_t is defined in set.h */
struct set {
    int * array;
    unsigned int array_size;
    unsigned int no_of_elements;
};

set_t *set_create(void)
{
    set_t *new_set;
    new_set = safe_malloc(sizeof(set_t));
    new_set->array_size =  ((UINT_MAX/NO_OF_BITS_IN_INT +1) * sizeof(int));
    new_set->no_of_elements =  0;
    new_set->array = safe_malloc(new_set->array_size);
    return new_set;
}

void set_destroy(set_t *set)
{
    free(set->array);
    free(set);
    return;
}

int set_insert(set_t *set, int new_val)
{
    unsigned int u_new_val = (unsigned int) new_val; 
    unsigned int i = u_new_val / NO_OF_BITS_IN_INT;
    unsigned int pos = u_new_val % NO_OF_BITS_IN_INT;
    /*
    printf("i%u pos%u arr_size=%u u_new_val=%u new_val=%d umax=%u\n",i,pos,set->array_size,u_new_val,new_val,UINT_MAX);
    */
    unsigned int flag = 1;  

    flag = flag << pos;    
    if(set->array[i] & flag)
        return 0;  /*repeat*/

    set->array[i] |= flag;      /* Set the bit corresponding to the integer in the bit array*/
    set->no_of_elements++;
	return 1; /*added*/
}

int set_delete(set_t *set, int del_val)
{

    unsigned int u_del_val = (unsigned int) del_val;
    unsigned int i = u_del_val / NO_OF_BITS_IN_INT;
    unsigned int pos = u_del_val % NO_OF_BITS_IN_INT;

    unsigned int flag = 1;

    flag = flag << pos;
    if(set->array[i] & flag)
    {
        set->array[i] &= ~flag;  
        set->no_of_elements--;
        return 1; /*exists and deleted*/
    }
    else
	    return 0; /*missing	*/
}

int set_search(set_t *set, int search_val)
{

    unsigned int u_search_val = (unsigned int) search_val;
    unsigned int i = u_search_val / NO_OF_BITS_IN_INT;
    unsigned int pos = u_search_val % NO_OF_BITS_IN_INT;

    unsigned int flag = 1;

    flag = flag << pos;
    if(set->array[i] & flag)
        return 1; /*present*/
    else
        return 0; /*absent*/

}

void set_print(set_t *set)
{
    unsigned int i;
    unsigned int pos;
    unsigned int flag = 1;
    unsigned int count = 0;

    for(i = 0 ;i < set->array_size ;i++) {
        for(pos=0 ;pos < NO_OF_BITS_IN_INT;pos++){
            flag = 1 << pos;
            if(set->array[i] & flag){
                printf("%d",(int) ((i* NO_OF_BITS_IN_INT) + pos));
                count++;
            }
            if(count == set->no_of_elements)
                printf("->");
            
        }
        if(count == set->no_of_elements){
            printf("\n");
            break;
        }
    }
}

