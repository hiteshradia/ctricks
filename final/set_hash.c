/*
set ADT for hash tables with separate chaining
*/

#include "set.h"
#include "util.h"
#include <stdlib.h>
#include <stdio.h>

#ifndef HASH_TBL_SZ
#define HASH_TBL_SZ 100
#endif

/******************************/

struct hash_node {
    int val;
    struct hash_node *next;
};
typedef struct hash_node* listhead; 

struct hash_table{
    int tblsize;
    struct hash_node **lists;
};


/* typedef struct set set_t is defined in set.h */
struct set {
    struct hash_table *table;   
};

/***********hash table related functions ****/

int next_prime( int N )
{
    int i;
     if( N % 2 == 0 )
        N++;
    for( ; ; N += 2 )
    {
        for( i = 3; i * i <= N; i += 2 )
            if( N % i == 0 )
                goto ContOuter; 
        return N;
        ContOuter: ;
    }
}

struct hash_table* init_hash_table(int table_size)
{
    struct hash_table* new_table =NULL;
    int i;
    new_table = safe_malloc(sizeof(struct hash_table));
    new_table->tblsize = next_prime(table_size);
    new_table->lists = safe_malloc(sizeof(listhead) * new_table->tblsize);
    for(i=0;i < new_table->tblsize ; i++){ /*init list heads*/
        new_table->lists[i] = safe_malloc(sizeof(struct hash_node));
        new_table->lists[i]->next = NULL;
    }
    return new_table;
}

void destroy_hash_table(struct hash_table *table)
{
    int i;

    for( i = 0; i < table->tblsize; i++ )
    {
        listhead P = table->lists[ i ];
        listhead temp;
         while( P != NULL )
        {
            temp = P->next;
            free(P);
            P = temp;
        }
    }
    free(table->lists );
    free(table);
}


/* Hash function */

unsigned int hash_func( unsigned int val, int size )
{
    return (unsigned)(val % size);
}


listhead find_list(struct hash_table* table, int val)
{
    listhead P;
    listhead L;
     L = table->lists[hash_func(val, table->tblsize)];
    P = L->next;
    while( P != NULL && P->val != val )
        P = P->next;
    return P;
}


int delete_from_hash_table(struct hash_table* table, int val)
{
    listhead P;
    listhead L;
    listhead tmp;
    L = table->lists[hash_func(val, table->tblsize)];
    P = L->next;
    tmp = L;
    while( P != NULL)
    {
        if(P->val == val){
            tmp->next = P->next;
            free(P);
            return 1; /*found and deleted*/
        }
        tmp=P;
        P = P->next;
    }
    return 0; /*missing*/

} 

int insert_in_hash_table(struct hash_table* table,int val)
{

    listhead pos, new_node;
    listhead L;
    pos = find_list(table, val);
    if( pos == NULL )   /* list is not found */
    {
        new_node = safe_malloc( sizeof( struct hash_node ) );
        L = table->lists[ hash_func(val,table->tblsize) ];
        new_node->next = L->next;
        new_node->val = val;
        L->next = new_node;
        return 1; /*added*/
    }
    else
        return 0; /*already present*/
}
/***********************************/


set_t *set_create(void)
{
    set_t *new_set;
    new_set = safe_malloc(sizeof(set_t));
    new_set->table = init_hash_table(HASH_TBL_SZ);
    return new_set;
}

void set_destroy(set_t *set)
{
    destroy_hash_table(set->table);
    free(set);
	return;	
}

int set_insert(set_t *set, int new_val)
{
    
	return insert_in_hash_table(set->table,new_val);
}

int set_delete(set_t *set, int del_val)
{
	return delete_from_hash_table(set->table,del_val);	
}

int set_search(set_t *set, int search_val)
{

    if(find_list(set->table,search_val) == NULL)
	    return 0;	/*absent*/
    else
        return 1;/*present*/
}

void set_print(set_t *set)
{
	/* optional, may help with testing */
    printf("%p",set);
}

