#include "bootstrap.h"
uint8_t digest[DIGESTLEN];
uint8_t signeddataparse(uint8_t *  signeddata,uint8_t contenttype)
{
    int parseoffset =0;
    uint8_t signeridtype;
    signeridtype = *signeddata
                   parseoffset++;

    if(signeridtype == certificate_digest_with_ecdsap224 || signeridtype == certificate_digest_with_ecdsap256)
    {
        memcpy(digest,signeddata+parseoffset,DIGESTLEN);
        parseoffset += DIGESTLEN;
    }
    if(signeridtype == certificate)
        certparse(signeddata+parseoffset);

}

uint32_t certParse(uint8_t *cert,Certificate * parsedcert)
{
    int certparseoffset =0;
    int ll =0;
    parsedcert->version_and_type =cert[0];
    parsedcert->subject_type =cert[1];
    parsedcert->cf =cert[2];
    certparseoffset +=3;
    if(parsedcert.subject_type ! = root_ca)
    {
        memcpy(parsedcert->signer_id,cert+certparseoffset,DIGESTLEN);
        certparseoffset += DIGESTLEN;
        parsedcert->signature_alg = cert[certparseoffset];
        certparseoffset +=1;
    }
//certspecificdata parsing
    if(parsedcert->subject_type != crl_signer) {
        parsedcert->scope.namelen = decode_length((cert + certparseoffset), &ll);
        certparseoffset += ll;
        parsedcert->scope.name =(uint8_t *)malloc(parsedcert->scope.namelen); //FREELATER
        memcpy(parsedcert->scope.name,(cert + certparseoffset),parsedcert->scope.namelen);
        certparseoffset += parsedcert->scope.namelen;
    }

    switch(parsedcert->subject_type)
    {

    case root_ca:
        ll=0;
        parsedcert->scope.permitted_subject_type =(uint16_t)decode_length((cert + certparseoffset), &ll);
        certparseoffset +=ll;
        ll=0;
        if(parsedcert->scope.permitted_subject_type &0x6f) //parse properly later
        {
            parsedcert->scope.psid_array.type =*(uint8_t *)(cert + certparseoffset);
            parsedcert->scope.psid_array.premissionslen= decode_length((cert + certparseoffset), &ll);
            certparseoffset +=ll;
            parsedcert->scope.psid_array.premissions_list =malloc(parsedcert->scope.psid_array.premissionslen);
            memcpy(parsedcert->scope.psid_array.premissions_list,(cert + certparseoffset),parsedcert->scope.psid_array.premissionslen);
            certparseoffset += parsedcert->scope.namelen;
        }
        break;
    }


}
}
