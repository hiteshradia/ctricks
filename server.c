//* udpserver.c */ 

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

typedef unsigned long int uint32;
typedef unsigned short int uint16;
typedef unsigned char uint8;
typedef signed  char int8;

struct edcaparam {
    u_int8_t aifsn;
    u_int8_t logcwmin;
    u_int8_t logcwmax;
    u_int16_t txopLimit;
    u_int8_t acm;
} ;

struct GenericDataLong{
    u_int16_t       length;
    char            contents[1300];
} __attribute__((__packed__));

typedef struct GenericDataLong  WSMData;

struct channelinfo {
        u_int8_t reg_class;
        u_int8_t channel;
        u_int8_t adaptable;
        u_int8_t rate;
        int8_t txpower;
        struct edcaparam edca[4];
        u_int8_t channelaccess;
}  __attribute__((__packed__));

struct wsm_indication {
    struct channelinfo chaninfo;
    u_int32_t psid;
    u_int8_t version;
    u_int8_t txpriority;
    u_int8_t wsmps;
    u_int8_t security;
    WSMData data;
    u_int8_t macaddr[6];
    u_int8_t rssi;
} __attribute__((__packed__));

typedef struct wsm_indication WSMIndication;

 WSMIndication Ind;

extern const struct in6_addr in6addr_any;



int main(int argc , char *argv[] )
{
        int sock,len;
        int addr_len, bytes_read;
        uint16_t listen_port;
	unsigned int count=0;        
	struct sockaddr_in6 server_addr , client_addr;
    
    if(argc < 2){
        printf("Please provide port to listen on\n");
        exit(0);
    }
    listen_port = atoi(argv[1]);
        if ((sock = socket(AF_INET6, SOCK_DGRAM, 0)) == -1) {
            perror("Socket");
            exit(1);
        }

        
	server_addr.sin6_family = AF_INET6;
	server_addr.sin6_addr= in6addr_any;        
	server_addr.sin6_port = htons(listen_port);

        if (bind(sock,(struct sockaddr *)&server_addr,
            sizeof(struct sockaddr_in6)) == -1)
        {
            perror("Bind");
            exit(1);
        }

        addr_len = sizeof(struct sockaddr_in6);
		
	printf("\nWSMPForward-Server Waiting for client on port %d",listen_port);
        fflush(stdout);

	while (1)
	{

	len=sizeof(Ind);          
	bytes_read = recvfrom(sock,&Ind,len,0,
	                    (struct sockaddr *)&client_addr, &addr_len);
	   

	  //recv_data[bytes_read] = '\0';

      printf("Contents  %x\tLength is %u\n", Ind.data.contents,Ind.data.length);
	  //printf("Psid is %lu\t Version is %x\n",Ind.psid,Ind.version);
	  printf("Rxpackets Packet no is %u\n",++count);  
	  fflush(stdout);

        }
        return 0;
}

