/*
* Application  A sample application
*
* Version:     SampleUSB.c    1.0.0  1391/11/09
*
* Company:     Jahad-Daneshgahi-Sharif
*
* Authors:     Mohammad Elahi, <elahi.mohamad@gmail.com>
*
* Fixes:
*
* Describtion:
*      This application Send the data to USB
*
*/
	

#include <stdio.h>
#include <libusb-1.0/libusb.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/timeb.h>

#define IN 0x81
#define OUT 0x2



#define VID 0x0fce
#define PID 0x5169

#define ACCESSORY_VID 0x18d1
#define ACCESSORY_PID 0x2d01
#define ACCESSORY_PID_ALT 0x2D00

#define LEN 2

static int mainPhase();
static int init(void);
static int deInit(void);
static void error(int code);
static void status(int code);
static int setupAccessory(
	const char* manufacturer,
	const char* modelName,
	const char* description,
	const char* version,
	const char* uri,
	const char* serialNumber);

//static
static struct libusb_device_handle* handle;
static char stop;
static char success = 0;

int main (int argc, char *argv[])
{

	if(init() < 0)
		return;

	if(setupAccessory(
		"Nexus-Computing GmbH",
		"osciprime",
		"Description",
		"antilope",
		"http://neuxs-computing.ch",
		"2254711SerialNo.") < 0)
	{
		fprintf(stdout, "Error setting up accessory\n");
		deInit();
		return -1;
	}

	if(mainPhase() < 0)
	{
		fprintf(stdout, "Error during main phase\n");
		deInit();
		return -1;
	}	

	deInit();
	fprintf(stdout, "Done, no errors\n");
	return 0;
}

static int mainPhase(){
	unsigned char buffer[500000];
	int response = 0;
	static int transferred;



	static char data[] = {'4'}; 
	char    buffer2[16384];
	
	int i=0;
	for( ;  ;i=0 )
	{
		printf("\nType a String: ");
		
		while((buffer2[i++] = getchar()) != 0x0a && i < 16384);
		buffer2[i]='\0';

		response = libusb_bulk_transfer(handle,OUT, buffer2, i, &transferred,0);	
		fprintf(stdout, "%d %s\n",response, buffer2);

		if(response < 0)
		{
			error(response);
			return -1;
		}

	}		

}



static int init(){

	libusb_context *ctx = NULL;
	int r;
	libusb_device **devs;
	ssize_t cnt; //holding number of devices in list
	struct libusb_device_descriptor desc;
	r = libusb_init(NULL); //initialize the library for the session we just declared

	if(r < 0) {
		printf("Init USB Error \n"); 
		return -1;
	}

	printf("number of usb devices = %d \n", cnt);

	cnt = libusb_get_device_list(ctx, &devs); 
	if(cnt < 0) {
		printf("Get Device Error\n");
		return -1;
	}

	printf("number of usb devices = %d \n", cnt);

	int i=0;
	for(i=0; i<cnt; i++)
	{
		r = libusb_get_device_descriptor(devs[i], &desc);
		if (r < 0)
		{
			printf("failed to get device descriptor\n");
			return -1;
		}
		else
		{
			printf("VendorID: %d \n",desc.idVendor);
			printf("ProductID: %d \n",desc.idProduct);

		}
	}

	r = libusb_get_device_descriptor(devs[1], &desc);
	printf("VendorID: %d \n",desc.idVendor);
	printf("ProductID: %d \n",desc.idProduct);

	sleep(2);

	if((handle = libusb_open_device_with_vid_pid(NULL, desc.idVendor,desc.idProduct)) == NULL){
			fprintf(stdout, "acquireing handle1 \n");
		if((handle = libusb_open_device_with_vid_pid(NULL, ACCESSORY_VID, ACCESSORY_PID)) == NULL){
			fprintf(stdout, "Problem acquireing handle\n");
			return -1;
		}
	}

	printf("claim interface \n");
	libusb_claim_interface(handle, 0);

	return 0;
}

static int deInit()
{
	if(handle != NULL)
		libusb_release_interface (handle, 0);
	libusb_close(handle);
	libusb_exit(NULL);
	return 0;
}

static int setupAccessory(
	const char* manufacturer,
	const char* modelName,
	const char* description,
	const char* version,
	const char* uri,
	const char* serialNumber)
{

	unsigned char ioBuffer[2];
	int devVersion;
	int response;
	int tries = 5;

	response = libusb_control_transfer(
		handle, //handle
		0xC0, //bmRequestType
		51, //bRequest
		0, //wValue
		0, //wIndex
		ioBuffer, //data
		2, //wLength
        	0 //timeout
	);

	if(response < 0){error(response);return-1;}

	devVersion = ioBuffer[1] << 8 | ioBuffer[0];
	fprintf(stdout,"Verion Code Device: %d\n", devVersion);
	
	usleep(1000);//sometimes hangs on the next transfer :(

	response = libusb_control_transfer(handle,0x40,52,0,0,(char*)manufacturer,strlen(manufacturer),0);
	if(response < 0){error(response);return -1;}
	response = libusb_control_transfer(handle,0x40,52,0,1,(char*)modelName,strlen(modelName)+1,0);
	if(response < 0){error(response);return -1;}
	response = libusb_control_transfer(handle,0x40,52,0,2,(char*)description,strlen(description)+1,0);
	if(response < 0){error(response);return -1;}
	response = libusb_control_transfer(handle,0x40,52,0,3,(char*)version,strlen(version)+1,0);
	if(response < 0){error(response);return -1;}
	response = libusb_control_transfer(handle,0x40,52,0,4,(char*)uri,strlen(uri)+1,0);
	if(response < 0){error(response);return -1;}
	response = libusb_control_transfer(handle,0x40,52,0,5,(char*)serialNumber,strlen(serialNumber)+1,0);
	if(response < 0){error(response);return -1;}

	fprintf(stdout,"Accessory Identification sent\n", devVersion);

	response = libusb_control_transfer(handle,0x40,53,0,0,NULL,0,0);
	if(response < 0){error(response);return -1;}

	fprintf(stdout,"Attempted to put device into accessory mode\n", devVersion);

	if(handle != NULL)
	{
		//libusb_close(handle);		
		libusb_release_interface (handle, 0);
		//libusb_close(handle);
	}

	libusb_close(handle);
	for(;;){//attempt to connect to new PID, if that doesn't work try ACCESSORY_PID_ALT
		tries--;
		
		if((handle = libusb_open_device_with_vid_pid(NULL, ACCESSORY_VID, ACCESSORY_PID_ALT)) == NULL){
			printf("\ntries : %d\n",tries);
			if(tries < 0){
				return -1;
			}
		}else{
			break;
		}
		
		sleep(1);
	}
	libusb_claim_interface(handle, 0);
	fprintf(stdout, "Interface claimed, ready to transfer data\n");
	return 0;
}

static void error(int code){
	fprintf(stdout,"\n");
	switch(code){
	case LIBUSB_ERROR_IO:
		fprintf(stdout,"Error: LIBUSB_ERROR_IO\nInput/output error.\n");
		break;
	case LIBUSB_ERROR_INVALID_PARAM:
		fprintf(stdout,"Error: LIBUSB_ERROR_INVALID_PARAM\nInvalid parameter.\n");
		break;
	case LIBUSB_ERROR_ACCESS:
		fprintf(stdout,"Error: LIBUSB_ERROR_ACCESS\nAccess denied (insufficient permissions).\n");
		break;
	case LIBUSB_ERROR_NO_DEVICE:
		fprintf(stdout,"Error: LIBUSB_ERROR_NO_DEVICE\nNo such device (it may have been disconnected).\n");
		break;
	case LIBUSB_ERROR_NOT_FOUND:
		fprintf(stdout,"Error: LIBUSB_ERROR_NOT_FOUND\nEntity not found.\n");
		break;
	case LIBUSB_ERROR_BUSY:
		fprintf(stdout,"Error: LIBUSB_ERROR_BUSY\nResource busy.\n");
		break;
	case LIBUSB_ERROR_TIMEOUT:
		fprintf(stdout,"Error: LIBUSB_ERROR_TIMEOUT\nOperation timed out.\n");
		break;
	case LIBUSB_ERROR_OVERFLOW:
		fprintf(stdout,"Error: LIBUSB_ERROR_OVERFLOW\nOverflow.\n");
		break;
	case LIBUSB_ERROR_PIPE:
		fprintf(stdout,"Error: LIBUSB_ERROR_PIPE\nPipe error.\n");
		break;
	case LIBUSB_ERROR_INTERRUPTED:
		fprintf(stdout,"Error:LIBUSB_ERROR_INTERRUPTED\nSystem call interrupted (perhaps due to signal).\n");
		break;
	case LIBUSB_ERROR_NO_MEM:
		fprintf(stdout,"Error: LIBUSB_ERROR_NO_MEM\nInsufficient memory.\n");
		break;
	case LIBUSB_ERROR_NOT_SUPPORTED:
		fprintf(stdout,"Error: LIBUSB_ERROR_NOT_SUPPORTED\nOperation not supported or unimplemented on this platform.\n");
		break;
	case LIBUSB_ERROR_OTHER:
		fprintf(stdout,"Error: LIBUSB_ERROR_OTHER\nOther error.\n");
		break;
	default:
		fprintf(stdout, "Error: unkown error\n");
	}
}

static void status(int code){
	fprintf(stdout,"\n");
	switch(code){
		case LIBUSB_TRANSFER_COMPLETED:
			fprintf(stdout,"Success: LIBUSB_TRANSFER_COMPLETED\nTransfer completed.\n");
			break;
		case LIBUSB_TRANSFER_ERROR:
			fprintf(stdout,"Error: LIBUSB_TRANSFER_ERROR\nTransfer failed.\n");
			break;
		case LIBUSB_TRANSFER_TIMED_OUT:
			fprintf(stdout,"Error: LIBUSB_TRANSFER_TIMED_OUT\nTransfer timed out.\n");
			break;
		case LIBUSB_TRANSFER_CANCELLED:
			fprintf(stdout,"Error: LIBUSB_TRANSFER_CANCELLED\nTransfer was cancelled.\n");
			break;
		case LIBUSB_TRANSFER_STALL:
			fprintf(stdout,"Error: LIBUSB_TRANSFER_STALL\nFor bulk/interrupt endpoints: halt condition detected (endpoint stalled).\nFor control endpoints: control request not supported.\n");
			break;
		case LIBUSB_TRANSFER_NO_DEVICE:
			fprintf(stdout,"Error: LIBUSB_TRANSFER_NO_DEVICE\nDevice was disconnected.\n");
			break;
		case LIBUSB_TRANSFER_OVERFLOW:
			fprintf(stdout,"Error: LIBUSB_TRANSFER_OVERFLOW\nDevice sent more data than requested.\n");
			break;
		default:
			fprintf(stdout,"Error: unknown error\nTry again(?)\n");
			break;
	}
}
