#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <syslog.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */

#define PUT_ORIGIN      0
#define putbyte(buf,off,b) do {buf[(off)-(PUT_ORIGIN)] = (unsigned char)(b);} while (0)
#define putleword(buf, off, w) do {putbyte(buf, (off)+1, (uint)(w) >> 8); putbyte(buf, (off), (w));} while (0)
#define putlelong(buf, off, l) do {putleword(buf, (off)+2, (uint)(l) >> 16); putleword(buf, (off), (l));} while (0)

speed_t get_baud(int baudrate);
int settings();
struct termios options;
int fd;  // File descriptor

int ubx_write(unsigned int msg_class, unsigned int msg_id,
           unsigned char *msg, unsigned short data_len)
{
    unsigned char CK_A, CK_B;
    ssize_t i, count,j;
    int ok,msgbuflen=0;
    unsigned char cmd_send[100] = { 0 }; //init send buffer

    /*@ -type @*/
    cmd_send[0] = 0xb5;
    cmd_send[1] = 0x62;

    CK_A = CK_B = 0;
    cmd_send[2] = msg_class;
    cmd_send[3] = msg_id;
    cmd_send[4] = data_len & 0xff;
    cmd_send[5] = (data_len >> 8) & 0xff;

    (void)memcpy(&cmd_send[6], msg, data_len);

    /* calculate CRC */
    for (i = 2; i < 6; i++) {
    CK_A += cmd_send[i];
    CK_B += CK_A;
    }
    /*@ -nullderef @*/
    for (i = 0; i < data_len; i++) {
    CK_A += msg[i];
    CK_B += CK_A;
    }

    cmd_send[6 + data_len] = CK_A;
    cmd_send[7 + data_len] = CK_B;
    msgbuflen = data_len + 8;
    /*@ +type @*/
    printf ("ubxwrite = ");
    for (j=0; j < msgbuflen;j++)
            printf("%02x ",cmd_send[j]);
    printf ("\n");
    tcflush(fd,TCIOFLUSH);
    count = write(fd,cmd_send,msgbuflen);
    (void)tcdrain(fd);
    ok = msgbuflen;
    /*@ +nullderef @*/
    return (ok);
}



int speed_default_buf(unsigned char * buff)
{
        buff[0]  = 0x01;
    	buff[1] = 0x00;
        buff[2] = 0x00;
    	buff[3] = 0x00;
    	buff[4] = 0xc0;
    	buff[5] = 0x08;
    	buff[6] = 0x00;
   	buff[7] = 0x00;
        buff[8] = 0x80;
        buff[9] = 0x25;
        buff[10] = 0x00;
        buff[11] = 0x00;
        buff[12] = 0x07;
        buff[13] = 0x00;
        buff[14] = 0x07;
        buff[15] = 0x00;
        buff[16] = 0x00;
        buff[17] = 0x00;
        buff[18] = 0x00;
        buff[19] = 0x00;
return 20;

}

int default_updaterate_buf(unsigned char *buf_rate)
{

	buf_rate[0] = 0xE8;
	buf_rate[1] = 0x03;
	buf_rate[2] = 0x01;
	buf_rate[3] = 0x00;
	buf_rate[4] = 0x01;
	buf_rate[5] = 0x00;

return 6;
}

int default_waas_status(unsigned char *waas)
{
	
	waas[0] = 0x01;
	waas[1] = 0x03;
	waas[2] = 0x03;
	waas[3] = 0x00;
	waas[4] = 0x51;
	waas[5] = 0x62;
	waas[6] = 0x06;
	waas[7] = 0x06;

}
int main(int argc ,char *argv[])

{

       char buf[30];int res;
       char temp_buf='\0';
       char temp_buf_next='\0';
       int baudrate;
       int charindex = 0; 
       unsigned int speed,len =0;
       int ubx_res,waas; 	 		 
       unsigned char send_bytes[100] = { 0 }; //init baudrate buffer
       unsigned char rate_buf[100] = { 0 }; //init update_rate buffer
       unsigned char waas_buf[50] = { 0 }; //init waas buffer
       speed_t spdcode =0; 
       int update_rate;	 
	// Read the configuration of the port
       fd = open_port();
       tcgetattr( fd, &options );
        
	
        if (argc != 6) { //need 3 args -> dev, baud, conffile
                printf( "Please provide device, baudrate and conffile_location by passing three arguments\n");
                printf("eg. ./ubloxconf /dev/ttyS0 9600 speed update_rate waas_status\n");
                return -1;
        }
	baudrate = atoi(argv[2]);
        /* SEt Baud Rate */
        spdcode=get_baud(baudrate);
	cfsetispeed( &options, spdcode);  //Setting Initial BaudRate to 9600
        cfsetospeed( &options, spdcode);
        res = settings();

	if(argv[3]== NULL || (atoi(argv[3])==0))
	    speed = 115200;
	else
	    speed = atoi(argv[3]);
	if(argv[4]== NULL || (atoi(argv[4])==0))
	    update_rate = 1000; 
	else
	    update_rate = atoi(argv[4]);
	if(argv[5]== NULL)
	    waas = 1;
	else
	    waas = atoi(argv[5]);
	
    	if(speed)
    	{
    	    len=speed_default_buf(send_bytes);
	    putlelong(send_bytes, 8, speed);
           (void)ubx_write(0x06u, 0x00, send_bytes, len); /* get this port's settings */
	   (void)usleep(50000);
            spdcode=get_baud(speed);
            cfsetispeed(&options, spdcode);
            cfsetospeed(&options, spdcode);
            settings();
 
        }
	(void)usleep(50000);
	if(update_rate)
	{
	    ubx_res = default_updaterate_buf(rate_buf);
	    putlelong(rate_buf, 0, update_rate);
	    (void)ubx_write(0x06u, 0x08, rate_buf, ubx_res);		
	    	
	}
	(void)usleep(50000);
	if(waas==0)
	{
	    waas_ret = default_waas_status(waas_buff);	
	    putlelong(waas_buf, 0, update_rate);
	    (void)ubx_write(0x06u, 0x08, waas_buf, waas_ret);	
	}
      close( fd );
      return(0);

}

//*************************Opening Port***********************
int open_port(void)
{
      int fd; /* File descriptor for the port */

      fd = open("/dev/ttyS0", O_RDWR | O_NOCTTY);//check if NDELAY is needed
      if (fd == -1)
        {
         /* Could not open the port. */
          perror("open_port: Unable to open /dev/ttyUSB0 - ");
        }
  //   else
  //   {
  //      fcntl(fd, F_SETFL, FNDELAY);
  //      printf("opening of device /dev/ttyUSB0 succeeded !!\n");
  //   }

      printf ( "In Open port fd = %i\n", fd);
      return (fd);

}

//***********************Port Setting**********************
int settings()
{
      options.c_cflag |= ( CLOCAL | CREAD );

      // Set the Charactor size

      options.c_cflag &= ~CSIZE; /* Mask the character size bits */
      options.c_cflag |= CS8;    /* Select 8 data bits */

      // Set parity - No Parity (8N1)
      options.c_cflag &= ~PARENB;
      options.c_cflag &= ~CSTOPB;
      options.c_cflag &= ~CSIZE;
      options.c_cflag |= CS8;

      //options.c_lflag |= CRTSCTS;
      //Disable Hardware flowcontrol
      //options.c_cflag &= ~CNEW_RTSCTS;  -- not supported

      //Enable Raw Input
      options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
      //Disable Software Flow control
      options.c_iflag &= ~(IXON | IXOFF | IXANY);
      //Chose raw (not processed) output
      options.c_oflag &= ~OPOST;

            /* set input mode (non-canonical, no echo,...) */
      options.c_lflag = 0;

      options.c_cc[VTIME]    = 0;   /* inter-character timer unused */
      options.c_cc[VMIN]     = 1;   /* blocking read until 1 chars received */
      tcflush(fd,TCIOFLUSH);
      if ( tcsetattr( fd, TCSAFLUSH, &options ) == -1 )
        (void)syslog(LOG_INFO, " Error with tcsetattr \n");
     else
        (void)syslog(LOG_INFO, "tcsetattr succeed \n");
   // fcntl(fd, F_SETFL, FNDELAY);
      return (0);

}
      
                                                                                                 
speed_t get_baud(int baudrate) {
    switch (baudrate) {
    case 0:
        (void)syslog(LOG_INFO,"Baudrate set to 0\n");
        return B0;
    case 50:
        (void)syslog(LOG_INFO,"Baudrate set to 50\n");
        return B50;
    case 75:
        (void)syslog(LOG_INFO,"Baudrate set to 75\n");
        return B75;
    case 110:
        (void)syslog(LOG_INFO,"Baudrate set to 110\n");
        return B110;
    case 134:
        (void)syslog(LOG_INFO,"Baudrate set to 134\n");
        return B134;
    case 150:
        (void)syslog(LOG_INFO,"Baudrate set to 150\n");
        return B150;
    case 200:
        (void)syslog(LOG_INFO,"Baudrate set to 200\n");
        return B200;
    case 300:
        (void)syslog(LOG_INFO,"Baudrate set to 300\n");
        return B300;
    case 600:
        (void)syslog(LOG_INFO,"Baudrate set to 600\n");
        return B600;
    case 1200:
        (void)syslog(LOG_INFO,"Baudrate set to 1200\n");
        return B1200;
    case 1800:
        (void)syslog(LOG_INFO,"Baudrate set to 1800\n");
        return B1800;
    case 2400:
        (void)syslog(LOG_INFO,"Baudrate set to 2400\n");
        return B2400;
    case 4800:
        (void)syslog(LOG_INFO,"Baudrate set to 4800\n");
        return B4800;
    case 9600:
        (void)syslog(LOG_INFO,"Baudrate set to 9600\n");
        return B9600;
    case 19200:
        (void)syslog(LOG_INFO,"Baudrate set to 19200\n");
        return B19200;
    case 38400:
        (void)syslog(LOG_INFO,"Baudrate set to 38400\n");
        return B38400;
    case 57600:
        (void)syslog(LOG_INFO,"Baudrate set to 57600\n");
        return B57600;
    case 115200:
        (void)syslog(LOG_INFO,"Baudrate set to 115200\n");
        printf( "baud set  115200\n");
        return B115200;
    default:
        return B9600;
    }
}


