#include <sys/ioctl.h>
#include <net/if.h> 
#include <unistd.h>
#include <netinet/in.h>
#include <string.h>
#include <stdio.h>

int main()
{
    struct ifreq ifr;
    unsigned char mac_address[6];

    int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
    if (sock == -1) { /* handle error*/ };
    strcpy(ifr.ifr_name,"eth0");
    printf("\n it%s\n",ifr.ifr_name);
    if (ioctl(sock, SIOCGIFHWADDR, &ifr) == 0) {

        memcpy(mac_address, ifr.ifr_hwaddr.sa_data, 6);
	printf("\n %02x:%02x:%02x:%02x:%02x:%02x\n",mac_address[0],mac_address[1],mac_address[2],mac_address[3],mac_address[4],mac_address[5]);
                   // break;
    }
    else { /* handle error */ }

}
