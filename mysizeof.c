#include<stdio.h>
#define my_sizeoft(type) ((size_t)(((type *)0) + 1))
#define my_sizeof(var) (size_t)((char *)(&var+1)-(char*)(&var))

int main()
{
int a;
char b;

printf("typesize=%d varsize=%d\n",my_sizeoft(int),my_sizeof(a));
printf("typesize=%d varsize=%d\n",my_sizeoft(char),my_sizeof(b));

}


