#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
struct bitf {
uint8_t inf_request        : 1,
        ht40_intolerant      : 1,
        ht20_width_req       : 1,
        obss_exempt_req    : 1,
        obss_exempt_grant  : 1,
        reserved3          : 1,
        reserved2          : 1,
        reserved1          : 1;
}__attribute__((packed));


union checkendian {
uint8_t num;
struct bitf bitf;
}__attribute__((packed));

int main()
{
union checkendian endian1;
endian1.num=0x05;
printf("p0=%d p1=%d p2=%d p3=%d p4=%d p5=%d p6=%d p7=%d num=%d\n",endian1.bitf.inf_request,endian1.bitf.ht40_intolerant,endian1.bitf.ht20_width_req,endian1.bitf.obss_exempt_req,endian1.bitf.obss_exempt_grant,endian1.bitf.reserved3,endian1.bitf.reserved2,endian1.bitf.reserved1,endian1.num);




}
