/*
binary tree ADT
*/

#include <stdlib.h>
#include <stdio.h>

/******************************/


struct btree_node {
    int val;
    struct btree_node* left;
    struct btree_node* right;
}; 



/********************btree functions *******************/
/*iterate and free the tree*/

void
chop_off_the_tree(struct btree_node* tree)
{

    if( tree != NULL )
    {
        chop_off_the_tree(tree->left);
        chop_off_the_tree(tree->right);
        free(tree);
    }
    return;

}


/*  allocate a new node and insert a value */
struct btree_node* btree_newnode(int val) 
{
    struct btree_node* node = malloc(sizeof(struct btree_node));
    node->val = val;
    node->left = NULL;
    node->right = NULL;

    return(node);
}
 

/* insert into our binary tree*/
struct btree_node* btree_insert(struct btree_node* node, int val) 
{
    /* tree is empty, return a new  node*/
    if (node == NULL) {
        return(btree_newnode(val));
    }
    else {
        /*insert as per binary tree rules*/
        if (val <  node->val) 
            node->left = btree_insert(node->left,val);
        else if (val > node->val)
            node->right = btree_insert(node->right, val);
        /*return the root of the tree/subtree only*/
        return(node); 
    }
} 

struct btree_node* btree_find(struct btree_node* node, int val)
{
    if(node == NULL)
        return NULL;
    if(val < node->val)
        return btree_find(node->left ,val);
    else
    if(val > node->val)
        return btree_find( node->right ,val);
    else
        return node;
}



struct btree_node* btree_findmin(struct btree_node* node)
{
    if( node == NULL )
        return NULL;
    else
    if( node->left == NULL )
        return node;
    else
        return btree_findmin(node->left);
}


struct btree_node* btree_delete(struct btree_node* node, int val)
{
    struct btree_node* tmpnode;
    if(node == NULL)
        return NULL;
    else
    if(val < node->val)  /* Go left */
        node->left = btree_delete(node->left, val);
    else
    if(val > node->val)  /* Go right */
        node->right = btree_delete(node->right, val);
    else  /* Found value to be deleted */
    if( node->left && node->right)  /* Two children */
    {
        /* Replace with smallest in right subtree */
        tmpnode = btree_findmin(node->right);
        node->val = tmpnode->val;
        node->right = btree_delete(node->right, node->val);
    }
    else  /* One or zero children */
    {
        tmpnode = node;
        if(node->left == NULL ) /* Also handles 0 children */
            node = node->right;
        else if( node->right == NULL )
            node = node->left;
        free(tmpnode);
    }
     return node;
}


void print_btree(struct btree_node* node)
{
    if(node == NULL)
        return;
    else{
        print_btree(node->left);
        printf("%d -> ",node->val);
        print_btree(node->right);
    }
    return;
}

/* Compute the "height" of a tree -- the number of
    nodes along the longest path from the root node
    down to the farthest leaf node.*/
int height(struct btree_node* node)
{
    if (node==NULL)
        return 0;
    else
    {
        /* compute the height of each subtree */
        int lheight = height(node->left);

        int rheight = height(node->right);
	printf("lheight:%d  rheight: %d\n",lheight, rheight);
        /* use the larger one */
        if (lheight > rheight)
            return(lheight+1);
        else return(rheight+1);
    }
}

 
/* Print nodes at a given level */
void print_level(struct btree_node* root, int level,int height)
{
    int i;
    if (root == NULL)
        return;
    if (level == 1)
        printf("%2d", root->val);
    else if (level > 1)
    {
	//	for(i=1;i<level;i++)
	//		printf(" ");
        print_level(root->left, level-1,level);
		for(i=1;i<=height/2;i++)
			printf(" ");
        print_level(root->right, level-1,level);
    }
}
 
/* Function to print level order traversal a tree*/
void print_levelorder(struct btree_node* root)
{
    int h = height(root);
    int i,j;
    for (i=1; i<=h; i++)
	{
        for(j=i;j<h;j++)
            printf(" ");

        print_level(root, i,i);
		printf("\n");
	}	
}



/*********************************************************/
struct btree_node * root=NULL;
int main()
{
root =btree_insert(root,6);
root =btree_insert(root,2);
root =btree_insert(root,3);
root =btree_insert(root,1);
root =btree_insert(root,10);
root =btree_insert(root,5);
root =btree_insert(root,11);
root =btree_insert(root,7);
root =btree_insert(root,12);
root =btree_insert(root,13);
root =btree_insert(root,14);
root =btree_insert(root,15);
print_btree(root);
printf("\n");
print_levelorder(root);
printf("\n");
}

