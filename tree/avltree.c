/*
AVL tree
*/

#include <stdlib.h>
#include <stdio.h>

/******************************/


struct btree_node {
    int val;
    struct btree_node* left;
    struct btree_node* right;
    int height;
}; 

// A utility function to get height of the tree
int height(struct btree_node *N)
{
    if (N == NULL)
        return 0;
    return N->height;
}
 
// A utility function to get maximum of two integers
int max(int a, int b)
{
    return (a > b)? a : b;
}

/********************btree functions *******************/
/*iterate and free the tree*/

void
chop_off_the_tree(struct btree_node* tree)
{

    if( tree != NULL )
    {
        chop_off_the_tree(tree->left);
        chop_off_the_tree(tree->right);
        free(tree);
    }
    return;

}


/*  allocate a new node and insert a value */
struct btree_node* btree_newnode(int val) 
{
    struct btree_node* node = malloc(sizeof(struct btree_node));
    node->val = val;
    node->left = NULL;
    node->right = NULL;
    node->height = 1;

    return(node);
}
 
/*rightrotate tree rooted at y
*     y             x
*   x      ==>        y
*     T2           T2
*/

struct btree_node* rightrotate(struct btree_node *y)
{
	struct btree_node *x,*T2;
	x=y->left;
	T2=x->right;
	
	x->right=y;
	y->left=T2;

	y->height = max(height(y->right),height(y->left)) +1;
	x->height = max(height(x->right),height(x->left)) +1;
	
	return x;	
} 



/*leftrotate tree rooted at y
*    y                x
*       x      ==>  y
*    T2               T2
*/

struct btree_node* leftrotate(struct btree_node *y)
{
    struct btree_node *x,*T2;
    x=y->right;
    T2=x->left;
    
    x->left=y;
    y->right=T2;

    y->height = max(height(y->right),height(y->left)) +1;
    x->height = max(height(x->right),height(x->left)) +1;
    
    return x;   
}

int getbalance(struct btree_node * node)
{
	if(node)
	{
		return(height(node->left) - height(node->right));
	}
	return 0;
}

/* insert into our binary tree*/
struct btree_node* btree_insert(struct btree_node* node, int val) 
{
	int balance;
    /* tree is empty, return a new  node*/
    if (node == NULL) {
        return(btree_newnode(val));
    }
    else {
        /*insert as per binary tree rules*/
        if (val <  node->val) 
            node->left = btree_insert(node->left,val);
        else if (val > node->val)
            node->right = btree_insert(node->right, val);
			/*update heights*/
		node->height = max(height(node->right),height(node->left)) +1;
		/*balance*/
		balance = getbalance(node);
		

		/*cases*/
		//left left case
		if((balance > 1) && (val < node->left->val))
			return rightrotate(node);
		//left right case
		if((balance > 1) && (val > node->left->val))
		{
			node->left = leftrotate(node->left);
			return rightrotate(node);
		}

	    //right right case
        if((balance < -1) && (val > node->right->val))
            return leftrotate(node);
        //right left case
        if((balance < -1) && (val < node->right->val))
        {
            node->right = rightrotate(node->right);
            return leftrotate(node);
        }
	
		
        /*return unchanged*/
        return(node); 
    }
} 

struct btree_node* btree_find(struct btree_node* node, int val)
{
    if(node == NULL)
        return NULL;
    if(val < node->val)
        return btree_find(node->left ,val);
    else
    if(val > node->val)
        return btree_find( node->right ,val);
    else
        return node;
}



struct btree_node* btree_findmin(struct btree_node* node)
{
    if( node == NULL )
        return NULL;
    else
    if( node->left == NULL )
        return node;
    else
        return btree_findmin(node->left);
}


struct btree_node* btree_delete(struct btree_node* node, int val)
{
    struct btree_node* tmpnode;
	int balance;
    if(node == NULL)
        return NULL;
    else
    if(val < node->val)  /* Go left */
        node->left = btree_delete(node->left, val);
    else
    if(val > node->val)  /* Go right */
        node->right = btree_delete(node->right, val);
    else  /* Found value to be deleted */
    if( node->left && node->right)  /* Two children */
    {
        /* Replace with smallest in right subtree */
        tmpnode = btree_findmin(node->right);
        node->val = tmpnode->val;
        node->right = btree_delete(node->right, node->val);
    }
    else  /* One or zero children */
    {
        tmpnode = node;
        if(node->left == NULL ) /* Also handles 0 children */
            node = node->right;
        else if( node->right == NULL )
            node = node->left;
        free(tmpnode);
    }

	if(node == NULL)
    	return node;

	/*update heights*/
	node->height = max(height(node->right),height(node->left)) +1;
   	/*balance*/
    balance = getbalance(node);
	
	/*cases*/
	//Left Left
	if((balance > 1) && getbalance(node->left >=0))
		return rightrotate(node);
	//Left Right
    if((balance > 1) && getbalance(node->left < 0))
	{	
		node->left =leftrotate(node->left);
        return rightrotate(node);
	}

	//Right Right
    if((balance < -1) && getbalance(node->right <= 0))
        return leftrotate(node);
    //Right Left
    if((balance < -1) && getbalance(node->right > 0))
    {
        node->right =rightrotate(node->right);
        return leftrotate(node);
    }

	return node;


}

/*inorder*/
void print_btree(struct btree_node* node)
{
    if(node == NULL)
        return;
    else{
        print_btree(node->left);
        printf("%d -> ",node->val);
        print_btree(node->right);
    }
    return;
}

/* Compute the "height" of a tree -- the number of
    nodes along the longest path from the root node
    down to the farthest leaf node.*/
int height_without_var(struct btree_node* node)
{
    if (node==NULL)
        return 0;
    else
    {
        /* compute the height of each subtree */
        int lheight = height(node->left);

        int rheight = height(node->right);
	printf("lheight:%d  rheight: %d\n",lheight, rheight);
        /* use the larger one */
        if (lheight > rheight)
            return(lheight+1);
        else return(rheight+1);
    }
}

 
/* Print nodes at a given level */
void print_level(struct btree_node* root, int level,int height)
{
    int i;
    if (root == NULL)
        return;
    if (level == 1)
        printf("%2d", root->val);
    else if (level > 1)
    {
	//	for(i=1;i<level;i++)
	//		printf(" ");
        print_level(root->left, level-1,level);
		for(i=1;i<=height/2;i++)
			printf(" ");
        print_level(root->right, level-1,level);
    }
}
 
/* Function to print level order traversal a tree*/
void print_levelorder(struct btree_node* root)
{
    int h = height_without_var(root);
    int i,j;
    for (i=1; i<=h; i++)
	{
        for(j=i;j<h;j++)
            printf(" ");

        print_level(root, i,i);
		printf("\n");
	}	
}



/*********************************************************/
struct btree_node * root=NULL;
int main()
{
root =btree_insert(root,6);
root =btree_insert(root,2);
root =btree_insert(root,3);
root =btree_insert(root,1);
root =btree_insert(root,10);
root =btree_insert(root,5);
root =btree_insert(root,11);
root =btree_insert(root,7);
root =btree_insert(root,12);
root =btree_insert(root,13);
root =btree_insert(root,14);
root =btree_insert(root,15);
print_btree(root);
printf("\n");
print_levelorder(root);
printf("\n");
}

