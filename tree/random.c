/*
RANDOM
*/

#include <stdlib.h>
#include <stdio.h>


void make_uuid(unsigned long * uuid)
{
	uuid[0] = random();
	uuid[1] = random();
	uuid[2] = random();
	uuid[3] = random();
}


void print_uuid(unsigned long * r)
{

printf("%08lx-%08lx-%08lx-%08lx\n",r[0],r[1],r[2],r[3]);
}

