/*
binary tree ADT
non recursive 
*/

#include <stdlib.h>
#include <stdio.h>

/******************************/


struct btree_node {
    int val;
    int height;
    struct btree_node* left;
    struct btree_node* right;
    struct btree_node* parent;
}; 



/********************btree functions *******************/
/*iterate and free the tree*/

void
chop_off_the_tree(struct btree_node* tree)
{

    if( tree != NULL )
    {
        chop_off_the_tree(tree->left);
        chop_off_the_tree(tree->right);
        free(tree);
    }
    return;

}


/*  allocate a new node and insert a value */
struct btree_node* btree_newnode(int val) 
{
    struct btree_node* node = malloc(sizeof(struct btree_node));
    node->val = val;
    node->height = 1;
    node->left = NULL;
    node->right = NULL;
    node->parent = NULL;

    return(node);
}
 

/* insert into our binary tree*/
struct btree_node* btree_insert(struct btree_node* node, int val) 
{
	struct btree_node* root = node;	    
	struct btree_node *other_node, *parent;
    bool is_left;

    /* tree is empty, return a new  node*/
    if (node == NULL) {
        return(btree_newnode(val));
    }
    else {
        /*insert as per binary tree rules*/
		while(1)
		{
        	if (val <  node->val)
			{
				if( node->left == NULL ) { 
					node->left=(btree_newnode(val));
					node->left->parent=node;
					node=node->left;	
					break;
				}
				else{
					node=node->left;	
				}
			}
        	else if (val > node->val)
			{
				if( node->right == NULL ) { 
					node->right=(btree_newnode(val));
					node->right->parent=node;
					node=node->right;	
					break;
				}
				else{
					node=node->right;	
				}
			}
			else
			{
				printf("val=%d exists at node=%p height=%d\n",val,node,node->height);
				return root;
			}
			
		}
		

		while(node->parent != NULL)
		{	
			parent = node->parent;
			is_left = (parent->left == node)?1:0;
			other_node = (is_left)?parent->right:parent->left;

			parent->height = max(height(node),height(other_node)) +1;
			node=node->parent;
		}
	}
		return root;
} 

struct btree_node* btree_find(struct btree_node* node, int val)
{
    if(node == NULL)
        return NULL;
	while(1)
        {
            if (val <  node->val)
            {
                if( node->left == NULL ) {
        			return NULL;
                }
                else{
                    node=node->left;
                }
            }
            else if (val > node->val)
            {
                if( node->right == NULL ) {
        			return NULL;
                }
                else{
                    node=node->right;
                }
            }
            else
            {
                printf("val=%d exists at node=%p height=%d\n",val,node,node->height);
                return node;
            }

        }

}



struct btree_node* btree_findmin(struct btree_node* node)
{
    if( node == NULL )
        return NULL;
    else
	{
    	while( node->left != NULL )
		{
			node=node->left;
		}
		return node;
	}
}


struct btree_node* btree_delete(struct btree_node* node, int val)
{
    struct btree_node* tmpnode;
    struct btree_node* root = node;
    struct btree_node *node_to_del, *parent;
	bool is_left;
    if(node == NULL)
        return NULL;
    else {
        /*insert as per binary tree rules*/
        while(1)
        {
            if (val <  node->val)
            {
                node=node->left;
                if( node == NULL ) {
                    break;
                }
            }
            else if (val > node->val)
            {
                node=node->right;
                if( node == NULL ) {
                    break;
                }
            }
            else
            {
                printf("val=%d exists at node=%p height=%d\n",val,node,node->height);
				node_to_del =node;
				if( node->left && node->right)  /* Two children */
   	 			{
        			/* Replace with smallest in right subtree */
        			node_to_del = btree_findmin(node->right);
        			node->val = node_to_del->val;
					
    			}

				parent = node_to_del->parent;
				is_left = (parent->left == node_to_del)?1:0;

        		tmpnode = node_to_del;
        		if(node_to_del->left == NULL ) /* Also handles 0 children */
            		node_to_del = node_to_del->right;
        		else if( node_to_del->right == NULL )
            		node_to_del = node_to_del->left;
        		free(tmpnode);
	
				if(is_left)
					parent->left =node_to_del;
				else
					parent->right =node_to_del;

            }

        }
		

		return root; //val to delete not found
	}
		return root; //val to delete not found
}


/* Compute the "height" of a tree -- the number of
    nodes along the longest path from the root node
    down to the farthest leaf node.*/
int height(struct btree_node* node)
{
	if(!node) return -1;

	return node->height;
}

void print_btree(struct btree_node* node)
{

	struct btree_node * lastnode =NULL;
    while(node != NULL)
	{

		if(lastnode == node->parent)
		{
			if(node->left != NULL)
			{
                lastnode=node;
				node=node->left;
				continue;
			}
			else
				lastnode=NULL;
		}

		if(lastnode == node->left)
        {
			printf("%d -> ",node->val);

            if(node->right != NULL)
            {
                lastnode=node;
                node=node->right;
                continue;
            }
            else
                lastnode=NULL;
        }

        if(lastnode == node->right)
        {
            lastnode=node;
			node = node->parent;
        }
	}
	return;
}

 
/* Print nodes at a given level */
void print_level(struct btree_node* root, int level)
{
    int i;
	struct btree_node * lastnode =NULL;
	struct btree_node * node =root;
    if (root == NULL)
        return;
    while(node != NULL)
    {

        if(lastnode == node->parent)
        {
            if(node->left != NULL)
            {
                lastnode=node;
                node=node->left;
                continue;
            }
            else
                lastnode=NULL;
        }

        if(lastnode == node->left)
        {
			if(node->height == level)
			{
				for(i=1;i<=node->height;i++)
					printf(" ");
            	printf("%d",node->val);
			}

            if(node->right != NULL)
            {
                lastnode=node;
                node=node->right;
                continue;
            }
            else
                lastnode=NULL;
        }

        if(lastnode == node->right)
        {
            lastnode=node;
            node = node->parent;
        }
    }
    return;

}
 
/* Function to print level order traversal a tree*/
void print_levelorder(struct btree_node* root)
{
    int h = root->height;
    int i,j;
    for (i=h; i>=1; i--)
	{

        print_level(root, i);
		printf("\n");
	}	
}



/*********************************************************/
struct btree_node * root=NULL;
int main()
{
root =btree_insert(root,6);
root =btree_insert(root,2);
root =btree_insert(root,3);
root =btree_insert(root,1);
root =btree_insert(root,10);
root =btree_insert(root,5);
root =btree_insert(root,11);
root =btree_insert(root,7);
root =btree_insert(root,12);
root =btree_insert(root,13);
root =btree_insert(root,14);
root =btree_insert(root,15);
print_btree(root);
printf("\n");
print_levelorder(root);
//printf("height=%d\n",height(root));
printf("height=%d\n",root->height);
}

