INC_DIR = include
BIN_DIR = bin
ODIR=obj
CFLAGS  += -Wall -O0 -g -I $(INC_DIR) 
LDFLAGS += -Wl,--no-as-needed,-lrt,-lpthread,-lz,-lcurl

ALL= 3d-array \
	endian-using-union \
	no_of_bit
#install: all
all: ${ALL}
	mv -f ${ALL} ${BIN_DIR}
$(ALL) : %:%.c
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)




clean:
	rm -f $(addprefix ${BIN_DIR}/,${ALL}) $(ODIR)/*.o
