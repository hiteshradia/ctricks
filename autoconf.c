#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>      /* for printf() and fprintf() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <time.h>       /* for timestamps */
#include <unistd.h>
#include <ctype.h> //tolower isdidgit etc.
#include <getopt.h>
#include <pthread.h>
#include <net/if.h>
#include "autoconf.h"
#define MAXUNIT_CNT 10
#define TIME_OUT 1

static char multicastIP[30] = {0};
static char multicastport[30] = {0};
static char iface[20] = {0};
static pthread_t txthread;
int sock;                             /* Socket */

struct autoconf aconf;

struct autoconf others_aconf[MAXUNIT_CNT];
int others_disc =0;
int master_cnt =0;

const char* mac_sprintf(const u_int8_t *mac)
{
    static char etherbuf[18];
    snprintf(etherbuf, sizeof(etherbuf), "%02x:%02x:%02x:%02x:%02x:%02x",
        mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    return etherbuf;
}

void mac_printf(const u_int8_t *mac)
{
    printf("macprint %02x:%02x:%02x:%02x:%02x:%02x",
        mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}

//converts a string of xx:xx:xx:xx:xx:xx hex digits to 6 byte mac address
int stringtomac(uint8_t*mac,char* macstr)
{
    uint8_t number,i;
    char ch;
    const char cSep = ':';
    for (i = 0; i < 6; ++i)
    {
        ch = tolower (*macstr++);
        if ((ch < '0' || ch > '9') && (ch < 'a' || ch > 'f'))
        {
            return 0;
        }
        number = isdigit (ch) ? (ch - '0') : (ch - 'a' + 10);
        ch = tolower (*macstr);
        if ((i < 5 && ch != cSep) || (i == 5 && ch != '\0' && !isspace (ch)))
        {
            if ((ch < '0' || ch > '9') && (ch < 'a' || ch > 'f'))
            {
                return 0;
            }
            number <<= 4;
            number += isdigit (ch) ? (ch - '0') : (ch - 'a' + 10);
            ++macstr;
            ch = *macstr;
            if (i < 5 && ch != cSep)
            {
                return 0;
            }
            mac[i] = number;

        }
        ++macstr;
    }
    return 1;
}

void diep(char *s)
{
    perror(s);
    exit(1);
}

void usage() {
    printf("\nusage: autoconf-deamon\n");
    printf(" \n******** Options ******\n");
    printf("\n\t -h:\tThis help \n");
    printf("\t -I:\tlocal Interface\n");
    printf("\t -m:\tMulticast address\n");
    printf("\t -p:\tMulticast port\n");
}

void Options(int argc, char *argv[])
{
    int index = 0;
    int t;
    static struct option opts[] =
    {
        {"help", no_argument, 0, 'h'},
        {"interface", required_argument, 0, 'I'},
        {"multicast address", required_argument, 0, 'm'},
        {"multicast port", required_argument, 0, 'm'},
        {0, 0, 0, 0}
    };
    while(1) {
        t = getopt_long(argc, argv, "hI:m:p:", opts, &index);
        if(t < 0) {
            break;
        }
        switch(t) {
        case 'h':
            usage();
            exit(1);
            break;
        case 'I':
            strcpy(iface, optarg);
            break;
        case 'm':
            strcpy(multicastIP, optarg);
            break;
        case 'p':
            strcpy(multicastport, optarg);
            break;

        default:
            usage();
            break;
        }
    }
}


void *tx_client( void *threaddata ) {

    char * sendString ="hits";
    int sendStringLen = strlen(sendString);
    int txsock;
    struct addrinfo * sendAddr;              /* Multicast send address */
    struct addrinfo hints1         = { 0 };       /* Hints for name lookup */
    int i;
    /* Create socket for sending multicast datagrams */
    if ((txsock = socket(PF_INET6, SOCK_DGRAM, 0)) == -1)
    { printf("txsocket() faile\n"); goto txthread_exit;}

    hints1.ai_family   = PF_INET6;
    hints1.ai_socktype = SOCK_DGRAM;
    hints1.ai_flags    = AI_NUMERICHOST;

    if (getaddrinfo(multicastIP, multicastport, &hints1, &sendAddr) != 0)
    {  printf("getaddrinfo() failed\n"); goto txthread_exit;}

    const int scope_id = if_nametoindex(iface);
    if (setsockopt(txsock, IPPROTO_IPV6, IPV6_MULTICAST_IF, (const char *) &scope_id, sizeof(scope_id)) != 0)
    {  printf("setsockopt(MULTICAST_IF) failed\n");     goto txthread_exit; }

    for (i=0; i<30; i++ ) /* Run 3 times */
    {
        int sendLen = sendto(txsock, &aconf,sizeof(struct autoconf), 0, sendAddr->ai_addr, sendAddr->ai_addrlen);
        if (sendLen != sizeof(struct autoconf) )
        {
            printf("%d",sendLen);
            printf("sendto() sent a different number of bytes than expected\n");
            goto txthread_exit;
        }
        usleep(200000); //200 millisecond
    }

txthread_exit:
    pthread_exit(NULL);
}

int main(int argc, char* argv[])
{
    struct addrinfo *  localAddr;                /* Local address to bind to */
    struct addrinfo hints          = { 0 };      /* Hints for name lookup */
    struct addrinfo *  multicastAddr;            /* Multicast Address */
    const int trueValue = 1;
    int ret;
    struct autoconf recaconf;
    void * thread_result;
    Options(argc,argv);
    if(argc < 4 )
    {
        usage();
        exit(1);
    }

    memset(&aconf, 0, sizeof(struct autoconf));
    //test
    char name[64]={0},tmp_buf[100]={0};
    FILE *status_ptr;
    struct timeval tv;
    sprintf(name,"ifconfig eth0 | grep -i hwaddr | awk '{print $5}'");
    // printmd | grep basemac | awk '{print $3}'
    status_ptr = popen(name,"r");
    fgets(tmp_buf,90,status_ptr);
    pclose(status_ptr);
    stringtomac(aconf.myunitid,tmp_buf);
    printf("mymac =%s\n",tmp_buf);
    mac_printf(aconf.myunitid);
    fflush(stdout);
    //  aconf.myunitid[5]=125;
    aconf.type= AC_INIT;
    aconf.seqno= 1;
    aconf.mystatus= UNSPECIFIED;
    /* Resolve the multicast group address */
    hints.ai_family = PF_INET6;
    hints.ai_flags  = AI_NUMERICHOST;
    if ( getaddrinfo(multicastIP, NULL, &hints, &multicastAddr) != 0 )
        diep("getaddrinfo() failed");

    /* Get a local address with the same family as our multicast group */
    hints.ai_family   = multicastAddr->ai_family;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags    = AI_PASSIVE; /* Return an address we can bind to */
    if ( getaddrinfo(NULL,multicastport, &hints, &localAddr) != 0 )
        diep("getaddrinfo() failed");

    /* Create socket for receiving datagrams */
    if ( (sock = socket(localAddr->ai_family, localAddr->ai_socktype, 0)) == -1 )
        diep("socket() failed");

    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (const void *) &trueValue, sizeof(trueValue));

    /* Bind to the multicast port */
    if ( bind(sock, localAddr->ai_addr, localAddr->ai_addrlen) != 0 )
        diep("bind() failed");

    /* Join the multicast group.  */
    if ((multicastAddr->ai_family == PF_INET6)&&(multicastAddr->ai_addrlen == sizeof(struct sockaddr_in6)))
    {
        struct ipv6_mreq multicastRequest;  /* Multicast address join structure */

        /* Specify the multicast group */
        memcpy(&multicastRequest.ipv6mr_multiaddr, &((struct sockaddr_in6*)(multicastAddr->ai_addr))->sin6_addr, sizeof(multicastRequest.ipv6mr_multiaddr));
        const int scope_id = if_nametoindex(iface);
        multicastRequest.ipv6mr_interface = scope_id;

        /* Join the multicast address */
        if ( setsockopt(sock, IPPROTO_IPV6, IPV6_JOIN_GROUP, (char*) &multicastRequest, sizeof(multicastRequest)) != 0 )
            diep("setsockopt(IPV6_JOIN_GROUP) failed");
    }
    else diep("Not IPv6");
        tv.tv_sec = TIME_OUT;
        tv.tv_usec = 0;
        if(setsockopt(sock, SOL_SOCKET , SO_RCVTIMEO, &tv , sizeof(tv)) < 0){
        diep("setsockopt(SO_RCVTIMEO) failed");
        }
    freeaddrinfo(localAddr);
    freeaddrinfo(multicastAddr);

    //init stage send finite number of init messages

    ret = pthread_create(&txthread, NULL, tx_client, NULL );
    ret = pthread_join(txthread,&thread_result);

    //configuration stage before this mystatus =UNSPECIFIED

    for (;; ) /* Run forever */
    {

        int recvStringLen = 0;
        int i = 0,repeated_unit = 0;
        memset(&recaconf, 0, sizeof(struct autoconf));
        printf("b recvfrom\n");
        /* Receive a single datagram from the server */ //use TIME_OUT for receive to detect master 
        if ((recvStringLen = recvfrom(sock, &recaconf, sizeof(struct autoconf) - 1, 0, NULL, 0)) < 0)
            {
                
                if((aconf.mystatus == UNSPECIFIED) && (others_disc !=0)) 
                {
                    if((master_cnt == others_disc)) //compare mac address for master decision
                    aconf.mystatus = MASTER;
                else
                    aconf.mystatus = SLAVE_ALONE;
                }
                   printf("mystatus = %d\n", aconf.mystatus); 
                continue;
            }
        if(MAC_ADDR_EQ(recaconf.myunitid,aconf.myunitid)) //if received udp from ourself dont process
           { continue;
            printf("1\n");
    }
            // here we can switch state to split different stages processing
        //conf stage processing --to use to some table to keep track of the other units TBD hits
        for(i=0; i < others_disc; i++)  //discard same messages which have been previously received
        {
            printf("2\n");
            if(MAC_ADDR_EQ(others_aconf[i].myunitid,recaconf.myunitid))
            {
            printf("3\n");
                repeated_unit =1;
                if((others_aconf[i].seqno == recaconf.seqno))

                   break;
            }
        }
        if(!repeated_unit)
        {
            printf("4\n");
            if(others_disc > MAXUNIT_CNT)
            {
            printf("5\n");
                printf("MAX UNits count exceeded !!!");
                continue;
            }
            others_disc++;
            others_aconf[others_disc] = recaconf;     //copy the aconf of others disc
            
                if(aconf.mystatus == UNSPECIFIED) //compare mac address for master decision
            {
               if(MAC_ADDR_CMP(aconf.myunitid,recaconf.myunitid) < 0){ //my mac is less than the other  can be master !!
            printf("5\n");
                master_cnt++; 
                }
            }
        }


    }

    /* NOT REACHED */
    close(sock);
    exit(EXIT_SUCCESS);
}

