#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/syslog.h>
#include <signal.h>
#include <time.h>
#include "bootstrap.h"
#include "awlocal.h"

#define REFERENCE_TIME 1072915234
#define NUMSECS_PERHOUR 3600
//static struct sockaddr_in6 server_addr6;
//static struct sockaddr_in server_addr;
//int port_no=16092;
//char ip_addr[25];
//static int i;
//static int ipaddrlen = 0,iplen = 0;
uint8_t MBrequest_hash[10];
uint8_t CRLrequest_hash[10];
uint8_t CRrequest_hash[10];
uint8_t CRresponse_hash[10];
uint8_t ToBeEncrypted[5120];
uint8_t ToBeSigned[5120];

//char dec1st[]="/tmp/usb/ModelDeploymentConfigurationItems/dec1st.dat";
//char dec2nd[]="/tmp/usb/ModelDeploymentConfigurationItems/dec2nd.dat";
char dec1st[]="/tmp/usb/lcm/dec1st.dat";
char dec2nd[]="/tmp/usb/lcm/dec2nd.dat";
char crtFile[255];
uint8_t *pDec1st=NULL,*pDec2nd=NULL;
Time32 start_time;
Time32 end_time;
Time32 request_time;
//[VAMSI-uncomment later]CertificateDuration batch_duration;
uint32_t storage_space_kb;
EC_KEY *MyOwnKey;
UINT8 pMyOwnTmpBufPub[33], pMyOwnTmpBufPriv[32];
uint32_t batchsLen=0;
uint8_t cert_batch_id[8];
uint8_t decryption_key[16];
uint8_t response_encryption_key[16];
uint32_t crlSeries=0x00000001;
uint32_t crlSerial=0xffffffff;
Time32 startPeriod,issueDate;
uint8_t ca_id[8];
uint8_t appStartFlag=0;

uint8_t crlErr=255,dkeyErr=255,CRSErr=255,CRCErr=255;

#define SHAIK 1
char ip[16]="12.172.124.246";


/*struct {
PublicKey A;
PublicKey H;
uint8 s[16];
uint8 e[16];
PsidSspArray permissions;
GeographicRegion region;
Time32 current_time;
Time32 start_time;
Time32 end_time;
CertificateDuration batch_duration;
uint32 storage_space_kb;
CertId8 known_cas<var>
} ToBeSignedAnonymousCertRequestReq;

typedef struct {
	uint8_t cert_batch_id[8];
	Time32 start_time;
	Time32 end_time;
	uint32_t validity_period;
	uint32_t overlap_period;
	uint8_t nonce[12];
	int ccm_ciphertext_len;
 	uint8_t *ccm_ciphertext;
} EncryptedCertificateBatch;*/
typedef struct {
    uint8_t certInfo[512];
    uint16_t certLen;
    uint8_t version_and_type;
    uint8_t subject_type;
    uint8_t cf;
    uint8_t signature_alg;
    uint16_t signer_id;
    uint16_t scopelen;
    uint16_t scope;
    uint16_t expiration;
    uint16_t start_validity_or_lft;
    uint16_t crl_series;
    uint16_t verification_key;
    uint16_t encryption_key;
    uint16_t sig_RV;
} Certificate;
Certificate RACert;
Certificate CACert;
Certificate CSRCert;
Certificate cert;

typedef struct{
	uint32_t certReqTime;
	uint32_t endReqTime;
	uint32_t DkReqTime;
	uint32_t cfmOffset;
	uint32_t batchOffset;
	uint32_t SLClen; //length of short_livid_certs<var>
	uint32_t SMlen; //length of SignedData<var>
	Time32 nextCrl;
}LCM_store;
LCM_store lcmStore;

// <----------------------Util Fun's----------------------------->

uint32_t saveLCMStore(void)
{
	FILE *fp;
	uint32_t offset=0;

	fp=fopen("/var/LCMStore","w");
	if(fp != NULL){
		offset = fwrite(&lcmStore,1,sizeof(lcmStore),fp);
		fclose(fp);
	}
	return offset;
}

uint32_t loadLCMStore(void)
{
	FILE *fp;
	uint32_t offset=0;

	fp=fopen("/var/LCMStore","r");
	if(fp != NULL){
		offset = fread(&lcmStore,1,sizeof(lcmStore),fp);
		fclose(fp);
	}
	if(lcmoptions.Request_Certificates_Time == 0)
		lcmStore.certReqTime = 0;
	if(lcmoptions.Request_Decryption_Key_Time == 0)
		lcmStore.DkReqTime = 0;
	return offset;
}

void print_keys(uint8_t flag)
{
	uint32_t i;

	printf("\n---------------------------------------------------------\n");
	if(flag==3){
	  printf("\n[pBootStrapKeyPriv]:\n");
	  for(i=0;i<32;i++)
		  printf("%02x ",SaveKeys.pBootStrapKeyPriv[i]);
	  printf("\n[pBootStrapKeyPub]:\n");
	  for(i=0;i<33;i++)
		  printf("%02x ",SaveKeys.pBootStrapKeyPub[i]);
	  printf("\n[pStaticKeyPriv]:\n");
	  for(i=0;i<32;i++)
		  printf("%02x ",SaveKeys.pStaticKeyPriv[i]);
	  printf("\n[pStaticKeyPub]:\n");
	  for(i=0;i<33;i++)
		  printf("%02x ",SaveKeys.pStaticKeyPub[i]);
	  printf("\n[pSignKeyPriv]:\n");
	  for(i=0;i<32;i++)
		  printf("%02x ",SaveKeys.pSignKeyPriv[i]);
	  printf("\n[pSignKeyPub]:\n");
	  for(i=0;i<33;i++)
		  printf("%02x ",SaveKeys.pSignKeyPub[i]);
	  printf("\n[pEncKeyPriv]:\n");
	  for(i=0;i<32;i++)
		  printf("%02x ",SaveKeys.pEncKeyPriv[i]);
	  printf("\n[pEncKeyPub]:\n");
	  for(i=0;i<33;i++)
		  printf("%02x ",SaveKeys.pEncKeyPub[i]);
	  printf("\n[pAESSignKey]:\n");
	  for(i=0;i<16;i++)
		  printf("%02x ",SaveKeys.pAESSignKey[i]);
	  printf("\n[pAESEncKey]:\n");
	  for(i=0;i<16;i++)
		  printf("%02x ",SaveKeys.pAESEncKey[i]);
	  printf("\n[pAESSignNonce]:\n");
	  for(i=0;i<12;i++)
		  printf("%02x ",SaveKeys.pAESSignNonce[i]);
	  printf("\n[pAESEncNonce]:\n");
	  for(i=0;i<12;i++)
		  printf("%02x ",SaveKeys.pAESEncNonce[i]);
	  printf("\n[DigestInfo]:\n");
	  for(i=0;i<32;i++)
		  printf("%02x ",SaveKeys.DigestInfo[i]);
	}
	if(flag==2){
	  printf("\n[RACert.verification_key]:%d\n",RACert.verification_key);
	  for(i=0;i<33;i++)
		  printf("%02x ",RACert.certInfo[RACert.verification_key+i]);
	  printf("\n[RACert.encryption_key]:\n");
	  for(i=0;i<33;i++)
		  printf("%02x ",RACert.certInfo[RACert.encryption_key+i]);
	  printf("\n[RACert.sig_RV]:\n");
	  for(i=0;i<65;i++)
		  printf("%02x ",RACert.certInfo[RACert.sig_RV+i]);
	  
	  printf("\n[CACert.verification_key]:\n");
	  for(i=0;i<33;i++)
		  printf("%02x ",CACert.certInfo[CACert.verification_key+i]);
	  printf("\n[CACert.encryption_key]:\n");
	  for(i=0;i<33;i++)
		  printf("%02x ",CACert.certInfo[CACert.encryption_key+i]);
	  printf("\n[CACert.sig_RV]:\n");
	  for(i=0;i<65;i++)
		  printf("%02x ",CACert.certInfo[CACert.sig_RV+i]);
	  
	  printf("\n[CSRCert.sig_RV]:\n");
	  for(i=0;i<33;i++)
		  printf("%02x ",CSRCert.certInfo[CSRCert.sig_RV+i]);
	  
	  printf("\n[recon_priv]:\n");
	  for(i=0;i<32;i++)
		  printf("%02x ",SaveKeys.recon_priv[i]);
	}
	printf("\n---------------------------------------------------------\n");
}

Time32 get_cur_time2004(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return(tv.tv_sec - REFERENCE_TIME);
}

uint32_t epoch_time_from_2004(Time32 *start, Time32 *end, CertificateDuration units, CertificateDuration SBduration)
{
    Time32 tmp_time=0,retTime=0,secs=0;
	    
    putenv("TZ=UTC");
    
    if(units == 0)
	secs = SBduration; //secs
    if(units == 1)
	secs = SBduration * 60; //min
    if(units == 2)
	secs = SBduration * NUMSECS_PERHOUR; //hours
    if(units == 3)
	secs = SBduration * 60 * NUMSECS_PERHOUR; //60hrs
    if(units == 4)
	secs = SBduration * 31536000; //years

	if(lcmStore.certReqTime == 0)
	    tmp_time = get_cur_time2004();
	else
	    tmp_time = lcmStore.endReqTime + 1;
   	if(!BIGENDIAN)
		retTime =swap_32(tmp_time); //later should be changed to htobe32()
	memcpy(start, &retTime, 4);
	tmp_time +=  secs;
	lcmStore.endReqTime = tmp_time;
   	if(!BIGENDIAN)
		retTime =swap_32(tmp_time); //later should be changed to htobe32()
	memcpy(end, &retTime, 4);
    return 0;
}
#if 0
int epoch_time_from_2004(uint8_t *buff, char *str_date, char *str_time){
	//usage: epoch_time_from_2004(inputbuffer,"2012-11-12","00:00:00")
	struct tm tminfo;
	char *token = NULL;
	char tmpstr[50];
	time_t epch_time,rettime;
	uint8_t *p = NULL;

	putenv("TZ=UTC");	
	memcpy(tmpstr,str_date,sizeof(tmpstr));
	//sscanf(tmpstr,"%s",str);
	token = strtok(tmpstr,"-");                   //year
	tminfo.tm_year = atoi(token) -1900;
	token = strtok(NULL,"-");                     //month
	tminfo.tm_mon = atoi(token) -1;
	token = strtok(NULL," ");					  //day
	tminfo.tm_mday = atoi(token);
	memcpy(tmpstr,str_time,sizeof(tmpstr));
	token = strtok(tmpstr,":");					  //hour
	tminfo.tm_hour = atoi(token);
	token = strtok(NULL,":");					  //minute
	tminfo.tm_min = atoi(token);
	token = strtok(NULL," ");					  //second
	tminfo.tm_sec = atoi(token);
	epch_time = mktime(&tminfo);                  //epoch time 
	epch_time = epch_time - REFERENCE_TIME;
	printf("epoch_time = %lu\n",epch_time);
   	if(!BIGENDIAN)
		rettime =swap_32(epch_time); //later should be changed to htobe32()
	p=&rettime;
	//memcpy(buff,&rettime,4); //will it work or not
	buff[0] = p[0];
	buff[1] = p[1];
	buff[2] = p[2];
	buff[3] = p[3];
	return epch_time;
}
#endif
/* addr: buffer in which encoded length will be copied
 * retIdx: no.of bytes to be incremented for buffer index
 * recv_size(return): will have length of type uint32_t
 */
uint32_t decode_length(uint8_t *addr , uint32_t *retIdx)
{
   uint32_t recv_size = 0;

   if ((addr[0] & 0x80) == 0x00) {
       recv_size = addr[0];
       *retIdx = 1;
   }
   else if ((addr[0] & 0xc0) == 0x80) {
       recv_size = (addr[0] << 8)|(addr[1]);
	   recv_size = recv_size & 0x3fff;
       *retIdx = 2;
   }
   else if ((addr[0] & 0xe0) == 0xc0) {
       recv_size = (addr[0] << 16)|(addr[1] << 8)|(addr[2]);
	   recv_size = recv_size & 0x1fffff;
       *retIdx = 3;
   }
   else if ((addr[0] & 0xf0) == 0xe0) {
       recv_size = (addr[0] << 24)|(addr[1] << 16)|(addr[2] << 8)|(addr[3]);
	   recv_size = recv_size & 0x0fffffff;
       *retIdx = 4;
   }
   return recv_size;
}

/* addr: buffer in which encoded length will be copied
 * val: 32bit length value
 * retIdx: no.of bytes to be incremented for buffer index
 */
uint32_t encode_length(uint8_t *addr ,uint32_t val , uint32_t *retIdx)
{
   uint32_t retval = 0;
   uint8_t *p = NULL;
   if(!BIGENDIAN){
    retval =swap_32(val); //later should be changed to htobe32()
    p=&retval;
   }
   else
    p=&val;
   if (val <= 0x7F) {
       *retIdx = 1;
	   addr[0] = p[3];
   }
   else if (val >= 0x80 && val <= 0x3FFF) {
       *retIdx = 2;
	   addr[0] = p[2] | 0x80;
	   addr[1] = p[3];
   }
   else if (val >= 0x4000 && val <= 0x1FFFFF) {
       *retIdx = 3;
	   addr[0] = p[1] | 0xd0;
	   addr[1] = p[2];
	   addr[2] = p[3];
   }
   else if (val >= 0x200000 && val <= 0xFFFFFFF) {
       *retIdx = 4;
	   addr[0] = p[0] | 0xe0;
	   addr[1] = p[1];
	   addr[2] = p[2];
	   addr[3] = p[3];
   }
}

uint32_t create_certFile(uint8_t *buf,  uint32_t len, char *file_name)
{
    uint8_t tmp[3],i=0;
    int fd = -1, wr_sts = -1; 

    //printf("file -> %s\n", file_name);
    fd = open(file_name, O_RDWR | O_CREAT);    
    if(fd < 0)
	return -1;

    if(fd > 0)
    {
    	while (i <= len-1){
    	    sprintf(&tmp, "%02x ", buf[i]);
	    wr_sts = write(fd, &tmp, 3);
	    if(wr_sts < 0) {
	    	printf("Failed to write %02x to %s\n", tmp, file_name);
		close(fd);
		return -1;
	    }
	    i++;
    	}
    }
    close(fd);
    return 0;
}

uint32_t mapFile(char *toFile, uint8_t *data, uint8_t closeFlag)
{
	int fd;
    	struct stat sbuf;
	if(!closeFlag){
 		if ((fd = open(toFile, O_RDWR|O_APPEND, S_IRWXU | S_IRWXO | S_IRWXG)) == -1) {
        		perror("open");
		        return -1;
    		}
    		if (stat(toFile, &sbuf) == -1) {
        		perror("stat");
		        return -1;
    		}
	    	data = mmap(0, sbuf.st_size, PROT_READ| PROT_WRITE, MAP_SHARED, fd, 0);
    		if (data <=0) {
        		perror("mmap");
		        return -1;
    		}
	        close(fd);
	}
	else if(closeFlag){
		if (munmap (data, sbuf.st_size) == -1) {
		    perror ("munmap");
		    return -1;
		  }
	}
	return 0;
}

uint32_t readFile(char *toFile)
{
	FILE *fp;
	uint32_t offset=0;

	fp=fopen(toFile,"r");
	if(fp != NULL){
		fscanf(fp,"%d",offset);
		fclose(fp);
	}
	return offset;
}

uint32_t createZeroFile(char *toFile, uint32_t size)
{
	char cmd[255];
	sprintf("dd if=/dev/zero of=%s bs=%d count=1",toFile,size);
	system(cmd);
	return 0;
}

// <----------------------Util Fun's----------------------------->


uint32_t certParse(uint8_t *cert,Certificate * parsedcert)
{
    uint32_t certparseoffset =0;
    uint32_t ll =0;
    uint32_t scope_namelen=0;
    uint32_t permitted_subject_type=0;
    uint32_t psid_array_premissionslen=0;
    uint8_t psid_array_type=0;
    uint8_t geographic_region_type=0;
    uint16_t polylen=0;
    uint16_t crllen=0;
    uint8_t version_and_type =cert[0];
    uint8_t subject_type =cert[1];
    uint8_t cf =cert[2];
    parsedcert->version_and_type =version_and_type;
    parsedcert->subject_type  =subject_type;
    parsedcert->cf  =cf;
    certparseoffset +=3;
    if(subject_type != root_ca)
    {
        parsedcert->signer_id= certparseoffset; //signer_id
        certparseoffset += DIGESTLEN;
        parsedcert->signature_alg = cert[certparseoffset]; //sig_alg
        certparseoffset +=1;
    }
//certspecificdata parsing
    if((subject_type != crl_signer) && (subject_type != message_anonymous)) {
        ll=0;
        scope_namelen = decode_length((cert + certparseoffset), &ll);
        certparseoffset += ll;
        certparseoffset += scope_namelen;
		printf("%d - %d\n",ll,certparseoffset);
    }
	printf("--%d\n",subject_type);
    switch(subject_type)
    {

    case root_ca:
        ll=0;
        permitted_subject_type =(uint16_t)decode_length((cert + certparseoffset), &ll);
        certparseoffset +=ll;
        if(permitted_subject_type & 0x024f) //parse properly later
        {
            psid_array_type =*(uint8_t *)(cert + certparseoffset);
            certparseoffset +=1; //psid_array_type
            if(psid_array_type ==specified) {
                ll=0;
                psid_array_premissionslen= decode_length((cert + certparseoffset), &ll);
                certparseoffset +=ll;
                certparseoffset += psid_array_premissionslen;
            }
        }
        if(permitted_subject_type & 0x00b0) //parse properly later
        {
            psid_array_type =*(uint8_t *)(cert + certparseoffset);
            certparseoffset +=1; //psid_array_type
            if(psid_array_type ==specified) {
                ll=0;
                psid_array_premissionslen= decode_length((cert + certparseoffset), &ll);
                certparseoffset +=ll;
                certparseoffset += psid_array_premissionslen;
            }

        }
        //geographic_region
        geographic_region_type = *(uint8_t*)(cert + certparseoffset);
        certparseoffset +=1;     //region_type

        switch(geographic_region_type)
        {
        case from_issuer_region:
        case none:
            break;
        case circle:
            certparseoffset+= (sizeof(TwoDLocation)*2 +sizeof(uint16_t));  //circularregionsize
            break;
        case rectangle:
        case polygon:
            ll=0;
            polylen= (uint16_t)decode_length((cert + certparseoffset), &ll);
            certparseoffset+= ll;
            certparseoffset+= polylen;
            break;
        }
        break;
    case message_ca:
    case message_ra:
    case message_csr:
    case wsa_ca:
    case wsa_csr:
    case message_identified_not_localized:
    case message_identified_localized:
    case message_anonymous:
    case wsa:
        //subject_type
        if(subject_type == message_ca || subject_type == message_ra || subject_type == message_csr) {
            ll=0;
            permitted_subject_type =(uint16_t)decode_length((cert + certparseoffset), &ll);
            certparseoffset +=ll;
		printf("%d - %d\n",ll,certparseoffset);
        }
        //psid-array
        psid_array_type =cert[certparseoffset];
        certparseoffset +=1;     //psid_array_type
        if(psid_array_type ==specified) {
            ll=0;
            psid_array_premissionslen= decode_length((cert + certparseoffset), &ll);
            certparseoffset +=ll;
            certparseoffset += psid_array_premissionslen;
		printf("%d - %d\n",ll,certparseoffset);
        }
        if(subject_type != message_identified_not_localized)
        {
            //geographic_region
            geographic_region_type = cert[certparseoffset];
            certparseoffset +=1; //region_type

            switch(geographic_region_type)
            {
            case from_issuer_region:
            case none:
                break;
            case circle:
                certparseoffset+= (sizeof(TwoDLocation)*2 +sizeof(uint16_t)); //circularregionsize
                break;
            case rectangle:
            case polygon:
                ll=0;
                polylen= (uint16_t)decode_length((cert + certparseoffset), &ll);
                certparseoffset+= ll;
                certparseoffset+= polylen;
                break;
            }
        }
        break;
    default: //default includes crl_signer
        ll=0;
        crllen= (uint16_t)decode_length((cert + certparseoffset), &ll);
        certparseoffset+= ll;
        certparseoffset+= crllen;

        break;
    }

		printf("%d - %d\n",ll,certparseoffset);
    parsedcert->expiration = certparseoffset; //expiration
    certparseoffset +=4; //sizeof Time32
	if(cf&0x01){
	    parsedcert->start_validity_or_lft = certparseoffset; //startvalidity_or_lft
		if((cf&0x02)==2)
    		certparseoffset+=2; //sizeof Time32
		else
    		certparseoffset+=4; //sizeof Time32
	}
		printf("%d - %d\n",ll,certparseoffset);
    parsedcert->crl_series = certparseoffset; //startvalidity_or_lft
    certparseoffset+=4; //sizeof Time32
		printf("%d - %d\n",ll,certparseoffset);
    if(version_and_type == 2) { //assuming only case 2 or case 3
        certparseoffset+=1; //PKAlgorithm
        parsedcert->verification_key =certparseoffset;
        certparseoffset +=33; //assuming SHA256
    }
		printf("%d - %d\n",parsedcert->verification_key,certparseoffset);
    if(cf & 0x04) //cf encryption
    {
        certparseoffset+=1; //PKAlgorithm
        certparseoffset+=1; //SymmAlgorithm
        parsedcert->encryption_key =certparseoffset;
        certparseoffset +=33; //assuming SHA256

    }
		printf("%d - %d\n",parsedcert->verification_key,certparseoffset);
    //TobeSignedCert done

    parsedcert->sig_RV = certparseoffset;
    switch(version_and_type) //assuming SHA256 and ignoring private_use
    {
    case 2:
        certparseoffset +=65;
        break;
    case 3:
        certparseoffset +=33;
        break;
    }
		printf("%d - %d\n",parsedcert->verification_key,certparseoffset);
    return certparseoffset;
}

uint32_t retLen_EncryptedData(uint8_t *tbsec)
{
	uint32_t idx=0,recLen=0,len=0;
	
	idx += 1; //SymmAlgorithm
	//RecipientInfo<var>.
	recLen = decode_length((tbsec+idx), &len);
	idx += len;
	idx += recLen;
	//AesCcmCiphertext->nonce
	idx += 12;
	//AesCcmCiphertext->ccm_ciphertext<var>
	len=0;
	recLen = decode_length((tbsec+idx), &len);
	idx += len;
	idx += recLen;

	return idx;
}

//[NAZEER] key info should be passed to this function by adding argument if need.
uint32_t decrypt_EncryptedData(uint8_t *tbsec, uint8_t *capkrv, uint8_t *rvLen)
{
	uint32_t len=0,idx=0;
	uint32_t ccm_ciphertext_len=0,recLen=0;;
	uint8_t nonce[12],alg;

	//decode/decrypt EncryptedMessage/EncryptedData
	alg = tbsec[idx];
	idx += 1; //SymmAlgorithm
	recLen = decode_length((tbsec+idx), &len);
	idx += len;
	//decode RecipientInfo<var>.
	idx += recLen;
	//AesCcmCiphertext->nonce
	memcpy(nonce,(tbsec+idx),12);
	idx += 12;
	len=0;
	ccm_ciphertext_len = decode_length((tbsec+idx), &len);
	idx += len;
	//decrypt AesCcmCiphertext->ccm_ciphertext<var> and store it in CertificateAndPrivKeyReconstructionValue=capkrv and also store length of capkrv in rvlen. do malloc 
	idx += ccm_ciphertext_len;

	return idx;

}

/* if encContentType is valied value, then signed message is encapsulated in ToBeEncrypted
 * if encContentType is 0xff then this function return only signedMessage.
 */
ECDSA_SIG *pSig_g = NULL;
uint32_t signInToBeEncrypted(uint8_t *SignedData, uint8_t encContentType, uint8_t identifierType, uint8_t *certData, uint8_t *ToBeSignedData, uint32_t dataLen)
{
   UINT32 uiTmpBufLen;
	uint32_t SIGNoffset = 0;
      
	if(encContentType != 0xff){
//start Encrypted Message		
		SignedData[SIGNoffset] = encContentType; //ToBeEncrypted->ContentType
		SIGNoffset += 1;
	}
//start Signed Message		
	SignedData[SIGNoffset] = identifierType; //SignerIdentifierType->certificate
	SIGNoffset += 1;

	if(identifierType == certificate_digest_with_ecdsap256){
		memcpy((SignedData + SIGNoffset), certData, 8);
		SIGNoffset += 1;
	}
	else if (identifierType == certificate){
		Certificate *cert = (Certificate *)certData;
		memcpy((SignedData + SIGNoffset), cert->certInfo, cert->certLen); 
		SIGNoffset += cert->certLen;
	}
	memcpy((SignedData + SIGNoffset), ToBeSignedData, dataLen); 
	SIGNoffset += dataLen;
        if(pSig_g != NULL)
        {
        SignedData[SIGNoffset++] = 0x00; //SaveKeys.pStaticKeyPub[0]; //NAZEER; //EccPublicKeyType+R+S. calculated over[from 'start ToBeSignedAnonymousCertRequestReq' to end 'ToBeSignedAnonymousCertRequestReq'], using the private key corresponding to the public verification key in CSR certificate from bootstarp resp.
        uiTmpBufLen = HexStr2ByteStr(BN_bn2hex(pSig_g->r), &SignedData[SIGNoffset], 32);
        SIGNoffset += uiTmpBufLen;
        uiTmpBufLen = HexStr2ByteStr(BN_bn2hex(pSig_g->s), &SignedData[SIGNoffset], 32);
        SIGNoffset += uiTmpBufLen;
        printf("ToBeSigned data length = %d\n", SIGNoffset);
        }
        else 
          return FAILURE;
        pSig_g = NULL;
	//SignedData[SIGNoffset] = SHAIK; //EccPublicKeyType+R+S.
	//SIGNoffset += (1+32+32);
//end Signed Message		

	return SIGNoffset;
}

//[NAZEER] key info should be passed to this function by adding argument if need.
uint32_t EncryptedDataIn16092(uint8_t *buff16092, uint8_t *toBeEnc, uint32_t tbeLen)
{
  UINT32 uiRetLen, uiDigestLen, uiCipherLen, i, outlen;
  UINT8 pDigest[32], pCipherData[1024];
  UINT32 uiTagLen = 16, outBufLen,ENCoffset, uiTmpBufLen;
  unsigned char * ciphered = NULL, *original = NULL, *pData = NULL;
  UINT8 pKeyBuf[100], TmpAESSymKey[16], TmpAESSymKeyNonce[12];
  UINT8 * body, *key, *mac;
  UINT8 pTmpBufPub[33], pTmpBufPriv[32];
  UINT8 pPubKeyData[33];
  UINT8 pRAPrivKeyBuf[] = {0x91, 0xA1, 0xFF, 0x64, 0x77, 0x39, 0x76, 0x01, 0xE0, 0x2B, 0xDA, 0xDC, 0xC8, 0xA4, 0xD4, 0xEB, 0x59, 0x0E, 0x41, 0x46, 0x41, 0x8E, 0xD9, 0x45, 0x62, 0x09, 0xF2, 0x82, 0xB0, 0xB4, 0xDE, 0xAE};
  printf("tbeLen = %d\n", tbeLen);
//final .2 format
	ENCoffset = 4; //[NAZEER]to fill length of whole .2 message
	buff16092[ENCoffset] = 2; //protocol_version
	ENCoffset += 1;
	buff16092[ENCoffset] = 2; //encrypted (2)
	ENCoffset += 1;
	
	//EncryptedData(page 73 in d9_3)
	buff16092[ENCoffset] = 0; //SymmAlgorithm->aes_128_ccm (0)
	ENCoffset += 1;
        encode_length(&buff16092[ENCoffset], 8 + 33 + 16 +20, &uiRetLen);
        ENCoffset += uiRetLen;
        if (AWSecHash(HASH_ALGO_SHA256, RACert.certInfo, RACert.certLen, pDigest, &uiDigestLen, 8) == NULL)
        {
          AWSECPRINTF((AWSecHash));
          return FAILURE;
        }

        memcpy(&buff16092[ENCoffset], pDigest, 8);
        ENCoffset += 8;
        /** Copy ephemeral Public Key computed in the ECIES encryption **/

        bzero(pKeyBuf, 100);
        ByteStr2HexStr(&RACert.certInfo[RACert.encryption_key], pKeyBuf, 33);
#ifdef NAZEER
        ByteStr2HexStr(pMyOwnTmpBufPub, pKeyBuf, 33);
        printf("Start of pMyOwnTmpBufPub\n");
        for(i = 0; i < 33; i++)
          printf("0x%02x, ", pMyOwnTmpBufPub[i]);
        printf("End of pMyOwnTmpBufPub\n");
#endif        
        RAND_bytes(TmpAESSymKey, 16);
        RAND_bytes(TmpAESSymKeyNonce, 12);
        printf("Original data TmpAESSymKey\n");
        for(i = 0; i < 16; i++)
          printf("0x%02x, ", TmpAESSymKey[i]);
        printf("End of Original data TmpAESSymKey\n");
        //if(!(ciphered = AWSec_ecies_encrypt(pKeyBuf, TmpAESSymKey, 16, uiTagLen, CIPHER_ALGO_AES128_ECB))) 
        if(!(ciphered = NazNew_ecies_encrypt(&RACert.certInfo[RACert.encryption_key], TmpAESSymKey, 16))) 
        {
          AWSECPRINTF((NazNew_ecies_encrypt));
          return FAILURE;
        }
        else
        {
          printf("Encrypt using ECIES is successful\n");
          printf("Encrypt data length = %d, ephemeral key length = %d, mac len = %d\n", secure_body_length(ciphered), secure_key_length(ciphered), secure_mac_length(ciphered));
        }
        key = (UINT8 *) secure_key_data(ciphered);
        memcpy(&buff16092[ENCoffset], key, secure_key_length(ciphered));
        ENCoffset += secure_key_length(ciphered);
        body = (UINT8 *)secure_body_data(ciphered);
        memcpy(&buff16092[ENCoffset], body, secure_body_length(ciphered));
        ENCoffset += secure_body_length(ciphered);
        mac = (UINT8 *)secure_mac_data(ciphered);
        memcpy(&buff16092[ENCoffset], (mac + 12), 20 /* We need only first 20 bytes of the HMAC SHA256 digest no need to secure_mac_length(ciphered) */);
        ENCoffset += 20;
        {
          if(!(original = NazNew_ecies_decrypt(pRAPrivKeyBuf, key, secure_key_length(ciphered), body, secure_body_length(ciphered), mac, secure_mac_length(ciphered), uiTagLen, CIPHER_ALGO_AES128_ECB, &outlen)))
          {
            AWSECPRINTF((NazNew_ecies_decrypt));
            //return FAILURE;
          }
          else
          {
            printf("Decrypt using ECIES is successful\n");
          }
        }
#ifdef NAZEER
        bzero(pKeyBuf, 100);
        ByteStr2HexStr(pRAPrivKeyBuf, pKeyBuf, 32);
        ByteStr2HexStr(pMyOwnTmpBufPriv, pKeyBuf, 32);
        printf("Start of pMyOwnTmpBufPriv\n");
        for(i = 0; i < 32; i++)
          printf("0x%02x, ", pMyOwnTmpBufPriv[i]);
        printf("End of pMyOwnTmpBufPriv\n");
        memcpy(pPubKeyData, key, secure_key_length(ciphered));
        if(!(original = AWSec_ecies_decrypt(pKeyBuf, key, secure_key_length(ciphered), body, secure_body_length(ciphered), mac, secure_mac_length(ciphered), uiTagLen, CIPHER_ALGO_AES128_ECB)))
        {
          AWSECPRINTF((AWSec_ecies_decrypt));
          return FAILURE;
        }
        else
        {
          printf("Decrypt using ECIES is successful\n");
        }
#endif
        if(AWSecEncrypt(CIPHER_ALGO_AES128_CCM, TmpAESSymKey, TmpAESSymKeyNonce, 12, toBeEnc, tbeLen, pCipherData, &uiCipherLen, 16) != SUCCESS)
        {
          AWSECPRINTF((AWSecEncrypt));
          return FAILURE;
        }
        else
          printf("Encryption successful\n");
        printf("uiCipherLen = %ld\n", uiCipherLen);
        memcpy(&buff16092[ENCoffset], TmpAESSymKeyNonce, 12);
	ENCoffset += 12;
        encode_length(&buff16092[ENCoffset], uiCipherLen, &uiRetLen); 
        ENCoffset += uiRetLen;
        printf("uiRetLen = %d\n", uiRetLen);
        memcpy(&buff16092[ENCoffset], pCipherData, uiCipherLen);
        ENCoffset += uiCipherLen;
        printf("Encode length bytes uiRetLen = %ld, ENCoffset = %d, uiCipherLen = %d\n", uiRetLen, ENCoffset, uiCipherLen);
        if (ciphered == NULL)
          printf("There is some issue with the Ciphered buffer\n");
        if (!ciphered)
          secure_free(ciphered);

	//encode total length into 1st 4bytes
	BUFPUT32(buff16092, ENCoffset - 4);
	return ENCoffset;
}

//[NAZEER]. this function returns length of data after decryption
uint32_t decryptData(uint8_t *ccm, uint8_t *plainData)
{

}

//Request CRL: RA -> LCM (Confirmation, failure)
uint32_t decode_ToBeEncryptedCrlRequestError(uint8_t *ToBeEncryptedCrlRequestError, uint8_t *err)
{
/*struct {
SignerIdentifier signer;
opaque request_hash[10];
CrlRequestErrorCode reason;
Signature signature;
} ToBeEncryptedCrlRequestError;
*/

	Certificate RcvCert;
	uint32_t idx=0,TBS_start=0; //length
	uint32_t len=0,ccm_ciphertext_len=0;

	if(ToBeEncryptedCrlRequestError[idx] == certificate_digest_with_ecdsap256){
		idx += 1;
		//handle digest here
		idx += 8;
	}	
	if(ToBeEncryptedCrlRequestError[idx] == certificate){
		idx += 1;
		len = certParse(&ToBeEncryptedCrlRequestError[idx],&RcvCert);		
		memcpy(RcvCert.certInfo,(ToBeEncryptedCrlRequestError+idx),len);
		RcvCert.certLen = len;
		idx += len;
	}
	TBS_start = idx;
	idx += 10;	
	idx += 1;	
	//caliculate HASH of ToBeSigned data from TBS_start to (idx-TBS_start) for verification
	//signature(1+32(R)+32(s)) which should be verified
	idx +=(1+32+32);
	
	// 10 bytes of CRLrequest_hash, if needed do memcmp
	if(memcmp((ToBeEncryptedCrlRequestError+TBS_start),CRLrequest_hash,10)!=0){
		printf("CRLrequest_hash not matched\n");
		return -1;
	}
	*err = ToBeEncryptedCrlRequestError[TBS_start+10];
	printf("ToBeEncryptedDecryptionKeyRequestError: %d\n",ToBeEncryptedCrlRequestError[TBS_start+10]);
	
	return idx;
}

//Request CRL: RA -> LCM (Confirmation, success)
uint32_t decode_ToBeSignedCrl(uint8_t *ToBeSignedCrl, uint8_t *Err)
{
/*struct {
CrlType type;
CrlSeries crl_series;
CertId8 ca_id;
uint32 crl_serial;
Time32 start_period;
Time32 issue_date;
Time32 next_crl;
Select (type) {
case (id_only):
CertID10 entries<var>;
case (id_and_expiry):
IdAndDate expiring_entries<var>;
// begin new material
case (anonymous_entry):
AnonymousEntry anonymous_crl_entries<var>;
// end new material
Unknown:
Opaque other_entries<var>;
}
} ToBeSignedCrl;
*/

	Certificate RcvCert;
	uint32_t idx=4,TBS_start=0,ll=0; //length
	uint32_t len=0,ccm_ciphertext_len=0;
	uint8_t alg,key_id[8],nonce[12];
	uint8_t *ToBeSignedCrl1=NULL;

	if(ToBeSignedCrl[idx] != 2) //protocol_version
		return -1;
	idx += 1;
	if(ToBeSignedCrl[idx] != 254) //symmetric_encrypted
		return -1;
	idx += 1;
	alg = ToBeSignedCrl[idx];
	idx += 1;

	memcpy(key_id, (ToBeSignedCrl + idx), 8);
	idx += 8;
	memcpy(nonce, (ToBeSignedCrl + idx), 12);
	idx += 12;
	ccm_ciphertext_len = decode_length((ToBeSignedCrl+idx), &len);
	idx += len;

	//[NAZEER]
	/*ciphertext(from (ToBeSignedCrl+idx) to (ToBeSignedCrl+idx+ccm_ciphertext_len)) 
	 * is decrypted with the key indicated by key_id*/
	// assuming decrypted message will be stored in ToBeSignedCrl1. do malloc. use decryptData((ToBeSignedCrl+idx),ToBeSignedCrl1)
	idx = 0;
	if(ToBeSignedCrl1[idx] != crl_req_error){
		idx += 1;
		decode_ToBeEncryptedCrlRequestError((ToBeSignedCrl + idx),Err);
		return 0;
	}
	if(ToBeSignedCrl1[idx] != crl)
		return -1;
	idx += 1;

	idx += 1; //version
	if(ToBeSignedCrl1[idx] == certificate_digest_with_ecdsap256){
		idx += 1;
		//handle digest here
		idx += 8;
	}	
	if(ToBeSignedCrl1[idx] == certificate){
		idx += 1;
		len = certParse(&ToBeSignedCrl1[idx],&RcvCert);		
		memcpy(RcvCert.certInfo,(ToBeSignedCrl1+idx),len);
		RcvCert.certLen = len;
		idx += len;
	}
	TBS_start = idx;
	idx += 29;
	len=0;
	ll = decode_length((ToBeSignedCrl+idx), &len);
	idx += len;
	idx += ll;
	//caliculate HASH of ToBeSigned data from TBS_start to (idx-TBS_start) for verification
	//signature(1+32(R)+32(s)) which should be verified
	idx +=(1+32+32);
	TBS_start += 1; //CrlType
       	if (memcmp(&ToBeSignedCrl1[TBS_start], &crlSeries, 4) != 0) {
       	    printf("%s: ToBeSignedCrl1 crlSeries not matched as per REQ\n");
	    memcpy(&crlSeries,(ToBeSignedCrl1+TBS_start),4);
       	}
	TBS_start += 4;
	//memcmp((ToBeSignedCrl1+idx),ca_id,8);
       	if (memcmp(&ToBeSignedCrl1[TBS_start], &ca_id, 8) != 0) {
       	    printf("%s: ToBeSignedCrl1 crlSeries caID not matched\n");
	    return -1;
       	}
	TBS_start += 8;
	memcpy(&crlSerial,(ToBeSignedCrl1+TBS_start),4);
	TBS_start += 4;
	memcpy(&startPeriod,(ToBeSignedCrl1+TBS_start),4);
	TBS_start += 4;
	memcpy(&issueDate,(ToBeSignedCrl1+TBS_start),4);
	TBS_start += 4;
	memcpy(&lcmStore.nextCrl,(ToBeSignedCrl1+TBS_start),4);
	TBS_start += 4;
	// decode AnonymousEntry;
	TBS_start += len;
	while(ll>0){
		uint32_t i,max_i;
		uint8_t lv1[16],lv2[16];
		len =0;
		memcpy(&i,(ToBeSignedCrl1+(TBS_start+len)),4);
		len += 4;
		memcpy(&lv1,(ToBeSignedCrl1+(TBS_start+len)),16);
		len += 16;
		memcpy(&lv2,(ToBeSignedCrl1+(TBS_start+len)),16);
		len += 16;
		memcpy(&max_i,(ToBeSignedCrl1+(TBS_start+len)),4);
		len += 4;
	
		ll -= len;	
	}

	return idx;
}

//Request CRL: LCM -> RA (Request)
uint32_t encode_ToBeSignedCrlReq(uint8_t *ToBeSignedCrlReq, uint32_t crlSeries, uint8_t *caId, uint32_t crlSerial, uint8_t *reKey)
{
/*struct {
CrlSeries crl_series;
CertId8 ca_id;
uint32 crl_serial;
SymmAlgorithm alg;
uint8 response_encryption_key[16];
} ToBeSignedCrlReq;
*/

	uint32_t idx=0,len=0;
	uint32_t tmp32,uiDigestLen=0;

//start ToBeSignedCrlReq
	if(!BIGENDIAN)
	    tmp32 = swap_32(crlSeries); //later should be changed to htobe32()
	else
	    tmp32 = crlSeries;
	memcpy((ToBeSignedCrlReq + idx), &tmp32, 4);
	idx += 4;
	memcpy((ToBeSignedCrlReq + idx), caId, 8);
	idx += 8;
	if(!BIGENDIAN)
	    tmp32 = swap_32(crlSerial); //later should be changed to htobe32()
	else
	    tmp32 = crlSerial;
	memcpy((ToBeSignedCrlReq + idx), &tmp32, 4);
	idx += 4;
	ToBeSignedCrlReq[idx] = 0; //aes_128_ccm
	memcpy((ToBeSignedCrlReq + idx), reKey, 16);
	idx += 16;
//end ToBeSignedCrlReq

	//cert can be currently valid non-expired anonymous certificate. but .xls sheet it was CSR
	len = signInToBeEncrypted(ToBeSigned,crl_req, certificate, &cert, ToBeSignedCrlReq, idx);
	len = EncryptedDataIn16092(ToBeEncrypted, ToBeSignedCrlReq, len);
  	if (AWSecHash(HASH_ALGO_SHA256, ToBeEncrypted, len, CRLrequest_hash, &uiDigestLen, 10) == NULL)
    	    AWSECPRINTF((AWSecHash));

	return len;
}

//Report Misbehavior: RA -> LCM (Acknowledgement)
uint32_t decode_ToBeSignedMisbehaviorReportAck(uint8_t *ToBeSignedMisbehaviorReportAck)
{
/*struct {
opaque request_hash[10];
} ToBeSignedMisbehaviorReportAck;
*/

	Certificate RcvCert;
	uint32_t idx=4,TBS_start=0; //length
	uint32_t len=0,ccm_ciphertext_len=0;
	uint8_t alg,key_id[8],nonce[12];
	uint8_t *ToBeSignedMisbehaviorReportAck1=NULL;
	
	if(ToBeSignedMisbehaviorReportAck[idx] != 2) //protocol_version
		return -1;
	idx += 1;
	if(ToBeSignedMisbehaviorReportAck[idx] != 254) //symmetric_encrypted
		return -1;
	idx += 1;
	alg = ToBeSignedMisbehaviorReportAck[idx];
	idx += 1;

	memcpy(key_id, (ToBeSignedMisbehaviorReportAck + idx), 8);
	idx += 8;
	memcpy(nonce, (ToBeSignedMisbehaviorReportAck + idx), 12);
	idx += 12;
	ccm_ciphertext_len = decode_length((ToBeSignedMisbehaviorReportAck+idx), &len);
	idx += len;

	//[NAZEER]
	/*ciphertext(from (ToBeSignedMisbehaviorReportAck+idx) to (ToBeSignedMisbehaviorReportAck+idx+ccm_ciphertext_len)) 
	 * is decrypted with the key indicated by key_id*/
	// assuming decrypted message will be stored in ToBeSignedMisbehaviorReportAck1. do malloc. use decryptData((ToBeSignedMisbehaviorReportAck+idx),ToBeSignedMisbehaviorReportAck1)
	idx = 0;
	if(ToBeSignedMisbehaviorReportAck1[idx] != 239) //misbehavior_report_ack
		return -1;
	idx += 1;

	if(ToBeSignedMisbehaviorReportAck1[idx] == certificate_digest_with_ecdsap256){
		idx += 1;
		//handle digest here
		idx += 8;
	}	
	else if(ToBeSignedMisbehaviorReportAck1[idx] == certificate){
		idx += 1;
		len = certParse(&ToBeSignedMisbehaviorReportAck1[idx],&RcvCert);		
		memcpy(RcvCert.certInfo,(ToBeSignedMisbehaviorReportAck1+idx),len);
		RcvCert.certLen = len;
		idx += len;
	}
	TBS_start = idx;
	idx += 10;
	//caliculate HASH of ToBeSigned data from TBS_start to (idx-TBS_start) for verification
	//signature(1+32(R)+32(s)) which should be verified
	idx +=(1+32+32);
	
	// the request_hash field shall be the low-order ten bytes of the SHA-256 hash of the encoded ToBeSignedMisbehaviorReportReq message contained in the report from LCM to RA.

	if(memcmp((ToBeSignedMisbehaviorReportAck1+TBS_start),MBrequest_hash,8)!=10){
		printf("MBrequest_hash not matched\n");
		return -1;
	}
	
	return idx;
}

/* call this function mutiple times for VSC3MisbehaviorReport<var>.
 * USAGE as below to encode variable length of VSC3MisbehaviorReport's
 * int len=0, totalLen=0;
 * uint8_t vsc3buff[1024];
 * len = encode_VSC3MisbehaviorReport((vsc3Buff+totalLen), ................); // for 1st VSC3MisbehaviorReport
 * totalLen += len;
 * len =0;
 * len = encode_VSC3MisbehaviorReport((vsc3Buff+totalLen), ................); // for 2nd VSC3MisbehaviorReport
 * totalLen += len;
 * after encoding required no.of VSC3MisbehaviorReport like above call as below to make final report:
 * encode_ToBeSignedMisbehaviorReportReq(ToBeSignedMisbehaviorReportReq, vsc3Buff, totalLen);
 */
uint32_t encode_VSC3MisbehaviorReport(uint8_t *vsc3mr, ThreeDLocation *tdl, Time64WithConfidence *t64wc, uint8_t *dot2msgs, uint32_t dot2Len, uint8_t rptCategory)
{
/*struct {
uint8 version;
ThreeDLocation observation_location;
Time64WithConfidence observation_time;
MisbehaviorReportCategory misbehavior_report_category;
1609Dot2Message misbehavior_report_entries<var>;
} VSC3MisbehaviorReport;
*/

	uint32_t idx=0,len=0;

//start ToBeSignedMisbehaviorReportReq
	vsc3mr[idx] = 1; //version
	idx += 1;
	memcpy((vsc3mr+idx),tdl,6);
	idx += 6;
	memcpy((vsc3mr+idx),t64wc,9);
	idx += 9;
	vsc3mr[idx] = rptCategory; //MisbehaviorReportCategory
	idx += 1;
	encode_length((vsc3mr+idx), dot2Len, &len);
	idx += len;
	memcpy((vsc3mr+idx),dot2msgs,dot2Len);
	idx += dot2Len;

	return idx;
}

//Report Misbehavior: LCM -> RA (Report)
uint32_t encode_ToBeSignedMisbehaviorReportReq(uint8_t *ToBeSignedMisbehaviorReportReq, uint8_t *vscBuff, uint32_t vsc3Len)
{
/*struct {
opaque misbehavior_report<var>;
SymmAlgorithm alg;
uint8 response_encryption_key[16];
} ToBeSignedMisbehaviorReportReq;
enum {
casual_report(0),
alert_related_report(1),
suspicious_message(2),
� (255)
} MisbehaviorReportCategory;
struct {
uint8 version;
ThreeDLocation observation_location;
Time64WithConfidence observation_time;
MisbehaviorReportCategory misbehavior_report_category;
1609Dot2Message misbehavior_report_entries<var>;
} VSC3MisbehaviorReport;

*/
	uint32_t idx=0,len=0;
	UINT32 uiDigestLen=0;

//start ToBeSignedMisbehaviorReportReq
	// Encode variable no.of VSC3MisbehaviorReport.

	encode_length((ToBeSignedMisbehaviorReportReq+idx), vsc3Len, &len);
	idx += len;
	memcpy((ToBeSignedMisbehaviorReportReq+idx),vscBuff,vsc3Len);
	idx += vsc3Len;	
	ToBeSignedMisbehaviorReportReq[idx] = 0; //aes_128_ccm
	idx += 1;
	memcpy((ToBeSignedMisbehaviorReportReq + idx), response_encryption_key, 16);
	idx += 16;
//end ToBeSignedMisbehaviorReportReq
  	if (AWSecHash(HASH_ALGO_SHA256, ToBeSignedMisbehaviorReportReq, idx, MBrequest_hash, &uiDigestLen, 10) == NULL)
    	    AWSECPRINTF((AWSecHash));

	//cert can be short-term certificate if available, otherwise fall-back certificate
	len = signInToBeEncrypted(ToBeSigned,misbehavior_report_req, certificate, &CSRCert, ToBeSignedMisbehaviorReportReq, idx);
	len = EncryptedDataIn16092(ToBeEncrypted, ToBeSigned, len);

	return len;
}

//Request Decryption Keys: LCM -> RA (Acknowledgement)
uint32_t encode_ToBeSignedAnonymousCertDecryptionKeyAck(uint8_t *ToBeSignedAnonymousCertDecryptionKeyAck)
{
/*struct {
uint8 cert_batch_id[8];
uint8 decryption_key[16];
} ToBeSignedAnonymousCertDecryptionKeyAck;
*/

	uint32_t idx=0,len=0;

//start ToBeSignedAnonymousCertDecryptionKeyAck
	memcpy((ToBeSignedAnonymousCertDecryptionKeyAck + idx), cert_batch_id, 8);
	idx += 8;
	memcpy((ToBeSignedAnonymousCertDecryptionKeyAck + idx), decryption_key, 16);
	idx += 16;
//end ToBeSignedAnonymousCertDecryptionKeyAck
	len = signInToBeEncrypted(ToBeSigned,anonymous_cert_decryption_key_ack, certificate, &CSRCert, ToBeSignedAnonymousCertDecryptionKeyAck, idx);
	len = EncryptedDataIn16092(ToBeEncrypted, ToBeSigned, len);

	return len;
}

//Request Decryption Keys: RA -> LCM (Confirm, failure)
uint32_t decode_ToBeEncryptedDecryptionKeyRequestError(uint8_t *ToBeEncryptedDecryptionKeyRequestError, uint8_t *err)
{
/*struct {
SignerIdentifier signer;
opaque cert_batch_id[8];
CertificateRequestErrorCode reason;
Signature signature;
} ToBeEncryptedDecryptionKeyRequestError;
*/
		
	Certificate RcvCert;
	uint32_t idx=0,TBS_start=0; //length
	uint32_t len=0;
#if 0	
	if(ToBeEncryptedDecryptionKeyRequestError[idx] != 2) //protocol_version
		return -1;
	idx += 1;
	if(ToBeEncryptedDecryptionKeyRequestError[idx] != 254) //symmetric_encrypted
		return -1;
	idx += 1;
	alg = ToBeEncryptedDecryptionKeyRequestError[idx];
	idx += 1;

	memcpy(key_id, (ToBeEncryptedDecryptionKeyRequestError + idx), 8);
	idx += 8;
	memcpy(nonce, (ToBeEncryptedDecryptionKeyRequestError + idx), 12);
	idx += 12;
	ccm_ciphertext_len = decode_length((ToBeEncryptedDecryptionKeyRequestError+idx), &len);
	idx += len;

	//[NAZEER]
	/*ciphertext(from (ToBeEncryptedDecryptionKeyRequestError+idx) to (ToBeEncryptedDecryptionKeyRequestError+idx+ccm_ciphertext_len)) 
	 * is decrypted with the key indicated by key_id*/
	// assuming decrypted message will be stored in SymmetricEncryptedMessage
	//after decryption, call encode_ToBeSignedAnonymousCertResponseAck(uint8_t *) and send it to RA
	idx = 0;
	if(ToBeEncryptedDecryptionKeyRequestError[idx] != 252) //anonymous_cert_decryption_key_error
		return -1;
	idx += 1;
#endif
	if(ToBeEncryptedDecryptionKeyRequestError[idx] == certificate_digest_with_ecdsap256){
		idx += 1;
		//handle digest here
		idx += 8;
	}	
	if(ToBeEncryptedDecryptionKeyRequestError[idx] == certificate){
		idx += 1;
		len = certParse(&ToBeEncryptedDecryptionKeyRequestError[idx],&RcvCert);		
		memcpy(RcvCert.certInfo,(ToBeEncryptedDecryptionKeyRequestError+idx),len);
		RcvCert.certLen = len;
		idx += len;
	}
	TBS_start = idx;
	idx += 8;	
	idx += 1;	
	//caliculate HASH of ToBeSigned data from TBS_start to (idx-TBS_start) for verification
	//signature(1+32(R)+32(s)) which should be verified
	idx +=(1+32+32);
	
	// 8 bytes of cert_batch_id, if needed do memcmp
	if(memcmp((ToBeEncryptedDecryptionKeyRequestError+TBS_start),cert_batch_id,8)!=0){
		printf("cert_batch_id not matched\n");
		return -1;
	}
	*err = ToBeEncryptedDecryptionKeyRequestError[TBS_start+8];
	printf("ToBeEncryptedDecryptionKeyRequestError: %d\n",ToBeEncryptedDecryptionKeyRequestError[TBS_start+8]);
	
	return idx;
}

//Request Decryption Keys: RA -> LCM (Confirm, success)
uint32_t decode_AnonymousCertDecryptionKeyCfm(uint8_t *AnonymousCertDecryptionKeyCfm, uint8_t *Err)
{
/*struct {
uint8 cert_batch_id[8];
SymmAlgorithm alg;
uint8 decryption_key[16];
} AnonymousCertDecryptionKeyCfm;
*/

	uint32_t idx=4; //length
	uint32_t len=0,ccm_ciphertext_len=0;
	uint8_t alg,key_id[8],nonce[12];
	uint8_t *AnonymousCertDecryptionKeyCfm1=NULL;
	
	if(AnonymousCertDecryptionKeyCfm[idx] != 2) //protocol_version
		return -1;
	idx += 1;
	if(AnonymousCertDecryptionKeyCfm[idx] != 254) //symmetric_encrypted
		return -1;
	idx += 1;
	alg = AnonymousCertDecryptionKeyCfm[idx];
	idx += 1;

	memcpy(key_id, (AnonymousCertDecryptionKeyCfm + idx), 8);
	idx += 8;
	memcpy(nonce, (AnonymousCertDecryptionKeyCfm + idx), 12);
	idx += 12;
	ccm_ciphertext_len = decode_length((AnonymousCertDecryptionKeyCfm+idx), &len);
	idx += len;

	//[NAZEER]
	/*ciphertext(from (AnonymousCertDecryptionKeyCfm+idx) to (AnonymousCertDecryptionKeyCfm+idx+ccm_ciphertext_len)) 
	 * is decrypted with the key indicated by key_id*/
	// assuming decrypted message will be stored in AnonymousCertDecryptionKeyCfm1. do malloc. use decryptDat((AnonymousCertDecryptionKeyCfm+idx),AnonymousCertDecryptionKeyCfm1)
	idx = 0;
	if(AnonymousCertDecryptionKeyCfm1[idx] != anonymous_cert_decryption_key_error){ //anonymous_cert_decryption_key_error
		idx += 1;
		decode_ToBeEncryptedDecryptionKeyRequestError((AnonymousCertDecryptionKeyCfm1 + idx), Err);
		return 0;
	}
	if(AnonymousCertDecryptionKeyCfm1[idx] != 251) //anonymous_cert_decryption_key_cfm
		return -1;
	idx += 1;

	// 8 bytes of cert_batch_id, if needed do memcmp
	if(memcmp((AnonymousCertDecryptionKeyCfm1+idx),cert_batch_id,8)!=0){
		printf("cert_batch_id not matched\n");
		return -1;
	}
	idx += 8;
	alg = AnonymousCertDecryptionKeyCfm1[idx];
	idx += 1;
	memcpy(decryption_key, (AnonymousCertDecryptionKeyCfm1 + idx), 16);
	idx += 16;
	
	return idx;
}

//Request Decryption Keys: LCM -> RA (Request)
uint32_t encode_ToBeSignedAnonymousCertDecryptionKeyReq(uint8_t *ToBeSignedAnonymousCertDecryptionKeyReq, uint8_t status)
{
/*struct {
Time32 current_time;
CertificateProcessingStatus status;
SymmAlgorithm alg;
uint8 response_encryption_key[16];
uint8 cert_batch_id[8];
} ToBeSignedAnonymousCertDecryptionKeyReq;
*/
	uint32_t idx=0,len=0;
	Time32 tmp32, current_time;

//start ToBeSignedAnonymousCertDecryptionKeyReq
  	tmp32 = get_cur_time2004();
	if(!BIGENDIAN)
	    current_time = swap_32(tmp32); //later should be changed to htobe32()
        //epoch_time_from_2004(current_time,"2012-11-17","01:00:00");
	memcpy((ToBeSignedAnonymousCertDecryptionKeyReq + idx), &current_time, 4);
	idx += 4;

	ToBeSignedAnonymousCertDecryptionKeyReq[idx] = status; //first_request(1)
	idx += 1;
	ToBeSignedAnonymousCertDecryptionKeyReq[idx] = 0; //aes_128_ccm
	idx += 1;
	
	//[NAZEER]response_encryption_key shall contain the AES key that shall be used to encrypt the response. This shall be different for every AnonymousCertDecryptionKeyReq from a given sender.
	memcpy((ToBeSignedAnonymousCertDecryptionKeyReq + idx), response_encryption_key, 10);
	idx += 10;
	
	memcpy((ToBeSignedAnonymousCertDecryptionKeyReq + idx), cert_batch_id, 8);
	idx += 8;
//end ToBeSignedAnonymousCertDecryptionKeyReq
	len = signInToBeEncrypted(ToBeSigned,anonymous_cert_decryption_key_req, certificate, &CSRCert, ToBeSignedAnonymousCertDecryptionKeyReq, idx);
	len = EncryptedDataIn16092(ToBeEncrypted, ToBeSigned, len);
	
	return len;
}

//Request Certificates: LCM -> RA (Status Confirm, failure)
uint32_t decode_ToBeEncryptedCertificateRequestError(uint8_t *ToBeEncryptedCertificateRequestError, uint8_t *err)
{

/*struct {
SignerIdentifier signer1 ;
opaque request_hash[10];
CertificateRequestErrorCode reason;
Signature signature;
} ToBeEncryptedCertificateRequestError;
*/
		
	Certificate RcvCert;
	uint32_t idx=0,TBS_start=0; //length
	uint32_t len=0;
#if 0	
	if(ToBeEncryptedCertificateRequestError[idx] != 2) //protocol_version
		return -1;
	idx += 1;
	if(ToBeEncryptedCertificateRequestError[idx] != 254) //symmetric_encrypted
		return -1;
	idx += 1;
	alg = ToBeEncryptedCertificateRequestError[idx];
	idx += 1;

	memcpy(key_id, (ToBeEncryptedCertificateRequestError + idx), 8);
	idx += 8;
	memcpy(nonce, (ToBeEncryptedCertificateRequestError + idx), 12);
	idx += 12;
	ccm_ciphertext_len = decode_length((ToBeEncryptedCertificateRequestError+idx), &len);
	idx += len;

	//[NAZEER]
	/*ciphertext(from (ToBeEncryptedCertificateRequestError+idx) to (ToBeEncryptedCertificateRequestError+idx+ccm_ciphertext_len)) 
	 * is decrypted with the key indicated by key_id*/
	// assuming decrypted message will be stored in SymmetricEncryptedMessage
	//after decryption, call encode_ToBeSignedAnonymousCertResponseAck(uint8_t *) and send it to RA
	idx = 0;
	if(ToBeEncryptedCertificateRequestError[idx] != 6) //certificate_request_error
		return -1;
	idx += 1;
#endif
	if(ToBeEncryptedCertificateRequestError[idx] == certificate_digest_with_ecdsap256){
		idx += 1;
		//handle digest here
		idx += 8;
	}	
	if(ToBeEncryptedCertificateRequestError[idx] == certificate){
		idx += 1;
		len = certParse(&ToBeEncryptedCertificateRequestError[idx],&RcvCert);		
		memcpy(RcvCert.certInfo,(ToBeEncryptedCertificateRequestError+idx),len);
		RcvCert.certLen = len;
		idx += len;
	}
	TBS_start = idx;
	idx += 10; //request_hash
	idx += 1;
	
	//caliculate HASH of ToBeSigned data from TBS_start to (idx-TBS_start) for verification
	//signature(1+32(R)+32(s)) which should be verified
	idx +=(1+32+32);

	//[NAZEER]
	/* request_hash is the low-order ten bytes of the SHA-256 hash of the 
	 * encoded ToBeSignedAnonymousCertRequestReq message contained in the request from the LCM to the RA.
	 */
	//from ToBeEncryptedCertificateRequestError[TBS_start] to ToBeEncryptedCertificateRequestError[TBS_start+10] is request_hash. we can do memcmp this hash and saved hash
        if (memcmp(&ToBeEncryptedCertificateRequestError[TBS_start], CRrequest_hash, 10) != 0){
          printf("%s: ToBeEncryptedCertificateRequestError not matched CRrequest_hash\n");
	  return -1;
        }
	TBS_start += 10;
	*err = ToBeEncryptedCertificateRequestError[TBS_start];
	printf("ToBeEncryptedCertificateRequestError: %d\n",ToBeEncryptedCertificateRequestError[TBS_start]);
	//NAZEER
	/* signature is the responding CA�s signature over the concatenation of the request_hash and
	 *  reason fields.
	 */
	
	
	return idx;
}

//Request Certificates: LCM -> RA (Status Confirm, Acknowledgement)
uint32_t encode_ToBeSignedAnonymousCertResponseAck(uint8_t *ToBeSignedAnonymousCertResponseAck)
{
/*struct {
opaque request_hash[10];
opaque response_hash[10];
} ToBeSignedAnonymousCertResponseAck;
*/
	uint32_t idx=0,len=0;

//start ToBeSignedAnonymousCertResponseAckq
	//low-order ten bytes of the SHA-256 hash of the encoded ToBeSignedAnonymousCertRequestReq
	memcpy((ToBeSignedAnonymousCertResponseAck + idx), CRrequest_hash, 10);
	idx += 10;
	
	//[NAZEER]low-order ten bytes of the SHA-256 hash of the encoded AnonymousCertRequestStatusCfm
	memcpy((ToBeSignedAnonymousCertResponseAck + idx), CRresponse_hash, 10);
	idx += 10;
//end ToBeSignedAnonymousCertResponseAck

	len = signInToBeEncrypted(ToBeSigned,anonymous_cert_response_ack, certificate, &CSRCert, ToBeSignedAnonymousCertResponseAck, idx);
	len = EncryptedDataIn16092(ToBeEncrypted, ToBeSigned, len);

	return len;
}

//[HITESH]
//if(lcmStore.SLClen != 0) memcpy(cert_batch_id, (pDec1st + lcmStore.cfmOffset), 8);
//else stop DK_THREAD & mapFile(decFile, pDec1st,1) and rm -rf dec1st;
		//request for decrypt key by calling encode_ToBeSignedAnonymousCertDecryptionKeyReq()
		// wait for response tcp_recv()
		// decode response by calling decode_AnonymousCertDecryptionKeyCfm()
		// and the call below function with pDec1st param
uint32_t decrypt_certificates(uint8_t *TBEAnonymousCertRequestStatusCfm, uint8_t *Err)
{
#if 0
EncryptedCertificateBatch short_lived_certs<var>;

struct {
uint8 cert_batch_id[8];
Time32 start_time;
Time32 end_time;
uint32 validity_period;
uint32 overlap_period;
AesCcmCiphertext enc_batch;3
} EncryptedCertificateBatch;

struct {
uint32 i;
uint32 j;
Time32 expiration;
CertificateDuration lifetime;
EncryptedMessage enc_cert;
} ToBeSignedEncCert;

struct {
Certificate cert;
uint8 s[32];
} CertificateAndPrivKeyReconstructionValue;

struct {
int32 i;
int32 j;
opaque cert_id[8];
} AnonymousRevocationInformation;

#endif
	Certificate RcvCert;
	uint8_t nonce[12];
	uint32_t ccm_ciphertext_len1=0;
	uint32_t len=0,ll=0,dec2_len=0;
	uint8_t *CertificateAndPrivKeyReconstructionValue = NULL;
	uint8_t capkrv_len = 0;
	uint8_t *ToBeSignedEncCert = NULL;
	uint32_t sem=0; //count for SymmetricEncryptedMessage
	UINT32 uiDigestLen=0;
	char cmd[50];
	uint32_t validity_period, overlap_period;
	Time32 stime,etime;

	if(lcmStore.SMlen == 0){
		sem = lcmStore.cfmOffset;
		sem += 8; //cert_batch_id
		memcpy(&stime, (TBEAnonymousCertRequestStatusCfm + sem), 4);
		sem += 4;
		memcpy(&etime, (TBEAnonymousCertRequestStatusCfm + sem), 4);
		sem += 4;
		memcpy(&validity_period, (TBEAnonymousCertRequestStatusCfm + sem), 4);
		sem += 4;
		memcpy(&overlap_period, (TBEAnonymousCertRequestStatusCfm + sem), 4);
		sem += 4;
		if(lcmoptions.Request_Decryption_Key_Time != 0)
			lcmStore.DkReqTime = (etime-lcmoptions.Request_Decryption_Key_Time-(lcmoptions.Request_Decryption_Key_Time/5));

		memcpy(nonce, (TBEAnonymousCertRequestStatusCfm + sem), 12);
		sem += 12;
		ll = 0;
		ccm_ciphertext_len1 = decode_length((TBEAnonymousCertRequestStatusCfm+sem), &ll);
		sem += ll;
		sem += ccm_ciphertext_len1;
		lcmStore.SLClen -= (8+4+4+4+4+12+ll+ccm_ciphertext_len1);
		lcmStore.cfmOffset = sem;
		createZeroFile(dec2nd,ccm_ciphertext_len1);
		mapFile(dec2nd, pDec2nd,0);
		//with help of (decryption_key) dectypt this cert_batch_id -> EncryptedCertificateBatch.enc_batch	
		//fill the dec2_len, with len of data after decrypt. which is may be ccm_ciphertext_len1 - 16
		//the AesCcmCiphertext enc_batch shall contain a ToBeEncrypted of type sig_enc_cert and the contents shall be a collection of SignedMessages Each SignedMessage shall contain a single ToBeSignedEncCert.
		ToBeSignedEncCert = pDec2nd;
		// assuming decrypted message will be stored in ToBeSignedEncCert. use decryptData((TBEAnonymousCertRequestStatusCfm+sem),ccm_ciphertext_len1,ToBeSignedEncCert).
		// On success send ack by calling encode_ToBeSignedAnonymousCertDecryptionKeyAck()	
		if(ToBeSignedEncCert[0] != sig_enc_cert)
			return -1;
		ll = 0;
		lcmStore.SMlen = decode_length((ToBeSignedEncCert+1), &ll); //SignedMessages<var>
		lcmStore.batchOffset = ll + 1; //  offset 1 for content type
	}
		while(lcmStore.SMlen > 0) //handling SignedMessages
		{
			uint32_t idx=0,TBS_start=0,ENCidx=0;
			uint32_t i;//[VAMSI] need to decide how to handle 2,4bytes values
			uint32_t j;
			Time32 expiration;
			CertificateDuration lifetime;
		
			idx = lcmStore.batchOffset;
			if(ToBeSignedEncCert[idx] == certificate_digest_with_ecdsap256){
				idx += 1;
				//handle digest here
				idx += 8;
			}	
			else if(ToBeSignedEncCert[idx] == certificate){
				idx += 1;
				len = certParse(&ToBeSignedEncCert[idx],&RcvCert);		
				memcpy(RcvCert.certInfo,(ToBeSignedEncCert+idx),len);
				RcvCert.certLen = len;
				idx += len;
			}
			//verify data from ToBeSignedEncCert[idx] to ToBeSignedEncCert[sizeof(EncryptedData.SymmAlgorithm)+sizeof(EncryptedData.RecipientInfo)+sizeof(EncryptedData.AesCcmCiphertext)]
			TBS_start = idx;
			memcpy(&i, (ToBeSignedEncCert + idx), 4);
			idx += 4;
			memcpy(&j, (ToBeSignedEncCert + idx), 4);
			idx += 4;
			memcpy(&expiration, (ToBeSignedEncCert + idx), 4);
			idx += 4;
			memcpy(&lifetime, (ToBeSignedEncCert + idx), 2);
			idx += 2;
			ENCidx = idx;
			len = 0;
			len = retLen_EncryptedData((ToBeSignedEncCert+idx));
			idx += len;
			//caliculate HASH of ToBeSigned data from TBS_start index to length (idx-TBS_start) for verification
			idx += (1+32+32); //signature
			decrypt_EncryptedData((ToBeSignedEncCert+ENCidx), CertificateAndPrivKeyReconstructionValue,&capkrv_len);
		

			//encode PrivateKeyAndCertificate and encrypt it.
			//add to .crt file
			lcmStore.batchOffset = idx;	
			lcmStore.SMlen -= idx;
		}
		lcmStore.batchOffset = 0;	
		lcmStore.SMlen = 0;	
	if(appStartFlag){
		appStartFlag = 0;
		//do appstart here
	}
	return sem;
}

//[HITESH]mapFile(dec1st, pDec1st,0);
//call below function with pDec1st param
//Request Certificates: LCM -> RA (Status Confirm, success)
uint32_t decode_statusCnfm(uint8_t *SymmetricEncryptedMessage,uint8_t *Err)
{
#if 0
struct {
SymmAlgorithm alg;
opaque key_id[8];
AesCcmCiphertext ciphertext;
} SymmetricEncryptedMessage;

struct {
opaque request_hash[10];
PsidSspArray permissions;
GeographicRegion region;
EncryptedCertificateBatch short_lived_certs<var>;
UnencryptedCertificateBlock fallback_certs;
} AnonymousCertRequestStatusCfm;
#endif
	uint8_t nonce[12],key_id[8],alg=0;
	uint32_t ccm_ciphertext_len1=0,psid_array_premissionslen=0;
	uint32_t len=0;
	uint8_t *TBEAnonymousCertRequestStatusCfm = NULL;
	uint32_t sem=0; //count for SymmetricEncryptedMessage
	UINT32 uiDigestLen=0;
	char cmd[50];
	uint32_t fd;
    	struct stat sbuf;
	
	
	//SymmetricEncryptedMessage[0 t0 3] is length
	sem = 256+4; //since we left 0 to 255 bytes blank in file + 4bytes len
	if(SymmetricEncryptedMessage[sem] != 2) //protocol_version
		return -1;
	sem += 1;
	if(SymmetricEncryptedMessage[sem] != 254) //symmetric_encrypted
		return -1;
	sem += 1;
	alg = SymmetricEncryptedMessage[sem];
	sem += 1;
	
	/*key_id is the low-order 8 bytes of the SHA-256 hash of the key used to encrypt
	the message. This shall be the response_encryption_key from the corresponding
	ToBeSignedAnonymousCertRequestStatusReq.*/
	memcpy(key_id, (SymmetricEncryptedMessage + sem), 8);
	sem += 8;
	memcpy(nonce, (SymmetricEncryptedMessage + sem), 12);
	sem += 12;
	ccm_ciphertext_len1 = decode_length((SymmetricEncryptedMessage+sem), &len);
	sem += len;

	//[NAZEER]
	/*ciphertext(from (SymmetricEncryptedMessage+sem) to (SymmetricEncryptedMessage+sem+ccm_ciphertext_len1)) 
	 * is decrypted with the key indicated by key_id, the result shall be a ToBeEncrypted with type 
	 * anonymous_cert_request_status_cfm and contents equal to a message of type AnonymousCertRequestStatusCfm*/
	//pass SymmetricEncryptedMessage pointer to store decrypted data.
	TBEAnonymousCertRequestStatusCfm = SymmetricEncryptedMessage; 
	// assuming decrypted message will be stored in TBEAnonymousCertRequestStatusCfm=SymmetricEncryptedMessage. store length in len. use 
	//     int decryptData(SymmetricEncryptedMessage+sem),ccm_ciphertext_len1,SymmetricEncryptedMessage) which return len;
	//after decryption, call
	//encode_ToBeSignedAnonymousCertResponseAck(tbsacra);
	TBEAnonymousCertRequestStatusCfm = SymmetricEncryptedMessage;
	sem = 0;

	if(TBEAnonymousCertRequestStatusCfm[sem] == 6) //certificate_request_error(6)
	{
		sem += 1;
		decode_ToBeEncryptedCertificateRequestError((TBEAnonymousCertRequestStatusCfm + sem), Err);
		return 0;
	}
	else if(TBEAnonymousCertRequestStatusCfm[sem] != 246) //anonymous_cert_request_status_cfm
		return -1;
	sem += 1;
	//[NAZEER] caliculate hash from TBEAnonymousCertRequestStatusCfm[sem] to (len-1) and store it in CRresponse_hash.
  	if (AWSecHash(HASH_ALGO_SHA256, (TBEAnonymousCertRequestStatusCfm+sem), len-1, CRresponse_hash, &uiDigestLen, 10) == NULL)
    	    AWSECPRINTF((AWSecHash));
	// TBEAnonymousCertRequestStatusCfm[sem] to TBEAnonymousCertRequestStatusCfm[sem+10] is request_hash. we can do memcmp this hash and saved hash
        if (memcmp(&TBEAnonymousCertRequestStatusCfm[sem], CRrequest_hash, 10) != 0){
          printf("%s: TBEAnonymousCertRequestStatusCfm not matched CRrequest_hash\n");
	  return -1;
        }
	sem += 10;
	if(lcmoptions.Request_Certificates_Time != 0)
		lcmStore.certReqTime = (end_time-lcmoptions.Request_Certificates_Time-(lcmoptions.Request_Certificates_Time/5));
	// At this point of time we dont need PsidSspArray & GeographicRegion
	sem += 1; //ArrayType
	{ //skiping variable length of PsidSsp
                len=0;
                psid_array_premissionslen= decode_length((TBEAnonymousCertRequestStatusCfm + sem), &len);
                sem +=len;
                sem += psid_array_premissionslen;
        }
	sem += 1; //GeographicRegion. usually it none
	
	len = 0;
	lcmStore.SLClen = decode_length((TBEAnonymousCertRequestStatusCfm + sem), &len);
        sem +=len;
	lcmStore.cfmOffset = sem;

	return sem;
}

//Request Certificates: LCM -> RA (Status Request)ToBeEncryptedCertificateRequestError
uint32_t encode_ToBeSignedAnonymousCertRequestStatusReq(uint8_t *ToBeSignedAnonymousCertRequestStatusReq, uint8_t status)
{
/*struct {
Time32 current_time;
CertificateProcessingStatus status;
opaque request_hash[10];
SymmAlgorithm alg;
opaque response_encryption_key [16];
} ToBeSignedAnonymousCertRequestStatusReq;
enum { success (0), first_request(1), got_certs_not_ready(2),
timeout(3), err_decrypt (4), .. (255)
} CertificateProcessingStatus; */

	uint32_t idx=0,len=0;
	Time32 tmp32, current_time;

//start ToBeSignedAnonymousCertRequestStatusReq
	//the current_time field shall contain the current time.
  	tmp32 = get_cur_time2004();
	if(!BIGENDIAN)
	    current_time =swap_32(tmp32); //later should be changed to htobe32()
	memcpy((ToBeSignedAnonymousCertRequestStatusReq + idx), &current_time, 4);
	idx += 4;
	
	ToBeSignedAnonymousCertRequestStatusReq[idx] = status; //first_request(1)
	idx += 1;
	
	//low-order ten bytes of the SHA-256 hash of the encoded ToBeSignedAnonymousCertRequestReq
	memcpy((ToBeSignedAnonymousCertRequestStatusReq + idx), CRrequest_hash, 10);
	idx += 10;
	
	ToBeSignedAnonymousCertRequestStatusReq[idx] = 0; //SymmAlgorithm->aes_128_ccm (0)
	idx += 1;

	//the response_encryption_key field shall contain the AES-CCM key to be used to encrypt the response. The LCM shall generate a fresh response encryption key every time it generates a ToBeSignedAnonymousCertRequestStatusReq and shall not (or shall only with negligible probability) repeat response encryption keys.
	memcpy((ToBeSignedAnonymousCertRequestStatusReq + idx), response_encryption_key, 16);
	idx += 16;
//end ToBeSignedAnonymousCertRequestStatusReq
	len = signInToBeEncrypted(ToBeSigned,anonymous_cert_request_status_req, certificate, &CSRCert, ToBeSignedAnonymousCertRequestStatusReq, idx);
	len = EncryptedDataIn16092(ToBeEncrypted, ToBeSigned, len);

	return len;
}

//Request Certificates: LCM -> RA (Confirm)
uint32_t decode_AnonymousCertRequestConfirm(uint8_t *AnonymousCertRequestConfirm, uint8_t *err)
{
/*struct {
uint8 accept; // shld be 1 for success
Time32 request_time;
opaque request_hash[10]; //low-order ten bytes of the SHA-256 hash of the encoded ToBeSignedAnonymousCertRequestReq
select(accept) {
case 0:
CertificateRequestErrorCode reason;
}
} AnonymousCertRequestConfirm*/
	
	Certificate RcvCert;
	uint32_t len = 0;
	uint32_t idx = 4; //crtReqCfm[0 to 3] is length
	uint32_t TBS_start=0;

	idx += 1;//crtReqCfm[4] is protocol_version
	if(AnonymousCertRequestConfirm[idx] != 244) //anonymous_cert_request_cfm
		return -1;
	idx += 1;

	//SignerIdentifierType. [NAZEER] As per my understanding this wiill be certificate_digest_with_ecdsap256, in that case next 8byte (from AnonymousCertRequestConfirm[7]) are certID8 Digest of RA certificate. if it is "certificate" then from AnonymousCertRequestConfirm[7] will have RA certificate, i dnt think this is the case since we hv whole RA certificate which is having publickey since it is explicit cert.
	if(AnonymousCertRequestConfirm[idx] == certificate_digest_with_ecdsap256){
		idx += 1;
		//handle digest here
		idx += 8;
	}	
	if(AnonymousCertRequestConfirm[idx] == certificate){
		idx += 1;
		len = certParse(&AnonymousCertRequestConfirm[idx],&RcvCert);		
		memcpy(RcvCert.certInfo,(AnonymousCertRequestConfirm+idx),len);
		RcvCert.certLen = len;
		idx += len;
	}
	TBS_start = idx;	
	if(AnonymousCertRequestConfirm[idx] != 1){
		printf("AnonymousCertRequestConfirm Err: %d\n",(uint8_t)AnonymousCertRequestConfirm[idx+1+4+10]);
		err = AnonymousCertRequestConfirm[idx+1+4+10];
		return -1;
	}
	idx += 1;
	//AnonymousCertRequestConfirm[idx] will have reuest_time, if necessary we can do memcmp
        if (memcmp(&AnonymousCertRequestConfirm[idx], &request_time, 4) != 0) {
          printf("%s: AnonymousCertRequestConfirm time not matched with request_time\n");
	  return -1;
        }
	idx += 4;
	//request_hash is the low-order 10 bytes of the SHA-256 hash of the encoded ToBeSignedAnonymousCertRequestReq contained in the request from the OBE to the RA. if need do memcmp
        if (memcmp(&AnonymousCertRequestConfirm[idx], CRrequest_hash, 10) != 0){
          printf("%s: AnonymousCertRequestConfirm not matched CRrequest_hash\n");
	  return -1;
        }
	idx += 10;
	//caliculate HASH of ToBeSigned data from TBS_start to (idx-TBS_start) for verification
       	if(VerifySignedData(&RACert.certInfo[RACert.verification_key], (AnonymousCertRequestConfirm + idx + 1), &AnonymousCertRequestConfirm[TBS_start], 32) != SUCCESS){
        	AWSECPRINTF((VerifySignedData));
		printf("Signature verification failed\n");
	  	return -1;
	}
	//signature(1+32(R)+32(s)) which should be verified
	idx += (1+32+32);
	
	return idx;	
}

uint32_t encode_ToBeSignedAnonymousCertRequestReq(uint8_t *ToBeSignedAnonymousCertRequestReq /*uint8_t *psidSspArray, Time32 start_time, Time32 end_time, CertificateDuration batch_duration, uint32 storage_space_kb*/)
{
#if VAMSI // Use global variables start_time, end_time, batch_duration, storage_space_kb filled from LCM.conf file
  uint8_t start_date[4]={0x10,0xb7,0x64,0xde}; //11-20-2012,00:00:00
  uint8_t end_date[4]={0x10,0xde,0xf1,0xde}; // 30days from start_date. 12-20-2012,00:00:00
  uint8_t duration[4]={0x42,0xd0}; //duration time 30days: first three bits represents interpretation (010=hours)
  uint8_t space[4]={0x00,0x00,0x0c,0x00}; //space in KB's: 3MB
#endif
  Time32 current_time,tmp32,start_time; /* ={0x10,0xad,0x34,0xe6}; */ //caliculate current time: HEXof(current epochtime - 1072915234)
  uint16_t tmp16;
  uint8_t certid[8]={0x7c,0xfa,0x71,0xfe,0xb7,0x12,0x69,0x8b}; //certID8 of root_ca 
  uint32_t ACRoffset=0,SIGNoffset=0,ENCoffset=0;
  //VAMSI-comment/remove below 3 lines later
  uint8_t end_time[4]={0x10,0xde,0xf1,0xde}; // 30days from start_date. 12-20-2012,00:00:00
  uint8_t batch_duration[2]={0x42,0xd0}; //duration time 30days: first three bits represents interpretation (010=hours)
  uint8_t space[4]={0x00,0x00,0x0c,0x00}; //space in KB's: 3MB

  
  
  //epoch_time_from_2004(current_time,"2012-11-21","2:00:00");
  tmp32 = get_cur_time2004();
  if(!BIGENDIAN)
	start_time = request_time = current_time =swap_32(tmp32); //later should be changed to htobe32()
  //[VAMSI-uncomment later]epoch_time_from_2004(start_time, end_time, lcmoptions.Superbatch_Duration_units, lcmoptions.Superbatch_Duration);
  //tmp16 = ((lcmoptions.Batch_Duration_Units << 13) || lcmoptions.Batch_Duration_Value);
  //if(!BIGENDIAN)
//	batch_duration =swap_32(tmp16); //later should be changed to htobe32() [VAMSI-uncomment later]
  EC_GROUP *pGroup = NULL, *pGroup1 = NULL;
  EC_KEY *ECKey1 = NULL, *ECKey2 = NULL, *ECKey3 = NULL;
  UINT32 iRet = FAILURE, uiPubKeyLen = 33, uiTmpBufLen, uiDigestLen, uiSignDataLen, i;
  BIGNUM *bn, *bn1, *bn2;
  EC_POINT * pPoint1 = NULL, *pPoint2 = NULL;
  ECDSA_SIG *pSig = NULL;
  UINT8 *pBufStr = NULL;
  UINT8 pTmpBuf[512], pNonce1[12], pNonce2[12], pDigest[32], pTmpPubKeyRecon[33], pSignData[100];
  UINT8 pKeyBuf[100], pCipherData[256];
  UINT8 pTmpRecon[33], pTmpCARecon[33];
  UINT32 uiCipherLen, uiRetLen;
  BN_CTX *ctx = NULL;

  if ((ctx = BN_CTX_new()) == NULL)
  {
    AWSECPRINTF((BN_CTX_new));
    goto err;
  }

  if((pGroup = CreateECCompGroup(ECDSA_NISTP_256)) == NULL)
  {
    AWSECPRINTF((CreateECCompGroup));
    goto err;
  }
  ECKey1 = EC_KEY_new();
  ECKey2 = EC_KEY_new();
  ECKey3 = EC_KEY_new();

  EC_KEY_set_group(ECKey1, pGroup);
  EC_KEY_generate_key(ECKey1);
  EC_KEY_set_conv_form(ECKey1, POINT_CONVERSION_COMPRESSED);

  uiTmpBufLen = EC_POINT_point2oct(pGroup, EC_KEY_get0_public_key(ECKey1), POINT_CONVERSION_COMPRESSED, SaveKeys.pSignKeyPub, 33, ctx);

  AWSecGetPrivKey(ECKey1, SaveKeys.pSignKeyPriv, &uiTmpBufLen);


  EC_KEY_set_group(ECKey2, pGroup);
  EC_KEY_generate_key(ECKey2);
  EC_KEY_set_conv_form(ECKey2, POINT_CONVERSION_COMPRESSED);

  uiTmpBufLen = EC_POINT_point2oct(pGroup, EC_KEY_get0_public_key(ECKey2), POINT_CONVERSION_COMPRESSED, SaveKeys.pEncKeyPub, 33, ctx);
  AWSecGetPrivKey(ECKey2, SaveKeys.pEncKeyPriv, &uiTmpBufLen);


  RAND_bytes(SaveKeys.pAESSignNonce, 12);
  RAND_bytes(SaveKeys.pAESEncNonce, 12);

//start ToBeSignedAnonymousCertRequestReq

  memcpy(&ToBeSignedAnonymousCertRequestReq[ACRoffset], SaveKeys.pSignKeyPub, sizeof(SaveKeys.pSignKeyPub));
  ACRoffset += sizeof(SaveKeys.pSignKeyPub);

        memcpy(&ToBeSignedAnonymousCertRequestReq[ACRoffset], SaveKeys.pEncKeyPub, sizeof(SaveKeys.pEncKeyPub));
 	ACRoffset += sizeof(SaveKeys.pEncKeyPub);

        RAND_bytes(SaveKeys.pAESSignKey, 16);
        memcpy(&ToBeSignedAnonymousCertRequestReq[ACRoffset], SaveKeys.pAESSignKey, 16);
 	ACRoffset += 16;

	//the e field shall contain a randomly-generated 16-byte AES key, which will be used to generate the cocoon keys for encryption.
        RAND_bytes(SaveKeys.pAESEncKey, 16);
        memcpy(&ToBeSignedAnonymousCertRequestReq[ACRoffset], SaveKeys.pAESEncKey, 16);
 	ACRoffset += 16;

	//The permissions field shall contain the permissions being requested. In this design, it shall contain a single PSID and a NULL SSP for that PSID.
	ToBeSignedAnonymousCertRequestReq[ACRoffset] = 1; //ArrayType-> specified(1)
 	ACRoffset += 1;
	ToBeSignedAnonymousCertRequestReq[ACRoffset] = 2; //len permissions_list<var> of type PsidSsp 
 	ACRoffset += 1;
	ToBeSignedAnonymousCertRequestReq[ACRoffset] = 32; //psid 32
 	ACRoffset += 1;
	ToBeSignedAnonymousCertRequestReq[ACRoffset] = 0; //NULL SSP
 	ACRoffset += 1;

	ToBeSignedAnonymousCertRequestReq[ACRoffset] = 4; //The region shall be none.
 	ACRoffset += 1;

	//the current_time field shall contain the current time.
	memcpy( ( ToBeSignedAnonymousCertRequestReq + ACRoffset ),&current_time, 4);
	ACRoffset += 4;
	//the start_time field shall contain the time at which the validity period is requested to begin for the short-lived certificates.
	memcpy( ( ToBeSignedAnonymousCertRequestReq + ACRoffset ),&start_time, 4);
	ACRoffset += 4;
	// the end_time field shall contain the time at which the validity period is requested to end for the short-lived certificates.
	memcpy( ( ToBeSignedAnonymousCertRequestReq + ACRoffset ),&end_time, 4);
	ACRoffset += 4;
	//the batch_duration field shall contain the requested duration of each encrypted batch of certificates.
	memcpy( ( ToBeSignedAnonymousCertRequestReq + ACRoffset ),&batch_duration, 2);
	ACRoffset += 2;
	//the storage_space_kb field shall contain the maximum amount of space (in kb, i.e. blocks of 1024 bytes) that the requester has available to store the requested certs.
 //[VAMSI-uncomment later] 	if(!BIGENDIAN)
	//	tmp32 =swap_32(lcmoptions.Storage_Space); //later should be changed to htobe32()
	//else
	//	tmp32 =lcmoptions.Storage_Space; //later should be changed to htobe32()
	//memcpy( ( ToBeSignedAnonymousCertRequestReq + ACRoffset ),&tmp32, 4); //[VAMSI-uncomment later]
	memcpy( ( ToBeSignedAnonymousCertRequestReq + ACRoffset ),space, 4);
	ACRoffset += 4;
	
	ToBeSignedAnonymousCertRequestReq[ACRoffset] = 8; // [NAZEER] try 8 if 1 doesn't work //length of certID8<var>
 	ACRoffset += 1;
	//the known_cas field shall contain the CertID8s for all the CAs that the requester recognizes.
	memcpy(( ToBeSignedAnonymousCertRequestReq + ACRoffset ),certid, 8); //[NAZEER] given certID8 of ra_certificate from bootstarp response. actually it should same as digest of root_ca cert from bootstarp rsp which is nothing but a CA
	ACRoffset += 8;
//end ToBeSignedAnonymousCertRequestReq
  if((pGroup1 = CreateECCompGroup(ECDSA_NISTP_256)) == NULL)
  {
    AWSECPRINTF((CreateECCompGroup));
    goto err;
  }
  if (EC_KEY_set_group(ECKey3, pGroup1) != 1)
  {
    AWSECPRINTF((EC_KEY_set_group));
    goto err;
  }
  if (AWSecHash(HASH_ALGO_SHA256, CACert.certInfo, CACert.certLen, pDigest, &uiDigestLen, 0) == NULL)
  {
    AWSec_Print("Error in calculating SHA256 hash of the message\n");
  }
  bzero(pTmpBuf, 512);
  memcpy(pTmpBuf, pDigest, uiDigestLen);
  memcpy(&pTmpBuf[uiDigestLen], CSRCert.certInfo, CSRCert.certLen);

  if (AWSecHash(HASH_ALGO_SHA256, pTmpBuf, CSRCert.certLen + uiDigestLen, pDigest, &uiDigestLen, 0) == NULL)
  {
    AWSec_Print("Error in calculating SHA256 hash of the message\n");
  }

  if ((pPoint2 = PrivateKeyReconstruct(pDigest, SaveKeys.pBootStrapKeyPriv, SaveKeys.recon_priv, SaveKeys.pStaticKeyPriv, pGroup1)) == NULL)
  {
    AWSECPRINTF((PrivateKeyReconstruct));
    goto err;
  }
#ifdef NAZEER
  memcpy(pTmpRecon, &CSRCert.certInfo[CSRCert.sig_RV], sizeof(pTmpRecon));
  memcpy(pTmpCARecon, &CACert.certInfo[CACert.verification_key], sizeof(pTmpCARecon));
  if ((pPoint1 = PublicKeyReconstruct(pDigest, pTmpRecon, pTmpCARecon, SaveKeys.pStaticKeyPub, pGroup1)) == NULL)
  {
    AWSECPRINTF((PublicKeyReconstruct));
    goto err;
  }
#endif
  uiPubKeyLen = EC_POINT_point2oct(pGroup1, pPoint2, POINT_CONVERSION_COMPRESSED, SaveKeys.pStaticKeyPub, 33, NULL);
  /** Due to some reason, Public Key generated from the code is not verifying the signature created by the Private key. 
   * As of now, we are setting only Public Key generated using Private Key times Generator of the Group. This needs to be resolved moving forward. 
   **/
  //if (EC_KEY_set_public_key(ECKey3, pPoint1) != 1) 
  if (EC_KEY_set_public_key(ECKey3, pPoint2) != 1) 
  {
    AWSECPRINTF((EC_KEY_set_public_key));
    goto err;
  }
  bn = BN_bin2bn(SaveKeys.pStaticKeyPriv,32,BN_new());
  if (EC_KEY_set_private_key(ECKey3, bn) != 1)
  {
    if (!bn)
      BN_free(bn);
    AWSECPRINTF((EC_KEY_set_private_key));
    goto err;
  }
  if (pGroup1)
    EC_GROUP_free(pGroup1);
  if(pPoint1)
    EC_POINT_free(pPoint1);
  if(pPoint2)
    EC_POINT_free(pPoint2);
  if (AWSecHash(HASH_ALGO_SHA256, &ToBeSignedAnonymousCertRequestReq, ACRoffset, pDigest, &uiDigestLen, 0) == NULL)
  {
    AWSECPRINTF((AWSecHash));
    goto err;
  }

  memcpy(SaveKeys.DigestInfo, pDigest, uiDigestLen);

  if(!(pSig = AWSecSignData(pDigest, uiDigestLen, ECKey3, pSignData, &uiSignDataLen)))
  {
    AWSECPRINTF((AWSecSignData));
    goto err;
  }
  else
  {
    pSig_g = pSig;
  }
  if ((iRet = signInToBeEncrypted(ToBeSigned, 243, 3, &CSRCert, ToBeSignedAnonymousCertRequestReq, ACRoffset)) == FAILURE)
  {
    AWSECPRINTF((signInToBeEncrypted));
    goto err;
  }
//end Encrypted Message. [NAZEER] Encrypt message (from 'start Encrypted Message' to 'end Encrypted Messag') with the public key of the RA,obtained from its certificate, to produce an EncryptedMessage
  MyOwnKey = EC_KEY_new();
  EC_KEY_set_group(MyOwnKey, pGroup);
  EC_KEY_generate_key(MyOwnKey);
  EC_KEY_set_conv_form(MyOwnKey, POINT_CONVERSION_COMPRESSED);
  pBufStr = (UINT8 *)ecies_key_private_get_hex(MyOwnKey);
  uiTmpBufLen = HexStr2ByteStr(pBufStr, pMyOwnTmpBufPriv, 32);
  uiTmpBufLen = EC_POINT_point2oct(pGroup, EC_KEY_get0_public_key(MyOwnKey), POINT_CONVERSION_COMPRESSED, pMyOwnTmpBufPub, 33, ctx);
  if ((iRet = EncryptedDataIn16092(ToBeEncrypted, ToBeSigned, iRet)) == FAILURE)
  {
    AWSECPRINTF((EncryptedDataIn16092));
    goto err;
  }
  if (pGroup)
    EC_GROUP_free(pGroup);
  
err:
   if (iRet == FAILURE)
     AWSec_Print("%s: %d: %s: Returned error. library = %s, func = %s, reason = %s, {error = %s}\n", __FUNCTION__, __LINE__, __FILE__, ERR_lib_error_string(ERR_get_error()), ERR_func_error_string(ERR_get_error()), ERR_reason_error_string(ERR_get_error()), ERR_error_string(ERR_get_error(), NULL));

    if(!bn)
      BN_clear_free(bn);
    if(!pGroup)
      EC_GROUP_free(pGroup);
    if(!pGroup1)
      EC_GROUP_free(pGroup1);
    if(!pPoint1)
      EC_POINT_free(pPoint1);
    if(!ECKey1)
      EC_KEY_free(ECKey1);
    if(!ECKey2)
      EC_KEY_free(ECKey2);
    if(!ECKey3)
      EC_KEY_free(ECKey3);
    if (ctx != NULL)
      BN_CTX_free(ctx);
   return iRet;
}

//Bootstrapping: RA -> LCM (Confirm)
//int decode_BootStrapConfirm(uint8_t *BootStrapConfirm1) //enable if you want test this func() with static buffer(BootStrapConfirm)
uint32_t decode_BootStrapConfirm(uint8_t *BootStrapConfirm)
{
	uint32_t rspLen=0,bsc=0,ll=0,len=0;
#if 0
	uint8_t BootStrapConfirm[]={0x00,0x00,0x02,0x8c,0x02,0xf1,0x02,0x09,0x04,0x7c,0xfa,0x71,0xfe,0xb7,0x12,0x69,0x8b,0x01,0x00,0x83,0x4f,0x01,0x01,0x20,0x04,0x13,0x97,0x09,0xdd,0x00,0x00,0x00,0x01,0x01,0x02,0x69,0xc6,0xa0,0x3e,0x3e,0x47,0xdd,0xda,0x54,0xb3,0xf2,0xa0,0x0a,0xc1,0x03,0xf0,0x9f,0xb3,0xae,0x0f,0xbf,0xd0,0x2d,0x68,0x88,0xe0,0x65,0xa7,0x62,0xee,0xd3,0x1d,0x02,0x00,0x03,0x54,0x90,0x94,0x3d,0xab,0xdb,0xaf,0xa8,0xf0,0x51,0x97,0x3c,0xdb,0x3e,0x88,0xc2,0x83,0x46,0x5d,0x06,0xb2,0xe2,0x27,0x6a,0xc5,0xb4,0x03,0x23,0xfb,0x34,0xf6,0xf0,0x00,0x5a,0xa6,0x93,0xa8,0xe9,0x5c,0x69,0x51,0xf1,0xd0,0xab,0x88,0xec,0x16,0xcc,0xa9,0x25,0x86,0xe5,0xa5,0xfe,0xd8,0x21,0xa1,0x6a,0x02,0xf5,0x15,0x3c,0x3c,0xb0,0x5a,0xd8,0x4f,0x33,0xa7,0x3a,0x88,0x53,0x51,0xe3,0x3b,0x0a,0xcc,0x62,0xe7,0x51,0x11,0x7c,0x0a,0x2d,0x0c,0xf7,0x99,0x2c,0x29,0x57,0x1e,0x79,0x8e,0xdd,0x98,0xa6,0x00,0x00,0x80,0xdd,0x02,0xff,0x04,0x00,0x83,0xff,0x01,0x00,0x01,0x00,0x04,0x13,0x97,0x09,0xdd,0x00,0x00,0x00,0x01,0x01,0x02,0x69,0xdf,0x01,0x90,0x66,0x7e,0x3c,0xbd,0xca,0xd0,0x9d,0x70,0x33,0xcc,0xac,0x2f,0xfc,0x0f,0xf1,0x30,0x24,0x96,0xcc,0x92,0xf9,0xc6,0xe5,0x0b,0x9d,0xb1,0x72,0xb4,0x02,0x00,0x02,0x93,0x53,0x54,0x7b,0xef,0x08,0x05,0x45,0x50,0xc0,0xa8,0x36,0x7f,0x9a,0xdc,0xc2,0xb2,0x3f,0xe2,0xb5,0xb0,0x17,0x46,0xb3,0xee,0xa4,0xa8,0x69,0xb5,0xf2,0xeb,0x30,0x00,0x3e,0xc9,0x34,0x3e,0xfe,0xf4,0xb9,0xd6,0xce,0x32,0x4b,0x93,0x35,0x4c,0x3e,0x8b,0x38,0x37,0xc4,0x93,0x67,0xb7,0x01,0xb5,0x64,0x15,0x3b,0x53,0x59,0xf8,0xe2,0x17,0x9d,0x1f,0xeb,0x3c,0x0c,0x34,0xbd,0xc2,0x95,0x7c,0xac,0xe8,0xc7,0xb3,0x3d,0x3a,0x9e,0xa1,0xdd,0x77,0xa7,0x3c,0x65,0x2e,0x07,0x26,0x4d,0xa9,0x02,0xb9,0x5a,0x09,0x03,0x03,0x01,0x7c,0xfa,0x71,0xfe,0xb7,0x12,0x69,0x8b,0x01,0x05,0x41,0x52,0x41,0x44,0x41,0x01,0x01,0x01,0x20,0x04,0x13,0x97,0x09,0xdd,0x0f,0xd4,0xa2,0xde,0x00,0x00,0x00,0x01,0x03,0x68,0xe0,0x29,0xcf,0x0b,0x47,0xcb,0x77,0x23,0x57,0xe0,0x50,0x15,0xd7,0x72,0x5b,0x9d,0x97,0x6d,0xd9,0xef,0xaa,0xc9,0x67,0x5c,0x93,0xbd,0x29,0xda,0xf7,0x70,0xaf,0x80,0x59,0xd8,0x50,0x23,0x42,0x5a,0xd9,0x2d,0xdb,0x21,0x8b,0xc5,0xc8,0x1b,0x4e,0x56,0x25,0xa1,0x3b,0x68,0x75,0xae,0xa4,0x2e,0x19,0xef,0x69,0x19,0x02,0x3d,0x86,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xe1,0x01,0x02,0x7c,0xfa,0x71,0xfe,0xb7,0x12,0x69,0x8b,0x02,0x00,0x00,0x00,0x01,0x7c,0xfa,0x71,0xfe,0xb7,0x12,0x69,0x8b,0x80,0x00,0x73,0x87,0x10,0xaf,0x22,0xf2,0x10,0xaf,0x24,0x1d,0x10,0xaf,0x25,0x48,0x78,0x80,0x00,0x00,0x00,0x6d,0xa2,0x96,0xdb,0xa4,0x53,0xab,0x05,0xb4,0x13,0x19,0x65,0xae,0x29,0xe1,0x0c,0xb4,0x61,0x85,0x40,0x41,0xa7,0x5b,0x9b,0x3b,0x00,0x35,0xc1,0x1b,0x33,0x09,0xfe,0x80,0x00,0x00,0x00,0x00,0x00,0x02,0x42,0x8d,0xfe,0x07,0xa8,0xcf,0x84,0x0f,0x63,0x49,0xca,0x4f,0x5b,0xf2,0xf3,0x58,0xda,0xbe,0x97,0x76,0x13,0x84,0xd7,0x9b,0x27,0x26,0x03,0x1b,0xb2,0xd2,0xe7,0xc2,0x1e,0x00,0x00,0x02,0xf4,0x00,0x00,0x02,0x72,0x7f,0x69,0xf3,0x3a,0xee,0x88,0x32,0xc2,0x64,0x22,0xe7,0x97,0xf4,0xed,0xfd,0x64,0xe6,0x69,0xa7,0xe1,0xcb,0x01,0x80,0xa3,0x0a,0x7a,0xa1,0xe7,0xa4,0xf8,0x88,0xe5,0x00,0x00,0x02,0xf8,0x00,0xe2,0xee,0x2e,0x7b,0x2a,0xc5,0xe2,0xda,0xa2,0xea,0x9a,0x20,0x91,0x47,0x22,0xaf,0xf9,0x33,0xef,0x9a,0x60,0x56,0xfd,0xc6,0x13,0xbc,0xd0,0x0d,0xf2,0x38,0x26,0x39,0x52,0xd2,0x04,0x8b,0x5a,0xdd,0xf2,0x32,0x00,0x45,0xec,0xf4,0x43,0x39,0xc3,0xc3,0x16,0xcd,0xb4,0x7a,0xae,0x29,0x2f,0x8d,0x7a,0xce,0x96,0x9f,0x51,0xad,0xcb,0xd4};
#endif
	memcpy(&rspLen, (BootStrapConfirm + bsc), 4);
	bsc += 4;	
	bsc += 1; //protocol_version

	if(BootStrapConfirm[bsc] != 241) //cert_bootstrap_cfm
		return -1;
	bsc += 1; 

	//deocde RA cert
	len = certParse(&BootStrapConfirm[bsc],&RACert);
	memcpy(RACert.certInfo,(BootStrapConfirm+bsc),len);
	RACert.certLen = len;
	bsc += len;
	//create_certFile(RACert.certInfo, RACert.certLen, "/tmp/usb/ModelDeploymentConfigurationItems/1609Certificates/ra.cert");
	create_certFile(RACert.certInfo, RACert.certLen, "/tmp/usb/lcm/ra.cert");
	bsc += 1; //ToBeEncryptedCertificateResponse.flags
	decode_length(&BootStrapConfirm[bsc],&len);	
	bsc += len; //ToBeEncryptedCertificateResponse.certificate_chain len
	//decode CA cert
	len = certParse(&BootStrapConfirm[bsc],&CACert);
	memcpy(CACert.certInfo,(BootStrapConfirm+bsc),len);
	CACert.certLen = len;
	bsc += len;
	//create_certFile(CACert.certInfo, CACert.certLen, "/tmp/usb/ModelDeploymentConfigurationItems/1609Certificates/root_ca.cert");
	create_certFile(CACert.certInfo, CACert.certLen, "/tmp/usb/lcm/root_ca.cert");
	//[NAZEER] need to create root_ca_dec.key file
	//decode CSR cert
	len = certParse(&BootStrapConfirm[bsc],&CSRCert);
	memcpy(CSRCert.certInfo,(BootStrapConfirm+bsc),len);
	CSRCert.certLen = len;
	bsc += len;
	//create_certFile(CSRCert.certInfo, CSRCert.certLen, "/tmp/usb/ModelDeploymentConfigurationItems/1609Certificates/csr.cert");
	create_certFile(CSRCert.certInfo, CSRCert.certLen, "/tmp/usb/lcm/csr.cert");
	//Store recon_priv[32]
	memcpy(SaveKeys.recon_priv,(BootStrapConfirm+bsc),32);
	bsc += 32;
	//CRL
	len = decode_length(&BootStrapConfirm[bsc],&ll);	
	bsc += ll; 
	memcpy(&lcmStore.nextCrl,(BootStrapConfirm+bsc+35),4);
	BootStrapConfirm[bsc-2] = 2;
	BootStrapConfirm[bsc-1] = 8;
	//create_certFile((BootStrapConfirm+bsc-2),  len, "/tmp/usb/ModelDeploymentConfigurationItems/1609Certificates/crl.dat");
	create_certFile((BootStrapConfirm+bsc-2),  len, "/tmp/usb/lcm/crl.dat");
	//if(rspLen != bsc)
	//	return -1;
	return bsc;
}

//Bootstrapping: LCM -> RA (Request)
uint32_t encode_ToBeSignedCertificateRequest(uint8_t *ToBeSignedCertificateRequest /*uint8_t *psidArray, uint32_t ev, Time32 sv*/)
{
        EC_GROUP *pGroup = NULL;
        point_conversion_form_t form = POINT_CONVERSION_COMPRESSED;
        UINT32 asn1_flag = OPENSSL_EC_NAMED_CURVE;
        EC_KEY *ECKey = NULL;
        UINT8 pTmpBuf[512], pDigest[512], pData[512], *pBufR, *pBufS, *pBufStr, pSignData[100];
        UINT32 uiTmpBufLen, uiDigestLen, uiDataLen, uiSignDataLen, uiPubKeyLen = 33, iRet = FAILURE;
        ECDSA_SIG *pSig;
        const BIGNUM *PrivKey;
	uint32_t offset=0;
        
	
	//uint8_t expiration_value[4]={0x11,0xdd,0x63,0x5d};
	//uint8_t start_validity[4]={0x0d,0x2b,0x0b,0x5e};
	uint8_t expiration_value[4]={0x13,0x97,0x09,0xdd};
	uint8_t start_validity[4]={0x0f,0xd4,0xa2,0xde};
	Time32 request_time,tmpTime; //caliculate time
	uint8_t zeros[34]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        BN_CTX *ctx = NULL;
        //epoch_time_from_2004(request_time,"2012-11-20","22:00:00");
	tmpTime = get_cur_time2004();
	if(!BIGENDIAN)
		request_time =swap_32(tmpTime); //later should be changed to htobe32()

        if ((ctx = BN_CTX_new()) == NULL)
        {
          AWSECPRINTF((BN_CTX_new));
          goto err;
        }
        ECKey = EC_KEY_new();

        if((pGroup = CreateECCompGroup(ECDSA_NISTP_256)) == NULL)
        {
          AWSECPRINTF((CreateECCompGroup));
          goto err;
        }

        EC_KEY_set_group(ECKey,pGroup);
        EC_KEY_generate_key(ECKey);
        EC_KEY_set_conv_form(ECKey, POINT_CONVERSION_COMPRESSED);
        
        AWSecGetPrivKey(ECKey, SaveKeys.pBootStrapKeyPriv, &uiTmpBufLen);

#ifdef NAZEER        
        PrivKey = EC_KEY_get0_private_key(ECKey); 

        pBufStr = BN_bn2hex(PrivKey);
        uiTmpBufLen = HexStr2ByteStr(pBufStr, SaveKeys.pBootStrapKeyPriv, 32);
#endif
        uiTmpBufLen = EC_POINT_point2oct(pGroup, EC_KEY_get0_public_key(ECKey), POINT_CONVERSION_COMPRESSED, pTmpBuf, uiPubKeyLen, ctx);
        memcpy(SaveKeys.pBootStrapKeyPub, pTmpBuf, uiTmpBufLen);
        
        /** Length of the message needs to be in Big Endian **/
        offset = 4;
	ToBeSignedCertificateRequest[offset] = 2;    //[0], protocol_version
	offset += 1;                  
 	ToBeSignedCertificateRequest[offset] = 240;//cert_bootstrap_req;    //[1], ContentType
	offset += 1;                  
	ToBeSignedCertificateRequest[offset] = 0;//self;    //[2], SignerIdentifier 
	offset += 1;

// digest caliculate starts from here
	ToBeSignedCertificateRequest[offset] = 2;    //[3], version_and_type. 3 for implicit cert
	offset += 1;

	memcpy( ( ToBeSignedCertificateRequest + offset ),&request_time, 4);
	//[4], request_time is the time that the request was formed, measured in (TAI)seconds since the epoch of 00:00:00 UTC, 1 January, 2004.
	offset += 4;

	ToBeSignedCertificateRequest[offset] = 3;//message_csr;    //[8], SubjectType 
	offset += 1;

	ToBeSignedCertificateRequest[offset] = 1; //use_start_validity;    //[9], CertificateContentFlags
	offset += 1;

	ToBeSignedCertificateRequest[offset] = 5;    //[10], since "name" in MessageCaScope is variable length we need to mention len feild.
	offset += 1;

	strcpy(ToBeSignedCertificateRequest+offset,"ARADA"); //[11], ARADA.
	offset += 5;	

	ToBeSignedCertificateRequest[offset] = 0x01;//message_anonymous;    //[16], SubjectTypeFlags.
	offset += 1;

	ToBeSignedCertificateRequest[offset] = 1;//specified;    //[17], ArrayType.
	offset += 1;

	ToBeSignedCertificateRequest[offset] = 0x01;    //[18], permissions_list len
	offset += 1;	

	ToBeSignedCertificateRequest[offset] = 0x20;    //[19], permissions_list of type Psid.
	offset += 1;		

	ToBeSignedCertificateRequest[offset] = 4;//none;    //[], GeographicRegion.
	offset += 1;		

	memcpy( ( ToBeSignedCertificateRequest + offset ) ,expiration_value, 4);   //[], Time32 expiration. the field expiration shall be set to 11:59:59 pm, June 30, 2013
	offset += 4;//sizeof(expiration_value);

	memcpy( ( ToBeSignedCertificateRequest + offset ) ,start_validity, 4 );   //[], Time32 start_validity. start_validity field shall be filled in with 00:00:00 am, Jan 1, 2011
	offset += 4;//sizeof(start_validity);
	

	// the field verification_key shall contain the seed public verification key of the sender. This shall be an ECDSA-256 key in compressed form.
	ToBeSignedCertificateRequest[offset] = 1;//ecdsa_nistp256_with_sha_256;    //[], PKAlgorithm
	offset += 1;	
	
	// extern uint8 field_size=32 as per page 71, no need to encode.
	
#ifdef NAZEER
        /** As this byte will be already added in Public Key. Setting it is not required **/
	ToBeSignedCertificateRequest[offset] = 0x03;//compressed_lsb_y_1    //[], EccPublicKeyType. ?? Page 71. Should be in compressed form.
	offset += 1;
#endif
	//ToBeSignedCertificateRequest[offset] = NAZEER;    //[], opaque x[field_size]=x[32]->public key
        memcpy(&ToBeSignedCertificateRequest[offset], pTmpBuf, uiTmpBufLen);
	offset += uiTmpBufLen;	
	//the field response_encryption_key may contain a freshly generated ECIESNISTp256 encryption public key or may be all NULLs. this field is not processed at the receiving end so it doesn�t matter what�s in it.
	ToBeSignedCertificateRequest[offset] = 2;//ecies_nistp256;    //[], PKAlgorithm
	offset += 1;
	memcpy( ( ToBeSignedCertificateRequest + offset ) ,zeros, 34 ); //bytes: SymmAlgorithm(1byte) + EccPublicKey(32bytes of x + EccPublicKeyType(1byte))
	offset += 34;
// digest caliculate Ends here


  if (AWSecHash(HASH_ALGO_SHA256, &ToBeSignedCertificateRequest[3 + 4], offset - 3 - 4, pDigest, &uiDigestLen, 0) == NULL)
  {
    AWSECPRINTF((AWSecHash));
    goto err;
  }
  pSig = AWSecSignData(pDigest, uiDigestLen, ECKey, pSignData, &uiSignDataLen);
  if (pSig == NULL)
  {
    AWSECPRINTF((ECDSA_do_sign));
    goto err;
  }

	// Signature signature; the signature is generated using the private key corresponding to the public key in the verification_key field.
	// extern PKAlgorithm algorithm; ecdsa_nistp256_with_sha_256 (1), no need to encode. encode EcdsaSignature As per page 70
	// EccPublicKey R

   ToBeSignedCertificateRequest[offset] = pTmpBuf[0]; /** 0x03; **/ /* NAZEER; */   //[], EccPublicKeyType. ?? Page 71. Should be in compressed form.
   offset += 1;	
   uiTmpBufLen = HexStr2ByteStr(BN_bn2hex(pSig->r),&ToBeSignedCertificateRequest[offset], 32);
   offset += uiTmpBufLen;
    
   uiTmpBufLen = HexStr2ByteStr(BN_bn2hex(pSig->s),&ToBeSignedCertificateRequest[offset], 32);
   offset += uiTmpBufLen;

   printf("offset = %d\n", offset);
  BUFPUT32(ToBeSignedCertificateRequest, offset - 4);

  iRet = offset;
err:
     if (iRet == FAILURE)
       AWSec_Print("%s: %d: %s: Returned error. library = %s, func = %s, reason = %s, {error = %s}\n", __FUNCTION__, __LINE__, __FILE__, ERR_lib_error_string(ERR_get_error()), ERR_func_error_string(ERR_get_error()), ERR_reason_error_string(ERR_get_error()), ERR_error_string(ERR_get_error(), NULL));
    if(!pGroup)
      EC_GROUP_free(pGroup);
    if(!ECKey)
      EC_KEY_free(ECKey);
    if(!pSig)
      ECDSA_SIG_free(pSig);
    if (ctx != NULL)
      BN_CTX_free(ctx);
      
    return iRet;
}
