#include<stdio.h>
#include <stdlib.h>
#include <unistd.h>           // close()
#include <string.h>           // strcpy, memset(), and memcpy()
#include <netdb.h>            // struct addrinfo
#include <sys/types.h>        // needed for socket()
#include <sys/socket.h>       // needed for socket()
#include <netinet/in.h>       // IPPROTO_RAW, IPPROTO_UDP
#include <netinet/ip.h>       // struct ip and IP_MAXPACKET (which is 65535)
#include <sys/ioctl.h>        // macro ioctl is defined
#include <bits/ioctls.h>      // defines values for argument "request" of ioctl.
#include <net/if.h>           // struct ifreq
#include <linux/if_ether.h>   // ETH_P_IP = 0x0800, ETH_P_IPV6 = 0x86DD
#include <linux/if_packet.h>  // struct sockaddr_ll (see man 7 packet)
#include <net/ethernet.h>

#include <errno.h>            // errno, perror()
#include <error.h>

#define PORT 8756

static struct sockaddr_in their_addr;
unsigned short csum(unsigned short *, int);
char datagram[4096];

unsigned short csum(unsigned short *buf, int nwords)
{
	unsigned long sum;
	for (sum = 0; nwords > 0; nwords--)
		sum += *buf++;
	sum = (sum >> 16) + (sum & 0xffff);
	sum += (sum >> 16);
	return ~sum;
}

int main(int argc, char *argv[])
{
	int sockfd = -1, ret = -1, i = 0;
	int one = 1;
	int broadcast = 1;
	struct ifreq ifr;
	const int *val = &one;
	struct sockaddr_in serveraddr;
	struct ip *iph = (struct ip *)& datagram;
	char dummy_buf[] = "This is dummy data to sent to an interface using raw sockets";
	uint8_t mac[6];
	char interface[10] = "";
    struct sockaddr_ll device;

	if(argc < 2 || argc > 2)
	{
		printf("Usage : %s <interface>\n", argv[0]);
		exit (1);
	}
	/* create a raw socket */
	sockfd = socket(PF_INET, SOCK_RAW, IPPROTO_RAW);
	if(sockfd < 0)
	{
		perror("socket\n");
		return -1;
	}

#if 1
  	strcpy (interface, argv[1]);
	/* Bind socket to a interface */
	memset(&ifr, 0, sizeof(ifr));
	snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s", interface);
	ret = setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, (void *)&ifr, sizeof(ifr));
	if(ret < 0) 
	{
		perror("setsockopt(BINDTODEVICE)\n");
		return -1;
	}
  	if (ioctl (sockfd, SIOCGIFHWADDR, &ifr) < 0) {
		perror("ioctl failed\n");
	}
  	memcpy (mac, ifr.ifr_hwaddr.sa_data, 6);
  	printf ("MAC address for %s is ", interface);
  	for (i=0; i<5; i++) {
    	printf ("%02x:", mac[i]);
  	}
  	printf ("%02x\n", mac[5]);
	// Resolve interface index.
  	if ((device.sll_ifindex = if_nametoindex (interface)) == 0) {
    	perror ("if_nametoindex() failed to obtain interface index ");
    	exit (EXIT_FAILURE);
  	}
  	printf ("Index for interface %s is %i\n", interface, device.sll_ifindex);
	/* bind to an address */
	memset(&serveraddr, 0x00, sizeof(struct sockaddr_in));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(PORT);
	serveraddr.sin_addr.s_addr = INADDR_ANY;
	ret = bind(sockfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
	if(ret < 0)
	{
		perror("bind\n");
		return -1;
	}
#endif
	ret = setsockopt(sockfd, IPPROTO_IP, IP_HDRINCL, val, sizeof(one));
	if(ret < 0) 
	{
		perror("setsockopt(HDRINCL)\n");
		return -1;
	}
	ret = setsockopt(sockfd, SOL_IP, IP_TTL, val, sizeof(one));
	if(ret < 0) 
	{
		perror("setsockopt(TTL)\n");
		return -1;
	}
	ret = setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast));
	if(ret < 0) 
	{
		perror("setsockopt(SO_BROADCAST)\n");
		return -1;
	}
	their_addr.sin_family = AF_INET;
	their_addr.sin_port = htons(PORT);
	memset(&(their_addr.sin_zero), '\0', 8);
	memset(&(their_addr.sin_addr.s_addr), 255, sizeof(their_addr.sin_addr.s_addr));
	memset(datagram, 0, 4096);


	/* fill the ip header */
	iph->ip_hl = 5;
	iph->ip_v = 4;
	iph->ip_tos = 0;
	iph->ip_len = sizeof(struct ip) + strlen(dummy_buf);
	iph->ip_id = htonl(54321);
	iph->ip_off = 0;
	iph->ip_ttl = 255;
	iph->ip_p = 0xff;
	iph->ip_sum = 0;
	iph->ip_src.s_addr = inet_addr("127.0.0.1");
	iph->ip_dst.s_addr = their_addr.sin_addr.s_addr;  //255
	
	/* copy user data to the datagram */
	memcpy(datagram + 20, dummy_buf, strlen(dummy_buf));
	iph->ip_sum = csum((unsigned short *)datagram, iph->ip_len >> 1);

	/* send the data to the socket */
	ret = sendto(sockfd, datagram, iph->ip_len, 0, (struct sockaddr *)&their_addr, sizeof(struct sockaddr));
	if(ret < 0)
	{
		perror("sendto\n");
		return -1;
	}
	printf("strlen = %d sent %d bytes to sockfd %d\n", strlen(dummy_buf), ret, sockfd);
	close(sockfd);
	return 0;
}
