/*

* Copyright (c) 2005-2007 Arada Syatems, Inc. All rights reserved.

* Proprietary and Confidential Material.

*

*/

#include "wave.h"
#include "wavelogger.h"
#include "chaninfo.h"
#include <stdio.h>
#include <ctype.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include "os.h"

static int notifrcvd;
static int wsmpdev;
static int wsmpblock;
static struct pollfd wsmppoll;
static int oflags;

char ip[128];
char iface[30];
static struct in6_addr notifaddr, serviceaddr;
extern const struct in6_addr in6addr_any;
static uint16_t notifport, serviceport;
WAVEHandler waveRequest;

#ifdef	WIN32
	static int wavereqmode = WAVEDEVICE_REMOTE;
#else
	static int wavereqmode = WAVEDEVICE_LOCAL;
#endif

#ifdef WIN32
static int size_sock = sizeof(struct sockaddr_storage);
#define MALLOC_BUFFER 15000
#endif
	
static int isAppRunning = 0;
static int isClientRunning = 0;
static int startServiceThread = 0;
static int wavesock = -1;
/*Sockets for notif, service and appind Clients, closed by closeWAVEDevice()*/
static int service_clsock, notif_clsock, appind_clsock;

static int waveaddrlen = 0;
static struct sockaddr_in6 waveserver6;
static struct sockaddr_in waveserver4;
static struct sockaddr_in6 wavefrom;

static pthread_t notifthread, appindthread, servicethread;
static void *notif_client(void *data);
static void *appind_client(void *data);
static void *service_client(void *data);

static void (*receiveWMEIndication)(WMENotificationIndication *);
static void (*receiveWSMIndication)(WSMIndication *);
static void (*receiveWRSSIndication)(WMEWRSSRequestIndication *);
static void (*receiveTsfTimerIndication)(TSFTimer *);
//static int (*confirmLink)(u_int8_t , ACM);
static int (*confirmLink)(WMEApplicationIndication *);
static int (*user_confirmLink)(struct wmeNotif_Indication *);
static int (*TAreq)(struct WmeTAIndication*);
/******** UTILITY Functions**************/


static  int isBigEndian()
{
     long one= 0x00000001;
     return !(*((char *)(&one)));
}

static void swapGenericData(int size, void *data);
static void swapWAVEData(WAVEHandler *wh);
static void swapIndicationData(int type, void *ind);

/********** Driver init Functions***********/


void Inet_Aton(char *x,struct in6_addr * aton )
{
	int rc;
#ifdef WIN32
	rc = WSAStringToAddress(x,AF_INET,0,(LPSOCKADDR)&waveserver4,&size_sock);
	if(rc!=0)
	{
		rc = WSAStringToAddress(x,AF_INET6,NULL,(LPSOCKADDR)&waveserver6,&size_sock);
		if(rc!=0)
			printf("Inet_Aton WSAStirngToAddress Failed %d\n",WSAGetLastError());
	}
#else
	rc = inet_pton(AF_INET, x, aton);
	if( rc != 1 )
	{
		rc = inet_pton(AF_INET6, x, aton );
		if( rc <= 0 )
			perror("inet_pton() failed");
	}
#endif
}

#ifdef WIN32
#define DELTA_EPOCH_IN_USEC  11644473600000000

static u_int64_t filetime_to_unix_epoch (const FILETIME *ft)
{
    u_int64_t res = (u_int64_t) ft->dwHighDateTime << 32;

    res |= ft->dwLowDateTime;
    res /= 10;                   // from 100 nano-sec periods to usec 
    res -= DELTA_EPOCH_IN_USEC;  // from Win epoch to Unix epoch 
    return (res);
}


int gettimeofday (struct timeval *tv, void *tz)
{
    FILETIME  ft;
    u_int64_t tim;

    if (!tv) {
        errno = EINVAL;
        return (-1);
    }
    GetSystemTimeAsFileTime (&ft);
    tim = filetime_to_unix_epoch (&ft);
    tv->tv_sec  = (long) (tim / 1000000L);
    tv->tv_usec = (long) (tim % 1000000L);
    return (0);
}
#endif

void inputhandler (int signo)
{
	WAVEHandler waverxRequest;
	WMEApplicationIndication *wmeappindication;
	WMENotificationIndication *wmenotifindication;
	WMEWRSSRequestIndication *wrssindication;
	//WMEApplicationResponse *appresponse;
	WMEApplicationResponse appresponse;
	struct wmeNotif_Indication *userjoin;
	struct WmeTAIndication *ta_receive;
	int result,iMode=1;
	int flag = 0;
	int confirm = 0;
	do {
#ifdef	WIN32
			result = ioctlsocket(wsmpdev, WAVE_INDICATION, &iMode);
			printf("\nRESULT in Libwave.c: %d\n",result);
#else
			result = ioctl(wsmpdev, WAVE_INDICATION, &waverxRequest);
#endif		
		if (result < 0) {
			flag = 1;
			break;
		}
		if (waverxRequest.type == WME_APP_INDICATION) {
			wmeappindication = (void *) waverxRequest.data;
			confirm = 0;
			if(confirmLink != NULL) confirm = confirmLink(wmeappindication);
			appresponse.linkConfirm = confirm;
			result = generateWMEApplResponse(&appresponse);
		}
		if (waverxRequest.type == WME_APP_NOTIFICATION) {
			notifrcvd = 1;
			wmenotifindication = (void *) waverxRequest.data;
			if (receiveWMEIndication) {
				receiveWMEIndication(wmenotifindication);
			}
			
			
		}
		if (waverxRequest.type == WAVE_WRSS_INDICATION) {
			notifrcvd = 1;
			wrssindication = (void *) waverxRequest.data;
			if (receiveWRSSIndication) {
				receiveWRSSIndication(wrssindication);
			}
			
		}
		if (waverxRequest.type == WAVE_WSA_INDICATION) {
			notifrcvd = 1;
			userjoin = (void *) waverxRequest.data;
			if(user_confirmLink != NULL) 
			    confirm = user_confirmLink(userjoin);
			
		}
		if (waverxRequest.type == WAVE_TA_INDICATION) {
			notifrcvd = 1;
			ta_receive = (void *) waverxRequest.data;	
			if(TAreq != NULL) 
			{
			    confirm = TAreq(ta_receive); 	
			}
			
		}
	} while (flag !=1);
}


int invokeWAVEDevice(int type, int blockflag)
{
	int result;
	
	switch(type) 
	{
#ifndef WIN32
		case WAVEDEVICE_LOCAL:
			result = invokeWAVEDriver(blockflag);
		break;
#endif
		case WAVEDEVICE_REMOTE:
			result = invokeWAVEServer();
		break;
	}
	
	if(result >= 0)
		wavereqmode = type;
		
	return result;					
}


//Call this function when your application EXITS
void closeWAVEDevice()
{
	if(notif_clsock)
		close(notif_clsock);
	if(appind_clsock)
		close(appind_clsock);
	if(service_clsock)
		close(service_clsock);
}


#ifndef WIN32	// This is not required in WINDOWS
int invokeWAVEDriver(int blockflag)
	{
		int result;

		wsmpblock = blockflag;
		wsmpdev = open(DEVNAME, O_RDWR, 0);
		if (wsmpdev > 0) {
			signal(SIGIO, &inputhandler);
			fcntl(wsmpdev, F_SETOWN, getpid());
			oflags = fcntl(wsmpdev, F_GETFL);
#if 1
			fcntl(wsmpdev, F_SETFL, oflags | FASYNC);
#endif
			if (blockflag == 0) {
				fcntl(wsmpdev, F_SETFL, oflags | O_NONBLOCK);
			} else {
				wsmppoll.fd = wsmpdev;
				wsmppoll.events = POLLIN | POLLOUT;
			}
		}
		return wsmpdev;
	}
#endif

int invokeWAVEServer(void)
{
	int ret;
	struct in6_addr serveraddr;
	
	WIN_SOCK_DLL_INVOKE  //Socket initialization for WINDOWS
		#ifndef WIN32
		ret = inet_pton(AF_INET, ip, &waveserver4.sin_addr );
        if( ret == 1 )
		#else
		ret = WSAStringToAddress(ip,AF_INET,0,(LPSOCKADDR)&waveserver4,&size_sock);
		if(ret==0)
		#endif
		{
                wavesock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
				if (wavesock < 0)
                return -1;

                waveserver4.sin_family = AF_INET;
				//waveserver.sin6_addr = serveraddr;
               	/*sin6_addr already set in setRemoteDeviceIP(char *ipaddr)*/
				waveserver4.sin_port = htons(WAVE_UDPSERVER_PORT);
                waveaddrlen = sizeof(struct sockaddr_in);
				
				return wavesock;
        }
	else
	{			
				#ifndef WIN32
					ret = inet_pton(AF_INET6, ip, &waveserver6.sin6_addr );
					if( ret == 1 )
                #else
					ret = WSAStringToAddress(ip,AF_INET6,0,(LPSOCKADDR)&waveserver6,&size_sock);
					if(ret==0)
				#endif
				{
                        wavesock = socket(AF_INET6, SOCK_DGRAM, 0);
                        if (wavesock < 0)
                                return -1;

                        if(setsockopt(wavesock, SOL_SOCKET, SO_REUSEADDR,(char *)&ret, sizeof(ret ) ) < 0 )
                        {
                                perror("setsockopt(SO_REUSEADDR) failed");
                        }
                        waveserver6.sin6_family = AF_INET6;
			//waveserver.sin6_addr = serveraddr;
                        /*sin6_addr already set in setRemoteDeviceIP(char *ipaddr)*/
                        waveserver6.sin6_port = htons(WAVE_UDPSERVER_PORT);
                        waveaddrlen = sizeof(struct sockaddr_in6);
			return wavesock;
                }
        }
}

int invokeWAVEClient() 
{
	if(isClientRunning)
		return -1;
	
	pthread_create(&notifthread, NULL, notif_client, NULL);
	
	pthread_create(&appindthread, NULL, appind_client, NULL);
	
	sched_yield();
	
	if(startServiceThread)
		pthread_create(&servicethread, NULL, service_client, NULL);

	isClientRunning = 1;
	
	sched_yield();
	
	return 0;	
}

void *notif_client(void *data)
{
	int clsock, ret;
	int len, lenfrom, iMode=1;
	uint16_t port;
	char buf[1024];
	struct sockaddr_in6 client;
	struct sockaddr_in6 from;

	WMENotificationIndication ind;
	void (*rcvWMEInd)(WMENotificationIndication *);

//	WIN_SOCK_DLL_INVOKE  //Socket initialization for WINDOWS

	clsock = socket(AF_INET6, SOCK_DGRAM, 0);
	notif_clsock = clsock;
	rcvWMEInd = receiveWMEIndication; 
	if (clsock < 0)
		perror("SOCK");
		#ifndef WIN32		
	if(setsockopt(wavesock, SOL_SOCKET, SO_BINDTODEVICE, iface, strlen(iface) ) < 0 )
        {
                perror("setsockopt(SO_BINDTODEVICE) failed");
        }
		#endif
	
	client.sin6_family = AF_INET6;
	client.sin6_addr = in6addr_any;
	//client.sin6_addr.s6_addr32[0] = notifaddr.s6_addr32[0];
	client.sin6_port = htons(notifport);
	client.sin6_scope_id =0;
	len = lenfrom = sizeof(struct sockaddr_in6);
	if (bind(clsock,(struct sockaddr *)&client, len)<0)
	{
		perror("bind() failed"); 
		return;
	}
	
	while(1) {
	        lenfrom = sizeof(struct sockaddr_in6);
		len = recvfrom(clsock, &ind, sizeof(ind), 0, (struct sockaddr *)&from, &lenfrom);
		if(isBigEndian())
			swapIndicationData(1, (void*)&ind);
		rcvWMEInd(&ind);
	}
	
}

void *appind_client(void *data)
{
	int clsock;
	int len, lenfrom, ret;
	uint16_t port;
	char buf[1024];
	struct sockaddr_in6 client;
	struct sockaddr_in6 from;

	WMEApplicationIndication ind;
	WMEApplicationResponse appresponse;
	int (*confirmcb)(WMEApplicationIndication *);

	WIN_SOCK_DLL_INVOKE  //Socket initialization for WINDOWS

	//int (*confirm)(u_int8_t , ACM);

	clsock = socket(AF_INET6, SOCK_DGRAM, 0);
	appind_clsock = clsock;	
	confirmcb = confirmLink; 
	if(clsock < 0)
		perror("SOCK");
	#ifndef WIN32
	if(setsockopt(wavesock, SOL_SOCKET, SO_BINDTODEVICE, iface, strlen(iface)) < 0 )
        {
                perror("setsockopt(SO_BINDTODEVICE) failed");
        }
	#endif	
	client.sin6_family = AF_INET6;
	client.sin6_addr = in6addr_any;
	//client.sin6_addr.s6_addr32[0] = notifaddr.s6_addr32[0];
    	port = htons(WME_APPIND_PORT);
    	if (!isBigEndian())
        	swapGenericData(sizeof(uint16_t), &port);
    	client.sin6_port = port;
		client.sin6_scope_id =0;
		len = lenfrom = sizeof(struct sockaddr_in6);
	if (bind(clsock,(struct sockaddr *)&client, len)<0) { 
		perror("bind() failed");
		return;
	}
	
	while (1) {
	        lenfrom = sizeof(struct sockaddr_in6);
		len = recvfrom(clsock, &ind, sizeof(ind), 0, (struct sockaddr *)&from, &lenfrom);
		if (isBigEndian())
			swapIndicationData(5, (void*)&ind);
		ret = confirmcb(&ind);
		appresponse.linkConfirm = ret;
		generateWMEApplResponse(&appresponse);
	}
}

void *service_client(void *data)
{
	int clsock;
	int len, lenfrom, ret;
	uint16_t port;
	char buf[1024];
	struct sockaddr_in6 client;
	struct sockaddr_in6 from;
	
	
	WSMIndication ind;
	void (*rcvWSMInd)(WSMIndication *);
	rcvWSMInd = receiveWSMIndication; 
	
	//WIN_SOCK_DLL_INVOKE  //Socket initialization for WINDOWS
	
	clsock = socket(AF_INET6, SOCK_DGRAM, 0);
	service_clsock = clsock;	
		
	if(clsock < 0)
		perror("SOCK");
	#ifndef WIN32
	if(setsockopt(wavesock, SOL_SOCKET, SO_BINDTODEVICE, iface, strlen(iface)) < 0 )
        {
                perror("setsockopt(SO_BINDTODEVICE) failed");
        }
	#endif

	client.sin6_family = AF_INET6;
	client.sin6_addr = in6addr_any;
	client.sin6_port = htons(serviceport);
	client.sin6_scope_id =0;
	len = lenfrom = sizeof(struct sockaddr_in6);
	if (bind(clsock,(struct sockaddr *)&client, len)<0) 
	{
		perror("bind() failed");
		printf("[LIBWAVE:Unable to bind]\n");
		return;
	}
	while(1) {
	        lenfrom = sizeof(struct sockaddr_in6);
			len = recvfrom(clsock, &ind, sizeof(ind), 0, (struct sockaddr *)&from, &lenfrom);
		if (isBigEndian())
		{
			swapGenericData(sizeof(ind.psid),&ind.psid);
			swapIndicationData(2, (void*)&ind);
		}
		rcvWSMInd(&ind);
	}
	
}




int setRemoteDeviceIP(char *ipaddr) 
{
int ret=-1;
char temp[128];
	strcpy(ip, ipaddr);
	strcpy(temp,ipaddr);
	#ifdef WIN32
		ret = WSAStringToAddress(ip,AF_INET,0,(LPSOCKADDR)&waveserver4,&size_sock);
		if(ret==0)
		strcat(ip,":9999");
		else {
 				ret = WSAStringToAddress(ip,AF_INET6,0,(LPSOCKADDR)&waveserver6,&size_sock);
				if(ret==0){
				strcat(temp,"%0]:9999");
				memset(ip,0,sizeof(ip));
				ip[0]='[';
				strcat(ip,temp);}
		}
	#endif
	if (ipaddr)
	{
		Inet_Aton(ipaddr,&waveserver6.sin6_addr);
		//Inet_Aton(ipaddr);
	}
	else
	
	{
		Inet_Aton(ipaddr,&waveserver6.sin6_addr);
		//Inet_Aton(ipaddr);
	}				
	return 0;
}

int setWMEApplRegNotifParams(WMEApplicationRequest *req) 
{
	if (!req)	
		return -1;
	memcpy(&notifaddr, &req->ipv6addr, sizeof(struct in6_addr));
	notifport = htons(req->notif_port);
	
	return 0;			
}

void registerWMENotifIndication ( void (*wmeNotifIndCallBack)(WMENotificationIndication *) )
{
	receiveWMEIndication = wmeNotifIndCallBack;
}


void registerWSMIndication ( void (*wsmIndCallBack)(WSMIndication *) )
{
	receiveWSMIndication = wsmIndCallBack;
}


void registerWRSSIndication ( void (*wrssIndCallBack)(WMEWRSSRequestIndication *) )
{
	receiveWRSSIndication = wrssIndCallBack;
}

void registertsfIndication(void (*tsfIndCallBack)(TSFTimer *))
{
	receiveTsfTimerIndication = tsfIndCallBack;
}

//void registerLinkConfirm( int (*confirmLinkCallBack)(u_int8_t , ACM) )
void registerLinkConfirm( int (*confirmLinkCallBack)(WMEApplicationIndication *) )
{
	confirmLink = confirmLinkCallBack;
}

void user_registerLinkConfirm( int (*user_confirmLinkCallBack)(struct wmeNotif_Indication *) )
{
	user_confirmLink = user_confirmLinkCallBack;
}

void registerTAIndication(int (*receiveTAIndication)(struct WmeTAIndication *))
{
	TAreq = receiveTAIndication;	
}
/*
********** Apllication-libwave Functions***********
*/

void getMACAddr(uint8_t *macadd, uint8_t curchan) 
{

	int result, ret;
	char rescode[4];
	/*uint8_t*/unsigned char tempmacaddr[IEEE80211_ADDR_LEN];
	int lenfrom;
	struct sockaddr_in6 from;
	WAVEHandler wave_Request;
	wave_Request.length = sizeof(uint8_t);
	wave_Request.type = WAVE_MACADDR_REQUEST;
	memcpy(wave_Request.data, &curchan, sizeof(uint8_t));
	if(!isBigEndian())
	swapWAVEData(&wave_Request);	
	switch(wavereqmode) {
#ifndef WIN32 
		case WAVEDEVICE_LOCAL:
			ioctl(wsmpdev, WAVE_REQUEST, &wave_Request);
			memcpy(macadd, &wave_Request.data, IEEE80211_ADDR_LEN);
			result = 0;
			break;
#endif
		case WAVEDEVICE_REMOTE:
			#ifndef WIN32			
			ret = inet_pton( AF_INET, ip, &waveserver4.sin_addr);
			if(ret == 1)
			#else
			ret = WSAStringToAddress(ip,AF_INET,0,(LPSOCKADDR)&waveserver4,&size_sock);
			if(ret==0)
			#endif
			{
				result = sendto(wavesock, (char *)&wave_Request, sizeof(wave_Request), 0, (struct sockaddr *)&waveserver4, waveaddrlen);
			}
			else
			{
				#ifndef WIN32
				ret = inet_pton( AF_INET6, ip, &waveserver6.sin6_addr);
				if( ret == 1)
				#else
					ret = WSAStringToAddress(ip,AF_INET6,0,(LPSOCKADDR)&waveserver6,&size_sock);
					if(ret==0)
				#endif
				{
					result = sendto(wavesock, (char *)&wave_Request, sizeof(wave_Request), 0, (struct sockaddr *)&waveserver6, waveaddrlen);
				}
			}
			if(result >= 0) {
				lenfrom = sizeof(struct sockaddr_in6);
				result = recvfrom(wavesock, rescode, 4, 0, (struct sockaddr *)&from, &lenfrom);
				memcpy(&result, rescode, sizeof(result));
				if(isBigEndian())
					swapGenericData(sizeof(result), &result);
				result = 0;
			}

			if(result >= 0) {
				lenfrom = sizeof(struct sockaddr_in6);
				recvfrom(wavesock, tempmacaddr, IEEE80211_ADDR_LEN, 0, (struct sockaddr *)&from, &lenfrom);
				memcpy(macadd, tempmacaddr, IEEE80211_ADDR_LEN);
				if(isBigEndian())
					swapGenericData(sizeof(macadd), macadd);
				result = 0;
			}
			break;

		default :
			result = -1;
	}
	return;
}


int Get_Available_Serviceinfo(int pid, struct availserviceInfo *get_astinfo ,uint32_t psid)
{

        int result, ret;int i =0;
        struct sockaddr_in6 from;
        WAVEHandler wave_Request;
        wave_Request.length = sizeof(struct availserviceInfo);
        wave_Request.type = WAVE_AVAILSERVICEINFO_REQUEST;
        get_astinfo->psid = psid ;
        memcpy(wave_Request.data, get_astinfo, sizeof(struct availserviceInfo));
	switch(wavereqmode) {
#ifndef WIN32 
                case WAVEDEVICE_LOCAL:
                        ret = ioctl(wsmpdev, WAVE_REQUEST, &wave_Request);
	                memcpy(get_astinfo,wave_Request.data,sizeof(struct availserviceInfo));			
                        result = 0;
                        break;
#endif

        } 
	return 0;  

}
int registerProvider(int pid, WMEApplicationRequest *appreq)
{
	int ret = 0;
        WMEApplicationRequest data;

        appreq->action = WME_ADD_PROVIDER;
        
        memcpy(&data, appreq, sizeof (WMEApplicationRequest));
//	memcpy(&appRegRequest.notif_port, &notifport, sizeof(uint16_t));
	ret =  generateWMEApplRegRequest((void *)&data);
	return ret;
}

int removeProvider(int pid, WMEApplicationRequest *appreq)
{	
        WMEApplicationRequest data;
	int ret = 0;
       
        appreq->action = WME_DEL_PROVIDER;
	memcpy (&data, appreq, sizeof (WMEApplicationRequest));
	ret = generateWMEApplRegRequest((void *)&data);

	return ret;
}

int removeAll()
{
	int ret = 0;
	
	ret = generateWMEApplDelRequest();
	return ret;
}

int registerUser(int pid, WMEApplicationRequest *appreq)
{	
	WMEApplicationRequest appRegRequest;
        WMEApplicationRequest data;
	
	appreq->action = WME_ADD_USER;
	//memcpy (&appRegRequest.entry.ustentry, ustentry, sizeof(USTEntry));
	//memcpy(&appRegRequest.notif_ipaddr, &notifaddr, sizeof(struct in6_addr));
	//memcpy(&appRegRequest.notif_port, &notifport, sizeof(uint16_t));
        memcpy(&data, appreq, sizeof (WMEApplicationRequest));
	
	return generateWMEApplRegRequest((void *)&data);
}


int removeUser(int pid, WMEApplicationRequest *appreq)
{	
        WMEApplicationRequest data;
	
	appreq->action = WME_DEL_USER;
	memcpy (&data, appreq, sizeof(WMEApplicationRequest));
	
	return generateWMEApplRegRequest((void *)&data);
}

int transmitTA(WMETARequest *tareq)
{
    WMETARequest data;

    memcpy(&data, tareq, sizeof (WMETARequest));
    return generateWMETARequest(&data);
}

int wsa_config(WMEWSAConfig *wsaconfig)
{
    WMEWSAConfig data;
    memcpy(&data, wsaconfig, sizeof (WMEWSAConfig));
    return generateWSAConfigfill(&data);
}

int startWBSS(int pid, WMEApplicationRequest *req)
{
	req->requestType = APP_ACTIVE;
	return generateWMEApplRequest(req);
}

int stopWBSS(int pid, WMEApplicationRequest *req)
{
	req->requestType = APP_INACTIVE;
	return generateWMEApplRequest(req);
}

int makeUnavailableWBSS(int pid, WMEApplicationRequest *req)
{
	req->requestType = APP_UNAVAILABLE;
	return generateWMEApplRequest(req);
}

int getWRSSReport(int pid, WMEWRSSRequest *req)
{
	if (req == NULL) return -1;
	return generateWMEWRSSRequest(req);
}

void generate_random_mac(uint8_t *mac_addr)
{
	int b, maddr;
	long long int tsf;
	uint8_t macadd[6];

	tsf = (uint64_t)generatetsfRequest();
	(void)getMACAddr(macadd,0); 

	memcpy(&maddr, &macadd[2], sizeof(int));
	maddr +=  (int) ( tsf % 1000);

	srand((unsigned int)maddr);

	for(b = 0; b < 5; b++)
	{
		macadd[b+1] = (rand()%16) * 16 + (rand()%16) ;
		//printf(" %02x *\n",macadd[b+3]);
	}
	memcpy(mac_addr, macadd, 6);
			
	return;
}

u_int64_t getTsfTimer(int pid)
{
	return generatetsfRequest();
}

u_int64_t setTsfTimer(uint64_t tsf)
{
	return settsfRequest(tsf);
}
int txWSMPacket(int pid, WSMRequest *req)
{
	int i = 0, j = 2, k = 0, sts;
	if (req == NULL)
		return -111;
 	if(req->security == 0) {
		for(k = 0; k < req->data.length; k++)
		{
			req->data.contents[j+req->data.length-k-1] = req->data.contents[i+req->data.length-k-1];
		}
	   //memmove(req->data.contents+2,req->data.contents,req->data.length);
	   req->data.contents[0]=2;
	   req->data.contents[1]=0;
          req->data.length = req->data.length +2;
        }

	sts = generateWSMRequest(req);
 	if(req->security == 0) {
          req->data.length -= 2;
        }
	 
	return sts;
}

int rxWSMPacket(int pid, WSMIndication *ind)
{
	u_int8_t *offset;
	int ret, wsmlen, i=0;
	int k = 0;	
	if (ind == NULL)
		return -1;

	ret = __rxWSMPacket(pid, ind);
	if(ind->psid != 9)
	{
		if((ret > 0) && (ind->data.contents[1]==0)) {
			for(k = 0; k < ind->data.length; k++)
			{
				ind->data.contents[k] = ind->data.contents[k+2];
			}
			
           		//memmove(ind->data.contents,ind->data.contents+2,ind->data.length-2);
           		ind->data.length = ind->data.length-2;
        	}
	}
	if ( ret > sizeof(WSMIndication) )		
		return -2;
		
	return ret;				
}		


#ifndef WIN32
int __txWSMPacket(int pid, void *txbuf, int size)
{
	return __generateWSMRequest(txbuf, size);
}
#endif



int __rxWSMPacket(int pid, void *rxbuf)
{
	int result;
	
	switch(wavereqmode)
	{
#ifndef WIN32
		case WAVEDEVICE_LOCAL:

			if (wsmpblock == 1)
			{
				wsmppoll.events = POLLIN;
				result = poll(&wsmppoll, 1,0);
				switch (result) 
				{
					case 0:
					//Timeout case
						break;
					case -1:
						printf("poll error\n");
						exit(1);
					default:
						if (wsmppoll.revents & POLLIN) 
							return read(wsmpdev, rxbuf, BUFSIZE);
						else 
							return -1;
				}
			} 
			else 
				return read(wsmpdev, rxbuf, BUFSIZE);
			break;
#endif
		case WAVEDEVICE_REMOTE:
			return -1;		
		break;
	}	
}


int cancelTX(int pid, WMECancelTxRequest *req) 
{
	if (req == NULL)
		return -1;

	return generateWMECancelTxRequest(req);
}

/*
********** libwave-WME Functions***********
*/


int generateWMEApplRequest(WMEApplicationRequest *req)
{
	int result, ret;
	char rescode[4];
	int lenfrom;
	struct sockaddr_in6 from;
	//WAVEHandler waveRequest;
	
	waveRequest.length = sizeof(WMEApplicationRequest);
	waveRequest.type = WAVE_APP_REQUEST;
	memcpy (waveRequest.data, req, waveRequest.length);			
	if(!isBigEndian()) 
	swapWAVEData(&waveRequest);
	
	switch(wavereqmode) {
#ifndef WIN32
		case WAVEDEVICE_LOCAL:

			result = ioctl(wsmpdev, WAVE_REQUEST, &waveRequest);
			break;
#endif
		case WAVEDEVICE_REMOTE:

			#ifndef WIN32
			ret = inet_pton( AF_INET, ip, &waveserver4.sin_addr );
			if( ret == 1 )
			#else
			ret = WSAStringToAddress(ip,AF_INET,0,(LPSOCKADDR)&waveserver4,&size_sock);
			if(ret==0)
			#endif
			{
				result = sendto(wavesock, (char *)&waveRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver4, waveaddrlen);
			}
			else
			{
				#ifndef WIN32
				ret = inet_pton( AF_INET6, ip, &waveserver6.sin6_addr );
				if( ret == 1 )
				#else
				ret = WSAStringToAddress(ip,AF_INET6,0,(LPSOCKADDR)&waveserver6,&size_sock);
				if(ret==0)
				#endif			
				{
					result = sendto(wavesock, (char *)&waveRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver6, waveaddrlen);
				}
			}

			if(result >=0 ) {
	                        lenfrom = sizeof(struct sockaddr_in6);
				result = recvfrom(wavesock, rescode, 4, 0, (struct sockaddr *)&from, &lenfrom);
				memcpy(&result, rescode, sizeof(result));
				if(isBigEndian())
					swapGenericData(sizeof(result), &result);
			}						
		break;
		
		default:
			result = -1;								
	}
	return result;
}

int generateWMEApplResponse(WMEApplicationResponse *req)
{
	int result, ret;
	char rescode[4];
	int lenfrom;
	struct sockaddr_in6 from;
	//WAVEHandler waveRequest;
	
	waveRequest.length = sizeof(WMEApplicationResponse);
	waveRequest.type = WAVE_APP_RESPONSE;
	memcpy (waveRequest.data, req, waveRequest.length);			
	if (!isBigEndian()) 
	swapWAVEData(&waveRequest);
	
	switch (wavereqmode) {
#ifndef WIN32
		case WAVEDEVICE_LOCAL:

			result = ioctl(wsmpdev, WAVE_REQUEST, &waveRequest);
			break;
#endif
		case WAVEDEVICE_REMOTE:
			#ifndef WIN32
			ret = inet_pton( AF_INET, ip, &waveserver4.sin_addr );
			if( ret == 1 )
			#else
			ret = WSAStringToAddress(ip,AF_INET,0,(LPSOCKADDR)&waveserver4,&size_sock);
			if(ret==0)
			#endif
			{
				result = sendto(wavesock, (char *)&waveRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver4, waveaddrlen);
			}
			else
			{
				#ifndef WIN32
				ret = inet_pton( AF_INET6, ip, &waveserver6.sin6_addr );
				if( ret == 1 )
				#else
				ret = WSAStringToAddress(ip,AF_INET6,0,(LPSOCKADDR)&waveserver6,&size_sock);
				if(ret==0)
				#endif
				{
					result = sendto(wavesock, (char *)&waveRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver6, waveaddrlen);
				}
			}

			if (result >=0 ) {
	                        lenfrom = sizeof(struct sockaddr_in6);
				result = recvfrom(wavesock, rescode, 4, 0, (struct sockaddr *)&from, &lenfrom);
				memcpy(&result, rescode, sizeof(result));
				if (isBigEndian())
					swapGenericData(sizeof(result), &result);
			}						
		break;
		
		default:
			result = -1;								
	}
	return result;
}


int generateWMEApplDelRequest()
{	
	int result, ret;
	char rescode[4];
	int lenfrom;
	struct sockaddr_in6 from;
	//WAVEHandler waveRequest;
	
	waveRequest.length = 0;
	waveRequest.type = WAVE_APP_DELETE;
	if (!isBigEndian()) 
	swapWAVEData(&waveRequest);
	
	switch (wavereqmode) {
#ifndef WIN32
		case WAVEDEVICE_LOCAL:


			result = ioctl(wsmpdev, WAVE_REQUEST, &waveRequest);
			break;
#endif
		case WAVEDEVICE_REMOTE:
			#ifndef WIN32
			ret = inet_pton( AF_INET, ip, &waveserver4.sin_addr );
			if( ret == 1 )
			#else
			ret = WSAStringToAddress(ip,AF_INET,0,(LPSOCKADDR)&waveserver4,&size_sock);
			if(ret==0)
			#endif
			{
				result = sendto(wavesock, (char *)&waveRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver4, waveaddrlen);
			}
			else
			{
				#ifndef WIN32
				ret = inet_pton( AF_INET6, ip, &waveserver6.sin6_addr );
				if( ret == 1 )
				#else
				ret = WSAStringToAddress(ip,AF_INET6,0,(LPSOCKADDR)&waveserver6,&size_sock);
				if(ret==0)
				#endif
				{
					result = sendto(wavesock, (char *)&waveRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver6, waveaddrlen);
				}
			}

			if (result >=0 ) {
	                        lenfrom = sizeof(struct sockaddr_in6);
				result = recvfrom(wavesock, rescode, 4, 0, (struct sockaddr *)&from, &lenfrom);
				memcpy(&result, rescode, sizeof(result));
				if (isBigEndian()) 
					swapGenericData(sizeof(result), &result);
			}						
		break;
		
		default:
			result = -1;								
	}
	return result;
}

int generateWMEApplRegRequest(void  *req)
{
	int result=-1, ret;
	int length;
	char rescode[4];
	int lenfrom;
	struct sockaddr_in6 from;
	//WAVEHandler waveRequest;
	
	WMEApplicationRequest *WAreq =(WMEApplicationRequest* )req;
	waveRequest.type = WAVE_APP_REGISTER;
        waveRequest.length = sizeof(WMEApplicationRequest);
	memcpy (waveRequest.data, WAreq, waveRequest.length);			
	if (!isBigEndian()) 
	swapWAVEData(&waveRequest);
	
	switch(wavereqmode) {
#ifndef WIN32
		case WAVEDEVICE_LOCAL:

			result = ioctl(wsmpdev, WAVE_REQUEST, &waveRequest);
			if ( ((WAreq->action == WME_ADD_PROVIDER) || (WAreq->action == WME_ADD_USER)) && 
				!isAppRunning && (result >= 0)) 
				isAppRunning = 1;
	
			if ( ((WAreq->action == WME_DEL_PROVIDER) || (WAreq->action == WME_DEL_USER)) && 
				isAppRunning && (result >= 0))
				isAppRunning = 0;		
			break;

#endif		
		case WAVEDEVICE_REMOTE:
			//printf(" %s %d \n",__func__,__LINE__);
			if ( ((WAreq->action == WME_ADD_PROVIDER) || (WAreq->action == WME_ADD_USER)) ) {
				
				if (!isAppRunning) {
				
					if (WAreq->action == WME_ADD_PROVIDER) {
						memcpy (&serviceaddr, &WAreq->ipv6addr, sizeof(struct in6_addr));
						serviceport = (WAreq->serviceport);
			//printf(" %s %d serviceport = %d \n",__func__,__LINE__,serviceport);
					} else {
						memcpy (&serviceaddr, &WAreq->ipv6addr, sizeof(struct in6_addr));
						serviceport = (WAreq->serviceport);
					}
				
					if (serviceaddr.s6_addr &&  serviceport)
							startServiceThread = 1;
					
					if ( invokeWAVEClient() >=0 )
						isAppRunning = 1;
				
				} else {
					goto hack;/*return -1*/
				}		
				
			} else  {
				if (!isAppRunning) { goto hack;/*return -1*/}
			}		
			
			while (!isClientRunning);
			hack:
			#ifndef WIN32
			ret = inet_pton(AF_INET, ip, &waveserver4.sin_addr);
			if( ret == 1 )
			#else
			ret = WSAStringToAddress(ip,AF_INET,0,(LPSOCKADDR)&waveserver4,&size_sock);
			if(ret==0)
			#endif
			{	
				result = sendto(wavesock, (char *)&waveRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver4, sizeof(struct sockaddr_in)); 
			//printf(" %s %d result = \n",__func__,__LINE__,result);
			}
			else
			{
				#ifndef WIN32
				ret = inet_pton(AF_INET6, ip, &waveserver6.sin6_addr );
				if( ret == 1)
				#else
				ret = WSAStringToAddress(ip,AF_INET6,0,(LPSOCKADDR)&waveserver6,&size_sock);
				if(ret==0)
				#endif
				{   
					result = sendto(wavesock, (char *)&waveRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver6, sizeof(struct sockaddr_in6));
				}
			}
				

			if(result < 0 )
				perror("sendto() failed");	

		
			if (result >=0 ) {
	                        lenfrom = sizeof(struct sockaddr_in6);
				result = recvfrom(wavesock, rescode, 4, 0, (struct sockaddr *)&from, &lenfrom);
				memcpy(&result, rescode, sizeof(result));
				if (isBigEndian())
					swapGenericData(sizeof(result), &result);
				if ( ((WAreq->action == WME_DEL_PROVIDER) || (WAreq->action == WME_DEL_USER)) && 
					(result == RESULT_SUCCESS) ) {
					isAppRunning = 0;
					isClientRunning = 0;
					startServiceThread = 0;
				}	
			}
											
		break;
		
		default:
			result = -1;								
	}
		
	return result;
}

int generateWMETARequest(WMETARequest *req) 
{
   // WAVEHandler waveRequest;
    int result, ret, lenfrom;
    char rescode[4];
    struct sockaddr_in6 from;

    waveRequest.length = sizeof (WMETARequest);
    waveRequest.type = WAVE_TA_REQUEST;
    memcpy (waveRequest.data, req, waveRequest.length); 
    if (!isBigEndian()) 
    swapWAVEData(&waveRequest);
            
    switch (wavereqmode) {
	#ifndef WIN32
		case WAVEDEVICE_LOCAL:
        {
			result = ioctl(wsmpdev, WAVE_REQUEST, &waveRequest);
		} 
        break; 
	#endif    
		case WAVEDEVICE_REMOTE:
        {
			#ifndef WIN32
			ret = inet_pton( AF_INET, ip, &waveserver4.sin_addr );
            if( ret == 1 ) 
			#else
			ret = WSAStringToAddress(ip,AF_INET,0,(LPSOCKADDR)&waveserver4,&size_sock);
			if(ret==0)
			#endif
			{
                result = sendto(wavesock, (char *)&waveRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver4, waveaddrlen);
            } 
			else 
			{
				#ifndef WIN32
				ret = inet_pton( AF_INET6, ip, &waveserver6.sin6_addr );
                if( ret == 1 ) 
				#else
				ret = WSAStringToAddress(ip,AF_INET6,0,(LPSOCKADDR)&waveserver6,&size_sock);
				if(ret==0)
				#endif
				{
                    result = sendto(wavesock, (char *)&waveRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver6, waveaddrlen);
                 }
             }
             if (result >=0 ) {
                 lenfrom = sizeof(struct sockaddr_in6);
                 result = recvfrom(wavesock, rescode, 4, 0, (struct sockaddr *)&from, &lenfrom);
                 memcpy(&result, rescode, sizeof(result));
                 if (isBigEndian()) {
                     swapGenericData(sizeof(result), &result);
                  }
              }
        }
        break; 
    }
    return result;
}


int generateWSAConfigfill(WMEWSAConfig * req)
{
    int result, ret, lenfrom;
    char rescode[4];
    struct sockaddr_in6 from;

    waveRequest.length = sizeof (WMEWSAConfig);
    waveRequest.type = WAVE_WSA_CONFIG;
    memcpy (waveRequest.data, req, waveRequest.length);
    if (!isBigEndian()) 
    swapWAVEData(&waveRequest);
            
    switch (wavereqmode) {
		#ifndef WIN32
		case WAVEDEVICE_LOCAL:
        {
            result = ioctl(wsmpdev, WAVE_REQUEST, &waveRequest);
        }
        break;
		#endif
		case WAVEDEVICE_REMOTE:
        {
            
			#ifndef WIN32
			ret = inet_pton( AF_INET, ip, &waveserver4.sin_addr );
            if( ret == 1 ) 
			#else
			ret = WSAStringToAddress(ip,AF_INET,0,(LPSOCKADDR)&waveserver4,&size_sock);
			if(ret==0)
			#endif
			{
                result = sendto(wavesock, (char *)&waveRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver4, waveaddrlen);
            } else 
			{
				#ifndef WIN32
				ret = inet_pton( AF_INET6, ip, &waveserver6.sin6_addr );
                if( ret == 1 ) 
				#else
				ret = WSAStringToAddress(ip,AF_INET,0,(LPSOCKADDR)&waveserver4,&size_sock);
				if(ret==0)
				#endif
				{
                    result = sendto(wavesock, (char *)&waveRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver6, waveaddrlen);
                 }
             }
             if (result >=0 ) {
                 lenfrom = sizeof(struct sockaddr_in6);
                 result = recvfrom(wavesock, rescode, 4, 0, (struct sockaddr *)&from, &lenfrom);
                 memcpy(&result, rescode, sizeof(result));
                 if (isBigEndian()) {
                     swapGenericData(sizeof(result), &result);
                  }
              }
        }
        break;
    }
    return result;
}


int generateMaxtxpowReq()
{
    int result, ret, lenfrom;
    char rescode[4];
    struct sockaddr_in6 from;
    WAVEHandler wavetxpowRequest;
    wavetxpowRequest.length = 0;
    wavetxpowRequest.type = WAVE_MAXTXPOWER_REQUEST;
    if (!isBigEndian()) 
    swapWAVEData(&wavetxpowRequest);
            
    switch (wavereqmode) {
		#ifndef WIN32
		case WAVEDEVICE_LOCAL:
        {
            result = ioctl(wsmpdev, WAVE_REQUEST, &wavetxpowRequest);
            return result;
        }
		#endif
		break;
        case WAVEDEVICE_REMOTE:
        {
			#ifndef WIN32
			ret = inet_pton( AF_INET, ip, &waveserver4.sin_addr );
            if( ret == 1 ) 
			#else
				ret = WSAStringToAddress(ip,AF_INET,0,(LPSOCKADDR)&waveserver4,&size_sock);
				if(ret==0)
			#endif
			{
                result = sendto(wavesock, (char *)&wavetxpowRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver4, waveaddrlen);
            } else 
			{
				#ifndef WIN32
                ret = inet_pton( AF_INET6, ip, &waveserver6.sin6_addr );
                if( ret == 1 ) 
				#else
				ret = WSAStringToAddress(ip,AF_INET6,0,(LPSOCKADDR)&waveserver6,&size_sock);
				if(ret==0)
				#endif
				{
                    result = sendto(wavesock, (char *)&wavetxpowRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver6, waveaddrlen);
                 }
             }
             if (result >= 0 ) {
                 lenfrom = sizeof(struct sockaddr_in6);
                 result = recvfrom(wavesock, rescode, 4, 0, (struct sockaddr *)&from, &lenfrom);
                 memcpy(&result, rescode, sizeof(result));
                 if (isBigEndian()) {
                     swapGenericData(sizeof(result), &result);
                  }
              }
        }
        break;
    }
    return result;
}

int generateWMEWRSSRequest(WMEWRSSRequest *req)
{
	int result, ret;
	char rescode[4];
	int lenfrom;
	struct sockaddr_in6 from;
	//WAVEHandler waveRequest;
	WMEWRSSRequestIndication wrssind;
	
	waveRequest.length = sizeof(WMEWRSSRequest);
	waveRequest.type = WAVE_WRSS_REQUEST;
	memcpy (waveRequest.data, req, waveRequest.length);
	if (!isBigEndian()) 
	swapWAVEData(&waveRequest);
	
	switch(wavereqmode) {
#ifndef WIN32
		case WAVEDEVICE_LOCAL:

			result = ioctl(wsmpdev, WAVE_REQUEST, &waveRequest);
			break;
#endif
		case WAVEDEVICE_REMOTE:
		
			#ifndef WIN32
			ret = inet_pton(AF_INET, ip, &waveserver4.sin_addr );
			if( ret == 1 )
			#else
			ret = WSAStringToAddress(ip,AF_INET,0,(LPSOCKADDR)&waveserver4,&size_sock);
			if(ret==0)
			#endif
			{		
				result = sendto(wavesock, (char *)&waveRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver4, waveaddrlen);
			}
			else
			{
				#ifndef WIN32
				ret = inet_pton(AF_INET6, ip, &waveserver6.sin6_addr );
				if( ret == 1 )
				#else
				ret = WSAStringToAddress(ip,AF_INET6,0,(LPSOCKADDR)&waveserver6,&size_sock);
				if(ret==0)
				#endif
				{
					result = sendto(wavesock, (char *)&waveRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver6, waveaddrlen);
				}
			}
					
			if (result >=0 ) {
	                        lenfrom = sizeof(struct sockaddr_in6);
				result = recvfrom(wavesock, rescode, 4, 0, (struct sockaddr *)&from, &lenfrom);
				memcpy(&result, rescode, sizeof(result));
	
				if (isBigEndian())
					swapGenericData(sizeof(result), &result);
			}
			if (result >= 0) {
	                        lenfrom = sizeof(struct sockaddr_in6);
				recvfrom(wavesock, &wrssind, sizeof(wrssind), 0, (struct sockaddr *)&from, &lenfrom);
				if (isBigEndian())
					swapIndicationData(3, (void*)&wrssind);
				receiveWRSSIndication(&wrssind);
			}																		
		break;
		
		default:
			result = -1;								
	}
	return result;
}


u_int64_t generatetsfRequest()
{
	int result, ret;
	char rescode[4];
	unsigned char restsf[8];
	int lenfrom;
	uint64_t temptsf = 0;
	struct sockaddr_in6 from;
	WAVEHandler wavetsfRequest;
		
	wavetsfRequest.length = 0;
	wavetsfRequest.type = WAVE_TSFTIMER_REQUEST;
	if (!isBigEndian()) 
	swapWAVEData(&wavetsfRequest);

	switch (wavereqmode) {
#ifndef WIN32
		case WAVEDEVICE_LOCAL:

			ioctl(wsmpdev, WAVE_REQUEST, &wavetsfRequest);
			return *(uint64_t *) wavetsfRequest.data;
			break;
#endif
		case WAVEDEVICE_REMOTE:

			#ifndef WIN32
			ret = inet_pton( AF_INET, ip, &waveserver4.sin_addr );
			if( ret == 1 )
			#else
				ret = WSAStringToAddress(ip,AF_INET,0,(LPSOCKADDR)&waveserver4,&size_sock);
				if(ret==0)
			#endif
			{	
				result = sendto(wavesock, (char *)&wavetsfRequest, sizeof(wavetsfRequest), 0, (struct sockaddr *)&waveserver4, waveaddrlen);
			}
			else
			{
				#ifndef WIN32
				ret = inet_pton( AF_INET6, ip, &waveserver6.sin6_addr );
				if( ret == 1 )
				#else
				ret = WSAStringToAddress(ip,AF_INET6,0,(LPSOCKADDR)&waveserver6,&size_sock);
				if(ret==0)
				#endif
				{
					result = sendto(wavesock, (char *)&wavetsfRequest, sizeof(wavetsfRequest), 0, (struct sockaddr *)&waveserver6, waveaddrlen);
				}
			}	

			if (result >=0 ) {
	                        lenfrom = sizeof(struct sockaddr_in6);
				result = recvfrom(wavesock, rescode, 4, 0, (struct sockaddr *)&from, &lenfrom);
				memcpy(&result, rescode, sizeof(result));
				if (isBigEndian())
					swapGenericData(sizeof(result), &result);
			}
			if (result >= 0) {
	                        lenfrom = sizeof(struct sockaddr_in6);
				recvfrom(wavesock, restsf, 8, 0, (struct sockaddr *)&from, &lenfrom);
				memcpy(&temptsf, restsf, 8);
				if (isBigEndian())
					swapGenericData(sizeof(temptsf), &temptsf);
				return temptsf;
			}																		
		break;
		
		default:
			result = -1;								
	}
	return ;
}
u_int64_t settsfRequest(uint64_t tsf)
{
	int result, ret;
	char rescode[4];
	unsigned char restsf[8];
	int lenfrom;
	uint64_t temptsf = 0;
	struct sockaddr_in6 from;
	WAVEHandler wavetsfRequest;
		
	wavetsfRequest.length = sizeof(uint64_t);
	wavetsfRequest.type = WAVE_SETTSFTIMER_REQUEST;
	memcpy(wavetsfRequest.data,&tsf,sizeof(uint64_t));
	if (!isBigEndian()) 
	swapWAVEData(&wavetsfRequest);

	switch (wavereqmode) {
#ifndef WIN32
		case WAVEDEVICE_LOCAL:

			ioctl(wsmpdev, WAVE_REQUEST, &wavetsfRequest);
			return 0;
			break;
#endif
		case WAVEDEVICE_REMOTE:

			#ifndef WIN32
			ret = inet_pton( AF_INET, ip, &waveserver4.sin_addr );
			if( ret == 1 )
			#else
				ret = WSAStringToAddress(ip,AF_INET,0,(LPSOCKADDR)&waveserver4,&size_sock);
				if(ret==0)
			#endif
			{	
				result = sendto(wavesock, (char *)&wavetsfRequest, sizeof(wavetsfRequest), 0, (struct sockaddr *)&waveserver4, waveaddrlen);
			}
			else
			{
				#ifndef WIN32
				ret = inet_pton( AF_INET6, ip, &waveserver6.sin6_addr );
				if( ret == 1 )
				#else
				ret = WSAStringToAddress(ip,AF_INET6,0,(LPSOCKADDR)&waveserver6,&size_sock);
				if(ret==0)
				#endif
				{
					result = sendto(wavesock, (char *)&wavetsfRequest, sizeof(wavetsfRequest), 0, (struct sockaddr *)&waveserver6, waveaddrlen);
				}
			}	

			if (result >=0 ) {
	                        lenfrom = sizeof(struct sockaddr_in6);
				result = recvfrom(wavesock, rescode, 4, 0, (struct sockaddr *)&from, &lenfrom);
				memcpy(&result, rescode, sizeof(result));
				if (isBigEndian())
					swapGenericData(sizeof(result), &result);
			}
			if (result >= 0) {
	                        lenfrom = sizeof(struct sockaddr_in6);
				recvfrom(wavesock, restsf, 8, 0, (struct sockaddr *)&from, &lenfrom);
				memcpy(&temptsf, restsf, 8);
				if (isBigEndian())
					swapGenericData(sizeof(temptsf), &temptsf);
				return temptsf;
			}																		
		break;
		
		default:
			result = -1;								
	}
	return ;
}

int generateWSMRequest(WSMRequest *req)
{	
	int result, ret;
	char rescode[4];
	int lenfrom;
	struct sockaddr_in6 from;
	
	waveRequest.length = sizeof(WSMRequest);
	waveRequest.type = WAVE_WSM_REQUEST;
	memcpy (waveRequest.data, req, waveRequest.length);
	if(!isBigEndian()) 
	swapWAVEData(&waveRequest);
	
	switch(wavereqmode) 
	{
#ifndef WIN32
		case WAVEDEVICE_LOCAL:		
		
			if (wsmpblock == 1) {
				wsmppoll.events = POLLOUT;
					result = poll(&wsmppoll, 1, 0);
					switch (result) {
					case 0:
						//Timeout case
						break;
					case -1:
						printf("poll error\n");
						exit(1);
					default:
						if (wsmppoll.revents & POLLOUT) {
							result = write(wsmpdev, waveRequest.data, sizeof(WSMRequest));
						}
					}
			} else {
				result = write(wsmpdev, waveRequest.data, sizeof(WSMRequest));
			}
			break;
#endif
		case WAVEDEVICE_REMOTE:
			#ifndef WIN32	
			ret = inet_pton(AF_INET, ip, &waveserver4.sin_addr );
			if( ret == 1 )
			#else
			ret = WSAStringToAddress(ip,AF_INET,0,(LPSOCKADDR)&waveserver4,&size_sock);
			if(ret==0)
			#endif
			{
				result = sendto(wavesock, (char *)&waveRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver4, waveaddrlen);
			}
			else
			{
				#ifndef WIN32
				ret = inet_pton(AF_INET6, ip, &waveserver6.sin6_addr );
				if( ret == 1 )
				#else
				ret = WSAStringToAddress(ip,AF_INET6,0,(LPSOCKADDR)&waveserver6,&size_sock);
				if(ret==0)
				#endif
				{
					result = sendto(wavesock, (char *)&waveRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver6, waveaddrlen);
				}
			}
					
			if(result >=0 ) {
	                        lenfrom = sizeof(struct sockaddr_in6);
				result = recvfrom(wavesock, rescode, 4, 0, (struct sockaddr *)&from, &lenfrom);
				memcpy(&result, rescode, sizeof(result));
				
				if(isBigEndian()) 
					swapGenericData(sizeof(result), &result);
			}
		break;
	}
	return result;	

}

#ifndef WIN32
int __generateWSMRequest(void *txbuf, int size)
{
	int result;
	if (wsmpblock == 1) {
		wsmppoll.events = POLLOUT;
		while (1) {
			result = poll(&wsmppoll, 1, 0);
			switch (result) {
				case 0:
					break;
				case -1:
					printf("poll error\n");
					exit(1);
				default:
					if (wsmppoll.revents & POLLOUT) {
						return write(wsmpdev, txbuf, size);
					}
			}
		}	
	} else {
		return write(wsmpdev, txbuf, size);
	}
}
#endif

int generateWMECancelTxRequest(WMECancelTxRequest *req)
{
	int result,ret;
	char rescode[4];
	int lenfrom;
	struct sockaddr_in6 from;
	//WAVEHandler waveRequest;

	waveRequest.length = sizeof(WMECancelTxRequest);
	waveRequest.type = WAVE_CANCELTX_REQUEST;
	memcpy (waveRequest.data, req, waveRequest.length);
	if(!isBigEndian()) 
	swapWAVEData(&waveRequest);
	
	switch(wavereqmode) {
#ifndef WIN32
		case WAVEDEVICE_LOCAL:
			result = ioctl(wsmpdev, WAVE_REQUEST, &waveRequest);
		break;
#endif
		case WAVEDEVICE_REMOTE:
			#ifndef WIN32
			ret = inet_pton(AF_INET, ip, &waveserver4.sin_addr );
			if( ret == 1 )
			#else
				ret = WSAStringToAddress(ip,AF_INET,0,(LPSOCKADDR)&waveserver4,&size_sock);
				if(ret==0)
			#endif
			{
				result = sendto(wavesock, (char *)&waveRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver4, waveaddrlen);
			}
			else
			{
				#ifndef WIN32
				ret = inet_pton( AF_INET6, ip, &waveserver6.sin6_addr );
				if( ret == 1 )
				#else
				ret = WSAStringToAddress(ip,AF_INET6,0,(LPSOCKADDR)&waveserver6,&size_sock);
				if(ret==0)
				#endif
				{
					result = sendto(wavesock, (char *)&waveRequest, sizeof(waveRequest), 0, (struct sockaddr *)&waveserver6, waveaddrlen);
				
				}
			}

			if(result >=0 ) {
	                        lenfrom = sizeof(struct sockaddr_in6);
				result = recvfrom(wavesock, rescode, 4, 0, (struct sockaddr *)&from, &lenfrom);
				memcpy(&result, rescode, sizeof(result));
				if(isBigEndian()) 
					swapGenericData(sizeof(result), &result);
			}						
		break;
		
		default:
			result = -1;								
	}
	return result;
}


void swapGenericData(int size, void *data)
{
	switch(size) {
		case 2:
			*(uint16_t*)data = swap16_(*(uint16_t*)data);
		break;
	
		case 4:
			*(uint32_t*)data = swap32_(*(uint32_t*)data);
		break;
		
		case 8:
			*(uint64_t*)data = swap64(*(uint64_t*)data);
		break;
		
	}
}
void swapWAVEData(WAVEHandler *wh)
{
	WSMRequest *wsm;
	WMEApplicationRequest *reg;
	WMEWRSSRequest *wrss;	
	if (!wh)
		return;
		
	switch(wh->type) {
		
		case WAVE_APP_REQUEST:
		
		break;
		
		case WAVE_APP_REGISTER:
			reg = (WMEApplicationRequest *)wh->data;
			//swapGenericData(sizeof(reg->notif_ipaddr.s_addr), &reg->notif_ipaddr.s_addr);
			//swapGenericData(sizeof(reg->notif_port), &reg->notif_port);
			if(reg->action == WME_ADD_PROVIDER || reg->action == WME_DEL_PROVIDER) {
				//swapGenericData(sizeof(reg->entry.pstentry.length), &reg->entry.pstentry.length);
				//swapGenericData(sizeof(reg->entry.pstentry.ipaddr.s_addr), &reg->entry.pstentry.ipaddr.s_addr);
				swapGenericData(sizeof(reg->psid), &reg->psid);
				swapGenericData(sizeof(reg->serviceport), &reg->serviceport);
				swapGenericData(sizeof(reg->localserviceid), &reg->localserviceid);
			} else if(reg->action == WME_ADD_USER || reg->action == WME_DEL_USER) {
				//swapGenericData(sizeof(reg->entry.ustentry.ipaddr.s_addr), &reg->entry.ustentry.ipaddr.s_addr);
				swapGenericData(sizeof(reg->psid), &reg->psid);
				swapGenericData(sizeof(reg->serviceport), &reg->serviceport);
				swapGenericData(sizeof(reg->localserviceid), &reg->localserviceid);
			}	 	 
		break;
		
		case WAVE_APP_DELETE:
		
		break;
		
		case WAVE_WSM_REQUEST:
			wsm = (WSMRequest *)wh->data;
			swapGenericData(sizeof(wsm->data.length), &wsm->data.length);	 
			swapGenericData(sizeof (wsm->psid), &wsm->psid);
			swapGenericData(sizeof(wsm->expirytime),&wsm->expirytime);

		break;
		
		case WAVE_WRSS_REQUEST:
			wrss = (WMEWRSSRequest *)wh->data;
			swapGenericData(sizeof(wrss->wrssreq_elem.request.startTime), &wrss->wrssreq_elem.request.startTime);
			swapGenericData(sizeof(wrss->wrssreq_elem.request.duration), &wrss->wrssreq_elem.request.duration);
		break;
		
		case WAVE_CANCELTX_REQUEST:
		
		break;
				
		case WAVE_TSFTIMER_REQUEST:
		
		break;
		
		case WAVE_MACADDR_REQUEST:
		
		break;
	}
	
	swapGenericData(sizeof(wh->type), &wh->type);				
	swapGenericData(sizeof(wh->length), &wh->length);
}

void swapIndicationData(int type, void *ind)
{
	WMENotificationIndication *notifind;
	WMEWRSSRequestIndication *wrssind;
	WSMIndication *wsmind;
	WMEApplicationIndication *appind;
	
	if (!ind)
		return;
		
	switch(type) {
	
		case 1:
			notifind = (WMENotificationIndication *)ind;
			swapGenericData(sizeof(notifind->ipaddr), &notifind->ipaddr);
			swapGenericData(sizeof(notifind->serviceport), &notifind->serviceport);
			swapGenericData(sizeof(notifind->timestamp), &notifind->timestamp);
			swapGenericData(sizeof(notifind->localtime), &notifind->localtime);
			swapGenericData(sizeof(notifind->wmeparam[0].txopLimit), &notifind->wmeparam[0].txopLimit);
			swapGenericData(sizeof(notifind->wmeparam[1].txopLimit), &notifind->wmeparam[1].txopLimit);
			swapGenericData(sizeof(notifind->wmeparam[2].txopLimit), &notifind->wmeparam[2].txopLimit);
			swapGenericData(sizeof(notifind->wmeparam[3].txopLimit), &notifind->wmeparam[3].txopLimit);
			swapGenericData(sizeof(notifind->wrssreport.measurementTime), &notifind->wrssreport.measurementTime);
		break;
		
		case 2:
			wsmind = (WSMIndication *)ind;
			swapGenericData(sizeof(wsmind->data.length), &wsmind->data.length);
		break;
		
		case 3:
			wrssind = (WMEWRSSRequestIndication*)ind;
			swapGenericData(sizeof(wrssind->wrssreport.measurementTime), &wrssind->wrssreport.measurementTime);
		break;
		
		case 4:
			/*For TSF, Not used now*/
		break;
		
		case 5:
			appind = (WMEApplicationIndication *)ind;
			swapGenericData(sizeof(appind->ipaddr), &appind->ipaddr);
		break;
	
	}

	
}

void getUSTIpv6Addr(struct in6_addr *ustAddr, char *ifName)
{
	int ret = 1;
	char str[46];
	struct ifaddrs *ifaddr, *ifa;
	struct ifreq ifr;
	struct sockaddr_in6 *sin6=malloc(28);
	int ath_ret;
	int broadcast = 1;
	strcpy(iface,ifName);
#ifdef  WIN32
	char ipv6[100]="\0";
	int ipv6length = sizeof(ipv6),size = sizeof(struct sockaddr_storage);
	WCHAR *Interface = calloc(25,2);
    unsigned long flags = GAA_FLAG_INCLUDE_PREFIX,outBufLen=0;

    PIP_ADAPTER_ADDRESSES pAddresses = NULL;
    PIP_ADAPTER_ADDRESSES pCurrAddresses = NULL;
    PIP_ADAPTER_UNICAST_ADDRESS pUnicast = NULL;

	mbstowcs(Interface,ifName,strlen(ifName)+1);
       // Allocate a 15 KB buffer to start with.
    outBufLen = MALLOC_BUFFER;

        pAddresses = (IP_ADAPTER_ADDRESSES *) malloc(outBufLen);
        if (pAddresses == NULL) 
		{
            printf("Memory allocation failed for IP_ADAPTER_ADDRESSES struct\n");
		}
        
		ret = GetAdaptersAddresses(AF_INET6, flags, NULL, pAddresses, &outBufLen); 

    if (ret == NO_ERROR) {
        pCurrAddresses = pAddresses;
        while (pCurrAddresses) {        
			//printf("\tFriendly name: %wS\n", pCurrAddresses->FriendlyName);		
			if(!wcscmp(pCurrAddresses->FriendlyName,Interface))
			{
				pUnicast = pCurrAddresses->FirstUnicastAddress;

					ret=WSAAddressToString((LPSOCKADDR)pUnicast->Address.lpSockaddr ,(DWORD)pUnicast->Address.iSockaddrLength , NULL , ipv6, &ipv6length);
					if(ret!=0)
						printf("WSAAdressToString  Failed Error No %u\n",WSAGetLastError());
					else {
						 wprintf(L"Interface Name \"%s\" ",pCurrAddresses->FriendlyName);
						  printf("Ipv6 Address --> %s\n",ipv6);
							ret = WSAStringToAddress(ipv6,AF_INET6,NULL,(LPSOCKADDR )sin6,&size);	  	
							if(ret!=0)
							printf("WSAStringToAddress  Failed. Error No= %u\n",WSAGetLastError());
							else 
								*ustAddr = sin6->sin6_addr;
									
					}
				break;
			}
			pCurrAddresses = pCurrAddresses->Next;
			if(pCurrAddresses==NULL){
			printf("\tEntered Interface Name is not Present on Host Pc\n\tUnable To fetch Ipv6 Address\n");
			exit(-1);
			}
		}
    } else 
        printf("Call to GetAdaptersAddresses failed with error: %d\n",WSAGetLastError());

    if (pAddresses)
        free(pAddresses);
#else
	memset(&ifaddr, 0, sizeof(ifaddr));	
	if( getifaddrs(&ifaddr) == 0 )
        {
            for( ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next )
            {
                if( ifa->ifa_addr == NULL ) continue;
                if( ( ifa->ifa_flags & IFF_UP ) == 0 ) continue;
                if( ifa->ifa_addr->sa_family != AF_INET6 ) continue;
				if((ret = strcmp(ifa->ifa_name,ifName)) == 0)
                {
	                sin6 = (struct sockaddr_in6 *)(ifa->ifa_addr);
					if(inet_ntop(ifa->ifa_addr->sa_family,(void *)&(sin6->sin6_addr), str, sizeof(str)) == NULL )
					{
						printf("IPV6 Interface = %s: inet_ntop failed\n", ifa->ifa_name );
					}
					else
					{
						printf("IPV6 Interface =%s      %s\n",ifa->ifa_name, str );
					}
                	printf("%s: inet_pton = %d\n", __func__, inet_pton(AF_INET6, str, &sin6->sin6_addr));
                }
            }

            *ustAddr = sin6->sin6_addr;
        }
	if(strcmp(iface,"ath0")==0)
	{	
	    ath_ret = setsockopt(wavesock, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(broadcast));
	    if(ath_ret < 0)
            {
                perror("setsockopt(SO_BROADCAST)\n");
            }
	    ath_ret = setsockopt(wavesock, SOL_SOCKET, SO_BINDTODEVICE, iface, strlen(iface));
	    if(ath_ret < 0)
            {
                perror("setsockopt(SO_BINDTODEVICE)\n");
            }

	}
#endif
	
}

#ifndef WIN32
void mysleep(int sleep_secs, long int sleep_nsecs) {

    struct timespec myntsleep, myntleft_sleep;
    int sts;

        myntsleep.tv_nsec = sleep_nsecs;
        myntsleep.tv_sec = sleep_secs;
        myntleft_sleep.tv_nsec = 0;
        myntleft_sleep.tv_sec = 0;
                do
                {
                        sts = nanosleep(&myntsleep, &myntleft_sleep);
                if((myntleft_sleep.tv_nsec == 0) && (myntleft_sleep.tv_sec == 0)) {
                break;
                }
                        memcpy(&myntsleep,&myntleft_sleep, sizeof(myntsleep));
                memset(&myntleft_sleep, 0, sizeof(myntleft_sleep));
                }
                while( 1 );
}
#endif

int get_date(int Tdate)
{
        char *token = NULL;
        char *str = NULL;
        char *temp = NULL;
        int ret_date = 0;
        time_t date;
        int i = 0, month_num = 0, day =0, year = 0;
        char mon[12][4]={ {"Jan\0"},
                          {"Feb\0"},
                          {"Mar\0"},
                          {"Apr\0"},
                          {"May\0"},
                          {"Jun\0"},
                          {"Jul\0"},
                          {"Aug\0"},
                          {"Sep\0"},
                          {"Oct\0"},
                          {"Nov\0"},
                          {"Dec\0"}
                        };
        date = Tdate;
        str = ctime(&date);
         //printf("Input date %ld to %s\n", date, str);
        //temp = (char *)malloc(sizeof(int));
        temp = (char *)malloc(10);
        token = strtok(str," ");                //week_day

        token = strtok(NULL," ");               //month
        sscanf(token,"%s",temp);
        for(i=1; i <= 12; i++)
        {
                if(!strcmp(mon[i-1],temp))
                        month_num = i;
        }

        token = strtok(NULL," ");               //date
        sscanf(token,"%s",temp);
        day = atoi(temp);
        //strcat(str,temp);

        token = strtok(NULL,":");               //hour

        token = strtok(NULL,":");               //min

        token = strtok(NULL," ");               //sec

        token = strtok(NULL," ");               //year
        sscanf(token,"%s",temp);
        year = atoi(temp);

        //strcat(str,temp);
        ret_date = (day * 10000) + (month_num * 100) + (year % 100) ;
        free(temp);
        return ret_date;
}


int get_date_yyyymmdd(int Tdate)
{
        char *token = NULL;
        char *str = NULL;
        char *temp = NULL;
        int ret_date = 0;
        time_t date;
        int i = 0, month_num = 0, day =0, year = 0;
        char mon[12][4]={ {"Jan\0"},
                          {"Feb\0"},
                          {"Mar\0"},
                          {"Apr\0"},
                          {"May\0"},
                          {"Jun\0"},
                          {"Jul\0"},
                          {"Aug\0"},
                          {"Sep\0"},
                          {"Oct\0"},
                          {"Nov\0"},
                          {"Dec\0"}
                        };
        date = Tdate;
        str = ctime(&date);
         //printf("Input date %ld to %s\n", date, str);
        //temp = (char *)malloc(sizeof(int));
        temp = (char *)malloc(10);
        token = strtok(str," ");                //week_day

        token = strtok(NULL," ");               //month
        sscanf(token,"%s",temp);
        for(i=1; i <= 12; i++)
        {
                if(!strcmp(mon[i-1],temp))
                        month_num = i;
        }

        token = strtok(NULL," ");               //date
        sscanf(token,"%s",temp);
        day = atoi(temp);
        //strcat(str,temp);

        token = strtok(NULL,":");               //hour

        token = strtok(NULL,":");               //min

        token = strtok(NULL," ");               //sec

        token = strtok(NULL," ");               //year
        sscanf(token,"%s",temp);
        year = atoi(temp);

        //strcat(str,temp);
        ret_date = (year * 10000) + (month_num * 100) + (day)  ;
        free(temp);
        return ret_date;
}

int get_time(int Ttime)
{
        char *token = NULL;
        char *str = NULL;
        char *temp = NULL;
        int ret_time = 0;
        time_t tme;
        tme = Ttime;
        str = ctime(&tme);

        temp = (char *)malloc(sizeof(int));
        token = strtok(str," ");                //week_day

        token = strtok(NULL," ");               //month

        token = strtok(NULL," ");               //date

        token = strtok(NULL,":");               //hour
        sscanf(token,"%s",temp);
        memcpy(str,temp,sizeof(int));

        token = strtok(NULL,":");               //min
        sscanf(token,"%s",temp);
        strcat(str,temp);

        token = strtok(NULL," ");               //sec
        sscanf(token,"%s",temp);
        strcat(str,temp);

        ret_time = atoi(str);

        free(temp);

        return ret_time;
}
