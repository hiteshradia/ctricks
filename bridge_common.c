
/* standard include files */
#include<stdio.h>
#include<syslog.h>
#include<string.h>
#include<sys/ioctl.h>
#include<net/if.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<linux/if_bridge.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include <config_sku.h>

/*symbolic constants */
#define BAV_SCRIPT_FILE "bav_script_file"
#define MAX_PORTS 100
#define NUM_OF_VAP (CONFIG_NO_OF_VAPS * CONFIG_NO_OF_RADIOS)+1
#define VAP_LEN  30

/*include files */
#include "bridge_common.h"


int get_vlan_id_of_vap(char *interface, char *vapIndx)
{
	char vapInt[100];
	char buff[100];
	char cmd[1024];
	FILE *fd;

	strcpy(vapInt, interface);
	strcat(vapInt, vapIndx);
	

	strcpy(cmd, "/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script ");
	strcat(cmd, vapInt);
	
	fd = popen(cmd, "r");
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	
	if(strcmp(buff, "")) {
		return atoi(buff);
	}
	return -1;
}
int spanning_tree_enabled()
{
	char buff[100];
	FILE *fd;

	

	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script spanningTree ", "r");
	
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	
	if(strcmp(buff, "")) {
		//syslog(LOG_ERR, "spanningTree:%d\n", atoi(buff));
		return atoi(buff);
	}
	return -1;
}
int get_network_integrality_status()
{
        char buff[100];
        FILE *fd;



        fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script networkIntegralityCheck ", "r");
        if(fd != NULL)  {
                fscanf(fd, "%s", buff);
        }
	pclose(fd);	

        if(strcmp(buff, "")) {
                return atoi(buff);
        }
        return -1;
}
int get_dhcp_client_status()
{
	char buff[100];
	FILE *fd;

	

	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script dhcpClientStatus ", "r");
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	
	if(strcmp(buff, "")) {
		return atoi(buff);
	}
	return -1;
}
#ifdef CONFIG_TUNNEL
int get_tunnel_status()
{
	char buff[10];
	FILE *fd;

	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script TunnelStatus ", "r");
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	
	if(strcmp(buff, "")) {
		return atoi(buff);
	}
	return -1;
}
int config_tunnel(int startup)
{
    char tunnel_name[50];
    char tunnelIpv6[50];
    char tunnelIpv6gw[50];
    char tunnelIpv6NetPrefix[50];
    char remotetunnIpv4[20];
    char ip[50];
    char cmd[150];
    FILE *fd = NULL;

    fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script TunnelName", "r");
    if(fd != NULL){
	fscanf(fd, "%s", tunnel_name);
    }
    pclose(fd);
    fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script TunnelIpv6Addr", "r");
    if(fd != NULL){
	fscanf(fd, "%s", tunnelIpv6);
    }
    pclose(fd);
    fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script RemoteTunnelIpv4Addr", "r");
    if(fd != NULL){
	fscanf(fd, "%s",  remotetunnIpv4);
    }
    pclose(fd);
    fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script TunnelIpv6Gateway", "r");
    if(fd != NULL){
	fscanf(fd, "%s", tunnelIpv6gw);
    }
    pclose(fd);
    fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script TunnelIpv6NetPrefix", "r");
    if(fd != NULL){
	fscanf(fd, "%s", tunnelIpv6NetPrefix);
    }
    pclose(fd);
    fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemIpAddr", "r");
    if(fd != NULL){
	fscanf(fd, "%s", ip);
    }
    pclose(fd);

    if(startup == 2)
    {
	sprintf(cmd, "ip -6 route flush dev %s", tunnel_name);
	system(cmd);
	sprintf(cmd, "ip -6 route flush dev %s", tunnel_name);
	system(cmd);
	sprintf(cmd, "ip li se %s down", tunnel_name);
	system(cmd);
	sprintf(cmd, "ip tu de %s", tunnel_name);
	system(cmd);
	return 0;
    }

    sprintf(cmd, "ip tu ad %s mode sit remote %s local %s ttl 255", tunnel_name, remotetunnIpv4, ip);
    system(cmd);
    sprintf(cmd, "ip li se %s up", tunnel_name);
    system(cmd);
    sprintf(cmd, "ip a a %s dev %s", tunnelIpv6, tunnel_name);
    system(cmd);
    if((strcmp(tunnelIpv6NetPrefix,"0") != 0) && (strcmp(tunnelIpv6gw,"0") != 0))                    {
        system("sysctl -w net.ipv6.conf.all.forwarding=1");
        sprintf(cmd,"ip r a %s via %s dev %s", tunnelIpv6NetPrefix, tunnelIpv6gw, tunnel_name);
        system(cmd);
    }
    return 0;
}
#endif
#ifdef LOCOMATE
void apply_nat_settings(void)
{
    FILE *fd = NULL;
    char buff[5] = "";
    int natsts = -1;
    fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script nat_status", "r");
    if(fd != NULL) {
    	fscanf(fd, "%s", buff);
        pclose(fd);	
    }
    if(strcmp(buff, "")) {
        natsts = atoi(buff);
    }
    if(natsts < 0)
	return;
    if(natsts) {
	/* insert the modules and add iptables rule */
        system("insmod /lib/modules/net/netfilter/x_tables.ko > /dev/null 2>&1");
        system("insmod /lib/modules/net/netfilter/xt_mark.ko > /dev/null 2>&1");
        system("insmod /lib/modules/net/netfilter/xt_MARK.ko > /dev/null 2>&1");
        system("insmod /lib/modules/net/netfilter/xt_mac.ko > /dev/null 2>&1");
        system("insmod /lib/modules/net/netfilter/xt_tcpudp.ko > /dev/null 2>&1");
        system("insmod /lib/modules/net/netfilter/nf_conntrack.ko > /dev/null 2>&1");
        system("insmod /lib/modules/net/ipv4/netfilter/nf_conntrack_ipv4.ko > /dev/null 2>&1");
        system("insmod /lib/modules/net/ipv4/netfilter/nf_nat.ko > /dev/null 2>&1");
        system("insmod /lib/modules/net/ipv4/netfilter/ip_tables.ko > /dev/null 2>&1");
        system("insmod /lib/modules/net/ipv4/netfilter/iptable_nat.ko > /dev/null 2>&1");
        system("insmod /lib/modules/net/ipv4/netfilter/iptable_filter.ko > /dev/null 2>&1");
        system("insmod /lib/modules/net/ipv4/netfilter/iptable_mangle.ko > /dev/null 2>&1");
        system("insmod /lib/modules/net/ipv4/netfilter/ipt_MASQUERADE.ko > /dev/null 2>&1");
        system("insmod /lib/modules/net/ipv4/netfilter/ipt_REDIRECT.ko > /dev/null 2>&1");
        system("insmod /lib/modules/net/ipv4/netfilter/ipt_REJECT.ko > /dev/null 2>&1");
        system("echo 1 > /proc/sys/net/ipv4/ip_forward");
        system("iptables -t nat -A POSTROUTING -o brtrunk -j MASQUERADE");
    } else {
	/* flush the iptables rule and remove modules */
        system("iptables -F -t nat");
        system("rmmod ipt_REJECT.ko");
        system("rmmod ipt_REDIRECT.ko");
        system("rmmod ipt_MASQUERADE.ko");
        system("rmmod iptable_mangle.ko");
        system("rmmod iptable_filter.ko");
        system("rmmod iptable_nat.ko");
        system("rmmod ip_tables.ko");
        system("rmmod nf_nat.ko");
        system("rmmod nf_conntrack_ipv4.ko");
        system("rmmod nf_conntrack.ko");
        system("rmmod xt_tcpudp.ko");
        system("rmmod xt_mac.ko");
        system("rmmod xt_MARK.ko");
        system("rmmod xt_mark.ko");
        system("rmmod x_tables.ko");
    }
    return;
}
int get_ipv6_status()
{
	char buff[100];
	FILE *fd;

	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script ipv6_status ", "r");
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	
	if(strcmp(buff, "")) {
		return atoi(buff);
	}
	return -1;
}

int set_ipv6_addr(char *ipv6_addr, char *old_ipv6_addr, char *interface)
{
    char cmd[200] = {"0"}, buff[100] = {"0"}, link_local[50] = {"0"};
    struct in6_addr addr1;
    struct in6_addr addr2;
    FILE *fd = NULL;
    char wlan_bridge_interface[50];
    int num_brdg = get_num_bridges();
    int if_num = get_interface_for_second_brdg(); 

    if(!strcmp(interface,"brtrunk"))
    {
    	sprintf(cmd, "ifconfig eth1 | grep -i inet6 | head -n1 | grep -i Link | awk '{print $3}'");
    	fd = popen(cmd,"r");
    	if(fd != NULL) {
	    memset(link_local, '\0', sizeof(link_local));
	    fscanf(fd, "%s", link_local);
	    pclose(fd);
	    if(!strcmp(link_local,""))
	    {
	    	sprintf(cmd, "ifconfig eth0 | grep -i inet6 | head -n1 | grep -i Link | awk '{print $3}'");
	    	fd = popen(cmd,"r");
            	if(fd != NULL) {
                    memset(link_local, '\0', sizeof(link_local));
                    fscanf(fd, "%s", link_local);
                    pclose(fd);
                }
	    }
    	}
    }
    /* set the given ipv6 address */
        printf("setip1\n");
        sprintf(cmd, "ip -6 ro");
        system(cmd);
        printf("sf%s\n",interface);
    sprintf(cmd, "ip -6 addr flush %s", interface);
    system(cmd);
        printf("setip2\n");
        sprintf(cmd, "ip -6 ro");
        system(cmd);
    if((num_brdg == 2) && (!strcmp(interface,"brwifi")))
    {
	if(if_num != 255) // only one interface on brwifi
        {
            sprintf(wlan_bridge_interface,"wifi%dvap0",if_num);
    	    sprintf(cmd, "ifconfig %s | grep -i inet6 | grep -i Link | head -n1 | awk '{print $3}' ", wlan_bridge_interface);
    	    fd = popen(cmd,"r");
    	    if(fd != NULL) {
	        memset(link_local, '\0', sizeof(link_local));
		fscanf(fd, "%s", link_local);
		pclose(fd);
    	    }
    	    sprintf(cmd, "ifconfig %s allmulti", wlan_bridge_interface);
	    system(cmd);
        }
    }
        printf("setip3\n");
        sprintf(cmd, "ip -6 ro");
        system(cmd);
    sprintf(cmd, "ip -6 addr add %s dev %s", link_local, interface);
    system(cmd);
        printf("setip4\n");
        sprintf(cmd, "ip -6 ro");
        system(cmd);
    sprintf(cmd, "ip -6 addr add %s dev %s", ipv6_addr, interface);
    system(cmd);
        printf("setip5\n");
        sprintf(cmd, "ip -6 ro");
        system(cmd);
    sprintf(cmd, "ip r a %s dev %s", ipv6_addr, interface);
    system(cmd);
        printf("setip6\n");
        sprintf(cmd, "ip -6 ro");
        system(cmd);

    /* confirm that the given ipv6 address sets properly, if not restore the old address */
    sprintf(cmd, "ifconfig %s | grep 'inet6 addr' | head -n1 | awk '{printf $3}'", interface);
    fd = popen(cmd,"r");
    if(fd != NULL)  {
	memset(buff, '\0', sizeof(buff));
	fscanf(fd, "%s", buff);
	pclose(fd);
    }
    inet_pton(AF_INET6, ipv6_addr, &addr1);
    inet_pton(AF_INET6, buff, &addr2);
    if((!strcmp(buff,"")) || (!memcmp(&addr1, &addr2, sizeof(struct in6_addr)))){
        printf("setip7\n");
        sprintf(cmd, "ip -6 ro");
        system(cmd);
        sprintf(cmd, "ip -6 addr flush %s", interface);
    	system(cmd);
        printf("setip8\n");
        sprintf(cmd, "ip -6 ro");
        system(cmd);
    	sprintf(cmd, "ip -6 addr add %s dev %s", old_ipv6_addr, interface);
    	system(cmd);
        printf("setip9\n");
        sprintf(cmd, "ip -6 ro");
        system(cmd);
    	sprintf(cmd, "ip r d %s dev %s", ipv6_addr, interface);
    	system(cmd);
        printf("setip10\n");
        sprintf(cmd, "ip -6 ro");
        system(cmd);
        if((num_brdg == 2) && (!strcmp(interface,"brwifi")))
	{
            if(if_num != 255) // only one interface on brwifi
            {
                sprintf(wlan_bridge_interface,"wifi%dvap0",if_num);
	        sprintf(cmd, "ip -6 addr flush %s", wlan_bridge_interface);
	        system(cmd);
        printf("setip11\n");
        sprintf(cmd, "ip -6 ro");
        system(cmd);
    		sprintf(cmd, "ip -6 addr add %s dev %s", link_local, wlan_bridge_interface);
   		system(cmd);
        printf("setip1\n");
        sprintf(cmd, "ip -6 ro");
        system(cmd);
            }
	}
        syslog(LOG_ERR,"Invalid ipv6 address, restoring into previous ipv6 address %s",old_ipv6_addr); 
	return -1;
    } else {
	syslog(LOG_ERR,"IPV6 %s is assigned to %s",ipv6_addr, interface); 
    	return 0;
    }
}
#endif
int get_management_vlanID()
{
	char buff[100] = {"0"};
	FILE *fd;

	

	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script management_vlanID ", "r");
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	
	if(strcmp(buff, "")) {
		return atoi(buff);
	}
	return -1;
}
int get_untagged_vlan_status()
{
	char buff[100];
	FILE *fd;

	

	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script untaggedVlanStatus ", "r");
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	
	if(strcmp(buff, "")) {
		return atoi(buff);
	}
	return -1;
}
int get_untagged_vlanID()
{
	char buff[100];
	FILE *fd;

	

	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script untaggedVlanID ", "r");
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	
	if(strcmp(buff, "")) {
		return atoi(buff);
	}
	return -1;
}
int get_dhcp_server_status()
{
	char buff[100];
	FILE *fd;

	

	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script dhcpServerStatus ", "r");
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	
	if(strcmp(buff, "")) {
		return atoi(buff);
	}
	return -1;
}
int get_dhcps_vlanId()
{
	char buff[100];
	FILE *fd;

	

	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script dhcpsVlanId ", "r");
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	
	if(strcmp(buff, "")) {
		return atoi(buff);
	}
	return -1;
}
int get_simple_config()
{
	char buff[100];
	FILE *fd;

	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script simpleConfig ", "r");
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	
	if(strcmp(buff, "")) {
		return atoi(buff);
	}
	return -1;
}
#ifdef CONFIG_MULTIBRIDGE
int get_brwifi_ipv6_status()
{
	char buff[100];
	FILE *fd;

	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script brwifi_ipv6_status ", "r");
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	
	if(strcmp(buff, "")) {
		return atoi(buff);
	}
	return -1;
}
int get_num_bridges()
{
	char buff[100];
	FILE *fd;
	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script totalBridges ","r");
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	
	if(strcmp(buff, "")) {
		return atoi(buff);
	}
	return -1;
}
int get_interface_for_second_brdg()
{
	char buff[100];
	FILE *fd;
	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script interfaceForbrwifi","r");
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	
	if(strcmp(buff, "")) {
		return atoi(buff);
	}
	return -1;
}
#endif  /* CONFIG_MULTIBRIDGE */
int get_old_management_vlanID()
{
	char buff[100];
	FILE *fd;

	

	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk  management_vlanID ", "r");
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	
	if(strcmp(buff, "")) {
		return atoi(buff);
	}
	return -1;
}
int get_old_dhcps_vlanId()
{
	char buff[100];
	FILE *fd;

	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk  dhcpsVlanId ", "r");
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	
	if(strcmp(buff, "")) {
		return atoi(buff);
	}
	return -1;
}
int get_old_vlan_id_of_vap(char *interface, char *vapIndx)
{
	char vapInt[100];
	char buff[100];
	char cmd[1024];
	FILE *fd;

	strcpy(vapInt, interface);
	strcat(vapInt, vapIndx);
	

	strcpy(cmd, "/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk ");
	strcat(cmd, vapInt);
	
	fd = popen(cmd, "r");
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	
	if(strcmp(buff, "")) {
		return atoi(buff);
	}
	return -1;
}
int get_old_simple_config()
{
	char buff[100];
	FILE *fd;

	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk simpleConfig ", "r");
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	
	if(strcmp(buff, "")) {
		return atoi(buff);
	}
	return -1;
}
#if 0
int get_num_interfaces()
{
	struct ifreq ifname;
	int sock_fd;
	int num_int = 0;
	sock_fd = socket(AF_INET, SOCK_STREAM, 0);
	if(sock_fd == -1)	{
		perror("socket");
		exit(-1);
	}
	
	memset(&ifname, 0, sizeof(ifname));
	strcpy(ifname.ifr_name, "wifi0"); 	
	ioctl(sock_fd, SIOCGIFINDEX,&ifname );
	if(ifname.ifr_ifindex)	{
		num_int++;
	}
	memset(&ifname, 0, sizeof(ifname));
	strcpy(ifname.ifr_name, "wifi1"); 	
	ioctl(sock_fd, SIOCGIFINDEX,&ifname );
	if(ifname.ifr_ifindex)	{
		num_int++;
	}
	
	return num_int;
}
#endif

char *change_in_vlan_id(char *interface, char *vapIndx, int vlanID)
{
	
	char vapInt[100];
	char buff[100];
	char cmd[1024];
	FILE *fd;
	

	strcpy(vapInt, interface);
	strcat(vapInt, vapIndx);

	strcpy(cmd, "/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk ");
	strcat(cmd, vapInt);
	
	fd = popen(cmd, "r");
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	if(!strcmp(buff, "")) {
		return NULL;
	}
	if(atoi(buff) == vlanID) {
		return "no";
	}
	else {
		return "yes";
	}

}
char *apply_system_settings(int management_vlan_ID)
{
	char ip[20], old_ip[20];
	char ipv6Status[4], old_ipv6Status[4];
	char ipv6[50], old_ipv6[50];
	char netmask[20], old_netmask[20];
	char gw[20], old_gw[20];
	char ipv6gw[50], old_ipv6gw[50];
	char wlanipv6gw[50], old_wlanipv6gw[50];
	char wlanipv4gw[50], old_wlanipv4gw[50];
	char wlanipv4[50], old_wlanipv4[50];
	char ipv6NetPrefix[50], old_ipv6NetPrefix[50];
	char wlanipv6NetPrefix[50], old_wlanipv6NetPrefix[50];
	char wlanipv6[50], old_wlanipv6[50];
	char hostname[20], old_hostname[20];
	char cmd[1024];
        char buff[100],prev_ip[50];
	FILE *fd;
	char managed_bridge[50];
	char wlan_bridge[50];
	int dhcpClientStatus = 0;
	int dhcpServerStatus = 0;
	int dhcpsVlanId = 0;
	char dhcpsIpAddr[20];
	char dhcpsNetMask[20];
	unsigned int simple_config;
	unsigned int old_simple_config;
    char *token = NULL;
	int num_int = get_num_interfaces();
    	struct in6_addr addr1={0};
    	struct in6_addr addr2={0};
	simple_config = get_simple_config();

	if (simple_config)
		sprintf(managed_bridge, "brtrunk");
	else
		sprintf(managed_bridge, "brvlan%d", management_vlan_ID);
#ifdef CONFIG_MULTIBRIDGE 
	int num_brdg = get_num_bridges();
	if(num_brdg == 2)
	    sprintf(wlan_bridge, "brwifi");
#endif /* CONFIG_MULTIBRIDGE */
	if (access("/tmp/bridge_vlan_script.bk", F_OK) == -1) {
		//printf("On startup\n");
		fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemName", "r");
		if(fd != NULL)	{
			fscanf(fd, "%s", hostname);
			sprintf(cmd, "hostname %s", hostname);
			system(cmd);
		}
		pclose(fd);	

		system("kill -9  `/bin/pidof udhcpc` > /dev/null 2>&1");
		/* First, assign the static IP configured in DB */
		fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemIpAddr", "r");
		if(fd != NULL)	{
			fscanf(fd, "%s", ip);
		}
		pclose(fd);	
		fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemNetMask", "r");
		if(fd != NULL)	{
			fscanf(fd, "%s", netmask);
		}
		pclose(fd);	
		sprintf(cmd, "ifconfig %s %s netmask %s", managed_bridge, ip, netmask);
                sprintf(buff,"IP Address: %s is assigned to %s.\n", ip, managed_bridge); 
                syslog(LOG_ERR,"%s",buff);  /*namita*/	
		system(cmd);

		fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemGateway", "r");
		if(fd != NULL)	{
			fscanf(fd, "%s", gw);
		}
		pclose(fd);	
		sprintf(cmd, "route add default gw %s dev %s", gw, managed_bridge);
		system(cmd);

#ifdef LOCOMATE
		apply_nat_settings();
		/* if ipv6 status is static, assign the static IPV6 configured in DB */
		if (get_ipv6_status() == 0) {
			fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemIpv6Addr", "r");
			if(fd != NULL)	{
				fscanf(fd, "%s", ipv6);
			}
			pclose(fd);
			sprintf(cmd, "ifconfig %s | grep 'inet6 addr' | head -n1 |awk '{printf $3}'", managed_bridge);
			fd = popen(cmd,"r");
			if(fd != NULL)  {
        	                fscanf(fd, "%s", old_ipv6);
			}
			pclose(fd);
			set_ipv6_addr(ipv6, old_ipv6, managed_bridge);

			fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script ipv6gateway", "r");
			if(fd != NULL)	{
				fscanf(fd, "%s", ipv6gw);
			}
			pclose(fd);
			fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script ipv6NetworkPrefix", "r");
			if(fd != NULL)	{
				fscanf(fd, "%s", ipv6NetPrefix);
			}
			pclose(fd);
			if((strcmp(ipv6NetPrefix,"0") != 0) && (strcmp(ipv6gw,"0") != 0))
			{
			    system("sysctl -w net.ipv6.conf.all.forwarding=1");
			    sprintf(cmd,"ip r a %s via %s dev %s",ipv6NetPrefix,ipv6gw,managed_bridge);
			    system(cmd);
			}
		}
#endif   /* LOCOMATE */
#ifdef CONFIG_MULTIBRIDGE
    	   if(num_brdg == 2) {
		 
		fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script brwifi_Ipv4Addr", "r");
		if(fd != NULL)  {
                                fscanf(fd, "%s", wlanipv4);
                        }
 		pclose(fd);
		sprintf(cmd, "ifconfig %s %s",wlan_bridge,wlanipv4);
		system(cmd);
                sprintf(buff,"IP Address: %s is assigned to %s.\n", wlanipv4,wlan_bridge); 

		if (get_brwifi_ipv6_status() == 0) {
			fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script brwifi_Ipv6Addr", "r");
			if(fd != NULL)	{
				fscanf(fd, "%s", wlanipv6);
			}
			pclose(fd);
			sprintf(cmd, "ifconfig %s | grep 'inet6 addr' | head -n1 | awk '{printf $3}'", wlan_bridge);
			fd = popen(cmd,"r");
			if(fd != NULL)  {
        	                fscanf(fd, "%s", old_wlanipv6);
			}
			pclose(fd);
			set_ipv6_addr(wlanipv6, old_wlanipv6, wlan_bridge);
		    	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script brwifi_ipv6gateway", "r");
			if(fd != NULL)	{
				fscanf(fd, "%s", wlanipv6gw);
			}
			pclose(fd);
			fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script brwifi_ipv4gateway", "r");
                        if(fd != NULL)  {
                                fscanf(fd, "%s", wlanipv4gw);
                        }
                        pclose(fd);
			if((strcmp(wlanipv4gw,"0.0.0.0")!=0)){
				sprintf(cmd, "route add default gw %s dev %s", wlanipv4gw, wlan_bridge);
	                	system(cmd);
			}
			fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script brwifi_ipv6NetworkPrefix", "r");
			if(fd != NULL)	{
				fscanf(fd, "%s", wlanipv6NetPrefix);
			}
			pclose(fd);
			if((strcmp(wlanipv6NetPrefix,"0") != 0) && (strcmp(wlanipv6gw,"0") != 0))
			{
				system("sysctl -w net.ipv6.conf.all.forwarding=1");
				sprintf(cmd,"ip r a %s via %s dev %s",wlanipv6NetPrefix,wlanipv6gw,wlan_bridge);
				system(cmd);
			}
		}
			    sprintf(cmd,"ip r f dev brwifi");
			    system(cmd);
			    sprintf(cmd,"ip r f dev brtrunk");
			    system(cmd);
	   }
#endif  /* CONFIG_MULTIBRIDGE */
#ifdef CONFIG_TUNNEL
		/* on startup */
		/* if tunnel status is enable add a tunnel */
		if (get_tunnel_status() == 1) {
		    config_tunnel(1);
		}
#endif  /* CONFIG_TUNNEL */
	
		/* Run DHCP client, if required */
		if (get_dhcp_client_status() == 1) {
			snprintf(cmd, sizeof(cmd), "/sbin/udhcpc -b -i %s -h %s", managed_bridge, 
							hostname);
			system(cmd);
			sleep(2);
			sprintf(cmd, "ifconfig %s | grep 'inet addr' | awk '{printf $2}' | awk -F : '{print $2}'", managed_bridge);
			fd = popen(cmd,"r");
			if(fd != NULL)  {
        	                fscanf(fd, "%s", buff);
	                }
                	syslog(LOG_ERR,"DHCP IP %s is assigned to %s",buff,managed_bridge); 
		}

	} else if (!strcmp(change_in_management_vlan_id( management_vlan_ID) ,"yes")) {
		//printf("Change in the management VLAN id\n");
		system("kill -9 `/bin/pidof udhcpc` > /dev/null 2>&1");
		/* First, assign the static IP configured in DB */
		fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemIpAddr", "r");
		if(fd != NULL)	{
			fscanf(fd, "%s", ip);
		}
		pclose(fd);	
		fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemNetMask", "r");
		if(fd != NULL)	{
			fscanf(fd, "%s", netmask);
		}
		pclose(fd);	

		sprintf(cmd, "ifconfig %s %s netmask %s", managed_bridge, ip, netmask);	
                sprintf(buff,"IP Address: %s is assigned to %s.\n", ip, managed_bridge); 
                syslog(LOG_ERR,"%s",buff); /*namita*/ 
		system(cmd);

		fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemGateway", "r");
		if(fd != NULL)	{
			fscanf(fd, "%s", gw);
		}
		pclose(fd);	
		system("route del default dev brtrunk");
		sprintf(cmd, "route add default gw %s dev %s", gw, managed_bridge);
		system(cmd);
		/* Run the DHCP client, if required */
		if (get_dhcp_client_status() == 1) {
			fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemName", "r");
			if(fd != NULL)	{
				fscanf(fd, "%s", hostname);
			}
			pclose(fd);	
			snprintf(cmd, sizeof(cmd), "/sbin/udhcpc -b -i %s -h %s", managed_bridge, 
							hostname);
			system(cmd);
			sleep(2);
                        sprintf(cmd, "ifconfig %s | grep 'inet addr' | awk '{printf $2}' | awk -F : '{print $2}'", managed_bridge);
			fd = popen(cmd,"r");
                        if(fd != NULL)  {
                                fscanf(fd, "%s", buff);
                        }
                        syslog(LOG_ERR,"DHCP IP %s is assigned to %s",buff,managed_bridge);
		}
	} else {
		//printf("Change in the AP name or IP address\n");
		dhcpClientStatus = get_dhcp_client_status();
		old_simple_config = get_old_simple_config();
		if (!strcmp(change_in_dhcp_client_status(dhcpClientStatus), "yes")) {
			//printf("Change in DHCP client status\n");
			if (get_dhcp_client_status() == 1) {
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemName", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", hostname);
				}
				pclose(fd);	
				system("kill -9 `/bin/pidof udhcpc` > /dev/null 2>&1");
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemIpAddr", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", ip);
				}
				pclose(fd);	
				snprintf(cmd, sizeof(cmd), "/sbin/udhcpc -b -i %s -h %s", managed_bridge, 
								hostname);
				system(cmd);
				sleep(2);
                        	sprintf(cmd, "ifconfig %s | grep 'inet addr' | awk '{printf $2}' | awk -F : '{print $2}'", managed_bridge);
				fd = popen(cmd,"r");
                        	if(fd != NULL)  {
                                	fscanf(fd, "%s", buff);
                        	}
                        	syslog(LOG_ERR,"DHCP IP %s is assigned to %s",buff,managed_bridge);
			} else {
				system("kill -9 `/bin/pidof udhcpc` > /dev/null 2>&1");
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemIpAddr", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", ip);
				}
				pclose(fd);	
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemNetMask", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", netmask);
				}
				pclose(fd);	
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemGateway", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", gw);
				}
				pclose(fd);	
	
				snprintf(cmd, sizeof(cmd), "/usr/local/bin/assign_static_ip %s %s %s %s", 
						managed_bridge, ip, netmask, gw);
                		sprintf(buff,"IP Address: %s is assigned to %s.\n", ip, managed_bridge); 
                                syslog(LOG_ERR,"%s",buff); /*namita*/
				system(cmd);
			}
		} else if (simple_config != old_simple_config) {
			//printf("Change in config transition\n");
			/* First, assign the static IP confgired in DB */
			system("kill -9  `/bin/pidof udhcpc` > /dev/null 2>&1");

			fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemIpAddr", "r");
			if(fd != NULL)	{
				fscanf(fd, "%s", ip);
			}
			pclose(fd);	
			fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemNetMask", "r");
			if(fd != NULL)	{
				fscanf(fd, "%s", netmask);
			}
			pclose(fd);	
			sprintf(cmd, "ifconfig %s %s netmask %s", managed_bridge, ip, netmask);
			sprintf(buff,"IP Address: %s is assigned to %s.\n", ip, managed_bridge); 
                        syslog(LOG_ERR,"%s",buff);                        
			system(cmd);

			fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemGateway", "r");
			if(fd != NULL)	{
				fscanf(fd, "%s", gw);
			}
			pclose(fd);	
			system("route del default dev brtrunk");
			sprintf(cmd, "route add default gw %s dev %s", gw, managed_bridge);
			system(cmd);
#ifdef LOCOMATE
		apply_nat_settings();
		/* if ipv6 status is static, assign the static IPV6 configured in DB */
			if (get_ipv6_status() == 0) {
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemIpv6Addr", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", ipv6);
				}
				pclose(fd);
				sprintf(cmd, "ifconfig %s | grep 'inet6 addr' | head -n1 | awk '{printf $3}'", managed_bridge);
				fd = popen(cmd,"r");
				if(fd != NULL)  {
        	                	fscanf(fd, "%s", old_ipv6);
	                	}
				pclose(fd);
				set_ipv6_addr(ipv6, old_ipv6, managed_bridge);
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script ipv6gateway", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", ipv6gw);
				}
				pclose(fd);
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script ipv6NetworkPrefix", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", ipv6NetPrefix);
				}
				pclose(fd);
				if((strcmp(ipv6NetPrefix,"0") != 0) && (strcmp(ipv6gw,"0") != 0))
				{
					system("sysctl -w net.ipv6.conf.all.forwarding=1");
					sprintf(cmd,"ip r a %s via %s dev %s",ipv6NetPrefix,ipv6gw,managed_bridge);
					system(cmd);	
				}
			}
#endif   /* LOCOMATE */
#ifdef CONFIG_MULTIBRIDGE
    	   if(num_brdg == 2) {

		fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script brwifi_Ipv4Addr", "r");
                                if(fd != NULL)  {
                                        fscanf(fd, "%s", wlanipv4);
                                }
                                pclose(fd);

		if (get_brwifi_ipv6_status() == 0) {
			fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script brwifi_Ipv6Addr", "r");
			if(fd != NULL)	{
				fscanf(fd, "%s", wlanipv6);
			}
			pclose(fd);
			sprintf(cmd, "ifconfig %s | grep 'inet6 addr' | head -n1 | awk '{printf $3}'", wlan_bridge);
			fd = popen(cmd,"r");
			if(fd != NULL)  {
        	                fscanf(fd, "%s", old_wlanipv6);
			}
			pclose(fd);
			set_ipv6_addr(wlanipv6, old_wlanipv6, wlan_bridge);
		    	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script brwifi_ipv6gateway", "r");
			if(fd != NULL)	{
				fscanf(fd, "%s", wlanipv6gw);
			}
			pclose(fd);
			fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script brwifi_ipv6NetworkPrefix", "r");
			if(fd != NULL)	{
				fscanf(fd, "%s", wlanipv6NetPrefix);
			}
			pclose(fd);
			if((strcmp(wlanipv6NetPrefix,"0") != 0) && (strcmp(wlanipv6gw,"0") != 0))
			{
				system("sysctl -w net.ipv6.conf.all.forwarding=1");
				sprintf(cmd,"ip r a %s via %s dev %s",wlanipv6NetPrefix,wlanipv6gw,wlan_bridge);
				system(cmd);
			}
		}
		 fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script brwifi_ipv4gateway", "r");
                        if(fd != NULL)  {
                                fscanf(fd, "%s", wlanipv4gw);
                        }
                        pclose(fd);

          	if((strcmp(wlanipv4,"0.0.0.0")!=0) || (strcmp(wlanipv4gw,"0.0.0.0") != 0)) {
                  snprintf(cmd, sizeof(cmd), "/usr/local/bin/assign_static_ip %s %s %s %s",
                                                 wlan_bridge,wlanipv4, netmask, wlanipv4gw);
                  sprintf(buff,"IP Address: %s is assigned to %s.\n", wlanipv4, wlan_bridge);
                  syslog(LOG_ERR,"%s",buff);
                  system(cmd);
                 }
                 system("route del default dev brwifi");
                 sprintf(cmd, "route add default gw %s dev %s",wlanipv4gw, wlan_bridge);
                 system(cmd);
	   }
#endif  /* CONFIG_MULTIBRIDGE */
#ifdef CONFIG_TUNNEL
		/* Change in config transition */
		/* if tunnel status is enable add a tunnel */
		if (get_tunnel_status() == 1) {  
		    config_tunnel(0);
		} else if (get_tunnel_status() == 0) {   /* delete the existing tunnel */
		    config_tunnel(2);
		}
#endif  /* CONFIG_TUNNEL */
			/* Run the DHCP client, if required */
			if (get_dhcp_client_status() == 1) {
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemName", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", hostname);
				}
				pclose(fd);	
				snprintf(cmd, sizeof(cmd), "/sbin/udhcpc -b -i %s -h %s", managed_bridge, 
								hostname);
				system(cmd);
				sleep(2);
				sprintf(cmd, "ifconfig %s | grep 'inet addr' | awk '{printf $2}' | awk -F : '{print $2}'", managed_bridge);
				fd = popen(cmd,"r");
                        	if(fd != NULL)  {
                                	fscanf(fd, "%s", buff);
                        	}
                        	syslog(LOG_ERR,"DHCP IP %s is assigned to %s",buff,managed_bridge);
			}
		} else {
			//printf("No change in the dhcp client status\n");
			if (get_dhcp_client_status() == 0) {
				//printf("dhcp client is disabled\n");
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemIpAddr", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", ip);
				}
				pclose(fd);	
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk systemIpAddr", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", old_ip);
				}
				pclose(fd);	
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemNetMask", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", netmask);
				}
				pclose(fd);	
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk systemNetMask", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", old_netmask);
				}
				pclose(fd);	
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemGateway", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", gw);
				}
				pclose(fd);	
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk systemGateway", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", old_gw);
				}
				pclose(fd);	
 
#ifdef LOCOMATE
			        apply_nat_settings();
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemIpv6Addr", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", ipv6);
				}
				pclose(fd);	
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk systemIpv6Addr", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", old_ipv6);
				}
				pclose(fd);	
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script ipv6gateway", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", ipv6gw);
				}
				pclose(fd);
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk ipv6gateway", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", old_ipv6gw);
				}
				pclose(fd);
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script ipv6NetworkPrefix", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", ipv6NetPrefix);
				}
				pclose(fd);
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk ipv6NetworkPrefix", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", old_ipv6NetPrefix);
				}
				pclose(fd);
                    strncpy(prev_ip,ipv6,50);
                    token=strtok(prev_ip,"/");
                    if(token)    
    				inet_pton(AF_INET6, token, &addr1);
                    strncpy(prev_ip,old_ipv6,50);
                    token=strtok(prev_ip,"/");
                    if(token)    
    				inet_pton(AF_INET6, token, &addr1);
                    printf("bt ip old%s new %s\n",old_ipv6,ipv6);
				if((memcmp(&addr1, &addr2, sizeof(struct in6_addr))) || strcmp(ipv6gw, old_ipv6gw) || strcmp(ipv6NetPrefix, old_ipv6NetPrefix))
				{
                    memset(prev_ip,0,50);
					sprintf(cmd, "ifconfig %s | grep 'inet6 addr' |  head -n1 |awk '{printf $3}'", managed_bridge);
					fd = popen(cmd,"r");
					if(fd != NULL)  {
        	                	fscanf(fd, "%s", prev_ip);
	                		}
					pclose(fd);
					set_ipv6_addr(ipv6, prev_ip, managed_bridge);

					system("sysctl -w net.ipv6.conf.all.forwarding=1");
					sprintf(cmd,"ip r d %s via %s dev %s",old_ipv6NetPrefix,old_ipv6gw,managed_bridge);
					system(cmd);
				        if((strcmp(ipv6NetPrefix,"0") != 0) && (strcmp(ipv6gw,"0") != 0))
			 	        {
					     sprintf(cmd,"ip r a %s via %s dev %s",ipv6NetPrefix,ipv6gw,managed_bridge);
					    system(cmd);
				        }
				}

#endif
#ifdef CONFIG_MULTIBRIDGE 
    	   if(num_brdg == 2) {
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script brwifi_Ipv4Addr", "r");
                                if(fd != NULL)  {
                                        fscanf(fd, "%s", wlanipv4);
                                }
                                pclose(fd);
                                fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk brwifi_Ipv4Addr", "r");
                                if(fd != NULL)  {
                                        fscanf(fd, "%s", old_wlanipv4);
                                }
                                pclose(fd);
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script brwifi_Ipv6Addr", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", wlanipv6);
				}
				pclose(fd);	
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk brwifi_Ipv6Addr", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", old_wlanipv6);
				}
				pclose(fd);	
		    		fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script brwifi_ipv6gateway", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", wlanipv6gw);
				}
				pclose(fd);
		    		fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk brwifi_ipv6gateway", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", old_wlanipv6gw);
				}
				pclose(fd);
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script brwifi_ipv4gateway", "r");
                                if(fd != NULL)  {
                                        fscanf(fd, "%s", wlanipv4gw);
				}
                                pclose(fd);
                                fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk brwifi_ipv4gateway", "r");
                                if(fd != NULL)  {
                                        fscanf(fd, "%s", old_wlanipv4gw);
                                }
                                pclose(fd);
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script brwifi_ipv6NetworkPrefix", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", wlanipv6NetPrefix);
				}
				pclose(fd);
				fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk brwifi_ipv6NetworkPrefix", "r");
				if(fd != NULL)	{
					fscanf(fd, "%s", old_wlanipv6NetPrefix);
				}
				pclose(fd);
        memset( &addr1, 0,sizeof(struct in6_addr));
        memset( &addr2, 0,sizeof(struct in6_addr));
            int retw=0;
                    strncpy(prev_ip,wlanipv6,50);
                    token=strtok(prev_ip,"/");
                    if(token)    
    			    retw=inet_pton(AF_INET6, token, &addr1);
                    if(retw <=0)
                        printf("prob in addr1 %d \n",retw);
                    retw=0;
                    strncpy(prev_ip,old_wlanipv6,50);
                    token=strtok(prev_ip,"/");
                    if(token)
    			    retw=inet_pton(AF_INET6, token, &addr2);
                    if(retw <=0)
                        printf("prob in addr2 %d \n",retw);
                char * tmpchar1=(char *)&addr1;
                int i2=0;
                printf("a1\n\n");
                for(i2=0;i2<sizeof(struct in6_addr);i2++)
                    printf("%02x ",*(tmpchar1 + i2) & 0xff);
                printf("\n");
                printf("a2\n\n");
                tmpchar1=(char *)&addr2;
                for(i2=0;i2<sizeof(struct in6_addr);i2++)
                    printf("%02x ",*(tmpchar1 + i2) & 0xff);
                printf("\n");
                    printf("ip old%s new %s\n",old_wlanipv6,wlanipv6);
                    printf("gw old%s new %s\n",old_wlanipv6gw,wlanipv6gw);
                printf("prefix old%s new %s\n",old_wlanipv6NetPrefix,wlanipv6NetPrefix);
				if(memcmp(&addr1, &addr2, sizeof(struct in6_addr))) 
                    printf("addr\n");
                if (strcmp(wlanipv6gw, old_wlanipv6gw) )
                    printf("gw\n");
                if(strcmp(wlanipv6NetPrefix, old_wlanipv6NetPrefix))
                    printf("prefix\n");
                
				if((memcmp(&addr1, &addr2, sizeof(struct in6_addr))) || strcmp(wlanipv6gw, old_wlanipv6gw) || strcmp(wlanipv6NetPrefix, old_wlanipv6NetPrefix))
				{
                    memset(prev_ip,0,50);
					sprintf(cmd, "ifconfig %s | grep 'inet6 addr' |  head -n1 |awk '{printf $3}'", wlan_bridge);
					fd = popen(cmd,"r");
					if(fd != NULL)  {
        	                	fscanf(fd, "%s", prev_ip);
	                		}
					pclose(fd);
        printf("bsi\n");
        sprintf(cmd, "ip -6 ro");
        system(cmd);
					set_ipv6_addr(wlanipv6, prev_ip, wlan_bridge);
        printf("asi\n");
        sprintf(cmd, "ip -6 ro");
        system(cmd);
					system("sysctl -w net.ipv6.conf.all.forwarding=1");
        printf("asysctl\n");
        sprintf(cmd, "ip -6 ro");
        system(cmd);
					sprintf(cmd,"ip r d %s via %s dev %s",old_wlanipv6NetPrefix,old_wlanipv6gw,wlan_bridge);
					system(cmd);
        printf("aiprd\n");
        sprintf(cmd, "ip -6 ro");
        system(cmd);
				        if((strcmp(wlanipv6NetPrefix,"0") != 0) && (strcmp(wlanipv6gw,"0") != 0))
			 	        {
					    sprintf(cmd,"ip r a %s via %s dev %s",wlanipv6NetPrefix,wlanipv6gw,wlan_bridge);
					    system(cmd);
					}
        printf("aifipra\n");
        sprintf(cmd, "ip -6 ro");
        system(cmd);
				}
				//brwifi ipv4 gateway
				if(strcmp(wlanipv4,old_wlanipv4) || strcmp(wlanipv4gw, old_wlanipv4gw)){
                                snprintf(cmd, sizeof(cmd), "/usr/local/bin/assign_static_ip %s %s %s %s",
                                                        wlan_bridge,wlanipv4, netmask, wlanipv4gw);
                                	system(cmd);
                                        sprintf(buff,"IP Address: %s is assigned to %s.\n", wlanipv4, wlan_bridge);
                                        syslog(LOG_ERR,"%s",buff);
				}
				system("route del default dev brwifi");
				sprintf(cmd, "route add default gw %s dev %s",wlanipv4gw, wlan_bridge);
                                system(cmd);   
	      }
#endif /* CONFIG_MULTIBRIDGE */
#ifdef CONFIG_TUNNEL
		/* Change in the database */
		/* if tunnel status is enable configure a tunnel */
		if (get_tunnel_status() == 1) {
		    config_tunnel(0);
		} else if (get_tunnel_status() == 0) {   /* delete the existing tunnel */
		    config_tunnel(2);
		}
#endif  /* CONFIG_TUNNEL */
				if(strcmp(ip, old_ip) || strcmp(netmask, old_netmask) || strcmp(gw, old_gw)) {
					//printf("Change in IP address, Netmask or Gateway\n");
					snprintf(cmd, sizeof(cmd), "/usr/local/bin/assign_static_ip %s %s %s %s",
							managed_bridge, ip, netmask, gw);
					sprintf(buff,"IP Address: %s is assigned to %s.\n", ip, managed_bridge); 
                                        syslog(LOG_ERR,"%s",buff); 
					system(cmd);
				}
		                system("route del default dev brtrunk");                        	
				sprintf(cmd, "route add default gw %s dev %s", gw, managed_bridge);
                        	system(cmd); 
			}
			fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script systemName", "r");
			if(fd != NULL)	{
				fscanf(fd, "%s", hostname);
			}
			pclose(fd);	
			fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk systemName", "r");
			if(fd != NULL)	{
				fscanf(fd, "%s", old_hostname);
			}
			pclose(fd);	
			if(strcmp(hostname, old_hostname)) {
				//printf("Change in system name\n");
				sprintf(cmd, "hostname %s", hostname);
				system(cmd);
				if (get_dhcp_client_status() == 1) {
					system("kill -9  `/bin/pidof udhcpc`");
					snprintf(cmd, sizeof(cmd), " /sbin/udhcpc -b  -i %s -h %s", managed_bridge, 
						hostname);	
					system(cmd);
						 sleep(2);
                       			sprintf(cmd, "ifconfig %s | grep 'inet addr' | awk '{printf $2}' | awk -F : '{print $2}'", managed_bridge);
					fd = popen(cmd,"r");
                        		if(fd != NULL)  {
                                		fscanf(fd, "%s", buff);
                        		}
                        		syslog(LOG_ERR,"DHCP IP %s is assigned to %s",buff,managed_bridge);
				}
			}
		}
	}
	/* DHCP Server VLAN settings */
	//printf("DHCP server VLAN settings\n");
	dhcpServerStatus = get_dhcp_server_status();
	dhcpsVlanId = get_dhcps_vlanId();
	if (dhcpServerStatus == 1) {
		if (dhcpsVlanId != management_vlan_ID) {
			fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script dhcpsIpAddr", "r");
			if(fd != NULL)	{
				fscanf(fd, "%s", dhcpsIpAddr);
			}
			pclose(fd);	
			fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script dhcpsNetMask", "r");
			if(fd != NULL)	{
				fscanf(fd, "%s", dhcpsNetMask);
			}
			pclose(fd);	
			sprintf(cmd, "ifconfig brvlan%d %s netmask %s;", dhcpsVlanId, dhcpsIpAddr, dhcpsNetMask);
			sprintf(buff,"IP Address: %s is assigned to brvlan%d.\n", dhcpsIpAddr, dhcpsVlanId); 
                        syslog(LOG_ERR,"%s",buff);  
			system(cmd);
			/* Load iptable modules */
			system("insmod /lib/modules/net/netfilter/x_tables.ko > /dev/null 2>&1");
			system("insmod /lib/modules/net/netfilter/xt_tcpudp.ko > /dev/null 2>&1");
			system("insmod /lib/modules/net/ipv4/netfilter/ip_tables.ko > /dev/null 2>&1");
			system("insmod /lib/modules/net/ipv4/netfilter/iptable_filter.ko > /dev/null 2>&1");
			/* Apply iptable rules */
			system("iptables -t filter -D INPUT -j dhcps_chain > /dev/null 2>&1");
			system("iptables -t filter -D OUTPUT -j dhcps_chain > /dev/null 2>&1");
			system("iptables -t filter -F dhcps_chain > /dev/null 2>&1");
			system("iptables -t filter -X dhcps_chain > /dev/null 2>&1");

			system("iptables -t filter -N dhcps_chain > /dev/null 2>&1");
			/* Insert rules to allow DHCP packets */
			system("iptables -t filter -A dhcps_chain -p udp --source-port 67 -j ACCEPT > /dev/null 2>&1");
			system("iptables -t filter -A dhcps_chain -p udp --destination-port 67 -j ACCEPT > /dev/null 2>&1");
			/* Insert iptables rules to allow redirected packets to wifidog */
			system("iptables -t filter -A dhcps_chain -p tcp --source-port 2060 -j ACCEPT > /dev/null 2>&1");
			system("iptables -t filter -A dhcps_chain -p tcp --destination-port 2060 -j ACCEPT > /dev/null 2>&1");
			/* Insert rules to block everything else */
			sprintf(cmd,"iptables -t filter -A dhcps_chain --destination %s -j DROP > /dev/null 2>&1",dhcpsIpAddr);
			system(cmd);
			sprintf(cmd,"iptables -t filter -A dhcps_chain --source %s -j DROP > /dev/null 2>&1",dhcpsIpAddr);
			system(cmd);
			system("iptables -t filter -A dhcps_chain -j RETURN > /dev/null 2>&1");
			system("iptables -t filter -I INPUT -j dhcps_chain > /dev/null 2>&1");
			system("iptables -t filter -I OUTPUT -j dhcps_chain > /dev/null 2>&1");
		} else {
			/* Remove IP table rules */
			system("iptables -t filter -D INPUT -j dhcps_chain > /dev/null 2>&1");
			system("iptables -t filter -D OUTPUT -j dhcps_chain > /dev/null 2>&1");
			system("iptables -t filter -F dhcps_chain > /dev/null 2>&1");
			system("iptables -t filter -X dhcps_chain > /dev/null 2>&1");
			/* Remove the iptable modules */
			system("rmmod iptable_filter > /dev/null 2>&1");
			system("rmmod ip_tables > /dev/null 2>&1");
			system("rmmod xt_tcpudp > /dev/null 2>&1");
			system("rmmod x_tables > /dev/null 2>&1");
		}
	}
}

char *change_in_management_vlan_id(int management_vlanID)
{
	
	char buff[100]={"0"};
	FILE *fd;
	
	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk management_vlanID","r");
	
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	if(!strcmp(buff, "")) {
		return NULL;
	}
	if(atoi(buff) == management_vlanID) {
		return "no";
	}
	else {
		return "yes";
	}

}

char *change_in_dhcps_vlanId(int dhcpsVlanId)
{
	
	char buff[100];
	FILE *fd;
	
	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk dhcpsVlanId","r");
	
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	if(!strcmp(buff, "")) {
		return NULL;
	}
	if(atoi(buff) == dhcpsVlanId) {
		return "no";
	}
	else {
		return "yes";
	}

}
char *change_in_network_integrality_status(int networkIntegralityCheck)
{

        char buff[100];
        FILE *fd;

        fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk networkIntegralityCheck","r");

        if(fd != NULL)  {
                fscanf(fd, "%s", buff);
        }
	pclose(fd);	
        if(!strcmp(buff, "")) {
                return NULL;
        }
        if(atoi(buff) == networkIntegralityCheck) {
                return "no";
        }
        else {
                return "yes";
        }

}
char *change_in_dhcp_client_status(int dhcpClientStatus)
{
	
	char buff[100];
	FILE *fd;
	
	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk dhcpClientStatus","r");
	
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	if(!strcmp(buff, "")) {
		return NULL;
	}
	if(atoi(buff) == dhcpClientStatus) {
		return "no";
	}
	else {
		return "yes";
	}

}
char *change_in_dhcp_server_status(int dhcpServerStatus)
{
	
	char buff[100];
	FILE *fd;
	
	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk dhcpServerStatus","r");
	
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	if(!strcmp(buff, "")) {
		return NULL;
	}
	if(atoi(buff) == dhcpServerStatus) {
		return "no";
	}
	else {
		return "yes";
	}

}
char *change_in_untagged_vlanid(int untagged_vlanID)
{
	
	char buff[100];
	FILE *fd;
	
	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk untaggedVlanID","r");
	
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	if(!strcmp(buff, "")) {
		return NULL;
	}
	if(atoi(buff) == untagged_vlanID) {
		return "no";
	}
	else {
		return "yes";
	}

}
#if 1
char *change_in_untagged_vlanid_status(int untaggedVlanStatus)
{
	char buff[100];
	FILE *fd;
	
	fd = popen("/etc/translator_scripts/get_file_eq /tmp/bridge_vlan_script.bk untaggedVlanStatus","r");
	
	if(fd != NULL)	{
		fscanf(fd, "%s", buff);
	}
	pclose(fd);	
	if(!strcmp(buff, "")) {
		return NULL;
	}
	if(atoi(buff) == untaggedVlanStatus) {
		return "no";
	}
	else {
		return "yes";
	}
	
}
#endif
char *bridge_exist(int vlanID)
{
	char bridge[100];
	
	sprintf(bridge, "brvlan%d", vlanID);
	return check_phy_interface(bridge);
}

void bridge_create(int vlanID)
{
	char cmd[1024];

	snprintf(cmd, sizeof(cmd), "brctl addbr brvlan%d", vlanID);
	system(cmd);

	snprintf(cmd, sizeof(cmd), "brctl setfd brvlan%d 1", vlanID);
	system(cmd);

	system("vconfig set_name_type VLAN_PLUS_VID_NO_PAD");

	snprintf(cmd, sizeof(cmd), "vconfig add brtrunk %d", vlanID);
	system(cmd);

	snprintf(cmd, sizeof(cmd), "brctl addif brvlan%d vlan%d", vlanID, vlanID);
	system(cmd);
	
	snprintf(cmd, sizeof(cmd), "ifconfig vlan%d up", vlanID);
	system(cmd);

	snprintf(cmd, sizeof(cmd), "ifconfig brvlan%d up", vlanID);
	system(cmd);
}

void bridge_destroy(int vlanID)
{
	char cmd[1024];

	snprintf(cmd, sizeof(cmd), "brctl delif brvlan%d vlan%d", vlanID, vlanID);
	system(cmd);

	snprintf(cmd, sizeof(cmd), "vconfig rem vlan%d", vlanID);
	system(cmd);

	snprintf(cmd, sizeof(cmd), "ifconfig brvlan%d down", vlanID);
	system(cmd);

	snprintf(cmd, sizeof(cmd), "brctl delbr brvlan%d", vlanID);
	system(cmd);
}

char *any_vap_under_bridge(char *bridge)
{
	int bridge_socket = -1;
 	int i, j, k, num_int;
	int count = 0;
        //int ifindex = if_nametoindex(ifname);
        int ifindices[MAX_PORTS];
	char ethName[10];
        unsigned long args[4] = { BRCTL_GET_PORT_LIST,
                                  (unsigned long)ifindices, 100, 0 };
        struct ifreq ifr;
	char vap_array[NUM_OF_VAP][VAP_LEN];
	// ={"eth0", "wifi0vap0", "wifi0vap1", "wifi0vap2", "wifi0vap3", "wifi0vap4",
	//					"wifi0vap5", "wifi0vap6","wifi0vap7"}; 
	char vap[100];

	num_int  = get_num_interfaces();
#ifdef CONFIG_PRIMARY_ETH_PORT
        sprintf(ethName,"%s",CONFIG_PRIMARY_ETH_PORT);
#else
        sprintf(ethName,"eth0");
#endif
	strcpy(vap_array[0],ethName);
	for (i = 0,k=1; i < num_int; i++) {
		for (j = 0; j < CONFIG_NO_OF_VAPS; j++,k++) {
			sprintf(vap,"wifi%dvap%d",i,j);
			strcpy(vap_array[k],vap);
		}		
	}


	bridge_socket = socket(AF_INET, SOCK_STREAM, 0);
	if(bridge_socket == -1) {
		syslog(LOG_ERR, "Unable to open the socket");
		exit(-1);
	}	
	


        memset(ifindices, 0, sizeof(ifindices));
        strcpy(ifr.ifr_name, bridge);
        ifr.ifr_data = (char *) &args;

        if (ioctl(bridge_socket, SIOCDEVPRIVATE, &ifr) < 0) {
        	syslog(LOG_ERR, "ioctl failed ");
		exit(-1);
	}
        for (i = 0; i < MAX_PORTS; i++) {
		if(ifindices[i] == 0) {
			continue;
		}
		ifindex_to_ifname(ifindices[i],vap);
		for(j = 0; j < NUM_OF_VAP; j++) {
			if(!strcmp(vap_array[j], vap)) {
				count++;
			}
		}
		
        }
        if (count == 0) {
        	return "no";
	}
	else {
		return "yes";
	}
}

void ifindex_to_ifname(int ifindex, char *vap)
{
	
	struct ifreq ifr;
	char *addr;
	struct sockaddr_in *sin;
	int fd = socket(AF_INET, SOCK_STREAM,0);
	ifr.ifr_ifindex = ifindex;
	ioctl(fd, SIOCGIFNAME, &ifr);
	strcpy(vap, ifr.ifr_name);
		
	
}

void load_ebtable(void)
{
	//system("insmod /lib/modules/net/bridge/netfilter/ebtables.ko");
	//system("insmod /lib/modules/net/bridge/netfilter/ebtable_nat.ko");
	//system("insmod /lib/modules/net/bridge/netfilter/ebt_vlan.ko");
	//system("insmod /lib/modules/net/bridge/netfilter/ebt_add1q.ko");
	//system("insmod /lib/modules/net/bridge/netfilter/ebt_rem1q.ko");
}

void unload_ebtable(void)
{
	//system("rmmod ebt_rem1q");
	//system("rmmod ebt_add1q");
	//system("rmmod ebt_vlan");
	//system("rmmod ebtable_nat");
	//system("rmmod ebtables");
}

void add_ebtable_rules(char *interface, int vlanID)
{
	int i;
	char cmd[1024];

	//for (i = 3; i >= 0; i--) {
	//	snprintf(cmd, sizeof(cmd), "export LD_LIBRARY_PATH=/usr/lib64/ebtables;"
	//		     "/usr/local/sbin/ebtables -t nat -I PREROUTING "
	//		     "-p EAPOL -i wifi0wds%d -j ACCEPT", i);
	//	system(cmd);
	//}
	//snprintf(cmd, sizeof(cmd), "export LD_LIBRARY_PATH=/usr/lib64/ebtables;"
	//	     "/usr/local/sbin/ebtables -t nat -I PREROUTING "
	//	     "-p EAPOL -i eth0 -j ACCEPT");
	//system(cmd);

	snprintf(cmd, sizeof(cmd), "brctl addrule I PREROUTING brtrunk ANY ANY "
					"EAPOL ACCEPT_EAPOL 0");
	system(cmd);

	//snprintf(cmd, sizeof(cmd), "export LD_LIBRARY_PATH=/usr/lib64/ebtables;"
	//	      "/usr/local/sbin/ebtables -t nat -A PREROUTING "
	//	      "-p ! 802_1Q -i eth0 -j add1q --add1q-id %d", vlanID);
	//system(cmd);

	//snprintf(cmd, sizeof(cmd), "export LD_LIBRARY_PATH=/usr/lib64/ebtables;"
	//	     "/usr/local/sbin/ebtables -t nat -A POSTROUTING "
	//	     "-p 802_1Q -o  eth0  --vlan-id %d -j rem1q --rem1q-target CONTINUE", 
	//	     vlanID);
	//system(cmd);

	//for(i =  0; i < 4; i++) {
	//	snprintf(cmd, sizeof(cmd), "export LD_LIBRARY_PATH=/usr/lib64/ebtables;"
	//		     "/usr/local/sbin/ebtables -t nat -A PREROUTING "
	//		     "-p ! 802_1Q -i %swds%d -j add1q --add1q-id %d", 
	//		     interface, i, vlanID);
	//	system(cmd);

	//	snprintf(cmd, sizeof(cmd), "export LD_LIBRARY_PATH=/usr/lib64/ebtables;"
	//		     "/usr/local/sbin/ebtables -t nat -A POSTROUTING "
	//		     "-p 802_1Q -o %swds%d --vlan-id %d -j rem1q "
	//		     "--rem1q-target CONTINUE", interface, i, vlanID);
	//	system(cmd);
	//}
	
	snprintf(cmd, sizeof(cmd), "brctl addrule A PREROUTING "
				"brtrunk ANY ANY !802_1Q ADD_VLAN_TAG_AND_CONTINUE %d", vlanID);
	system(cmd);

	snprintf(cmd, sizeof(cmd), "brctl addrule A POSTROUTING "
				"brtrunk ANY ANY 802_1Q REM_VLAN_TAG_AND_CONTINUE %d", vlanID);
	system(cmd);
}
